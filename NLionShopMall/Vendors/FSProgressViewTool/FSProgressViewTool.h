//
//  FSProgressViewTool.h
//  SpreadItApp
//
//  Created by 奇 on 2017/5/18.
//  Copyright © 2017年 spreadit. All rights reserved.
//  用途: 加载动画（菊花）工具类

#import <UIKit/UIKit.h>

@interface FSProgressViewTool : UILabel

+ (FSProgressViewTool *)shareInstance;

/**
 显示加载动画
 
 @param view 显示的位置
 @param message 显示的提示
 */
- (void)showProgressView:(UIView *)view message:(NSString *)message;

/**
 删除加载动画
 */
- (void)removeProgressView;
- (void)removeProgressView:(UIView *)view;

@end
