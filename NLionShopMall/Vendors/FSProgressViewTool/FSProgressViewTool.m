//
//  FSProgressViewTool.m
//  SpreadItApp
//
//  Created by 奇 on 2017/5/18.
//  Copyright © 2017年 spreadit. All rights reserved.
//

#import "FSProgressViewTool.h"
#import "MBProgressHUD.h"
//#import "MBProgressHUD+MJ.h"

@interface FSProgressViewTool ()

@property(nonatomic, strong) UIView *hudView;

@end

@implementation FSProgressViewTool

+ (FSProgressViewTool *)shareInstance{
    
    static FSProgressViewTool *instance =  nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}


/**
 显示加载动画
 @param view 显示的位置
 @param message 显示的提示
 */
- (void)showProgressView:(UIView *)view message:(NSString *)message{
    
    [self setHudView:view];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    if (message)  hud.labelText = message;
    hud.margin = 10.f;
    //    hud.removeFromSuperViewOnHide = YES;
    //    [hud hide:YES afterDelay:1.5];
}

/**
 删除加载动画
 */
- (void)removeProgressView:(UIView *)view {
    
    [MBProgressHUD hideHUDForView:view animated:YES];
}

/**
 删除加载动画
 */
- (void)removeProgressView {
    
    [MBProgressHUD hideHUDForView:_hudView animated:YES];
}



@end
