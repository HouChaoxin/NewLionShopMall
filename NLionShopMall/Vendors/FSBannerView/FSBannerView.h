//
//  FSBannerView.h
//  FSBanner
//
//  Created by admin on 15/12/9.
//  Copyright © 2015年 FORMS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FSBannerScrollViewDelegate <NSObject>
@optional
- (void) touchesBegan;
- (void) touchesCancelled;
- (void) touchesEnded;

@end

@interface FSBannerScrollView : UIScrollView

@property (weak, nonatomic) id<FSBannerScrollViewDelegate> mDelegate;

@end

@protocol FSBannerViewDelegate <NSObject>

/**
 *  图片的数量
 *
 *  @return 显示的数量
 */
- (NSUInteger) countOfBanner;

/**
 * 点击事件
 *
 * @param imageView 点击的imageView
 * @param index   位置，从0开始
 */
- (void)imageView:(UIImageView *)imageView didSelectAtIndex:(NSInteger)index;

/**
 * 显示图片
 *
 * @param imageView 显示的imageView
 * @param index   位置，从0开始
 */
- (void)imageView:(UIImageView *)imageView imageViewAtIndex:(NSInteger)index;

@end

/* pageControll显示的位置 */
typedef enum {
    PageControllShowLeft,   //居左
    PageControllShowMiddle, //居中
    PageControllShowRight   //居右
}PageControllPosition;

/* 图片滚动的速度 */
typedef enum {
    ScrollSpeedCustom,  //自定义
    ScrollSpeedSystem   //系统
}ScrollSpeedSelected;

@interface FSBannerView : UIView<UIScrollViewDelegate, FSBannerScrollViewDelegate>

@property BOOL autoScroll;  //是否要自动播放
@property NSTimeInterval switchTimeInterval;    //切换图片的时间
@property PageControllPosition pageControllPosition;    //PageControll的位置，目前提供底部左侧、底部中间、底部右侧显示
@property ScrollSpeedSelected scrollSpeedSelected;  //图片滚动的速度
@property (assign,nonatomic) float scrollTime;//图片滚动的时间
@property (weak, nonatomic) id<FSBannerViewDelegate>delegate;

/*
 *  刷新数据
 */
- (void) reloadData;

- (void)fixContentSize;

@end
