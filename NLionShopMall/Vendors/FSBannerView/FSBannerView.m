//
//  FSBannerView.m
//  FSBanner
//
//  Created by admin on 15/12/9.
//  Copyright © 2015年 FORMS. All rights reserved.
//

#import "FSBannerView.h"

#define Tag_scrollView 786
#define Tag_pageControll 678

@implementation FSBannerScrollView

#pragma mark -UIResponder手势

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    LLog(@"touchesBegan....");
    if (self.mDelegate) {
        [self.mDelegate touchesBegan];
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    LLog(@"touchesEnded....");
    if (self.mDelegate) {
        [self.mDelegate touchesEnded];
    }
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    LLog(@"touchesCancelled....");
    if (self.mDelegate) {
        [self.mDelegate touchesCancelled];
    }
}

@end

@implementation FSBannerView {
    FSBannerScrollView *mScrollView;
    UIPageControl *mPageControll;
    int count;  //显示的数量
    NSTimer *switchTimer; //图片替换计时器
    NSTimer *scrollTimer; //图片滚动计时器
    float times;    //计算图片滚动
    PageControllPosition lastPageControllPosition;  //记录pageControll的位置，避免每次reloadData要重新布局pageControll

}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initPropertiesAndViews];
    }
    return self;
}

- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initPropertiesAndViews];
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
}

//初始化属性
- (void) initPropertiesAndViews {
    self.switchTimeInterval = 4;
    self.autoScroll = YES;
    self.pageControllPosition = PageControllShowMiddle;
    lastPageControllPosition = PageControllShowMiddle;
    self.scrollSpeedSelected = ScrollSpeedSystem;
    self.scrollTime = 0.5;
    [self initScrollView];
    [self initPageControll];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)applicationWillResignActive:(NSNotification*)notify {
    NSLog(@"FSBanner load applicationWillResignActive+");
}

- (void)applicationDidBecomeActive:(NSNotification*)notify {
    NSLog(@"FSBanner load applicationDidBecomeActive-");
    [self fixContentSize];
}

- (void)fixContentSize {
    if (self.scrollSpeedSelected == ScrollSpeedCustom) {
        if (count != 1) {
            BOOL needFix = YES;
            CGFloat width = mScrollView.frame.size.width;
            CGFloat x = mScrollView.contentOffset.x;
            for (int i=0; i<count +2; i++) {
                CGFloat widthX = width*i;
                if (widthX == x) {
                    needFix = NO;
                    break;
                }
            }
            
            if (needFix) {
                if (x < width) {
//                    [mScrollView scrollRectToVisible:CGRectMake(width,0,width,height) animated:NO];
                    [mScrollView setContentOffset:CGPointMake(width, 0) animated:NO];
                }else {
                    for (int i=0; i< count +2; i++) {
                        CGFloat widthX = width*i;
                        if (x < widthX) {
                            [mScrollView setContentOffset:CGPointMake(widthX, 0) animated:NO];
                            break;
                        }
                    }
                }
            }
        }
    }
    
}

- (void) layoutSubviews {
    [self updateInViewsFrames];
}

//刷新frame
- (void) updateInViewsFrames {
    mScrollView = [self viewWithTag:Tag_scrollView];
    if (mScrollView) {
        CGRect rect = self.frame;
        rect.origin.x = 0;
        rect.origin.y = 0;
        [mScrollView setFrame:rect];
    }
    mPageControll = [self viewWithTag:Tag_pageControll];
    if (mPageControll) {
        CGRect mPageControllrect = self.frame;
        mPageControllrect.origin.x = 0;
        mPageControllrect.origin.y = mPageControllrect.size.height - 30;
        mPageControllrect.size.height = 30;
        [mPageControll setFrame:mPageControllrect];
    }
    [self reloadData];
}

- (void)setDelegate:(id<FSBannerViewDelegate>)delegate {
    _delegate = delegate;
}

- (void) reloadData {
    [self stopSwitchTimer];
    [self stopScrollTimer];
    
    count = 1;
    if ([self.delegate respondsToSelector:@selector(countOfBanner)]) {
        count = (int)[self.delegate countOfBanner];
    }
    if (count == 0) {
//        LLog(@"countOfBanner最小应该为1........");
    }
    
    for (UIView *view in mScrollView.subviews) {    //先移除子View，然后重新添加
        [view removeFromSuperview];
    }
    mScrollView.contentSize = CGSizeMake(0, 0);
    if (count == 1 || count == 0) {   //显示数量为1的时候，不需要循环，mPageControll不需要显示
        [self addImageOnlyOne];
        if ([self.delegate respondsToSelector:@selector(imageView:imageViewAtIndex:)]) {
            UIImageView *imageView = (UIImageView *)[mScrollView viewWithTag:1];
            if (imageView) {
                [self.delegate imageView:imageView imageViewAtIndex:0];
            }
        }
        CGRect rect = self.frame;
        mScrollView.contentSize = CGSizeMake(rect.size.width, rect.size.height);
        [mPageControll setHidden:YES];
    }else { //显示数量不为1的时候，添加效果如：1,2,3 -> 3',1,2,3,1'
        [self addImageWithPosition:1];  //在第0位置添加最后一张显示图片
        if ([self.delegate respondsToSelector:@selector(imageView:imageViewAtIndex:)]) {
            UIImageView *imageView = (UIImageView *)[mScrollView viewWithTag:1];
            if (imageView) {
                [self.delegate imageView:imageView imageViewAtIndex:count-1];
            }
        }
        for (int i=2; i<count +2; i++) {
            [self addImageWithPosition:i];
            if ([self.delegate respondsToSelector:@selector(imageView:imageViewAtIndex:)]) {
                UIImageView *imageView = (UIImageView *)[mScrollView viewWithTag:i];
                if (imageView) {
                    [self.delegate imageView:imageView imageViewAtIndex:i-2];
                }
            }
        }
        [self addImageWithPosition:(count+2)];  //在最后位置添加第0张显示图片
        if ([self.delegate respondsToSelector:@selector(imageView:imageViewAtIndex:)]) {
            UIImageView *imageView = (UIImageView *)[mScrollView viewWithTag:(count+2)];
            if (imageView) {
                [self.delegate imageView:imageView imageViewAtIndex:0];
            }
        }
        //设置mScrollView的frame和contentSize
        CGRect rect = self.frame;
        mScrollView.contentSize = CGSizeMake(rect.size.width*(count + 2), rect.size.height);
        [mScrollView scrollRectToVisible:CGRectMake(rect.size.width,0,rect.size.width,rect.size.height) animated:NO];
        [mPageControll setHidden:NO];
        [mPageControll setNumberOfPages:count];
        [self showPagePosition];
        if (self.autoScroll) {
            [self startSwitchTimer];
        }
    }
    
}

/**
 * 显示下一页
 */
- (void) nextPage {
    if (self.self.scrollSpeedSelected == ScrollSpeedSystem) {
        mScrollView.userInteractionEnabled = NO;
        [mScrollView setContentOffset:CGPointMake(mScrollView.contentOffset.x+mScrollView.frame.size.width, 0) animated:YES];
        [self performSelector:@selector(OpenInteraction) withObject:nil afterDelay:0.4];
    
    }else {
        times = 0;
        [self stopScrollTimer];
        [self startScrollTimer];
    }
    
}

/**
 * 图片滚动计时器实现
 *
 * @param   timer   计时器
 */
- (void)scrollScrollView:(NSTimer *)timer{
    
    mScrollView.userInteractionEnabled = NO;
    CGRect rect = self.frame;
    if (self.scrollTime > self.switchTimeInterval - 2) {
        self.scrollTime =  self.switchTimeInterval - 2;
    }
    int speed = (int) 1/self.scrollTime;
    
    times += speed;
    if (times == mScrollView.frame.size.width) {    //这一次刚好滚动到边界
        [self stopScrollTimer];
        [self performSelector:@selector(OpenInteraction) withObject:nil afterDelay:0.2];
        CGPoint newScrollViewContentOffset = mScrollView.contentOffset;
        newScrollViewContentOffset.x += speed;
        [mScrollView setContentOffset:newScrollViewContentOffset];
        
    }else if(times > mScrollView.frame.size.width){     //这一次滚动超过了边界
        [self stopScrollTimer];
        [self performSelector:@selector(OpenInteraction) withObject:nil afterDelay:0.2];
        CGPoint newScrollViewContentOffset = mScrollView.contentOffset;
        newScrollViewContentOffset.x += mScrollView.frame.size.width +speed - times;
        [mScrollView setContentOffset:newScrollViewContentOffset];
    }else {     //这一次滚动小于边界
        CGPoint newScrollViewContentOffset = mScrollView.contentOffset;
        newScrollViewContentOffset.x += speed;
        [mScrollView setContentOffset:newScrollViewContentOffset];
    }
    
    //数据为 1,2,3->3',1,2,3,1'    滚动到3的时候，实际移动到了3'的位置；滚动到1的时候，实际到了1'的位置
    if (mScrollView.contentOffset.x == 0) {
        [mScrollView scrollRectToVisible:CGRectMake(rect.size.width*count,0,rect.size.width,rect.size.height) animated:NO];
        [mPageControll setCurrentPage:count-1];
    }
    else if (mScrollView.contentOffset.x == rect.size.width*(count+1)) {
        [mScrollView scrollRectToVisible:CGRectMake(rect.size.width,0,rect.size.width,rect.size.height) animated:NO];
        [mPageControll setCurrentPage:0];
    }
}

/**
 * 初始化mPageControll
 *
 */
- (void) initPageControll {
    mPageControll = [self viewWithTag:Tag_pageControll];
    if (mPageControll == nil) {
        CGRect rect = self.frame;
        rect.origin.y = rect.size.height - 30;
        rect.origin.x = 0;
        rect.size.height = 30;
        mPageControll = [[UIPageControl alloc] initWithFrame:rect];
        mPageControll.userInteractionEnabled = NO;
        [mPageControll setPageIndicatorTintColor:[UIColor colorWithWhite:1.0 alpha:0.4]];
        [mPageControll setCurrentPageIndicatorTintColor:[UIColor whiteColor]];
        [mPageControll setHidden:YES];
        mPageControll.tag = Tag_pageControll;
        [self addSubview:mPageControll];
        
    }
}

/**
 * 布局pageControll的显示位置
 *
 */
- (void) showPagePosition {
    if (lastPageControllPosition != self.pageControllPosition) {
        CGSize pointSize = [mPageControll sizeForNumberOfPages:count];
        if (self.pageControllPosition == PageControllShowRight) {
            CGFloat page_x = -(mPageControll.bounds.size.width - pointSize.width) / 2 +10;
            [mPageControll setBounds:CGRectMake(page_x, mPageControll.bounds.origin.y, mPageControll.bounds.size.width, mPageControll.bounds.size.height)];
        }else if (self.pageControllPosition == PageControllShowMiddle) {
            [mPageControll setBounds:CGRectMake(0, mPageControll.bounds.origin.y, mPageControll.bounds.size.width, mPageControll.bounds.size.height)];
        }else if (self.pageControllPosition == PageControllShowLeft) {
            CGFloat page_x = (mPageControll.bounds.size.width - pointSize.width) / 2 -10;
            [mPageControll setBounds:CGRectMake(page_x, mPageControll.bounds.origin.y, mPageControll.bounds.size.width, mPageControll.bounds.size.height)];
        }
        
        lastPageControllPosition = self.pageControllPosition;
    }
    
}

/**
 * 初始化mScrollView
 *
 */
- (void) initScrollView {
    mScrollView = [self viewWithTag:Tag_scrollView];
    if (mScrollView == nil) {
        CGRect rect = self.frame;
        rect.origin.x = 0;
        rect.origin.y = 0;
        mScrollView = [[FSBannerScrollView alloc] initWithFrame:rect];
        mScrollView.bounces = NO;
        mScrollView.pagingEnabled = YES;
        mScrollView.scrollEnabled = YES;
        mScrollView.showsHorizontalScrollIndicator = NO;
        mScrollView.showsVerticalScrollIndicator = NO;
        mScrollView.delaysContentTouches = NO;
        mScrollView.delegate = self;
        mScrollView.tag = Tag_scrollView;
        mScrollView.mDelegate = self;
        [self addSubview:mScrollView];
    }
    
}

/**
 * 只显示一张的时候添加图片方法
 *
 */
- (void) addImageOnlyOne {
    CGRect rect = self.frame;
    rect.origin.x = 0;
    rect.origin.y = 0;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
    imageView.tag = 1;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom:)];
    [imageView addGestureRecognizer:singleRecognizer];
    [mScrollView addSubview:imageView];
}

/**
 * mScrollView添加图片的方法
 *
 * @param   position    位置
 */
- (void)addImageWithPosition:(int)position {
    // add image to scroll view
    CGRect rect = self.frame;
    rect.origin.x = (position-1)*rect.size.width;
    rect.origin.y = 0;
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:rect];
    imageView.tag = position;
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapFrom:)];
    [imageView addGestureRecognizer:singleRecognizer];
    [mScrollView addSubview:imageView];
}

/**
 * 图片点击手势
 *
 * @param   recognizer   单击手势
 */
- (void)handleSingleTapFrom:(UISwipeGestureRecognizer*)recognizer {
    
    UIImageView *imageView = (UIImageView *)recognizer.view;
    if (count == 1) {
        NSInteger index = imageView.tag -1;
        if ([self.delegate respondsToSelector:@selector(imageView:didSelectAtIndex:)]) {
            [self.delegate imageView:imageView didSelectAtIndex:index];
        }
    }else {
        NSInteger index = imageView.tag -2;
        if ([self.delegate respondsToSelector:@selector(imageView:didSelectAtIndex:)]) {
            [self.delegate imageView:imageView didSelectAtIndex:index];
        }
        [self startSwitchTimer];
    }
    
}

#pragma mark -UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGRect rect = self.frame;
    if (mScrollView.contentOffset.x == 0) {
        [mScrollView scrollRectToVisible:CGRectMake(rect.size.width*count,0,rect.size.width,rect.size.height) animated:NO];
        [mPageControll setCurrentPage:count-1];
    }else if (mScrollView.contentOffset.x == rect.size.width*(count+1)) {
        [mScrollView scrollRectToVisible:CGRectMake(rect.size.width,0,rect.size.width,rect.size.height) animated:NO];
        [mPageControll setCurrentPage:0];
    }
    CGFloat f = scrollView.contentOffset.x/scrollView.frame.size.width;
    [mPageControll setCurrentPage:f-1];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
//    LLog(@"scrollViewWillBeginDragging...............");
    [self stopScrollTimer];
    [self stopSwitchTimer];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
//    LLog(@"scrollViewDidEndDecelerating------------");
    [self stopSwitchTimer];
    [self stopScrollTimer];
    if (self.autoScroll) {
        [self startSwitchTimer];
    }
    
}

/**
 * 开始滚动计时器
 *
 */
- (void) startScrollTimer {
    scrollTimer = [NSTimer scheduledTimerWithTimeInterval:1/mScrollView.frame.size.width target:self selector:@selector(scrollScrollView:) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:scrollTimer forMode:NSRunLoopCommonModes];
    
    
}

/**
 * 开始切换图片计时器
 *
 */
- (void) startSwitchTimer {
    if (switchTimer) {
        return;
    }
    if (self.switchTimeInterval < 3) {
        self.switchTimeInterval = 3;
    }
    
    switchTimer = [NSTimer scheduledTimerWithTimeInterval:self.switchTimeInterval target:self selector:@selector(nextPage) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:switchTimer forMode:NSRunLoopCommonModes];
}

/**
 * 停止滚动计时器
 *
 */
- (void) stopScrollTimer {
    if (scrollTimer) {
        [scrollTimer invalidate];
        scrollTimer = nil;
    }
    
}

/**
 * 停止切换图片计时器
 *
 */
- (void) stopSwitchTimer {
    if (switchTimer) {
        [switchTimer invalidate];
        switchTimer = nil;
    }
    
}

//手指按下，计时器停止
- (void) touchesBegan {
    [self stopSwitchTimer];
}

- (void) touchesCancelled {
    //    if (self.autoScroll) {
    //        [self startSwitchTimer];
    //    }
}

//手指放开，计时器开始
- (void) touchesEnded {
    if (count != 1) {
        if (self.autoScroll) {
            [self startSwitchTimer];
        }
    }
}

/**
 *  允许交互
 */
- (void) OpenInteraction{
    mScrollView.userInteractionEnabled = YES;
}

@end
