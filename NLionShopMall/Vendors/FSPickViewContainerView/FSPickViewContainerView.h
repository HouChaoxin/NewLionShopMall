//
//  FSPickViewContainerView.h
//  FSPickViewProject
//
//  Created by gumou on 15/12/24.
//  Copyright © 2015年 FORMS. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FSPickViewContainerViewDelegate <NSObject>

/**
 *  设置"取消"、“确定”按钮的superView
 *
 *  @return "取消"、“确定”按钮的superView
 */
- (UIView *)buttonContainerView;

/**
 *  设置"取消"、“确定”按钮的superView的高
 *
 *  @return "取消"、“确定”按钮的superView的高
 */
- (CGFloat)buttonContainerViewHeight;

@optional
/**
 *  设置pickView的高度
 *
 *  @return pickView的高度
 */
- (CGFloat)pickViewHeight;

/**
 *  pickView上面View的点击响应
 */
- (void)clickTopBlankViewAction;

@optional
/**
 *  pickView选中row的响应
 *
 *  @param string 选中的值
 *  @param row    选中的行数
 */
- (void)didSelectWithString:(NSString *)string didSelectRow:(NSInteger)row;

@end

@interface FSPickViewShowModel : NSObject

@property (strong, nonatomic) NSString *showName;
@property (strong, nonatomic) NSArray *nextLevelArray;
@property (strong, nonatomic) NSString *selectedString;
@property NSInteger belongComponent;
@property NSInteger belongRow;

- (instancetype) initWitShowName:(NSString *)showName nextLevelArray:(NSArray *)nextLevelArray;

@end

/**
 *  pickView显示的内容和关联的view
 */
@interface FSPickViewModel : NSObject

@property (weak) UIView *relateView;    //关联的view，要改变title的View，目前只支持UILabel、UITextField、UIButton
@property (strong, nonatomic) NSArray *textArray;   //pickView要显示的内容
@property (strong, nonatomic) NSString *selectedString; //选中值
@property (strong, nonatomic) NSArray *selectedStringArray; //选中值
@property (strong, nonatomic) NSArray *showModelArray;
@property (readonly)BOOL showOneComponent;  //是否显示一列
@property (readonly)NSInteger componentsCount;  //列数
@property NSMutableDictionary *temporaryModelDic;   //临时选中值
@property NSMutableDictionary *selectedModelDic;    //选中值
@property NSString *separator;  //relateView显示分隔符

/**
 *  初始化
 *
 *  @param relateView     关联的view，要改变title的View，目前只支持UILabel、UITextField、UIButton
 *  @param textArray      pickView要显示的内容
 *  @param selectedString 选中值
 *
 *  @return FSPickViewModel
 */
- (instancetype)initWithRelateView:(UIView *)relateView textArray:(NSArray *)textArray selectedString:(NSString *)selectedString;

/**
 *  初始化
 *
 *  @param relateView          关联的view，要改变title的View，目前只支持UILabel、UITextField、UIButton
 *  @param showModelArray      FSPickViewShowModel数组
 *  @param selectedStringArray 选中值
 *
 *  @return FSPickViewModel
 */
- (instancetype)initWithRelateView:(UIView *)relateView showModelArray:(NSArray *)showModelArray selectedStringArray:(NSArray *)selectedStringArray;

@end

@interface FSPickViewContainerView : UIView<UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) id<FSPickViewContainerViewDelegate>delegate;
@property (strong, nonatomic) NSString *showString;
@property (nonatomic,copy) NSArray *showAry;
/**
 *  显示FSPickViewContainerView
 *
 *  @param model FSPickViewModel
 */
- (void)showWithModel:(FSPickViewModel *)model;

/**
 *  隐藏FSPickViewContainerView
 *
 *  @param changeSelectedValue 是否要改变FSPickViewModel.relateView的显示内容
 */
- (void)hiddenByChangeSelectedValue:(BOOL)changeSelectedValue;

@end
