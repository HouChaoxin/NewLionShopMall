//
//  FSPickViewContainerView.m
//  FSPickViewProject
//
//  Created by gumou on 15/12/24.
//  Copyright © 2015年 FORMS. All rights reserved.
//

#import "FSPickViewContainerView.h"

#define Tag_pickView 888
#define Tag_blankClickView 889
#define Tag_buttonContainerView 887

@implementation FSPickViewShowModel

/**
 *  初始化FSPickViewShowModel
 *  
 *  @param showName 显示的名称
 *  @param nextLevelArray 下一级别的数据，类型为FSPickViewShowModel
 *  @return FSPickViewShowModel
 */
- (instancetype) initWitShowName:(NSString *)showName nextLevelArray:(NSArray *)nextLevelArray {
    self = [super init];
    if (self) {
        self.showName = showName;
        self.nextLevelArray = nextLevelArray;
    }
    return self;
}

@end

@implementation FSPickViewModel {
    NSInteger component;    //记录FSPickViewShowModel所属列数
}

- (instancetype)initWithRelateView:(UIView *)relateView textArray:(NSArray *)textArray selectedString:(NSString *)selectedString {
    self = [super init];
    if (self) {
        self.relateView = relateView;
        self.textArray = textArray;
        self.selectedString = selectedString;
        _componentsCount = 1;
        _showOneComponent = YES;
    }
    return self;
}

- (instancetype)initWithRelateView:(UIView *)relateView showModelArray:(NSArray *)showModelArray selectedStringArray:(NSArray *)selectedStringArray {
    self = [super init];
    if (self) {
        self.relateView = relateView;
        self.showModelArray = showModelArray;
        self.selectedStringArray = selectedStringArray;
        self.separator = @"";
        _componentsCount = 1;
        component = 0;
        [self setModelComponentAndRow:showModelArray];
        [self getTemporaryArrayCount:showModelArray];
        _showOneComponent = NO;
        _temporaryModelDic = [[NSMutableDictionary alloc] init];
        _selectedModelDic = [[NSMutableDictionary alloc] init];
        if (selectedStringArray == nil || selectedStringArray.count !=_componentsCount) {
            //            FSPickViewShowModel *model;
            //            model = [self.showModelArray objectAtIndex:0];
            //            [_temporaryModelDic setValue:model forKey:[NSString stringWithFormat:@"component%d",0]];
            //            [self setTempoaryStringDicWithIndex:1 model:model];
            
        }else {
            [self setTempoaryDicWithDefault:selectedStringArray];
        }
        
    }
    return self;
}

/**
 *  设置FSPickViewShowModel所属列数与行数
 *
 *  @param array FSPickViewShowModel.nextLevelArray
 */
- (void)setModelComponentAndRow:(NSArray *)array {
    for (int i=0; i<array.count; i++) {
        FSPickViewShowModel *model = [array objectAtIndex:i];
        model.belongRow = i;
        model.belongComponent = component;
        if (model.nextLevelArray != nil && model.nextLevelArray.count >0) {
            component++;
            [self setModelComponentAndRow:model.nextLevelArray];
        }
    }
    component--;
}

/**
 *  设置_temporaryModelDic
 *
 *  @param index 列数
 *  @param model FSPickViewShowModel
 */
- (void)setTempoaryStringDicWithIndex:(int)index model:(FSPickViewShowModel *)model {
    if (index >= _componentsCount) {
        return;
    }
    NSArray *array = model.nextLevelArray;
    if (array != nil && array.count > 0) {
        FSPickViewShowModel *subModel = [array objectAtIndex:0];
        [_temporaryModelDic setValue:subModel forKey:[NSString stringWithFormat:@"component%d",index]];
        [self setTempoaryStringDicWithIndex:index+1 model:subModel];
    }
}

/**
 *  根据传入的选中数组设置_selectedModelDic的初始值
 *
 *  @param array 默认选中值
 */
- (void)setTempoaryDicWithDefault:(NSArray *)array {
    BOOL key0HasDefault = NO;
    NSString *string1 = [array objectAtIndex:0];
    for (int j=0; j<self.showModelArray.count; j++) {
        FSPickViewShowModel *subModel = [self.showModelArray objectAtIndex:j];
        if ([string1 isEqualToString:subModel.showName]) {
            [_selectedModelDic setValue:subModel forKey:[NSString stringWithFormat:@"component0"]];
            key0HasDefault = YES;
            break;
        }
    }
    if (!key0HasDefault) {
        [_selectedModelDic setValue:[self.showModelArray objectAtIndex:0] forKey:[NSString stringWithFormat:@"component0"]];
    }else {
        FSPickViewShowModel *model = [_selectedModelDic valueForKey:@"component0"];
        [self setTempoaryDicWithIndex:1 array:array model:model];
    }
    
}

/**
 *  遍历设置_selectedModelDic
 *
 *  @param index 列数
 *  @param array 传入的选中数组
 *  @param model FSPickViewShowModel
 */
- (void)setTempoaryDicWithIndex:(int)index array:(NSArray *)array model:(FSPickViewShowModel *)model{
    if (index >= _componentsCount) {
        return;
    }
    BOOL keyHasValue = NO;
    FSPickViewShowModel *subModel;
    NSArray *nextLevelArray = model.nextLevelArray;
    NSString *value = [array objectAtIndex:index];
    if (nextLevelArray != nil && nextLevelArray.count >0) {
        for (int i=0; i<nextLevelArray.count; i++) {
            subModel = [nextLevelArray objectAtIndex:i];
            if ([value isEqualToString:subModel.showName]) {
                [_selectedModelDic setValue:subModel forKey:[NSString stringWithFormat:@"component%d",index]];
                keyHasValue = YES;
                break;
            }
        }
        [self setTempoaryDicWithIndex:index+1 array:array model:subModel];
    }
    
    
}

/**
 *  判断列数
 *
 *  @param array
 */
- (void) getTemporaryArrayCount:(NSArray *)array {
    if (array != nil && array.count > 0) {
        FSPickViewShowModel *model = [array objectAtIndex:0];
        if (model.nextLevelArray && model.nextLevelArray.count != 0) {
            [self getHowDeepLevel:model.nextLevelArray];
        }
    }
}

- (void)getHowDeepLevel:(NSArray *)nextLevelArray {
    _componentsCount++;
    if (nextLevelArray != nil && nextLevelArray.count >0) {
        FSPickViewShowModel *model = [nextLevelArray objectAtIndex:0];
        if (model.nextLevelArray != nil && model.nextLevelArray.count >0) {
            [self getHowDeepLevel:model.nextLevelArray];
        }
    }
}

@end

@implementation FSPickViewContainerView {
    UIPickerView *mPickView;    //pickView
    UIView *blankView;  //空白的View
    UIView *buttonView; //"取消"、“确定”按钮的superView的superView
    FSPickViewModel *mModel;    //显示关联model
    CGFloat pickViewHeight; //pickView的高，默认216
    CGFloat buttonContainerViewHeight;  //"取消"、“确定”按钮的superView的superView的高，默认60
    NSString *temporaryString;  //临时选中值
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initAllSubViewsAndProperties];
    }
    return self;
}

- (instancetype) initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initAllSubViewsAndProperties];
    }
    return  self;
}

//初始化subView和属性
- (void) initAllSubViewsAndProperties {
    pickViewHeight = 216;
    buttonContainerViewHeight = 60;
    self.backgroundColor = [UIColor clearColor];
    [self addAllChildView];
    self.hidden = YES;
}

- (void)addAllChildView {
    [self addPickView];
    [self addButtonView];
    [self addBlankView];
}

//添加UIPickView
- (void) addPickView {
    mPickView = [self viewWithTag:Tag_pickView];
    if (mPickView == nil) {
        CGRect rect = self.frame;
        CGFloat y = rect.size.height - pickViewHeight;
        mPickView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, y, rect.size.width, pickViewHeight)];
        mPickView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        mPickView.tag = Tag_pickView;
        mPickView.delegate = self;
        mPickView.dataSource = self;
        [self addSubview:mPickView];
    }
}

//添加"取消"、"确定"按钮的显示View
- (void) addButtonView {
    buttonView = [self viewWithTag:Tag_buttonContainerView];
    if (buttonView == nil) {
        CGRect rect = self.frame;
        buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, rect.size.height-pickViewHeight-buttonContainerViewHeight, rect.size.width, buttonContainerViewHeight)];
        buttonView.tag = Tag_buttonContainerView;
        buttonView.clipsToBounds = YES;
        [self addSubview:buttonView];
    }
}

//添加pickView上面灰色透明View
- (void) addBlankView {
    blankView = [self viewWithTag:Tag_blankClickView];
    if (blankView == nil) {
        CGRect rect = self.frame;
        blankView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, rect.size.height-pickViewHeight-buttonContainerViewHeight)];
        blankView.backgroundColor = [UIColor colorWithWhite:0.5 alpha:0.5];
        blankView.tag = Tag_blankClickView;
        [self addSubview:blankView];
    }
}

- (void)layoutSubviews {
    [self updateChildViewFrames];
}

//更新子View的frames，先设置pickView，再设置"取消"、"确定"按钮的显示View，最后设置灰色透明View
- (void)updateChildViewFrames {
    mPickView = [self viewWithTag:Tag_pickView];
    CGRect rect = self.frame;
    if (mPickView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(pickViewHeight)]) {
            pickViewHeight = [self.delegate pickViewHeight];
        }
        [mPickView setFrame:CGRectMake(0, rect.size.height - pickViewHeight, rect.size.width, pickViewHeight)];
    }
    
    buttonView = [self viewWithTag:Tag_buttonContainerView];
    if (buttonView) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(buttonContainerViewHeight)]) {
            buttonContainerViewHeight = [self.delegate buttonContainerViewHeight];
        }
        [buttonView setFrame:CGRectMake(0, rect.size.height-pickViewHeight-buttonContainerViewHeight, rect.size.width, buttonContainerViewHeight)];
        for (UIView *view in buttonView.subviews) {
            [view removeFromSuperview];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(buttonContainerView)]) {
            [buttonView addSubview:[self.delegate buttonContainerView]];
        }
    }
    
    blankView = [self viewWithTag:Tag_blankClickView];
    if (blankView) {
        [blankView setFrame:CGRectMake(0, 0, rect.size.width, rect.size.height-pickViewHeight-buttonContainerViewHeight)];
        UITapGestureRecognizer  * tapBackgroundGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackground:)];
        [tapBackgroundGR setCancelsTouchesInView:NO];
        [blankView addGestureRecognizer:tapBackgroundGR];
    }
    
}

//blankView的点击手势响应
- (void)tapBackground:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTopBlankViewAction)]) {
        [self.delegate clickTopBlankViewAction];
    }
}

- (void)hiddenByChangeSelectedValue:(BOOL)changeSelectedValue {
    [self setHidden:YES];
    if (mModel.showOneComponent) {  //单列显示
        if (changeSelectedValue) {  //是否要改变内容
            mModel.selectedString = temporaryString;
        }else {
            
        }
        if (mModel.relateView) {
            [self setViewShowText:mModel.relateView text:mModel.selectedString];
        }
    }else { //多列显示
        if (changeSelectedValue) {  //是否要改变内容
            mModel.selectedModelDic = [[NSMutableDictionary alloc] initWithDictionary:mModel.temporaryModelDic];
        }else {
            
        }
        if (mModel.relateView) {
            NSArray *array = [self sortedAfterByDicKey:mModel.selectedModelDic];
            [self setViewShowText:mModel.relateView text:[array componentsJoinedByString:mModel.separator]];
        }
    }
}

- (void)showWithModel:(FSPickViewModel *)model {
    [self setHidden:NO];
    mModel = model;
    if (mModel.showOneComponent) {  //单列显示
        if (model.selectedString == nil || [model.selectedString isEqualToString:@""]) {    //选中值为nil或@""时，取view的显示内容
            if (model.relateView) {
                if ([model.relateView isKindOfClass:[UILabel class]]) {
                    model.selectedString = ((UILabel *)model.relateView).text;
                }
                if ([model.relateView isKindOfClass:[UITextField class]]) {
                    model.selectedString = ((UITextField *)model.relateView).text;
                }
                if ([model.relateView isKindOfClass:[UIButton class]]) {
                    UIButton *btn = (UIButton *)model.relateView;
                    [btn setTitleColor:kBlackColor forState:UIControlStateNormal];
                    model.selectedString = ((UIButton *)model.relateView).currentTitle;
                }
            }
        }
        temporaryString = model.selectedString;
        [mPickView reloadAllComponents];
        if (model.selectedString == nil || [model.selectedString isEqualToString:@""]) {    //如果选中值为空，滚动到row0
            [self setViewShowText:model.relateView text:[model.textArray objectAtIndex:0]];
            temporaryString = [model.textArray objectAtIndex:0];
            [mPickView selectRow:0 inComponent:0 animated:NO];
            
        }else { //如果选中值在显示数组里面，滚动到选中值所属row
            for (int i=0; i<model.textArray.count; i++) {
                NSString *string = [model.textArray objectAtIndex:i];
                if ([model.selectedString isEqualToString:string]) {
                    [mPickView selectRow:i inComponent:0 animated:NO];
                }
            }
        }
        
    }else { //多列显示
        if (model.selectedModelDic == nil || model.selectedModelDic.count == 0) {
            FSPickViewShowModel *model0;
            model0 = [model.showModelArray objectAtIndex:0];
            [model.temporaryModelDic setValue:model0 forKey:[NSString stringWithFormat:@"component%d",0]];
            [self setTempoaryStringDicWithIndex:1 model:model0];
        }else {
            model.temporaryModelDic = [[NSMutableDictionary alloc] initWithDictionary:model.selectedModelDic];
        }
        [mPickView reloadAllComponents];
        for (FSPickViewShowModel *subModel in [model.temporaryModelDic allValues]) {
            [mPickView selectRow:subModel.belongRow inComponent:subModel.belongComponent animated:NO];
        }
        
        NSArray *array = [self sortedAfterByDicKey:model.temporaryModelDic];
        [self setViewShowText:model.relateView text:[array componentsJoinedByString:model.separator]];
    }
}

/**
 *  view设置内容
 *
 *  @param view view
 *  @param text titleText
 */
- (void)setViewShowText:(UIView *)view text:(NSString *)text {
    self.showString = text;
    if ([view isKindOfClass:[UILabel class]]) {
        UILabel *label = (UILabel *)view;
        [label setText:text];
    }
    if ([view isKindOfClass:[UITextField class]]) {
        ((UITextField *)view).text = text;
    }
    if ([view isKindOfClass:[UIButton class]]) {
        [((UIButton *)view) setTitleColor:kBlackColor forState:UIControlStateNormal];
        [((UIButton *)view) setTitle:text forState:UIControlStateNormal];
    }
}

/**
 *  设置FSPickViewModel.temporaryModelDic初始值全为第0个
 *
 *  @param index 列数
 *  @param model FSPickViewShowModel
 */
- (void)setTempoaryStringDicWithIndex:(int)index model:(FSPickViewShowModel *)model {
    if (index >= mModel.componentsCount) {
        return;
    }
    NSArray *array = model.nextLevelArray;
    if (array != nil && array.count > 0) {
        FSPickViewShowModel *subModel = [array objectAtIndex:0];
        [mModel.temporaryModelDic setValue:subModel forKey:[NSString stringWithFormat:@"component%d",index]];
        [self setTempoaryStringDicWithIndex:index+1 model:subModel];
    }
}

/**
 *  FSPickViewModel.temporaryModelDic根据key排序，并返回FSPickViewShowModel.showName的数组
 *
 *  @param dictionary FSPickViewModel.temporaryModelDic
 *
 *  @return FSPickViewShowModel.showName的数组
 */
- (NSArray *)sortedAfterByDicKey:(NSDictionary *)dictionary {
    NSArray *keys = [dictionary allKeys];
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    NSMutableArray *afterArray = [[NSMutableArray alloc] init];
    for (NSString *key in sortedArray) {
        FSPickViewShowModel *model = [dictionary objectForKey:key];
        [afterArray addObject:model.showName];
    }
    return [afterArray copy];
}

#pragma pickView delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (mModel.showOneComponent) {
        if (mModel && mModel.textArray.count > 0) {
            return [mModel.textArray objectAtIndex:row];
        }
    }
    if (component == 0) {
        FSPickViewShowModel *model = [mModel.showModelArray objectAtIndex:row];
        return model.showName;
    }else {
        FSPickViewShowModel *model = [mModel.temporaryModelDic objectForKey:[NSString stringWithFormat:@"component%ld",component-1]];
        FSPickViewShowModel *subModel = [model.nextLevelArray objectAtIndex:row];
        return subModel.showName;
    }
    
    return @"";
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (mModel.showOneComponent) {
        NSString *string = [mModel.textArray objectAtIndex:row];
        temporaryString = string;
        self.showString = string;
        if (mModel.relateView) {
            [self setViewShowText:mModel.relateView text:string];
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectWithString:didSelectRow:)]) {
            [self.delegate didSelectWithString:string didSelectRow:row];
        }
    }else {
        if (component == 0) {
            FSPickViewShowModel *subModel = [mModel.showModelArray objectAtIndex:row];
            [mModel.temporaryModelDic setValue:subModel forKey:[NSString stringWithFormat:@"component%ld",component]];
        }else {
            FSPickViewShowModel *subModel = [mModel.temporaryModelDic objectForKey:[NSString stringWithFormat:@"component%ld",component-1]];
            [mModel.temporaryModelDic setValue:[subModel.nextLevelArray objectAtIndex:row] forKey:[NSString stringWithFormat:@"component%ld",component]];
        }
        
        for (NSInteger i=component+1; i<mModel.componentsCount; i++) {
            FSPickViewShowModel *subModel = [mModel.temporaryModelDic objectForKey:[NSString stringWithFormat:@"component%ld",i-1]];
            [mModel.temporaryModelDic setValue:[subModel.nextLevelArray objectAtIndex:0] forKey:[NSString stringWithFormat:@"component%ld",i]];
            [mPickView selectRow:0 inComponent:i animated:NO];
        }
        
        NSArray *array = [self sortedAfterByDicKey:mModel.temporaryModelDic];
        self.showString = [array lastObject];
        self.showAry = [NSArray arrayWithArray:array];
        if (mModel.relateView) {
            [self setViewShowText:mModel.relateView text:[array componentsJoinedByString:mModel.separator]];
        }
        [mPickView reloadAllComponents];
    }
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return mModel.componentsCount;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (mModel.showOneComponent) {
        if (mModel && mModel.textArray.count > 0) {
            return mModel.textArray.count;
        }
    }
    
    if (component == 0) {
        return mModel.showModelArray.count;
    }else {
        FSPickViewShowModel *model = [mModel.temporaryModelDic objectForKey:[NSString stringWithFormat:@"component%ld",component-1]];
        return model.nextLevelArray.count;
    }
    
    return 0;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

@end
