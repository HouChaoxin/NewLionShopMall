//
//  NetHelper.m
//  LionMall
//
//  Created by casanova on 2017/4/13.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "NetHelper.h"
#import "AFNetworking.h"

@interface NetHelper ()

@end

@implementation NetHelper

/**
 通过GET方法获取网络数据

 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 处理进度的代码块
 @param success 处理成功数据的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)GET:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure{
    
    // 获取AFHTTPSessionManager对象
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    // 设置可接受的文件类型
//    sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
    sessionManager.requestSerializer.timeoutInterval = 500;
    
//    [sessionManager.requestSerializer setValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"Content-Type"];
//    [sessionManager.requestSerializer setValue:@"application/json;" forHTTPHeaderField:@"Content-Type"];
    // 开始请求数据
    [sessionManager GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (success) {
            success(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // 请求失败后的动作
        if (failure) {
            failure(error);
        }
    }];
}

/**
 通过GET方法获取网络数据
 
 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 处理进度的代码块
 @param success 处理成功数据的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)GET1:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure{
    
    // 获取AFHTTPSessionManager对象
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
 
    sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    sessionManager.requestSerializer = [AFHTTPRequestSerializer serializer];
     sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain",nil];
//    [sessionManager.requestSerializer  setValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"Content-Type"];
    sessionManager.requestSerializer.timeoutInterval = 500;
    // 开始请求数据
    [sessionManager GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // 获取数据后将数据传入success代码块中执行
        // 因为编码的原因需要编码解析
        NSData *doubi = responseObject;
        NSJSONSerialization *jsobj = [NSJSONSerialization JSONObjectWithData:doubi options:NSJSONReadingAllowFragments error:nil];
        success(jsobj);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // 请求失败后的动作
        if (failure) {
            failure(error);
        }
    }];
}



/**
 通过post方法请求数据

 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 进度处理代码块
 @param success 处理请求成功的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)POST:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure {
    
    // 获取AFHTTPSessionManager对象
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    // 设置可接受的文件类型
    sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    sessionManager.responseSerializer.acceptableContentTypes =  [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"text/plain", nil];
    [sessionManager.requestSerializer  setValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"Content-Type"];
    sessionManager.requestSerializer.timeoutInterval = 500;
    [sessionManager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 计算进度
//        long long pro = uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
//        progress(pro);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // 因为编码的原因需要编码解析
        NSString *responseStr =  [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        // 编程JSON对象
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
//        id obj = [responseStr mj_JSONObject];
        NSJSONSerialization *jsonObj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        // 获取数据后将数据传入success代码块中执行
        success(jsonObj);

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // 请求失败后的动作
        failure(error);
    }];
}

/**
 通过post方法请求数据
 
 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 进度处理代码块
 @param success 处理请求成功的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)POST1:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure {
    
    // 获取AFHTTPSessionManager对象
    AFHTTPSessionManager *sessionManager = [AFHTTPSessionManager manager];
    // 设置可接受的文件类型
    sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
    [sessionManager.requestSerializer  setValue:@"application/x-www-form-urlencoded;" forHTTPHeaderField:@"Content-Type"];
    sessionManager.requestSerializer.timeoutInterval = 500;
    [sessionManager POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress) {
        
        // 计算进度
        //        long long pro = uploadProgress.completedUnitCount / uploadProgress.totalUnitCount;
        //        progress(pro);
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        // 因为编码的原因需要编码解析
        NSString *responseStr =  [[NSString alloc]initWithData:responseObject encoding:NSUTF8StringEncoding];
        // 编程JSON对象
        NSData *data = [responseStr dataUsingEncoding:NSUTF8StringEncoding];
        NSJSONSerialization *jsonObj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
        // 获取数据后将数据传入success代码块中执行
        success(jsonObj);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        // 请求失败后的动作
        failure(error);
    }];
}


@end
