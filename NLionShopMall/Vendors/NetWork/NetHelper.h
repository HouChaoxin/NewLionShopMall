//
//  NetHelper.h
//  LionMall
//
//  Created by casanova on 2017/4/13.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetHelper : NSObject

// 定义网络进度block
typedef  void (^ProgressBlock)(NSInteger downloadProgress);

// 定义获取数据成功的block
typedef  void (^SuccessBlock)(id responseObject);

// 定义获取数据失败的block
typedef  void (^FailureBlock)(NSError *error);

typedef  void (^FailBlock)(void);

/**
 通过GET方法获取网络数据
 
 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 处理进度的代码块
 @param success 处理成功数据的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)GET:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure;

/**
 通过post方法请求数据
 
 @param urlString 请求url字符串
 @param parameters 请求参数
 @param progress 进度处理代码块
 @param success 处理请求成功的代码块
 @param failure 处理请求失败的代码块
 */
+ (void)POST:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure;

//+ (void)POST1:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure;
+ (void)GET1:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure;

+ (void)POST1:(NSString *)urlString parameters:(id)parameters progressBlock:(ProgressBlock)progress success:(SuccessBlock)success failure:(FailureBlock)failure ;
@end
