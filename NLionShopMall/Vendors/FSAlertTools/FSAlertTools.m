//
//  FSAlertTools.m
//  Created by gumou on 16/7/12.
//  Copyright © 2016 bochk. All rights reserved.

#import "FSAlertTools.h"

@implementation FSAlertTools

+ (FSAlertTools *)sharedInstance {
    static dispatch_once_t once;
    static FSAlertTools *sharedInstance;
    dispatch_once(&once, ^{
        
        sharedInstance = [[FSAlertTools alloc] init];
    });
    return sharedInstance;
}

- (void)showAlert:(UIViewController *)targetVC title:(NSString *)titleStr message:(NSString *)messageStr cancleTitle:(NSString *)cancleTitle confirmTitle:(NSString *)confirmTitle cancleBlock:(void (^)(void))cancleBlock confirmBlock:(void (^)(void))confirmBlock{
        
    if ([UIAlertController class]) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titleStr message:messageStr preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:cancleTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            if (cancleBlock) {
                cancleBlock();
            }
        }];
        [alertController addAction:cancelAction];
        
        if (confirmTitle) {
            UIAlertAction *otherAction = [UIAlertAction actionWithTitle:confirmTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                if (confirmBlock) {
                    confirmBlock();
                }
            }];
            [alertController addAction:otherAction];
        }
        
        [targetVC presentViewController:alertController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alt = [[UIAlertView alloc] initWithTitle:titleStr message:messageStr delegate:self cancelButtonTitle:cancleTitle otherButtonTitles:confirmTitle,nil];
        self.cancleBlock = cancleBlock;
        self.confirmBlock = confirmBlock;
        [alt show];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1){
        if (self.confirmBlock) {
            self.confirmBlock();
        }
    }else{
        if (self.cancleBlock) {
            self.cancleBlock();
        }
    }
    
}

@end
