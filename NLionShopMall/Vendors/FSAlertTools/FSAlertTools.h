//
//  FSAlertTools.h
//  Created by gumou on 16/7/12.
//  Copyright © 2016 bochk. All rights reserved.

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CancleBlock)();
typedef void (^ConfirmBlock)();

@interface FSAlertTools : NSObject<UIAlertViewDelegate>

@property (nonatomic, strong) CancleBlock cancleBlock;
@property (nonatomic, strong) ConfirmBlock confirmBlock;

+ (FSAlertTools *)sharedInstance;

- (void)showAlert:(UIViewController *)targetVC title:(NSString *)titleStr message:(NSString *)messageStr cancleTitle:(NSString *)cancleTitle confirmTitle:(NSString *)confirmTitle cancleBlock:(void (^)(void))cancleBlock confirmBlock:(void (^)(void))confirmBlock;

@end
