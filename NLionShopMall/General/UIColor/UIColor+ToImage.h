//
//  UIColor+ToImage.h
//  LionMall
//
//  Created by casanova on 2017/4/14.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (ToImage)

// 转换成图片image
- (UIImage *)toImage;

@end
