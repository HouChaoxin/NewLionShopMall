//
//  UIColor+ToImage.m
//  LionMall
//
//  Created by casanova on 2017/4/14.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "UIColor+ToImage.h"

@implementation UIColor (ToImage)

// 转换成图片image
- (UIImage *)toImage {
    
    CGFloat imageW = 3;
    CGFloat imageH = 3;
    // 1.开启基于位图的图形上下文
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(imageW, imageH), NO, 0.0);
    // 2.画一个color颜色的矩形框
    [self set];
    UIRectFill(CGRectMake(0, 0, imageW, imageH));
    
    // 3.拿到图片
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    // 4.关闭上下文
    UIGraphicsEndImageContext();
    
    return image;
}

@end
