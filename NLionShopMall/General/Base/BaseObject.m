//
//  BaseObject.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "BaseObject.h"

@implementation BaseObject

- (NSString *)isNil:(NSString *)obj {
    return obj?[NSString stringWithFormat:@"%@",obj]:@"";
}

@end
