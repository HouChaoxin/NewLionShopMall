//
//  SelectDateView.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/28.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "SelectDateView.h"

@interface SelectDateView()

@property (nonatomic,strong) NSString *selectDateStr;

@end

@implementation SelectDateView


- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:[NSDate date]];
    self.selectDateStr = dateStr;
    
    [self createDatePicker];
}

- (void)createDatePicker {
    UIView *titeView = [[UIView alloc] init];
    titeView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:titeView];
    [titeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-180);
        make.height.equalTo(@40);
    }];
    
    UIButton *sureBtn = [FSViewHelper createBtnWithTitle:@"确定"
                                                 bgColor:[UIColor clearColor]
                                              titleColor:kBlackColor];
    [sureBtn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.right.equalTo(titeView).offset(-20);
        make.width.equalTo(@40);
    }];
    
    UIButton *cancelBtn = [FSViewHelper createBtnWithTitle:@"取消"
                                                   bgColor:[UIColor clearColor]
                                                titleColor:kBlackColor];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.left.equalTo(titeView).offset(20);
        make.width.equalTo(@40);
    }];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.backgroundColor = [UIColor groupTableViewBackgroundColor];
    datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:datePicker];
    [datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@180);
    }];
}

- (void)valueChange:(UIDatePicker *)datePicker{
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:datePicker.date];
    self.selectDateStr = dateStr;
}

- (void)sureBtnClick {
    
    _sureBtnAction(self.selectDateStr);
    [self removeFromSuperview];
}

- (void)cancelBtnClick {
    [self removeFromSuperview];
}

@end
