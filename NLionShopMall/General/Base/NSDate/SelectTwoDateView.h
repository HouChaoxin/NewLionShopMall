//
//  SelectDateView.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/28.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^SelectDateClick)(NSString *startDateStr,NSString *endDateStr);

@interface SelectTwoDateView : UIView

@property (nonatomic,copy) SelectDateClick sureBtnAction;

@end
