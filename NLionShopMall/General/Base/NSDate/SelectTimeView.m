//
//  SelectTimeView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "SelectTimeView.h"

@interface SelectTimeView()
@property (nonatomic,strong) NSString *selectTimeStr;
@end

@implementation SelectTimeView


- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    self.selectTimeStr = @"00:01";
    [self createDatePicker];
}

- (void)createDatePicker {
    UIView *titeView = [[UIView alloc] init];
    titeView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:titeView];
    [titeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-180);
        make.height.equalTo(@40);
    }];
    
    UIButton *sureBtn = [FSViewHelper createBtnWithTitle:@"确定"
                                                 bgColor:[UIColor clearColor]
                                              titleColor:kBlackColor];
    [sureBtn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.right.equalTo(titeView).offset(-20);
        make.width.equalTo(@40);
    }];
    
    UIButton *cancelBtn = [FSViewHelper createBtnWithTitle:@"取消"
                                                   bgColor:[UIColor clearColor]
                                                titleColor:kBlackColor];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.left.equalTo(titeView).offset(20);
        make.width.equalTo(@40);
    }];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.backgroundColor = [UIColor groupTableViewBackgroundColor];
    datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
    [datePicker addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:datePicker];
    [datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@180);
    }];
}

- (void)valueChange:(UIDatePicker *)datePicker{
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"HH:mm";
    NSString *dateStr = [fmt stringFromDate:datePicker.date];
    self.selectTimeStr = dateStr;
}

- (void)sureBtnClick {
    
    _sureBtnAction(self.selectTimeStr);
    [self removeFromSuperview];
}

- (void)cancelBtnClick {
    [self removeFromSuperview];
}

@end
