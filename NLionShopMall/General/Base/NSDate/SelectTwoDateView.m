//
//  SelectDateView.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/28.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "SelectTwoDateView.h"

@interface SelectTwoDateView()

@property (nonatomic,strong) NSString *selectType;
@property (nonatomic,strong) UIButton *startDateBtn;
@property (nonatomic,strong) UIButton *endDateBtn;
@property (nonatomic,strong) NSString *startDateStr;
@property (nonatomic,strong) NSString *endDateStr;
@end

@implementation SelectTwoDateView


- (instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.6];
    self.selectType = @"1";
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:[NSDate date]];
    self.startDateStr = dateStr;
    self.endDateStr = @"";
    
    [self createDatePicker];
}

- (void)createDatePicker {
    UIView *titeView = [[UIView alloc] init];
    titeView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self addSubview:titeView];
    [titeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-260);
        make.height.equalTo(@40);
    }];
    
    UIButton *sureBtn = [FSViewHelper createBtnWithTitle:@"确定"
                                                 bgColor:[UIColor clearColor]
                                              titleColor:kBlackColor];
    [sureBtn addTarget:self action:@selector(sureBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:sureBtn];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.right.equalTo(titeView).offset(-20);
        make.width.equalTo(@40);
    }];
    
    UIButton *cancelBtn = [FSViewHelper createBtnWithTitle:@"取消"
                                                   bgColor:[UIColor clearColor]
                                                titleColor:kBlackColor];
    [cancelBtn addTarget:self action:@selector(cancelBtnClick) forControlEvents:UIControlEventTouchDown];
    [titeView addSubview:cancelBtn];
    [cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(titeView);
        make.left.equalTo(titeView).offset(20);
        make.width.equalTo(@40);
    }];
    
    _startDateBtn = [FSViewHelper createBtnWithTitle:self.startDateStr
                                                      bgColor:[UIColor groupTableViewBackgroundColor]
                                                   titleColor:kBlackColor];
    [_startDateBtn setBackgroundColor:[UIColor lightGrayColor]];
    [_startDateBtn addTarget:self action:@selector(startDateBtnClick) forControlEvents:UIControlEventTouchDown];
    [self addSubview:_startDateBtn];
    [_startDateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-220);
        make.height.equalTo(@40);
    }];
    
    _endDateBtn = [FSViewHelper createBtnWithTitle:@"请选择结束日期"
                                                      bgColor:[UIColor groupTableViewBackgroundColor]
                                                   titleColor:kBlackColor];
    [_endDateBtn addTarget:self action:@selector(endDateBtnClick) forControlEvents:UIControlEventTouchDown];
    [self addSubview:_endDateBtn];
    [_endDateBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-180);
        make.height.equalTo(@40);
    }];
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.backgroundColor = [UIColor groupTableViewBackgroundColor];
    datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"zh"];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:datePicker];
    [datePicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(@180);
    }];
}

- (void)valueChange:(UIDatePicker *)datePicker{
    
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:datePicker.date];
    if ([self.selectType isEqualToString:@"1"]) {
        self.startDateStr = dateStr;
        [_startDateBtn setTitle:dateStr forState:UIControlStateNormal];
    }else {
        self.endDateStr = dateStr;
        [_endDateBtn setTitle:dateStr forState:UIControlStateNormal];
    }
}

- (void)sureBtnClick {
    
    if ([self.endDateStr isEqualToString:@""]) {
        [self makeToast:@"请选择结束日期" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    
    if (_sureBtnAction) {
        _sureBtnAction(_startDateStr,_endDateStr);
    }
    [self removeFromSuperview];
}

- (void)cancelBtnClick {
    [self removeFromSuperview];
}

- (void)startDateBtnClick {
    [_startDateBtn setBackgroundColor:[UIColor lightGrayColor]];
    [_endDateBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    self.selectType = @"1";
}

- (void)endDateBtnClick {
    [_startDateBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [_endDateBtn setBackgroundColor:[UIColor lightGrayColor]];
    self.selectType = @"2";
}

@end
