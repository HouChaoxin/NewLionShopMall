//
//  SelectDateView.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/28.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^BtnClick)(NSString *dateStr);

@interface SelectDateView : UIView

@property (nonatomic,copy) BtnClick sureBtnAction;

@end
