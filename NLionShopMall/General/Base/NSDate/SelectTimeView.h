//
//  SelectTimeView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^TimeAction)(NSString *timeStr);

@interface SelectTimeView : UIView

@property (nonatomic,copy) TimeAction sureBtnAction;

@end
