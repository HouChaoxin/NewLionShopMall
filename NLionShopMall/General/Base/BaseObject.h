//
//  BaseObject.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseObject : NSObject

- (NSString *)isNil:(NSString *)obj;

@end
