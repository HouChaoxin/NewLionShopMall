//
//  define.h
//  FormsFrame
//
//  Created by forms on 14-9-16.
//  Copyright (c) 2014年 ZhanHongjin. All rights reserved.
//

//用作为全局的Define宏定义

#ifndef FormsFrame_define_h
#define FormsFrame_define_h

#endif

//#define OPEN_OFFLINE_MODE 1

//******************************通用定义***********************************
#define kDocumentDirectory              [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define kApplicationSupportDirectory    [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0]
#define screenWidth                     [UIScreen mainScreen].bounds.size.width
#define screenHeight                    [UIScreen mainScreen].bounds.size.height
#define LocalizedString(key)            NSLocalizedString(key, nil)
#define scaleH(value)                   (((value) * [UIScreen mainScreen].bounds.size.height) / 1334.0)
#define scaleW(value)                   (((value) * [UIScreen mainScreen].bounds.size.width) / 750.0)
#define isPad                           (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

#ifdef DEBUG
#define LLog(fmt, ...)                  NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define LLog(format, ...)               while(0){}
#endif

#define kFontName @"limited-mgenplus-1cp-medium"

//******************************通知相关***********************************
#define SelectMarkerState                   @"SelectMarkerState"
#define NSNotificationStopTheCheckinTime    @"StopTheCheckinTime"
#define NSNotificationPostMsg               @"postNotification"
#define NSNotificationPostNoLoginMsg        @"postNotificationNoLogin"
#define applicationEnterBackground          @"applicationDidEnterBackground"

//******************************颜色控制***********************************
#define colorOf(x,y,z)          [UIColor colorWithRed:x/255.0 green:y/255.0 blue:z/255.0 alpha:1.0]
#define kWhiteColor             [UIColor whiteColor]
#define kBlackColor             [UIColor blackColor]
#define kRedColor               [UIColor redColor]
#define kTableViewColor         [UIColor groupTableViewBackgroundColor]
#define kLightGrayColor         [UIColor lightGrayColor]
#define kClearColor             [UIColor clearColor]
#define kWordColor              [ColorConverter colorWithHexString:@"0x333333"]
#define kLightWordColor         [ColorConverter colorWithHexString:@"0x666666"]
#define kLineColor              [ColorConverter colorWithHexString:@"0xE6E6E6"]

//******************************其他***********************************
#define kMemberPhone         @"memberPhone"
#define kMemberHeadImg       @"memberHeadImg"
#define kMemberToken         @"memberToken"
#define kMainStoreId         @"mainStoreId"
#define kStoreName          @"kStoreName"
#define kGuLiJinNum         @"kGuLiJinNum"
#define kMemberId           @"kMemberId"
#define kUserName           @"kUserName"

#define UserMainInfo        @"USERINFO"// 用户信息
#define MainUserDefaults    [NSUserDefaults standardUserDefaults]
