//
//  NSString+md5String.h
//  LionMall
//
//  Created by Casanova.Z on 2017/11/22.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (md5String)

+ (NSString *)md5String:(NSString *)str;

/** md5 NB(牛逼的意思)加密*/
+ (NSString *)md5StringNB:(NSString *)str;

@end
