//
//  NSString+Size.m
//  LionMall
//
//  Created by casanova on 2017/4/27.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

/**
 文本的宽度和高度

 @param font 字号
 @param height 文本高度，一般设定为frame高度
 @return 宽度
 */
- (CGFloat)calculateRowWithFont:(UIFont *)font height:(CGFloat)height{
    
        NSDictionary *dic = @{NSFontAttributeName:font};  //指定字号
        CGRect rect = [self boundingRectWithSize:CGSizeMake(0, height)/*计算宽度时要确定高度*/ options:NSStringDrawingUsesLineFragmentOrigin |
                       NSStringDrawingUsesFontLeading attributes:dic context:nil];
        return rect.size.width;
    
}

@end
