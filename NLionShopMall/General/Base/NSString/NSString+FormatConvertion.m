//
//  NSString+FormatConvertion.m
//  LionMall
//
//  Created by casanova on 2017/4/13.
//  Copyright © 2017年 lionmall. All rights reserved.
//

/*
 * 数据类型之间的转换
 */
#import "NSString+FormatConvertion.h"

@implementation NSString (FormatConvertion)

/**
 字符串转换成NSNumber类型
 
 @return 转换后的数据
 */
- (NSNumber *)numberValue {
    
    NSInteger integerValue = [self integerValue];
    return [NSNumber numberWithInteger:integerValue];
}

@end
