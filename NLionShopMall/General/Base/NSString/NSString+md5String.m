//
//  NSString+md5String.m
//  LionMall
//
//  Created by Casanova.Z on 2017/11/22.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "NSString+md5String.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (md5String)

/** md5 一般加密 */

+ (NSString *)md5String:(NSString *)str
{
    const char *myPasswd = [str UTF8String];
    unsigned char mdc[16];
    CC_MD5(myPasswd, (CC_LONG)strlen(myPasswd), mdc);
    NSMutableString *md5String = [NSMutableString string];
    for (int i = 0; i< 16; i++) {
        [md5String appendFormat:@"%02x",mdc[i]];
    }
    return md5String;
}

/** md5 NB(牛逼的意思)加密*/
+ (NSString *)md5StringNB:(NSString *)str
{
    const char *myPasswd = [str UTF8String];
    
    unsigned char mdc[16];
    
    CC_MD5(myPasswd, (CC_LONG)strlen(myPasswd), mdc);
    
    NSMutableString *md5String = [NSMutableString string];
    
    [md5String appendFormat:@"%02x",mdc[0]];
    
    for (int i = 1; i< 16; i++) {
        
        [md5String appendFormat:@"%02x",mdc[i]^mdc[0]];
        
    }
    
    return md5String;
    
}

@end

