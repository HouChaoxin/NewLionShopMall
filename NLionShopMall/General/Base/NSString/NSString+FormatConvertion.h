//
//  NSString+FormatConvertion.h
//  LionMall
//
//  Created by casanova on 2017/4/13.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormatConvertion)

/**
  字符串转换成NSNumber类型

 @return 转换后的数据
 */
- (NSNumber *)numberValue;


@end
