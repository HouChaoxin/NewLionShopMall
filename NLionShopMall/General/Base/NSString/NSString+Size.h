//
//  NSString+Size.h
//  LionMall
//
//  Created by casanova on 2017/4/27.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Size)

/**
 文本的宽度和高度
 
 @param font 字号
 @param height 文本高度，一般设定为frame高度
 @return 宽度
 */
- (CGFloat)calculateRowWithFont:(UIFont *)font height:(CGFloat)height;

@end
