//
//  TableViewCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/11.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoDataCell : UITableViewCell

- (void)refreshNoDataView;

- (void)refreshErrorView;

@end
