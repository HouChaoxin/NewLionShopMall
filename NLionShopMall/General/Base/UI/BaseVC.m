//
//  BaseVC.m
//  HCX_Practice003
//
//  Created by houchaoxin on 16/3/14.
//  Copyright © 2016年 houchaoxin. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()
    
@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initBaseView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initBaseView{
    
    self.view.backgroundColor = kWhiteColor;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)leftBtnClick:(UIButton *)sender{
    
}


- (void)setNavigationBarTitleViewWithTitle:(NSString *)title{
    UILabel * titleView = [UILabel new];
    titleView.text = title;
    [titleView setTextColor:[UIColor blackColor]];
    [titleView setFont:[UIFont systemFontOfSize:20]];
    [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
}


- (void)hideBackButtonTitle {
    UIView * itmeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12,11, 20)];
    imageView.image = [UIImage imageNamed:@"backBtn"];
    [itmeView addSubview:imageView];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popView)];
    [itmeView addGestureRecognizer:tap];
    
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:itmeView];
    [self.navigationItem setLeftBarButtonItem:leftButton];
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)setDismissKeyboradWhenBackgroundTouched:(UIView *)backgroundView{
    UITapGestureRecognizer  * tapBackgroundGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapBackground:)];
    [tapBackgroundGR setCancelsTouchesInView:NO];
    [backgroundView addGestureRecognizer:tapBackgroundGR];
}

- (void)tapBackground:(id)sender{
    [self.view endEditing:YES];
}

- (void)oneActionAlert:(NSString *)title Msg:(NSString *)msg confirmHander:(void(^)( UIAlertAction * _Nullable action))confirmAction{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *alertA = [UIAlertAction actionWithTitle:LocalizedString(@"P001") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (action) {
                confirmAction(action);
            }
        });
    }];
    [alertC addAction:alertA];
    
    [self presentViewController:alertC animated:YES completion:^{
        
    }];
}

- (void)twoActionAlert:(NSString *)title Msg:(NSString *)msg confirmHander:(void(^)(UIAlertAction *action))confirmAction cancelHander:(void(^)(UIAlertAction *action))cancelAction{
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *alertA = [UIAlertAction actionWithTitle:LocalizedString(@"P001") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (action) {
                confirmAction(action);
            }
        });
    }];
    
    UIAlertAction *alertB = [UIAlertAction actionWithTitle:LocalizedString(@"P004") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (action) {
                cancelAction(action);
            }
        });
    }];
    
    [alertC addAction:alertB];
    [alertC addAction:alertA];
    [self presentViewController:alertC animated:YES completion:^{
        
    }];
}


- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize{
    CGFloat height = 30;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX,height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.width;
}

- (CGFloat)caculateLblHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width{
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(width,CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.height;
}

- (UIColor *)colorFromRGB:(NSInteger)rgbValue{
    UIColor *color = [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(rgbValue & 0x0000FF))/255.0 alpha:1.0];
    return color;
}

@end
