//
//  BaseTableViewCell.m
//  Proxi
//
//  Created by houchaoxin on 2017/2/15.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import "BaseTableViewCell.h"

@implementation BaseTableViewCell

- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize{
    CGFloat height = 30;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX,height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.width;
}

- (UIColor *)colorFromRGB:(NSInteger)rgbValue{
    UIColor *color = [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(rgbValue & 0x0000FF))/255.0 alpha:1.0];
    return color;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
@end
