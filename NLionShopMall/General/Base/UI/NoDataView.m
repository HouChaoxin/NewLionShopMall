//
//  NoDataView.m
//  Proxi
//
//  Created by houchaoxin on 2017/5/23.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import "NoDataView.h"

@interface NoDataView()

@end

@implementation NoDataView

+ (NoDataView *)shareInstance {
    
    static NoDataView *instance =  nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

//展示无数据页面
- (void)showNoDataView:(UIView *)view {
    
    if (view) {
        [view addSubview:self];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    
    [self initView];
}

//移除无数据页面
- (void)removeNoDataView {
    [self removeFromSuperview];
}

- (void)initView{
    UIImageView *imageView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"magnifier"]
                                                bgColor:kWhiteColor];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-70);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(60, 60)]);
    }];
    
    UILabel *alertLbl = [FSViewHelper createLblWithText:@"暂无数据"
                                  textAlignment:NSTextAlignmentCenter
                                       fontSize:16];
    alertLbl.textColor = kBlackColor;
    alertLbl.adjustsFontSizeToFitWidth = YES;
    [self addSubview:alertLbl];
    [alertLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 35)]);
    }];
}

@end
