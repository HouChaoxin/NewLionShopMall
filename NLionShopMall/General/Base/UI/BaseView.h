//
//  BaseView.h
//  Proxi
//
//  Created by houchaoxin on 2017/2/15.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseView : UIView

- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize;

- (CGFloat)caculateHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width;

- (UIColor *)colorFromRGB:(NSInteger)rgbValue;

- (void)ALERT_MSG:(NSString *)title Msg:(NSString *)msg;
@end
