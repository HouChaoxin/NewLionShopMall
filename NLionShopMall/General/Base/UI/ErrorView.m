//
//  ErrorView.m
//  Proxi
//
//  Created by houchaoxin on 2017/3/14.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import "ErrorView.h"

typedef void(^ReloadActionBlock)(void);

@interface ErrorView()

@property (nonatomic,strong) ReloadActionBlock reloadBlock;

@end

@implementation ErrorView

+ (ErrorView *)shareInstance{
    
    static ErrorView *instance =  nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

//展示错误页面
- (void)showErrorView:(UIView *)view reloadAction:(void(^)(void))action {
    _reloadBlock = action;
    if (view) {
        [view addSubview:self];
        [self mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    }
    
    [self initView];
}

//移除错误页面
- (void)removeErrorView {
    [self removeFromSuperview];
}


- (void)initView{
    UIImageView *imageView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"click"]
                                                bgColor:kWhiteColor];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-70);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(70, 80)]);
    }];
    
    UILabel *alertLbl = [FSViewHelper createLblWithText:@"网络不给力，点击刷新"
                                  textAlignment:NSTextAlignmentCenter
                                       fontSize:16];
    alertLbl.textColor = kBlackColor;
    alertLbl.adjustsFontSizeToFitWidth = YES;
    [self addSubview:alertLbl];
    [alertLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 35)]);
    }];
    
    UIButton *btn = [FSViewHelper createBtnWithTitle:@""
                                     bgColor:[UIColor clearColor]
                                  titleColor:kWhiteColor];
    [self addSubview:btn];
    [btn addTarget:self action:@selector(btnAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 150)]);
    }];
}

- (void)btnAction:(UIButton *)sender {
    _reloadBlock();
}

@end
