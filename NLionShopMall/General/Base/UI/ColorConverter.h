//
//  ColorConverter.h
//  Proxi
//
//  Created by houchaoxin on 2017/3/10.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorConverter : NSObject

+ (UIColor *) colorWithHexString: (NSString *)color;

@end
