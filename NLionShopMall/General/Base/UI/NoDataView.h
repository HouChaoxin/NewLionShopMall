//
//  NoDataView.h
//  Proxi
//
//  Created by houchaoxin on 2017/5/23.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NoDataView : BaseView

+ (NoDataView *)shareInstance;

//展示无数据页面
- (void)showNoDataView:(UIView *)view;

//移除无数据页面
- (void)removeNoDataView;

@end
