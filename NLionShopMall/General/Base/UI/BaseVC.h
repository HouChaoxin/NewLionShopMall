//
//  BaseVC.h
//  HCX_Practice003
//
//  Created by houchaoxin on 16/3/14.
//  Copyright © 2016年 houchaoxin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController

- (void)hideBackButtonTitle;

- (void)setNavigationBarTitleViewWithTitle:(NSString *)title;

-(void)setDismissKeyboradWhenBackgroundTouched:(UIView *)backgroundView;

- (void)oneActionAlert:(NSString *)title Msg:(NSString *)msg confirmHander:(void(^)(UIAlertAction *action))confirmAction;

- (void)twoActionAlert:(NSString *)title Msg:(NSString *)msg confirmHander:(void(^)(UIAlertAction *action))confirmAction cancelHander:(void(^)(UIAlertAction *action))cancelAction;

- (void)leftBtnClick:(UIButton *)sender;

- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize;

- (CGFloat)caculateLblHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width;

- (UIColor *)colorFromRGB:(NSInteger)rgbValue;

@end
