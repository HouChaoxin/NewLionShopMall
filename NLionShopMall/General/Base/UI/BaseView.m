//
//  BaseView.m
//  Proxi
//
//  Created by houchaoxin on 2017/2/15.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize{
    CGFloat height = 30;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX,height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.width;
}

- (CGFloat)caculateHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width{
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(width,CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.height;
}

- (UIColor *)colorFromRGB:(NSInteger)rgbValue{
    UIColor *color = [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0x00FF00) >> 8))/255.0 blue:((float)(rgbValue & 0x0000FF))/255.0 alpha:1.0];
    return color;
}

- (void)ALERT_MSG:(NSString *)title Msg:(NSString *)msg {
    
//    UIViewController *currentVC = [FSViewHelper getCurrentVc:self.window.rootViewController];
//    [[FSAlertTools sharedInstance] showAlertWithMessage:msg inViewController:currentVC handler:^(UIAlertAction * _Nullable action) {
//    }];
}

@end
