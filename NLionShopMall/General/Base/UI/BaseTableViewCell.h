//
//  BaseTableViewCell.h
//  Proxi
//
//  Created by houchaoxin on 2017/2/15.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseTableViewCell : UITableViewCell

- (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize;
- (UIColor *)colorFromRGB:(NSInteger)rgbValue;

@end
