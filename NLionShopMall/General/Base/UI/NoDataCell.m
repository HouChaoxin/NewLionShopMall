//
//  TableViewCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/11.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "NoDataCell.h"

@interface NoDataCell()
{
    UILabel *alertLbl;
    UIImageView *imageView;
}

@end

@implementation NoDataCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    imageView = [FSViewHelper createImgViewWithImg:nil
                                           bgColor:[UIColor clearColor]];
    [self addSubview:imageView];
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-20);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(60, 60)]);
    }];
    
    alertLbl = [FSViewHelper createLblWithText:@""
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:16];
    alertLbl.textColor = kBlackColor;
    alertLbl.adjustsFontSizeToFitWidth = YES;
    [self addSubview:alertLbl];
    [alertLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(imageView.mas_bottom).offset(20);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 35)]);
    }];
}

- (void)refreshNoDataView {
    imageView.image = [UIImage imageNamed:@"magnifier"];
    alertLbl.text = @"抱歉，暂无数据";
}

- (void)refreshErrorView {
    imageView.image = [UIImage imageNamed:@"magnifier"];
    alertLbl.text = @"网络错误，请稍后再试";
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
