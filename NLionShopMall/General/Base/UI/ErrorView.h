//
//  ErrorView.h
//  Proxi
//
//  Created by houchaoxin on 2017/3/14.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorView : BaseView

+ (ErrorView *)shareInstance;
//展示错误页面
- (void)showErrorView:(UIView *)view reloadAction:(void(^)(void))action;
//移除错误页面
- (void)removeErrorView;

@end
