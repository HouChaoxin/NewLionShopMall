//
//  UrlDefine.h
//  Proxi
//
//  Created by houchaoxin on 2017/2/23.
//  Copyright © 2017年 formssi. All rights reserved.
//

#ifndef UrlDefine_h
#define UrlDefine_h


#endif /* UrlDefine_h */



#define baseUrl @"http://pd.lion-mall.com/?r="
//#define baseUrl @"http://pd.local.com/?r="

#define imgBaseUrl @"http://img.lion-mall.com/"

#define kTiXianRecordUrl @"business/withdraw-list"
#define kGetBankCardInfoUrl @"business-bank/get-card-info"
#define kGetPhoneCodeUrl @"sms/user-bank-card"
#define kAddBankCardUrl @"business-bank/add"
#define kGuLiJinDetailUrl @"excitation/getamount"
#define kTiXianUrl @"business/withdraw"
#define kGetBankCardListUrl @"business-bank/list"
#define kLoginVertifyCode @"sms/user-login"
#define kLoginUrl @"business/login"
#define kGetCareerUrl @"zhusq/hangye"
#define kGetCareerTwoUrl @"http://website.iyfpay.com/lcsw/mccwx/getmccwx2"
#define kGetBankCityUrl @"http://website.iyfpay.com/lcsw/provincecitystd/getcity"
#define kGetCityUrl @"http://pd.lion-mall.com/?r=area/list"
#define kGetBranchCityUrl @"http://website.iyfpay.com/lcsw/bank/getbank"
#define kMainInfoUrl @"pd-store-xin/get-store-money"
#define kIncomeUrl @"pd-store-xin/getpaylist"
#define kAddBusinessUrl @"store/add-business-store"
