//
//  FSViewHelper.m
//  Proxi
//
//  Created by huangguojian on 2017/7/10.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import "FSViewHelper.h"
#import "sys/utsname.h"

static NSString * const kBOCBuildDate = @"SIT-20170927-1555-";

@implementation FSViewHelper

+ (UILabel *)createLblWithText:(NSString *)text
                 textAlignment:(NSTextAlignment)textAlignment
                      fontSize:(CGFloat)fontSize{
    
    UILabel *lbl = [[UILabel alloc] init];
    lbl.text = text;
    lbl.textAlignment = textAlignment;
    lbl.font = [UIFont systemFontOfSize:fontSize];
    return lbl;
}

+ (UIButton *)createBtnWithTitle:(NSString *)title
                         bgColor:(UIColor *)bgColor
                      titleColor:(UIColor *)titleColor{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    btn.backgroundColor = bgColor;
    [btn setTitleColor:titleColor forState:UIControlStateNormal];
    return btn;
}

+ (UIImageView *)createImgViewWithImg:(UIImage *)img
                              bgColor:(UIColor *)bgColor{
    
    UIImageView *imgView = [[UIImageView alloc] init];
    imgView.backgroundColor = bgColor;
    if (img) {
        imgView.image = img;
    }
    return imgView;
}

+ (UITextField *)creatTextFieldWithTextAlignment:(NSTextAlignment)textAlignment
                                       fontSize:(CGFloat)fontSize
                                      textColor:(UIColor *)color {
    
    UITextField *textField = [[UITextField alloc] init];
    textField.font = [UIFont systemFontOfSize:fontSize];
    textField.textColor = color;
    textField.textAlignment = textAlignment;
    return textField;
}


+ (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize {
    CGFloat height = 30;
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(CGFLOAT_MAX,height) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.width;
}

+ (CGFloat)caculateHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width {
    
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:fonSize]};
    
    CGRect frameSize = [string boundingRectWithSize:CGSizeMake(width,CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil];
    return frameSize.size.height;
}


+ (UIViewController *)getCurrentVc:(UIViewController *)vc {
    if (vc.presentedViewController) {
        return [self getCurrentVc:vc.presentedViewController];
    }else if ([vc isKindOfClass:[UINavigationController class]]){
        UINavigationController *nav = (UINavigationController *)vc;
        if (nav.viewControllers.count > 0) {
            return [self getCurrentVc:nav.topViewController];
        }else{
            return vc;
        }
    }else{
        return vc;
    }
}

+ (UIViewController *)getCurrentViewController:(UIView *) currentView {
    for (UIView* next = [currentView superview]; next; next = next.superview)
    {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]])
        {
            return (UIViewController *)nextResponder;
        }
    }
    return nil;
}

+ (NSArray<NSValue *> *)findNumRangesFromStr:(NSString *)originalString {
    
    NSMutableArray *ranges = [NSMutableArray array];
    
    // Intermediate
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    while (![scanner isAtEnd]) {
        //Throw away characters before the first number.
        [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
        
        //Collect numbers.
        NSString *tempStr;
        if([scanner scanCharactersFromSet:numbers intoString:&tempStr]){
            NSRange aRange = NSMakeRange(scanner.scanLocation - tempStr.length, tempStr.length);
            [ranges addObject:[NSValue valueWithRange:aRange]];
        }
    }
    return ranges;
}

+ (NSString *)transform:(NSString *)chinese {
    NSMutableString *pinyin = [chinese mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    return [pinyin uppercaseString];
}

+ (NotificationSettingType)checkNotificationSetting
{
    __block NotificationSettingType status  = Notification_Unkown;
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0){
        dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
        UNUserNotificationCenter* current = [UNUserNotificationCenter currentNotificationCenter];
        [current getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            if (settings.authorizationStatus == UNAuthorizationStatusNotDetermined) {
                status = Notification_Unkown;
            }else if (settings.authorizationStatus == UNAuthorizationStatusDenied) {
                status = Notification_Deny;
            }else if (settings.authorizationStatus == UNAuthorizationStatusAuthorized) {
                status = Notification_Allow;
            }
            dispatch_semaphore_signal(semaphore);
        }];
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
        
    }else if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){
        UIUserNotificationSettings *notificationSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        // UIRemoteNotificationType is deprecated, 3 = None
        if (!notificationSettings || (notificationSettings && (notificationSettings.types == 3)) || (notificationSettings && (notificationSettings.types == UIUserNotificationTypeNone))) {
            status = Notification_Deny;
        } else {
            status = Notification_Allow;
        }
    }
    return status;
}

+ (NSArray *)getBunldCertDataArr {
    
    NSString *cerBundlePath = [[NSBundle mainBundle] pathForResource:@"Certificate" ofType:@"bundle"];
    NSBundle *cerBundle = [NSBundle bundleWithPath:cerBundlePath];
    NSArray *certArrPaths = [NSBundle pathsForResourcesOfType:@"cer" inDirectory:[cerBundle bundlePath]];
    NSArray *certArrPath = [NSBundle pathsForResourcesOfType:@"der" inDirectory:[cerBundle bundlePath]];
    
    NSMutableArray *paths = [NSMutableArray arrayWithArray:certArrPaths];
    for (NSString *path in certArrPath) {
        [paths addObject:path];
    }
    
    NSMutableArray *cerDatas = [[NSMutableArray alloc] init];
    for (NSString *path in paths) {
        NSData *localCertificateData = [NSData dataWithContentsOfFile:path];
        [cerDatas addObject:localCertificateData];
    }
    
    return cerDatas;
}

+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setDateFormat:format];
    
    NSTimeZone* timeZone = [NSTimeZone timeZoneWithName:@"Asia/Beijing"];
    [formatter setTimeZone:timeZone];
    
    NSDate* date = [formatter dateFromString:formatTime];
    
    NSInteger timeSp = [[NSNumber numberWithDouble:[date timeIntervalSince1970]] integerValue];
    
    return timeSp;
}

@end
