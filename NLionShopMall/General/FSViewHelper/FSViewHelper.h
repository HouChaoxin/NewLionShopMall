//
//  FSViewHelper.h
//  Proxi
//
//  Created by huangguojian on 2017/7/10.
//  Copyright © 2017年 formssi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

typedef NS_ENUM(NSUInteger, NotificationSettingType) {
    Notification_Allow,
    Notification_Deny,
    Notification_Unkown,
};

@interface FSViewHelper : NSObject

+ (UILabel *)createLblWithText:(NSString *)text
                 textAlignment:(NSTextAlignment)textAlignment
                      fontSize:(CGFloat)fontSize;

+ (UIButton *)createBtnWithTitle:(NSString *)title
                         bgColor:(UIColor *)bgColor
                      titleColor:(UIColor *)titleColor;

+ (UIImageView *)createImgViewWithImg:(UIImage *)img
                              bgColor:(UIColor *)bgColor;

+ (UITextField *)creatTextFieldWithTextAlignment:(NSTextAlignment)textAlignment
                                        fontSize:(CGFloat)fontSize
                                       textColor:(UIColor *)color;

+ (UIViewController *)getCurrentVc:(UIViewController *)vc;

+ (UIViewController *)getCurrentViewController:(UIView *)currentView;

+ (NSArray<NSValue *> *)findNumRangesFromStr:(NSString *)originalString;

+ (CGFloat)caculateLblWidthWithString:(NSString *)string andFontSize:(CGFloat)fonSize;

+ (CGFloat)caculateHeightWithString:(NSString *)string andFontSize:(CGFloat)fonSize width:(CGFloat)width;

+ (NSString *)transform:(NSString *)chinese;

+ (NotificationSettingType)checkNotificationSetting;

+ (NSArray *)getBunldCertDataArr;
+(NSInteger)timeSwitchTimestamp:(NSString *)formatTime andFormatter:(NSString *)format;
@end
