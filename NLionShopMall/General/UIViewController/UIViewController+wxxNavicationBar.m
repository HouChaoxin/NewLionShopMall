//
//  UIViewController+wxxNavicationBar.m
//  ZhaoCaiHuiBaoSeller
//
//  Created by Wzs 王 on 2017/9/19.
//  Copyright © 2017年 Jekity. All rights reserved.
//

#import "UIViewController+wxxNavicationBar.h"
#import <objc/runtime.h>


@implementation UIViewController (wxxNavicationBar)

#pragma mark---- load ----
+ (void)load{
    
    Method mathod1 = class_getInstanceMethod(self, NSSelectorFromString(@"wxx_viewWillAppear:"));
    Method mathod2 = class_getInstanceMethod(self, @selector(viewWillAppear:));
    if (mathod1 && mathod2) {
        method_exchangeImplementations(mathod1, mathod2);
    }
}

#pragma mark---- 属性 ----
- (BOOL)isHiddenNavigationBar{
    
    return [objc_getAssociatedObject(self, @selector(isHiddenNavigationBar)) boolValue];
    
}
- (void)setIsHiddenNavigationBar:(BOOL)isHiddenNavigationBar{
    
    objc_setAssociatedObject(self, @selector(isHiddenNavigationBar), @(isHiddenNavigationBar), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



- (void)wxx_viewWillAppear:(BOOL)animated
{
    [self wxx_viewWillAppear:animated];
    
    if (![self.navigationController isKindOfClass:NSClassFromString(@"JB_BasicNavigationController")]) {
        
        return;
    }

    if (objc_getAssociatedObject(self, @selector(isHiddenNavigationBar)) == nil) {
        return;
    }
    
    if (self.isHiddenNavigationBar) {
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
    }else{
        
        [self.navigationController setNavigationBarHidden:NO animated:NO];

    }
    

    
}


- (void)wxxSetDefaultStyleTitle:(NSString *)title {
    
    [self wxxTitle:title titleColor:[UIColor blackColor] font:[UIFont systemFontOfSize:17]];
}

- (void)wxxTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font{
    
    UILabel *label = [UILabel new];
    label.text = title;
    label.textColor = titleColor;
    [label sizeToFit];
    if (font) {
        label.font = font;
    }
    
    self.navigationItem.titleView = label;
}


- (void)wxxLeftTitle:(NSString *)LeftTitle titleColor:(UIColor *)titleColor LeftImg:(NSString *)LeftImg font:(UIFont *)font{
    
    UIButton *btn = [self btnWithTitle:LeftTitle titleColor:titleColor Img:LeftImg font:font];
    
    [btn addTarget:self action:@selector(wxxClickLeftBtn:) forControlEvents:(UIControlEventTouchUpInside)];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    
}

- (void)wxxRightTitle:(NSString *)RightTitle titleColor:(UIColor *)titleColor rightImg:(NSString *)rightImg font:(UIFont *)font{
    UIButton *btn = [self btnWithTitle:RightTitle titleColor:titleColor Img:rightImg font:font];
    
    
    [btn addTarget:self action:@selector(wxxClickRightBtn:) forControlEvents:(UIControlEventTouchUpInside)];

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];

}

- (UIButton *)btnWithTitle:(NSString *)title titleColor:(UIColor *)titleColor Img:(NSString *)Img font:(UIFont *)font{
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    if (title) {
        [btn setTitle:title forState:(UIControlStateNormal)];
    }
    
    if (titleColor) {
        [btn setTitleColor:titleColor forState:(UIControlStateNormal)];
    }
    
    if (Img) {
        [btn setImage:[UIImage imageNamed:Img] forState:(UIControlStateNormal)];
    }
    
    if (font) {
        btn.titleLabel.font = font;
    }
    
    [btn sizeToFit];
    
    return btn;
}
wxxClickLeftHandle{
    
}

wxxClickRightHandle{
    
}



@dynamic wxxCallBack;

-(wxxCallBackBlock)wxxCallBack
{
    if (!objc_getAssociatedObject(self, @selector(wxxCallBack))) {
        self.wxxCallBack = ^(id parms){};
    }
    return objc_getAssociatedObject(self, @selector(wxxCallBack));
}
-(void)setWxxCallBack:(wxxCallBackBlock)wxxCallBack
{
    objc_setAssociatedObject(self, @selector(wxxCallBack), wxxCallBack, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
