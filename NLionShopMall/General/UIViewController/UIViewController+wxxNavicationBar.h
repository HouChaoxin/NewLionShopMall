//
//  UIViewController+wxxNavicationBar.h
//  ZhaoCaiHuiBaoSeller
//
//  Created by Wzs 王 on 2017/9/19.
//  Copyright © 2017年 Jekity. All rights reserved.
//

//#import <UIKit/UIKit.h>


typedef void (^wxxCallBackBlock)(id parameter);

#define wxxClickLeftHandle       -(void)wxxClickLeftBtn:(UIButton *)btn
#define wxxClickRightHandle      -(void)wxxClickRightBtn:(UIButton *)btn

@interface UIViewController (wxxNavicationBar)

@property (nonatomic, copy)wxxCallBackBlock wxxCallBack;


@property (nonatomic,assign)BOOL isHiddenNavigationBar;

- (void)wxxSetDefaultStyleTitle:(NSString *)title ;

- (void)wxxTitle:(NSString *)title titleColor:(UIColor *)titleColor font:(UIFont *)font;


- (void)wxxLeftTitle:(NSString *)LeftTitle titleColor:(UIColor *)titleColor LeftImg:(NSString *)LeftImg font:(UIFont *)font;

- (void)wxxRightTitle:(NSString *)RightTitle titleColor:(UIColor *)titleColor rightImg:(NSString *)rightImg font:(UIFont *)font;




@end
