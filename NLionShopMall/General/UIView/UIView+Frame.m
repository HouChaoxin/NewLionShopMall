//
//  UIView+Frame.m
//  Wallet
//
//  Created by wufengheng on 16/6/7.
//  Copyright © 2016年 orangenat. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

- (CGSize)fh_size
{
    return self.frame.size;
}

- (void)setFh_size:(CGSize)fh_size
{
    CGRect frame = self.frame;
    frame.size = fh_size;
    self.frame = frame;
}

- (CGFloat)fh_width
{
    return self.frame.size.width;
}

- (CGFloat)fh_height
{
    return self.frame.size.height;
}

- (void)setFh_width:(CGFloat)fh_width
{
    CGRect frame = self.frame;
    frame.size.width = fh_width;
    self.frame = frame;
}

- (void)setFh_height:(CGFloat)fh_height
{
    CGRect frame = self.frame;
    frame.size.height = fh_height;
    self.frame = frame;
}

- (CGFloat)fh_x
{
    return self.frame.origin.x;
}

- (void)setFh_x:(CGFloat)fh_x
{
    CGRect frame = self.frame;
    frame.origin.x = fh_x;
    self.frame = frame;
}

- (CGFloat)fh_y
{
    return self.frame.origin.y;
}

- (void)setFh_y:(CGFloat)fh_y
{
    CGRect frame = self.frame;
    frame.origin.y = fh_y;
    self.frame = frame;
}

- (CGFloat)fh_centerX
{
    return self.center.x;
}

- (void)setFh_centerX:(CGFloat)fh_centerX
{
    CGPoint center = self.center;
    center.x = fh_centerX;
    self.center = center;
}

- (CGFloat)fh_centerY
{
    return self.center.y;
}

- (void)setFh_centerY:(CGFloat)fh_centerY
{
    CGPoint center = self.center;
    center.y = fh_centerY;
    self.center = center;
}

- (CGFloat)fh_right
{
    return CGRectGetMaxX(self.frame);
}

- (CGFloat)fh_bottom
{
    return CGRectGetMaxY(self.frame);
}

- (void)setFh_right:(CGFloat)fh_right
{
    self.fh_x = fh_right - self.fh_width;
}

- (void)setFh_bottom:(CGFloat)fh_bottom
{
    self.fh_y = fh_bottom - self.fh_height;
}

+ (instancetype)viewFromXib
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].lastObject;
}

- (BOOL)intersectWithView:(UIView *)view
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    CGRect selfRect = [self convertRect:self.bounds toView:window];
    CGRect viewRect = [view convertRect:view.bounds toView:window];
    return CGRectIntersectsRect(selfRect, viewRect);
}


@end
