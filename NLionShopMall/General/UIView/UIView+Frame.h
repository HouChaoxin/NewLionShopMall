//
//  UIView+Frame.h
//  Wallet
//
//  Created by wufengheng on 16/6/7.
//  Copyright © 2016年 orangenat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property (nonatomic, assign) CGSize fh_size;
@property (nonatomic, assign) CGFloat fh_width;
@property (nonatomic, assign) CGFloat fh_height;
@property (nonatomic, assign) CGFloat fh_x;
@property (nonatomic, assign) CGFloat fh_y;
@property (nonatomic, assign) CGFloat fh_centerX;
@property (nonatomic, assign) CGFloat fh_centerY;
@property (nonatomic, assign) CGFloat fh_right;
@property (nonatomic, assign) CGFloat fh_bottom;

+ (instancetype)viewFromXib;

- (BOOL)intersectWithView:(UIView *)view;
@end
