//
//  AppDelegate.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/12.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "AppDelegate.h"
#import "HLoginVC.h"
#import "RootViewController.h"


#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "WXApi.h"

#define WXAppID @"wxb5c50c6d6586ecbb" // 微信appid
#define WXAppSecret @"8b2ae6609d08a5d5a44adfd9c4467ee8" // 微信AppSecret

#define QQAppID @"1106658028"
#define QQAppSecret @"z9ICLhm9Vs4SNGwj"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [self firstShow];
    [self initMob];
    return YES;
}

- (void)firstShow {
    self.window.backgroundColor = kWhiteColor;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kMemberToken]) {

        RootViewController *rootVC = [[RootViewController alloc] init];
        self.window.rootViewController = rootVC;
    }else {
        HLoginVC *loginVC = [[HLoginVC alloc] init];
        UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:loginVC];
        self.window.rootViewController = nv;
    }
    [self.window makeKeyAndVisible];
}


// 初始化分享的相关
- (void)initMob {
    
    [ShareSDK registerActivePlatforms:@[@(SSDKPlatformTypeWechat),@(SSDKPlatformTypeQQ)] onImport:^(SSDKPlatformType platformType) {
        
        switch (platformType) {
            case SSDKPlatformTypeWechat: // 微信
                [ShareSDKConnector connectWeChat:[WXApi class]];
                break;
            case SSDKPlatformTypeQQ:     // qq
                [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                break;
            default:
                break;
        }
    } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
        switch (platformType)
        {
            case SSDKPlatformTypeWechat:
                [appInfo SSDKSetupWeChatByAppId:WXAppID
                                      appSecret:WXAppSecret];
                break;
            case SSDKPlatformTypeQQ:
                [appInfo SSDKSetupQQByAppId:QQAppID
                                     appKey:QQAppSecret
                                   authType:SSDKAuthTypeBoth];
                break;
                
            default:
                break;
        }
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
}


@end
