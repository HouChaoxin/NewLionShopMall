//
//  BaseNavViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BaseNavViewController.h"
#import "SearchGoodsViewController.h"
#import "MainMerchantViewController.h"

@interface BaseNavViewController ()<UIGestureRecognizerDelegate>

@end

@implementation BaseNavViewController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController  {
    if (self = [super initWithRootViewController:rootViewController]) {
        // 设置导航栏不透明
        self.navigationBar.translucent = NO;
        self.interactivePopGestureRecognizer.delegate = self;
        self.navigationBar.barTintColor = colorOf(244, 245, 247);
    }
    return self;
}

//- (void)viewDidLoad {
//    [super viewDidLoad];
//    self.interactivePopGestureRecognizer.delegate = self;
//}

//+ (void)initialize
//{
//    UINavigationBar *bar = [UINavigationBar appearanceWhenContainedIn:[BaseNavViewController class], nil];
//    [bar setBarTintColor:colorOf(244, 245, 247)];
//}


/**
 push到下一个控制器
 
 @param viewController 下一个控制器
 @param animated 是否支持动画
 */
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    
    if ([viewController isKindOfClass:[SearchGoodsViewController class]] ) {
        viewController.hidesBottomBarWhenPushed = YES;
        [super pushViewController:viewController animated:YES];
        [self setNavigationBarHidden:NO animated:animated];
        return;
    }
    
    // || [viewController isKindOfClass:[MainMerchantViewController class]]
    
    
    if (self.viewControllers.count == 0) {
        [super pushViewController:viewController animated:animated];
    }else {
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"backBtn"] imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal] style: UIBarButtonItemStylePlain target: self action:@selector(backPrevious)];
        [super pushViewController:viewController animated:YES];
    }
}

/**
 点击标题栏上的左箭头返回上一个控制器
 */
- (void)backPrevious {
    [super popViewControllerAnimated:true];
}

/**
 向右滑动，返回上一个控制器
 
 @param gestureRecognizer 手势
 @return true：支持，false：不支持
 */
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if (self.viewControllers.count == 0) {
        return false;
    }
    return true;
}

@end
