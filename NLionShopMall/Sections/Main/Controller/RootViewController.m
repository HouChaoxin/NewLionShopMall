//
//  RootViewController.m
//  LionMall
//
//  Created by casanova on 2017/4/14.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "RootViewController.h"
#import "MainVC.h"
#import "HLoginVC.h"
#import "MainUserViewController.h"
#import "MainMerchantViewController.h"

#import "BaseNavViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tabBar.translucent = NO;
    
    // 初始化界面
    [self initView];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginAction) name:LoginNotificationName object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeTab:) name:ChangeTabNotificationName object:nil];
}

- (void)loginAction {
    
    HLoginVC *loginVC = [[HLoginVC alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:loginVC];
    [self presentViewController:nav animated:YES completion:^{

        NSLog(@"前往登录页面");
    }];
}

// 切换tab
- (void)changeTab:(NSNotification *)notif {
    
    NSNumber *tabIndex = notif.object;
    NSInteger index = [tabIndex integerValue];
    self.selectedIndex = index;
}

/**
 初始化界面
 */
- (void)initView {
    
    [self addNavViewController:[MainVC new] title:@"首页" image:[UIImage imageNamed:@"ic_home_2"] selectedImage:[UIImage imageNamed:@"ic_home_1"]];
    [self addNavViewController:[MainMerchantViewController new] title:@"商铺" image:[UIImage imageNamed:@"ic_dianpu_2"] selectedImage:[UIImage imageNamed:@"ic_dianpu_1"]];
    [self addNavViewController:[MainUserViewController new] title:@"我的" image:[UIImage imageNamed:@"ic_wo_1"] selectedImage:[UIImage imageNamed:@""]];
    self.selectedIndex = 0;
}

- (void)addViewController:(UIViewController *)viewController title:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage{
    
    viewController.tabBarItem.title = title;
    viewController.tabBarItem.image = image;
    viewController.tabBarItem.selectedImage = selectedImage;
    
    [self addChildViewController:viewController];
}

- (void)addNavViewController:(UIViewController *)viewController title:(NSString *)title image:(UIImage *)image selectedImage:(UIImage *)selectedImage{
    
    BaseNavViewController *nav = [[BaseNavViewController alloc] initWithRootViewController:viewController];
    nav.tabBarItem.title = title;
    nav.tabBarItem.image = image;
    nav.tabBarItem.selectedImage = selectedImage;
    
    [self addChildViewController:nav];
}

@end
