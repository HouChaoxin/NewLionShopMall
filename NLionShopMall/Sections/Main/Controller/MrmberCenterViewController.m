//
//  MrmberCenterViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MrmberCenterViewController.h"
#import "MemberDetailViewController.h"
#import "MemberListViewCell.h"
#import "SearchView.h"


@interface MrmberCenterViewController ()

@property (nonatomic ,strong) SearchView *searchView;

@end

@implementation MrmberCenterViewController

-(SearchView *)searchView{
    if (!_searchView) {
        _searchView = [[SearchView alloc] initWithFrame:CGRectMake(0, 0, screenW, 50)];
    }
    return _searchView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"会员中心";
    self.tableView.rowHeight = 70;
    [self.tableView registerClass:[MemberListViewCell class] forCellReuseIdentifier:NSStringFromClass([MemberListViewCell class])];
    self.tableView.tableHeaderView = self.searchView;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MemberListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MemberListViewCell class]) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MemberDetailViewController *vc = [[MemberDetailViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
