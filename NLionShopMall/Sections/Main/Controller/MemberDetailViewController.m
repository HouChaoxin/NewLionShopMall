//
//  MemberDetailViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MemberDetailViewController.h"
#import "HistoryTableViewCell.h"
#import "LSMDetailHeaderView.h"

@interface MemberDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) LSMDetailHeaderView *headerView;

@end

@implementation MemberDetailViewController


-(LSMDetailHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[LSMDetailHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenW, 168)];
//        _headerView.model = self.model;
    }
    return _headerView;
}


-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH - 64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[HistoryTableViewCell class] forCellReuseIdentifier:NSStringFromClass([HistoryTableViewCell class])];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _tableView.tableHeaderView = self.headerView;
        _tableView.rowHeight = 52;
        _tableView.sectionHeaderHeight = 52;
        _tableView.sectionFooterHeight = 0;
        _tableView.tableFooterView = [UIView new];
    }
    return _tableView;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"会员详情";
    self.tableView.backgroundColor = [UIColor getColor:@"#e6e6e6"];
    [self.view addSubview:self.tableView];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    HistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([HistoryTableViewCell class]) forIndexPath:indexPath];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = [UIColor whiteColor];
    NSArray *titleArr = @[@"时间",@"金额",@"积分"];
    CGFloat item_W = screenW/4;
    for (int i=0; i<titleArr.count; i++) {
        
        UILabel *titleLab = [[UILabel alloc] init];
        titleLab.font = [UIFont boldSystemFontOfSize:15];
        if (i == 0) {
            titleLab.frame = CGRectMake(item_W*i, 0, item_W*2, 51);
        }else if (i == 1){
            titleLab.frame = CGRectMake(item_W*2, 0, item_W, 51);
        }else{
            titleLab.frame = CGRectMake(item_W*3, 0, item_W, 51);
        }
        titleLab.text = titleArr[i];
        titleLab.textAlignment = NSTextAlignmentCenter;
        [view addSubview:titleLab];
    }
    UILabel *link = [[UILabel alloc] initWithFrame:CGRectMake(0, 51, screenW, 1)];
    link.backgroundColor = [UIColor getColor:@"f3f3f3"];
    [view addSubview:link];
    return view;
}


@end
