//
//  MemberListViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MemberListViewController.h"
#import "MemberDetailViewController.h"
#import "MrmberCenterViewController.h"
#import "MemberListViewCell.h"
#import "SearchView.h"

#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"
#import "BrowseMemberBannerView.h"
#import "SelectDateView.h"

@interface MemberListViewController ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>

@property (nonatomic ,strong) SearchView *searchView;

@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSArray *defaultDateAry;
@end

@implementation MemberListViewController

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(SearchView *)searchView{
    if (!_searchView) {
        _searchView = [[SearchView alloc] initWithFrame:CGRectMake(0, 0, screenW, 50)];
    }
    return _searchView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"会员管理";
    self.tableView.rowHeight = 60;
    [self.tableView registerClass:[MemberListViewCell class] forCellReuseIdentifier:NSStringFromClass([MemberListViewCell class])];
//    self.tableView.tableHeaderView = self.searchView;
    
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithTitle:@"会员中心" style:UIBarButtonItemStylePlain target:self action:@selector(rightAction)];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    [self.dataArray addObject:@"noTodayIncome"];
    [self.dataArray addObject:@"todayIncome"];
    
    [self setupFlowUI];
    
    [self loadBaseData];
}

-(void)rightAction{
    
    MrmberCenterViewController *vc = [[MrmberCenterViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setupFlowUI {
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, screenW, 160)];
    pageFlowView.backgroundColor = kWhiteColor;
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.4;
    pageFlowView.leftRightMargin = 30;
    pageFlowView.topBottomMargin = 0;
    pageFlowView.orginPageCount = self.dataArray.count;
    pageFlowView.isOpenAutoScroll = NO;
//    [self.view addSubview:pageFlowView];
    self.tableView.tableHeaderView = pageFlowView;
    [pageFlowView reloadData];
    self.pageFlowView = pageFlowView;
}

#pragma mark --NewPagedFlowView Delegate
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    
    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(screenW-50, 164);
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return self.dataArray.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    BrowseMemberBannerView *bannerView = (BrowseMemberBannerView *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[BrowseMemberBannerView alloc] init];
        bannerView.titleArr = @[@"2018-5-24",@"会员总数(人)",@"同比数据"];
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
        
        __weak typeof(bannerView) weakBannerView = bannerView;
        bannerView.calendarTapAct = ^{
            //日历被点击
            NSLog(@"日历被点击");
            SelectDateView *view = [[SelectDateView alloc] init];
            view.sureBtnAction = ^(NSString *dateStr) {
                weakBannerView.titleLbl.text = dateStr;
            };
            [self.view addSubview:view];
            
//            [[UIApplication sharedApplication].keyWindow addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        };
    }
    bannerView.mainImageView.image = [UIImage imageNamed:self.dataArray[index]];
    return bannerView;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MemberListViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MemberListViewCell class]) forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MemberDetailViewController *vc = [[MemberDetailViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)getDefaultDateTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components setDay:([components day]+1)];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd"];
    NSString *tomorrowDate = [dateday stringFromDate:beginningOfWeek];
    
    NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components1 = [gregorian1 components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components1 setDay:([components1 day]-1)];
    NSDate *beginningOfWeek1 = [gregorian dateFromComponents:components1];
    NSDateFormatter *dateday1 = [[NSDateFormatter alloc] init];
    [dateday1 setDateFormat:@"yyyy-MM-dd"];
    NSString *yesterdayDate = [dateday stringFromDate:beginningOfWeek1];
    
    _defaultDateAry = [NSArray arrayWithObjects:yesterdayDate,dateTime,tomorrowDate, nil];
}


- (void)loadBaseData{
    
    NSString *url = @"http://pd.lion-mall.com/index.php?r=pd-store-xin/getpaylist&store_id=15214574562913460000&start_time=2018-03-01&end_time=2018-05-23";
    [NetHelper GET:url parameters:nil progressBlock:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
