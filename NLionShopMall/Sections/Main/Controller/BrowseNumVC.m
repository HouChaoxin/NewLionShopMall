//
//  BrowseNumVC.m
//  
//
//  Created by hcx_ios on 2018/5/18.
//

#import "BrowseNumVC.h"
#import "JLChart.h"
#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"
#import "BrowseMemberBannerView.h"
#import "SelectDateView.h"
#import "BrowseNumVC.h"

@interface BrowseNumVC ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>

@property (nonatomic,strong) UIView *chartView;
@property (nonatomic,strong) JLSingleLineChart *singleChart;

@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (nonatomic,strong) NSArray *defaultDateAry;

@property (nonatomic,strong) BrowseNumVC *vcModel;

@end

@implementation BrowseNumVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    
    _vcModel = [[BrowseNumVC alloc] init];
    
    _dataArray = [[NSMutableArray alloc] init];
    [_dataArray addObject:@"noTodayIncome"];
    [_dataArray addObject:@"todayIncome"];
    
    [self getDefaultDateTime];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"浏览次数"];
    [self hideBackButtonTitle];
    UIImage *image = [kWhiteColor toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    [self setupFlowUI];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"浏览次数曲线图"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    [self.view addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(180);
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.height.equalTo(@25);
    }];
    
    _chartView = [self createChartViewWithTitle:@"浏览次数" color:kRedColor];
    [self.view addSubview:_chartView];
    [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@260);
    }];
    [self creatJLSingleLineChartWithSuperView:_chartView
                                         xAry:_defaultDateAry
                                         yAry:[NSArray arrayWithObjects:@"0",@"0",@"0",nil]];
}

- (UIView *)createChartViewWithTitle:(NSString *)title color:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    
    UILabel *customerLbl = [FSViewHelper createLblWithText:title
                                             textAlignment:NSTextAlignmentLeft
                                                  fontSize:14];
    [view addSubview:customerLbl];
    [customerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.equalTo(view).offset(-10);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(80, 20)]);
    }];
    
    UIImageView *yelloImg = [FSViewHelper createImgViewWithImg:nil
                                                       bgColor:color];
    [view addSubview:yelloImg];
    [yelloImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view).offset(-19);
        make.right.equalTo(customerLbl.mas_left).offset(-4);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(35, 1)]);
    }];
    
    return view;
}

- (void)creatJLSingleLineChartWithSuperView:(UIView *)superView
                                       xAry:(NSArray *)xAry
                                       yAry:(NSArray *)yAry{
    if (!_singleChart) {
        _singleChart = [[JLSingleLineChart alloc]initWithFrame:CGRectMake(0, 0, screenW, 220)];
        [superView addSubview:_singleChart];
    }
    
    JLLineChartData *data = [[JLLineChartData alloc] init];
    JLChartPointSet *set = [[JLChartPointSet alloc] init];
    
    for (int i = 0; i < xAry.count; i++) {
        JLChartPointItem *point = [JLChartPointItem pointItemWithRawX:[xAry objectAtIndex:i] andRowY:[yAry objectAtIndex:i]];
        [set.items addObject:point];
    }
    [data.sets addObject:set];
    data.lineColor = colorOf(165, 129, 44);
    data.lineWidth = 2.0f;
    data.fillColor = [UIColor clearColor];
    _singleChart.chartData = data;
    [_singleChart strokeChart];
}

- (void)setupFlowUI {
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, screenW, 160)];
    pageFlowView.backgroundColor = kWhiteColor;
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.4;
    pageFlowView.leftRightMargin = 30;
    pageFlowView.topBottomMargin = 0;
    pageFlowView.orginPageCount = self.dataArray.count;
    pageFlowView.isOpenAutoScroll = NO;
    [self.view addSubview:pageFlowView];
    [pageFlowView reloadData];
    self.pageFlowView = pageFlowView;
}

#pragma mark --NewPagedFlowView Delegate
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    
    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(screenW-50, 164);
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return self.dataArray.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    BrowseMemberBannerView *bannerView = (BrowseMemberBannerView *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[BrowseMemberBannerView alloc] init];
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
        
        __weak typeof(bannerView) weakBannerView = bannerView;
        bannerView.calendarTapAct = ^{
            //日历被点击
            NSLog(@"日历被点击");
            SelectDateView *view = [[SelectDateView alloc] init];
            view.sureBtnAction = ^(NSString *dateStr) {
                weakBannerView.titleLbl.text = dateStr;
            };
            [self.view addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        };
    }
    bannerView.mainImageView.image = [UIImage imageNamed:self.dataArray[index]];
    return bannerView;
}

- (void)getDefaultDateTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components setDay:([components day]+1)];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd"];
    NSString *tomorrowDate = [dateday stringFromDate:beginningOfWeek];
    
    NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components1 = [gregorian1 components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components1 setDay:([components1 day]-1)];
    NSDate *beginningOfWeek1 = [gregorian dateFromComponents:components1];
    NSDateFormatter *dateday1 = [[NSDateFormatter alloc] init];
    [dateday1 setDateFormat:@"yyyy-MM-dd"];
    NSString *yesterdayDate = [dateday stringFromDate:beginningOfWeek1];
    
    _defaultDateAry = [NSArray arrayWithObjects:yesterdayDate,dateTime,tomorrowDate, nil];
}

@end
