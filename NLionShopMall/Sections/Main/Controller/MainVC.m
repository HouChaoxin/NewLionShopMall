//
//  MainVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/12.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainVC.h"
#import "MainCarouselCell.h"
#import "BusinessDataCell.h"
#import "MarketDataCell.h"
#import "GuLiJinVC.h"
#import "BusinessDetailVC.h"
#import "MemberListViewController.h"
#import "OrderManageListViewController.h"
#import "BrowseMemberVC.h"
#import "BrowseNumVC.h"
#import "MainVCModel.h"

static NSString *mainCarouselCellIdentify = @"mainCarouselCellIdentify";
static NSString *businessCellIdentify = @"businessCellIdentify";
static NSString *marketCellIdentify = @"marketCellIdentify";

@interface MainVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView_;
@property (nonatomic,strong) MainVCModel *vcModel;

@end

@implementation MainVC

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _vcModel = [[MainVCModel alloc] init];
}

- (void)requestData {
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    __weak typeof(self) weakSelf = self;
    NSMutableDictionary *dictPara = [[NSMutableDictionary alloc] init];
    [dictPara setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kMainStoreId] forKey:@"store_id"];
    [dictPara setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kMemberId] forKey:@"uid"];
    [_vcModel getMainInfoWithPara:dictPara success:^(id responseObject) {
        
        [[FSProgressViewTool shareInstance] removeProgressView];
        [weakSelf.tableView_.mj_header endRefreshing];
        [weakSelf.tableView_ reloadData];
        
    } failure:^(NSError *error) {
        
        [[FSProgressViewTool shareInstance] removeProgressView];
        [weakSelf.tableView_.mj_header endRefreshing];
    }];
}

- (void)initView {
    NSString *restaurantName = [[NSUserDefaults standardUserDefaults] objectForKey:kStoreName];
    [self setNavigationBarTitleViewWithTitle:restaurantName];
    
    self.tableView_ = [self creatTableView];
    [self.view addSubview:self.tableView_];
    __weak typeof(self) weakSelf = self;
    [self.tableView_ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self addRefreshFunction];
}


- (UITableView *)creatTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = kWhiteColor;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.rowHeight = UITableViewAutomaticDimension;
    tableView.estimatedRowHeight = 100;
    return tableView;
}

#pragma mark - 上拉下拉刷新数据
- (void)addRefreshFunction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    self.tableView_.mj_header = header;
    
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松手刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新数据" forState:MJRefreshStateRefreshing];
}

- (void)downRefreshData {
    [self requestData];
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:{
            return 150;
            break;
        }case 1:{
            return 123;
            break;
        }default:{
            return 240;
            break;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:{
            MainCarouselCell *cell = [tableView dequeueReusableCellWithIdentifier:mainCarouselCellIdentify];
            if (!cell) {
                cell = [[MainCarouselCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:mainCarouselCellIdentify];
            }
            return cell;
            break;
        }
        case 1:{
            BusinessDataCell *cell = [tableView dequeueReusableCellWithIdentifier:businessCellIdentify];
            if (!cell) {
                cell = [[BusinessDataCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:businessCellIdentify];
                cell.todayIncomeAction = ^{
                    BusinessDetailVC *vc = [[BusinessDetailVC alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
                cell.tiXianGuLiJinAction = ^{
                    GuLiJinVC *vc = [[GuLiJinVC alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
            }
            [cell refreshViewWithModel:_vcModel.mainDataModel];
            return cell;
            break;
        }
        default:{
            MarketDataCell *cell = [tableView dequeueReusableCellWithIdentifier:marketCellIdentify];
            if (!cell) {
                cell = [[MarketDataCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:marketCellIdentify];
                cell.todayMemberAction = ^{
                    MemberListViewController *vc = [[MemberListViewController alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
                cell.todayOrderAction = ^{
                    OrderManageListViewController *vc = [[OrderManageListViewController alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
                cell.browserMemberAction = ^{
                    BrowseMemberVC *vc = [[BrowseMemberVC alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
                cell.browserNumAction = ^{
                    BrowseNumVC *vc = [[BrowseNumVC alloc] init];
                    [self.navigationController pushViewController:vc animated:NO];
                };
            }
            [cell refreshViewWithModel:_vcModel.mainDataModel];
            return cell;
            break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
