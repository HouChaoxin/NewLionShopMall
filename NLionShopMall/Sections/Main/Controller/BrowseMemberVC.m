//
//  BrowseMemberVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BrowseMemberVC.h"
#import "JLChart.h"
#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"
#import "BrowseMemberBannerView.h"
#import "SelectTwoDateView.h"
#import "BrowseMemberVCModel.h"

@interface BrowseMemberVC ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>

@property (nonatomic,strong) BrowseMemberVCModel *vcModel;
@property (nonatomic,strong) UIView *chartView;
@property (nonatomic,strong) JLSingleLineChart *singleChart;

@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (nonatomic,strong) NSArray *defaultDateAry;
@property (nonatomic,strong) NSString *startDateStr;
@property (nonatomic,strong) NSString *endDateStr;

@property (nonatomic,assign) NSInteger currentIndex;

@end

@implementation BrowseMemberVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _vcModel = [[BrowseMemberVCModel alloc] init];
    
    [self getDefaultDateTime];
}

- (void)requestData {
//    __weak typeof(self) weakSelf = self;
//    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
//
//    BOOL isToday = NO;
//    NSString *periodStr = [NSString stringWithFormat:@"%@~%@",_startDateStr,_endDateStr];
//    if ([_startDateStr isEqualToString:_endDateStr]) {
//        periodStr = _startDateStr;
//        if ([_startDateStr isEqualToString:[self getCurrentDate]]) {
//            isToday = YES;
//        }
//    }
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
//    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kMainStoreId] forKey:@"store_id"];
//    [dict setObject:_startDateStr forKey:@"start_time"];
//    [dict setObject:_endDateStr forKey:@"end_time"];
//
//    [_vcModel getBrowseMemberDataWithPara:dict success:^(id responseObject) {
//        //todo...
//    } failure:^(NSError *error) {
//
//    }];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"浏览用户"];
    [self hideBackButtonTitle];
    UIImage *image = [kWhiteColor toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    
    [self setupFlowUI];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"浏览用户曲线图"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    [self.view addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(180);
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.height.equalTo(@25);
    }];
    
    _chartView = [self createChartViewWithTitle:@"浏览用户数" color:kRedColor];
    [self.view addSubview:_chartView];
    [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@260);
    }];
    [self creatJLSingleLineChartWithSuperView:_chartView
                                         xAry:_defaultDateAry
                                         yAry:[NSArray arrayWithObjects:@"0",@"0",@"0",nil]];
}

- (UIView *)createChartViewWithTitle:(NSString *)title color:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    
    UILabel *customerLbl = [FSViewHelper createLblWithText:title
                                             textAlignment:NSTextAlignmentLeft
                                                  fontSize:14];
    [view addSubview:customerLbl];
    [customerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.equalTo(view).offset(-10);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(80, 20)]);
    }];
    
    UIImageView *yelloImg = [FSViewHelper createImgViewWithImg:nil
                                                       bgColor:color];
    [view addSubview:yelloImg];
    [yelloImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(view).offset(-19);
        make.right.equalTo(customerLbl.mas_left).offset(-4);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(35, 1)]);
    }];
    
    return view;
}

- (void)creatJLSingleLineChartWithSuperView:(UIView *)superView
                                       xAry:(NSArray *)xAry
                                       yAry:(NSArray *)yAry{
    if (!_singleChart) {
        _singleChart = [[JLSingleLineChart alloc]initWithFrame:CGRectMake(0, 0, screenW, 220)];
        [superView addSubview:_singleChart];
    }
    
    JLLineChartData *data = [[JLLineChartData alloc] init];
    JLChartPointSet *set = [[JLChartPointSet alloc] init];
    
    for (int i = 0; i < xAry.count; i++) {
        JLChartPointItem *point = [JLChartPointItem pointItemWithRawX:[xAry objectAtIndex:i] andRowY:[yAry objectAtIndex:i]];
        [set.items addObject:point];
    }
    [data.sets addObject:set];
    data.lineColor = colorOf(165, 129, 44);
    data.lineWidth = 2.0f;
    data.fillColor = [UIColor clearColor];
    _singleChart.chartData = data;
    [_singleChart strokeChart];
}

- (void)setupFlowUI {
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, screenW, 160)];
    pageFlowView.backgroundColor = kWhiteColor;
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.4;
    pageFlowView.leftRightMargin = 30;
    pageFlowView.topBottomMargin = 0;
    pageFlowView.orginPageCount = 3;
    pageFlowView.isOpenAutoScroll = NO;
    [self.view addSubview:pageFlowView];
    [pageFlowView reloadData];
    self.pageFlowView = pageFlowView;
}

#pragma mark --NewPagedFlowView Delegate
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
    if (((pageNumber > _currentIndex) && (pageNumber - _currentIndex == 1)) ||
        ((pageNumber < _currentIndex) && (_currentIndex - pageNumber == 2))){
        NSLog(@"***右滑***");
        NSInteger interval = [self numberOfDaysWithTwoDay];
        self.startDateStr = [self dateWithFromDate:self.startDateStr days:interval+1];
        self.endDateStr = [self dateWithFromDate:self.endDateStr days:interval+1];
        NSLog(@"dateStr = %@",self.startDateStr);
        [self requestData];
    }else if (((pageNumber < _currentIndex) && (_currentIndex - pageNumber == 1)) ||
              ((pageNumber > _currentIndex) && (pageNumber - _currentIndex == 2))) {
        NSLog(@"***左滑***");
        NSInteger interval = [self numberOfDaysWithTwoDay];
        self.startDateStr = [self dateWithFromDate:self.startDateStr days:-interval-1];
        self.endDateStr = [self dateWithFromDate:self.endDateStr days:-interval-1];
        NSLog(@"startDateStr = %@",self.startDateStr);
        NSLog(@"endDateStr = %@",self.endDateStr);
        [self requestData];
    }
    _currentIndex = pageNumber;
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(screenW-50, 164);
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return 3;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    BrowseMemberBannerView *bannerView = (BrowseMemberBannerView *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[BrowseMemberBannerView alloc] init];
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
        
        bannerView.calendarTapAct = ^{
            //日历被点击
            NSLog(@"日历被点击");
            SelectTwoDateView *view = [[SelectTwoDateView alloc] init];
            view.sureBtnAction = ^(NSString *startDateStr, NSString *endDateStr) {
                self.startDateStr = startDateStr;
                self.endDateStr = endDateStr;
                [self requestData];
            };
            [self.view addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        };
    }
    bannerView.mainImageView.image = [UIImage imageNamed:@"noTodayIncome"];
    return bannerView;
}

- (void)getDefaultDateTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components setDay:([components day]+1)];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd"];
    NSString *tomorrowDate = [dateday stringFromDate:beginningOfWeek];
    
    NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components1 = [gregorian1 components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components1 setDay:([components1 day]-1)];
    NSDate *beginningOfWeek1 = [gregorian dateFromComponents:components1];
    NSDateFormatter *dateday1 = [[NSDateFormatter alloc] init];
    [dateday1 setDateFormat:@"yyyy-MM-dd"];
    NSString *yesterdayDate = [dateday stringFromDate:beginningOfWeek1];
    
    _defaultDateAry = [NSArray arrayWithObjects:yesterdayDate,dateTime,tomorrowDate, nil];
}

- (NSString *)getCurrentDate {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:[NSDate date]];
    return dateStr;
}

- (NSInteger)numberOfDaysWithTwoDay{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *fromDate = [dateFormatter dateFromString:_startDateStr];
    NSDate *endDate = [dateFormatter dateFromString:_endDateStr];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comp = [calendar components:NSCalendarUnitDay
                                         fromDate:fromDate
                                           toDate:endDate
                                          options:NSCalendarWrapComponents];
    return comp.day;
}

- (NSString *)dateWithFromDate:(NSString *)dateStr days:(NSInteger)days{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *latterDate = [dateFormatter dateFromString:dateStr];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:latterDate];
    
    [comps setYear:0];
    [comps setMonth:0];
    [comps setDay:days];
    
    NSDate *getDate = [calendar dateByAddingComponents:comps toDate:latterDate options:0];
    return [dateFormatter stringFromDate:getDate];
}

@end
