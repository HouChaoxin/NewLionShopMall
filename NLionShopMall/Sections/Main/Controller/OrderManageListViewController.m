//
//  OrderManageListViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "OrderManageListViewController.h"
#import "OrderTableViewCell.h"

#import "NewPagedFlowView.h"
#import "PGCustomBannerView.h"
#import "BrowseMemberBannerView.h"
#import "SelectDateView.h"
#import "OrderModel.h"

@interface OrderManageListViewController ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>

@property (nonatomic, strong) NewPagedFlowView *pageFlowView;
@property (nonatomic, strong) NSMutableArray *dataArray;
@property (nonatomic,strong) NSArray *defaultDateAry;

@property (nonatomic ,strong) NSArray *dataSource;

@end

@implementation OrderManageListViewController

- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"订单管理";
    [self creatTableView];
    [self loadBaseData];
    [self.dataArray addObject:@"noTodayIncome"];
    [self.dataArray addObject:@"todayIncome"];
    [self setupFlowUI];
    
}

- (void)creatTableView{
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 50;
    [self.tableView registerClass:[OrderTableViewCell class] forCellReuseIdentifier:NSStringFromClass([OrderTableViewCell class])];
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0 , 15);
}

- (void)setupFlowUI {
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, screenW, 160)];
    pageFlowView.backgroundColor = kWhiteColor;
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.4;
    pageFlowView.leftRightMargin = 30;
    pageFlowView.topBottomMargin = 0;
    pageFlowView.orginPageCount = self.dataArray.count;
    pageFlowView.isOpenAutoScroll = NO;
    //    [self.view addSubview:pageFlowView];
    self.tableView.tableHeaderView = pageFlowView;
    [pageFlowView reloadData];
    self.pageFlowView = pageFlowView;
}

#pragma mark --NewPagedFlowView Delegate
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    
    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(screenW-50, 164);
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return self.dataArray.count;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    BrowseMemberBannerView *bannerView = (BrowseMemberBannerView *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[BrowseMemberBannerView alloc] init];
        bannerView.titleArr = @[@"2018-5-24",@"订单总数(单)",@"同比数据"];
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
        
        __weak typeof(bannerView) weakBannerView = bannerView;
        bannerView.calendarTapAct = ^{
            //日历被点击
            NSLog(@"日历被点击");
            SelectDateView *view = [[SelectDateView alloc] init];
            view.sureBtnAction = ^(NSString *dateStr) {
                weakBannerView.titleLbl.text = dateStr;
            };
            [self.view addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
            }];
        };
    }
    bannerView.mainImageView.image = [UIImage imageNamed:self.dataArray[index]];
    return bannerView;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([OrderTableViewCell class]) forIndexPath:indexPath];
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)getDefaultDateTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateTime = [formatter stringFromDate:[NSDate date]];
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorian components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components setDay:([components day]+1)];
    NSDate *beginningOfWeek = [gregorian dateFromComponents:components];
    NSDateFormatter *dateday = [[NSDateFormatter alloc] init];
    [dateday setDateFormat:@"yyyy-MM-dd"];
    NSString *tomorrowDate = [dateday stringFromDate:beginningOfWeek];
    
    NSCalendar *gregorian1 = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components1 = [gregorian1 components:NSCalendarUnitWeekday | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    [components1 setDay:([components1 day]-1)];
    NSDate *beginningOfWeek1 = [gregorian dateFromComponents:components1];
    NSDateFormatter *dateday1 = [[NSDateFormatter alloc] init];
    [dateday1 setDateFormat:@"yyyy-MM-dd"];
    NSString *yesterdayDate = [dateday stringFromDate:beginningOfWeek1];
    
    _defaultDateAry = [NSArray arrayWithObjects:yesterdayDate,dateTime,tomorrowDate, nil];
}

- (void)loadBaseData{
    
    NSString *url = @"http://pd.lion-mall.com/index.php?r=pd-store-xin/getpaylist&store_id=15214574562913460000&start_time=2018-03-01&end_time=2018-05-23";
    [NetHelper GET:url parameters:nil progressBlock:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        if (rootDict && rootDict.count > 0) {
            BOOL success = [rootDict[@"status"] boolValue];
            if (success) {
                NSArray *tempArr = rootDict[@"data"][@"list"];
                self.dataSource = [OrderModel mj_objectArrayWithKeyValuesArray:tempArr];
                [self.tableView reloadData];
            }
            else {
                [self.view makeToast:rootDict[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}
@end
