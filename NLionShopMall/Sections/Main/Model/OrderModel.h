//
//  OrderModel.h
//  NLionShopMall
//
//  Created by apple on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderModel : NSObject

@property (nonatomic ,strong) NSString *order_amount;
@property (nonatomic ,strong) NSString *order_points;
@property (nonatomic ,strong) NSString *typess;
@property (nonatomic ,strong) NSString *payment_time;
@property (nonatomic ,strong) NSString *member_name;
@property (nonatomic ,strong) NSString *pay_type;
@end

//[0]    (null)    @"order_amount" : (double)0.01
//[1]    (null)    @"order_points" : @"+0.01"
//[2]    (null)    @"typess" : @"线下买单"
//[3]    (null)    @"payment_time" : @"2018-03-26 09:29:21"
//[4]    (null)    @"member_name" : @"王子"
//paytype  1是支付宝   2是微信
//[0]    (null)    @"pay_type" : @"1"
