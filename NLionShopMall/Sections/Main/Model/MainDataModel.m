//
//  MainDataModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainDataModel.h"

@implementation TodayDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.pay_num = [self isNil:[dict objectForKey:@"pay_num"]];
        self.store_id = [self isNil:[dict objectForKey:@"store_id"]];
        self.totalmoney = [self isNil:[dict objectForKey:@"totalmoney"]];
    }
    return self;
}

@end

@implementation MonthDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.store_id = [self isNil:[dict objectForKey:@"store_id"]];
        self.totalmoney = [self isNil:[dict objectForKey:@"totalmoney"]];
    }
    return self;
}

@end

@implementation MembersDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.member_add = [self isNil:[dict objectForKey:@"member_add"]];
    }
    return self;
}

@end

@implementation LiuLanDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.member = [self isNil:[dict objectForKey:@"member"]];
        self.num = [self isNil:[dict objectForKey:@"num"]];
    }
    return self;
}

@end

@implementation GuLiJinDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.all_amount = [self isNil:[dict objectForKey:@"all_amount"]];
        self.cash_amount = [self isNil:[dict objectForKey:@"cash_amount"]];
        self.freeze_meney = [self isNil:[dict objectForKey:@"freeze_meney"]];
        self.reflect_amount = [self isNil:[dict objectForKey:@"reflect_amount"]];
    }
    return self;
}

@end

@implementation MainDataModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.gulijin = [[GuLiJinDataModel alloc] initWithDict:[dict objectForKey:@"gulijin"]];
        self.liulan = [[LiuLanDataModel alloc] initWithDict:[dict objectForKey:@"liulan"]];
        self.members = [[MembersDataModel alloc] initWithDict:[dict objectForKey:@"members"]];
        self.month = [[MonthDataModel alloc] initWithDict:[dict objectForKey:@"month"]];
        self.today = [[TodayDataModel alloc] initWithDict:[dict objectForKey:@"today"]];
    }
    return self;
}

@end
