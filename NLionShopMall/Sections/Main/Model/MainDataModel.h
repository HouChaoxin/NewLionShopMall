//
//  MainDataModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayDataModel : BaseObject

@property (nonatomic,copy) NSString *pay_num;
@property (nonatomic,copy) NSString *store_id;
@property (nonatomic,copy) NSString *totalmoney;
-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface MonthDataModel : BaseObject
@property (nonatomic,copy) NSString *store_id;
@property (nonatomic,copy) NSString *totalmoney;
-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface MembersDataModel : BaseObject

@property (nonatomic,copy) NSString *member_add;
-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface LiuLanDataModel : BaseObject

@property (nonatomic,copy) NSString *member;
@property (nonatomic,copy) NSString *num;
-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface GuLiJinDataModel : BaseObject

@property (nonatomic,copy) NSString *all_amount;
@property (nonatomic,copy) NSString *cash_amount;
@property (nonatomic,copy) NSString *freeze_meney;
@property (nonatomic,copy) NSString *reflect_amount;
-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface MainDataModel : BaseObject

@property (nonatomic,strong) GuLiJinDataModel *gulijin;
@property (nonatomic,strong) LiuLanDataModel *liulan;
@property (nonatomic,strong) MembersDataModel *members;
@property (nonatomic,strong) MonthDataModel *month;
@property (nonatomic,strong) TodayDataModel *today;

-(instancetype)initWithDict:(NSDictionary *)dict;

@end
