//
//  SearchView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "SearchView.h"

@interface SearchView ()<UITextFieldDelegate>

@end

@implementation SearchView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor getColor:@"#f3f3f3"];
       
        UIView *searchView = [[UIView alloc] init];
        searchView.backgroundColor = [UIColor whiteColor];
        [self addSubview:searchView];
        
        UIImageView *searchImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchicon"]];
        [self addSubview:searchImg];
        
        self.searchTF = [[UITextField alloc] init];
        self.searchTF.placeholder  = @"请输入关键字";
//        self.searchTF.delegate = self;
        self.searchTF.returnKeyType = UIReturnKeySearch;
        //    [self.searchTF becomeFirstResponder];
        [searchView addSubview:self.searchTF];
        
        self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.searchBtn setTitle:@"搜索" forState:0];
//        [self.searchBtn setTitleColor:mainColor forState:0];
//        [self.searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        self.searchBtn.backgroundColor = mainColor;
        [self addSubview:self.searchBtn];
        
        
        [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-10);
            make.centerY.equalTo(self);
            make.height.equalTo(@30);
            make.width.equalTo(@(60));
        }];
        self.searchBtn.layer.cornerRadius = 5.0f;
        self.searchBtn.layer.masksToBounds = YES;
        
        [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self).offset(20);
//            make.bottom.equalTo(self).offset(-8);
            make.centerY.equalTo(self);
            make.left.equalTo(self).offset(10);
            make.right.equalTo(self.searchBtn.mas_left).offset(-10);
            make.height.equalTo(@30);
        }];
        searchView.layer.cornerRadius = 5.0;
        searchView.layer.masksToBounds = YES;
        
        [searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(searchView);
            make.left.equalTo(searchView).offset(5);
            make.width.equalTo(searchImg.mas_height);
        }];
        
        [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(searchView);
            make.bottom.equalTo(searchView);
            make.left.equalTo(searchImg.mas_right);
            make.right.equalTo(searchView);
        }];
    }
    return self;
}



@end
