//
//  LSMDetailHeaderView.m
//  LionShopMall
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "LSMDetailHeaderView.h"

@implementation LSMDetailHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.icon = [[UIImageView alloc] init];
        self.icon.image = [UIImage imageNamed:@"Image_demo"];
        [self addSubview:self.icon];
        
        self.phoneTitleLab = [[UILabel alloc] init];
        self.phoneTitleLab.text = @"手机号:";
        self.phoneTitleLab.font = [UIFont systemFontOfSize:14];
        self.phoneTitleLab.textColor = [UIColor getColor:@"333333"];
        [self addSubview:self.phoneTitleLab];
        
        self.phoneLab = [[UILabel alloc] init];
        self.phoneLab.text = @"13871577361";
        self.phoneLab.font = [UIFont systemFontOfSize:14];
        self.phoneLab.textColor = [UIColor getColor:@"333333"];
        [self addSubview:self.phoneLab];
        
        self.nameLab = [[UILabel alloc] init];
        self.nameLab.text = @"想哭";
        self.nameLab.font = [UIFont boldSystemFontOfSize:16];
        self.nameLab.textColor = [UIColor getColor:@"333333"];
        [self addSubview:self.nameLab];
        
        UIView *link = [[UILabel alloc] init];
        link.backgroundColor = [UIColor getColor:@"f0f0f0"];
        [self addSubview:link];
        
        self.jifenLab = [[UILabel alloc] init];
        self.jifenLab.text = @"1500.00";
        self.jifenLab.font = [UIFont systemFontOfSize:17];
        self.jifenLab.textColor = [UIColor getColor:@"333333"];
        self.jifenLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.jifenLab];
        
        self.jifenTitleLab = [[UILabel alloc] init];
        self.jifenTitleLab.text = @"消费金额（元)";
        self.jifenTitleLab.font = [UIFont systemFontOfSize:13];
        self.jifenTitleLab.textColor = [UIColor getColor:@"333333"];
        self.jifenTitleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.jifenTitleLab];
        
        UIView *firstLink = [[UIView alloc] init];
        firstLink.backgroundColor = [UIColor getColor:@"f0f0f0"];
        [self addSubview:firstLink];
        
        self.jineLab = [[UILabel alloc] init];
        self.jineLab.text = @"888";
        self.jineLab.font = [UIFont systemFontOfSize:17];
        self.jineLab.textColor = [UIColor getColor:@"333333"];
        self.jineLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.jineLab];
        
        self.jineTitleLab = [[UILabel alloc] init];
        self.jineTitleLab.text = @"积分";
        self.jineTitleLab.font = [UIFont systemFontOfSize:13];
        self.jineTitleLab.textColor = [UIColor getColor:@"333333"];
        self.jineTitleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.jineTitleLab];
        
        UIView *secondLink = [[UIView alloc] init];
        secondLink.backgroundColor = [UIColor getColor:@"f0f0f0"];
        [self addSubview:secondLink];
        
        self.youhuiquanLab = [[UILabel alloc] init];
        self.youhuiquanLab.text = @"111";
        self.youhuiquanLab.font = [UIFont systemFontOfSize:17];
        self.youhuiquanLab.textColor = [UIColor getColor:@"333333"];
        self.youhuiquanLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.youhuiquanLab];
        
        self.youhuiquanTitleLab = [[UILabel alloc] init];
        self.youhuiquanTitleLab.text = @"消费次数（次）";
        self.youhuiquanTitleLab.font = [UIFont systemFontOfSize:13];
        self.youhuiquanTitleLab.textColor = [UIColor getColor:@"333333"];
        self.youhuiquanTitleLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.youhuiquanTitleLab];
        
        
        UIView *bottomView = [[UIView alloc] init];
        bottomView.backgroundColor = [UIColor getColor:@"e6e6e6"];
        [self addSubview:bottomView];
        
        [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self);
            make.left.equalTo(self);
            make.right.equalTo(self);
            make.height.equalTo(@8);
        }];
        
        
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.top.equalTo(self).offset(10);
            make.width.equalTo(@56);
            make.height.equalTo(@56);
        }];
        self.icon.layer.cornerRadius = 28;
        self.icon.layer.masksToBounds = YES;
        
        [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.icon.mas_right).offset(10);
            make.top.equalTo(self.icon).offset(8);
        }];
        
        [self.phoneTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.icon.mas_right).offset(10);
            make.top.equalTo(self.nameLab.mas_bottom).offset(5);
        }];
        
        [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.phoneTitleLab);
            make.left.equalTo(self.phoneTitleLab.mas_right);
        }];
        
        [link mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.right.equalTo(self).offset(-15);
            make.top.equalTo(self.icon.mas_bottom).offset(20);
            make.height.equalTo(@1);
        }];
        
        [self.jifenLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(link.mas_bottom).offset(15);
            make.left.equalTo(self);
            make.width.equalTo(@(screenW/3-1));
        }];
        
        [self.jifenTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.jifenLab.mas_bottom).offset(10);
            make.left.equalTo(self);
            make.width.equalTo(@(screenW/3-1));
        }];
        
        [firstLink mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.jifenLab.mas_right);
            make.top.equalTo(self.jifenLab);
            make.bottom.equalTo(self.jifenTitleLab);
            make.width.equalTo(@1);
        }];
        
        [self.jineLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(link.mas_bottom).offset(15);
            make.left.equalTo(firstLink.mas_right);
            make.width.equalTo(@(screenW/3-1));
        }];
        
        [self.jineTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.jifenLab.mas_bottom).offset(10);
            make.left.equalTo(self.jifenLab.mas_right);
            make.width.equalTo(@(screenW/3-1));
        }];
        
        [secondLink mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.jineLab.mas_right);
            make.top.equalTo(self.jineLab);
            make.bottom.equalTo(self.jineTitleLab);
            make.width.equalTo(@1);
        }];
        
        [self.youhuiquanLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(link.mas_bottom).offset(15);
            make.left.equalTo(secondLink.mas_right);
            make.width.equalTo(@(screenW/3-1));
        }];
        
        [self.youhuiquanTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.youhuiquanLab.mas_bottom).offset(10);
            make.left.equalTo(self.youhuiquanLab);
            make.width.equalTo(@(screenW/3-1));
        }];
    }
    return self;
}


//-(void)setModel:(LSMMemberModel *)model{
//    if (model) {
//        NSString *url = [picBaseUrl stringByAppendingString:model.member_avatar];
//        [self.icon sd_setImageWithURL:[NSURL URLWithString:url]];
//        self.nameLab.text = model.member_name;
//        self.jineLab.text = model.total_money;
//        self.youhuiquanLab.text = model.paynum;
//    }
//}




@end
