//
//  HistoryTableViewCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryTableViewCell : UITableViewCell

@property (nonatomic ,strong) UILabel *timeLabel;
@property (nonatomic ,strong) UILabel *jineLabel;
@property (nonatomic ,strong) UILabel *jifenLabel;

@end
