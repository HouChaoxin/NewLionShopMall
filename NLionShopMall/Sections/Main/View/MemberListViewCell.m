//
//  MemberListViewCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MemberListViewCell.h"

@implementation MemberListViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.icon = [[UIImageView alloc] init];
        self.icon.image = [UIImage imageNamed:@"Image_demo"];
        [self.contentView addSubview:self.icon];
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.text = @"德玛西亚";
        self.nameLabel.font = [UIFont boldSystemFontOfSize:15];
        self.nameLabel.textColor = [UIColor getColor:@"333333"];
        [self.contentView addSubview:self.nameLabel];
        
        self.loginTypeImg = [[UIImageView alloc]init];
        self.loginTypeImg.image = [UIImage imageNamed:@"ic_weixin"];
        [self.contentView addSubview:self.loginTypeImg];
        
        self.sexImg = [[UIImageView alloc] init];
        self.sexImg.image = [UIImage imageNamed:@"ic_nvxing"];
        [self.contentView addSubview:self.sexImg];
        
        self.phoneTitleLab = [[UILabel alloc] init];
        self.phoneTitleLab.text = @"手机号:";
        self.phoneTitleLab.font = [UIFont systemFontOfSize:13];
        self.phoneTitleLab.textColor = [UIColor getColor:@"333333"];
        [self.contentView addSubview:self.phoneTitleLab];
        
        self.phoneLab = [[UILabel alloc] init];
        self.phoneLab.text = @"13871577361";
        self.phoneLab.font = [UIFont systemFontOfSize:13];
        self.phoneLab.textColor = [UIColor getColor:@"333333"];
        [self.contentView addSubview:self.phoneLab];
        
        self.countLab = [[UILabel alloc] init];
        self.countLab.text = @"2000积分";
        self.countLab.font = [UIFont systemFontOfSize:15];
        self.countLab.textColor = [UIColor getColor:@"333333"];
        [self.contentView addSubview:self.countLab];
        
        [self setSubView_layout];
    }
    return self;
}

- (void)setSubView_layout{
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.width.equalTo(@44);
        make.height.equalTo(@44);
        make.left.equalTo(self.contentView).offset(16);
    }];
    self.icon.layer.cornerRadius = 22;
    self.icon.layer.masksToBounds = YES;
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.icon.mas_right).offset(10);
        make.top.equalTo(self.icon);
    }];
    
    [self.loginTypeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.nameLabel.mas_right).offset(10);
    }];
    
    [self.sexImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.nameLabel);
        make.left.equalTo(self.loginTypeImg.mas_right).offset(10);
    }];
    
    [self.phoneTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.icon.mas_right).offset(10);
        make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
    }];
    
    [self.phoneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneTitleLab);
        make.left.equalTo(self.phoneTitleLab.mas_right);
    }];
    
    [self.countLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.centerY.equalTo(self.contentView);
    }];
}

@end
