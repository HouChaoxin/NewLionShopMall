//
//  BusinessDataCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainDataModel.h"

typedef void(^BusinessDataAction)(void);
@interface BDSubView : UIView

@end

@interface BusinessDataCell : BaseTableViewCell

@property (nonatomic,copy) BusinessDataAction todayIncomeAction;
@property (nonatomic,copy) BusinessDataAction tiXianGuLiJinAction;

- (void)refreshViewWithModel:(MainDataModel *)model;
@end
