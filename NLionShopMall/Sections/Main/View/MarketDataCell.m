//
//  MarketDataCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MarketDataCell.h"

@interface MDSubView()

@property (nonatomic,strong) UILabel *textLbl;

@end

@implementation MDSubView

- (void)createViewWithTite:(NSString *)title text:(NSString *)text {
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:14];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x666666"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@25);
    }];
    
    _textLbl = [FSViewHelper createLblWithText:text
                                 textAlignment:NSTextAlignmentCenter
                                      fontSize:20];
    _textLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    _textLbl.font = [UIFont fontWithName:kFontName size:20];
    [self addSubview:_textLbl];
    [_textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@35);
    }];
}

- (void)refreshViewWithTitle:(NSString *)title {
    _textLbl.text = title;
}

@end

@interface MarketDataCell()

@property (nonatomic,strong) MDSubView *todayMemberView;
@property (nonatomic,strong) MDSubView *todayOrderView;
@property (nonatomic,strong) MDSubView *browseMemberView;
@property (nonatomic,strong) MDSubView *browseNumView;

@end

@implementation MarketDataCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:[ColorConverter colorWithHexString:@"0x4593E8"]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(3, 16)]);
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(12);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"营销数据"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.left.equalTo(lineImgView.mas_right).offset(8);
        make.height.equalTo(@40);
    }];
    
    UIImageView *lineImgViewTwo = [FSViewHelper createImgViewWithImg:nil
                                                             bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:lineImgViewTwo];
    [lineImgViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(@1);
    }];
    
    _todayMemberView = [[MDSubView alloc] init];
    [self addSubview:_todayMemberView];
    [_todayMemberView createViewWithTite:@"今日会员(人)" text:@"1000000"];
    [_todayMemberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgViewTwo.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(todayMemberClick:)];
    [_todayMemberView addGestureRecognizer:tapGR];
    _todayOrderView = [[MDSubView alloc] init];
    [self addSubview:_todayOrderView];
    [_todayOrderView createViewWithTite:@"今日订单(单)" text:@"0"];
    [_todayOrderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgViewTwo.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    UITapGestureRecognizer *orderTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(todayOrderClick:)];
    [_todayOrderView addGestureRecognizer:orderTapGR];
    
    UIImageView *lineImgV = [FSViewHelper createImgViewWithImg:nil
                                                       bgColor:kTableViewColor];
    [self addSubview:lineImgV];
    [lineImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_todayOrderView.mas_bottom).offset(9);
        make.right.equalTo(self).offset(-16);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@1);
    }];
    
    _browseMemberView = [[MDSubView alloc] init];
    [self addSubview:_browseMemberView];
    [_browseMemberView createViewWithTite:@"浏览用户(人)" text:@"0"];
    [_browseMemberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgV.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    UITapGestureRecognizer *bmTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(browseMemberClick:)];
    [_browseMemberView addGestureRecognizer:bmTapGR];
    
    _browseNumView = [[MDSubView alloc] init];
    [self addSubview:_browseNumView];
    [_browseNumView createViewWithTite:@"浏览次数(次)" text:@"0"];
    [_browseNumView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgV.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    UITapGestureRecognizer *bnTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(browseNumClick:)];
    [_browseNumView addGestureRecognizer:bnTapGR];
}

- (void)todayMemberClick:(UITapGestureRecognizer *)tapGR {
    if (_todayMemberAction) {
        _todayMemberAction();
    }
}

- (void)todayOrderClick:(UITapGestureRecognizer *)tapGR {
    if (_todayOrderAction) {
        _todayOrderAction();
    }
}

- (void)browseMemberClick:(UITapGestureRecognizer *)tapGR {
    if (_browserMemberAction) {
        _browserMemberAction();
    }
}

- (void)browseNumClick:(UITapGestureRecognizer *)tapGR {
    if (_browserNumAction) {
        _browserNumAction();
    }
}

- (void)refreshViewWithModel:(MainDataModel *)model {
    if (!model) return;
//    @property (nonatomic,strong) MDSubView *todayMemberView;
//    @property (nonatomic,strong) MDSubView *todayOrderView;
//    @property (nonatomic,strong) MDSubView *browseMemberView;
//    @property (nonatomic,strong) MDSubView *browseNumView;
    [_todayMemberView refreshViewWithTitle:model.members.member_add];
    [_todayOrderView refreshViewWithTitle:model.today.pay_num];
    [_browseMemberView refreshViewWithTitle:model.liulan.member];
    [_browseMemberView refreshViewWithTitle:model.liulan.num];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
