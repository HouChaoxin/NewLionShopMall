//
//  OrderTableViewCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "OrderTableViewCell.h"

@implementation OrderTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:15];
        self.nameLabel.textColor = [UIColor getColor:@"333333"];
        self.nameLabel.text = @"我用双手成就你的梦想";
        [self.contentView addSubview:self.nameLabel];
        
        self.jineLabel = [[UILabel alloc] init];
        self.jineLabel.font = [UIFont boldSystemFontOfSize:15];
        self.jineLabel.textColor = [UIColor getColor:@"333333"];
        self.jineLabel.text = @"￥159.00";
        [self.contentView addSubview:self.jineLabel];
        
        self.payStyleImg = [[UIImageView alloc] init];
        self.payStyleImg.image = [UIImage imageNamed:@"ic_xianjin"];
        [self.contentView addSubview:self.payStyleImg];
        
        [self.jineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-15);
            make.centerY.equalTo(self.contentView);
        }];
        
        [self.payStyleImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.centerY.equalTo(self.contentView);
            make.width.equalTo(@15);
            make.height.equalTo(@15);
        }];
        self.payStyleImg.layer.cornerRadius = 2.0f;
        self.payStyleImg.layer.masksToBounds = YES;
        
        [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.payStyleImg.mas_right).offset(5);
            make.centerY.equalTo(self.contentView);
        }];
    }
    return self;
}

-(void)setModel:(OrderModel *)model{
    if (model) {
        self.nameLabel.text = model.member_name;
        if ([model.pay_type isEqualToString:@"1"]) {
            self.payStyleImg.image = [UIImage imageNamed:@"ic_zhifubao"];
        }else if ([model.pay_type isEqualToString:@"2"]){
            self.payStyleImg.image = [UIImage imageNamed:@"ic_weixin"];
        }else {
            self.payStyleImg.image = [UIImage imageNamed:@"ic_xianjin"];
        }
        self.jineLabel.text = [model.order_amount stringByAppendingString:@"元"];
    }
}

@end
