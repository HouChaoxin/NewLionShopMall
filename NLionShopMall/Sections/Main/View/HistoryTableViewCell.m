//
//  HistoryTableViewCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "HistoryTableViewCell.h"


@implementation HistoryTableViewCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.font = [UIFont systemFontOfSize:14.0f];
        self.timeLabel.text = @"2018-5-18 11:27:45";
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.timeLabel];
        
        self.jineLabel = [[UILabel alloc] init];
        self.jineLabel.font = [UIFont systemFontOfSize:14.0f];
        self.jineLabel.text = @"159.00";
        self.jineLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.jineLabel];
        
        self.jifenLabel = [[UILabel alloc] init];
        self.jifenLabel.font = [UIFont systemFontOfSize:14.0f];
        self.jifenLabel.text = @"120";
        self.jifenLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.jifenLabel];
        
        [self setSubView_layout];
    }
    return self;
}

- (void)setSubView_layout{
    
    CGFloat item_W = (screenW - 20)/4;
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.centerY.equalTo(self.contentView);
        make.width.equalTo(@(item_W*2));
    }];
    
    [self.jineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.timeLabel.mas_right);
        make.width.equalTo(@(item_W));
    }];
    
    [self.jifenLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.jineLabel.mas_right);
        make.width.equalTo(@(item_W));
    }];
}

@end
