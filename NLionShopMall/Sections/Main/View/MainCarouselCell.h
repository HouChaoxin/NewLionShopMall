//
//  HomePageCarouselView.h
//  LionMall
//
//  Created by Casanova.Z on 2018/3/1.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^CarouselViewClick)(NSString *viewId);

@interface MainCarouselCell : BaseTableViewCell

@property (nonatomic,copy) CarouselViewClick carouselViewClick;
- (void)refreshViewWithArray:(NSArray *)ary;

@end
