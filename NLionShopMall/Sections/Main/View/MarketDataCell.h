//
//  MarketDataCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainDataModel.h"

typedef void(^MarketDataAction)(void);
@interface MDSubView : UIView

@end

@interface MarketDataCell : UITableViewCell

@property (nonatomic,copy) MarketDataAction todayMemberAction;
@property (nonatomic,copy) MarketDataAction todayOrderAction;
@property (nonatomic,copy) MarketDataAction browserMemberAction;
@property (nonatomic,copy) MarketDataAction browserNumAction;

- (void)refreshViewWithModel:(MainDataModel *)model;
@end
