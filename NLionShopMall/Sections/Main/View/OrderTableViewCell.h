//
//  OrderTableViewCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"

@interface OrderTableViewCell : UITableViewCell

@property (nonatomic ,strong) UILabel *nameLabel;
@property (nonatomic ,strong) UILabel *jineLabel;
@property (nonatomic ,strong) UIImageView *payStyleImg;

@property (nonatomic ,strong) OrderModel *model;
@end
