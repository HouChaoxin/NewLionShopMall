//
//  BrowseMemberBannerView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BrowseMemberBannerView.h"

@interface BrowseMemberBannerView()
{
    UILabel *_recordLbl;
    UILabel *_incomeTitleLbl;
}
@property (nonatomic,strong) UILabel *todayBrowerLbl;
@property (nonatomic,strong) UILabel *totalBrowerLbl;
@property (nonatomic,strong) UILabel *increseLbl;

@end

@implementation BrowseMemberBannerView

- (instancetype)init{
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    UIImageView *calendarImgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"ic_riqi"]
                                                              bgColor:kClearColor];
    calendarImgView.userInteractionEnabled = YES;
    [self addSubview:calendarImgView];
    [calendarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(12);
        make.right.equalTo(self).offset(-12);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(16, 16)]);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calendarTapAction:)];
    [calendarImgView addGestureRecognizer:tapGR];
    
    _titleLbl = [FSViewHelper createLblWithText:@"今日新增(人)"
                                           textAlignment:NSTextAlignmentCenter
                                                fontSize:14];
    _titleLbl.textColor = kWhiteColor;
    [self addSubview:_titleLbl];
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(120, 20)]);
    }];
    
    _todayBrowerLbl = [FSViewHelper createLblWithText:@"0"
                                   textAlignment:NSTextAlignmentCenter
                                        fontSize:28];
    _todayBrowerLbl.textColor = kWhiteColor;
    _todayBrowerLbl.font = [UIFont boldSystemFontOfSize:28];
    [self addSubview:_todayBrowerLbl];
    [_todayBrowerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@35);
    }];
    
    _recordLbl = [FSViewHelper createLblWithText:@"浏览次数(次)"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:14];
    _recordLbl.textColor = kWhiteColor;
    [self addSubview:_recordLbl];
    [_recordLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_todayBrowerLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    _totalBrowerLbl = [FSViewHelper createLblWithText:@"0"
                                        textAlignment:NSTextAlignmentCenter
                                             fontSize:18];
    _totalBrowerLbl.textColor = kWhiteColor;
    [self addSubview:_totalBrowerLbl];
    [_totalBrowerLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_recordLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    _incomeTitleLbl = [FSViewHelper createLblWithText:@"同比收益"
                                           textAlignment:NSTextAlignmentCenter
                                                fontSize:14];
    _incomeTitleLbl.textColor = kWhiteColor;
    [self addSubview:_incomeTitleLbl];
    [_incomeTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_todayBrowerLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    _increseLbl = [FSViewHelper createLblWithText:@"0.00%"
                                        textAlignment:NSTextAlignmentCenter
                                             fontSize:18];
    _increseLbl.textColor = kWhiteColor;
    [self addSubview:_increseLbl];
    [_increseLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_incomeTitleLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
}

- (void)setTitleArr:(NSArray *)titleArr{
    if (titleArr) {
        _titleLbl.text = titleArr[0];
        _recordLbl.text = titleArr[1];
        _incomeTitleLbl.text = titleArr[2];
    }
    
}

- (void)calendarTapAction:(UITapGestureRecognizer *)tapGR {
    if (_calendarTapAct) {
        _calendarTapAct();
    }
}

@end
