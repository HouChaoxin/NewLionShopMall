//
//  BrowseMemberBannerView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGIndexBannerSubiew.h"

typedef void(^CalendarTapAction)(void);
@interface BrowseMemberBannerView : PGIndexBannerSubiew

@property (nonatomic,strong) UILabel *titleLbl;
@property (nonatomic,copy) CalendarTapAction calendarTapAct;

@property (nonatomic ,strong) NSArray *titleArr;

@end
