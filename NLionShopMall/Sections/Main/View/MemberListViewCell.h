//
//  MemberListViewCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/17.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MemberListViewCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *icon;
@property (nonatomic ,strong) UILabel *nameLabel;
@property (nonatomic ,strong) UIImageView *loginTypeImg;
@property (nonatomic ,strong) UIImageView *sexImg;
@property (nonatomic ,strong) UILabel *phoneTitleLab;
@property (nonatomic ,strong) UILabel *phoneLab;
@property (nonatomic ,strong) UILabel *countLab;

@end
