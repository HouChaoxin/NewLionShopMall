//
//  HomePageCarouselView.m
//  LionMall
//
//  Created by Casanova.Z on 2018/3/1.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "MainCarouselCell.h"
#import "FSBannerView.h"
//#import "HomeCarouselModel.h"

@interface MainCarouselCell()<FSBannerViewDelegate>
{
    FSBannerView *bannerView;
    NSArray *imgAry;
}

@end

@implementation MainCarouselCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initData];
        [self initView];
    }
    return self;
}

- (void)initData {
    imgAry = [[NSArray alloc] init];
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    bannerView = ({
        FSBannerView *bView = [[FSBannerView alloc] init];
        [self addSubview:bView];
        bView.delegate = self;
        bView.pageControllPosition = PageControllShowMiddle;
        bView.autoScroll = YES;
        bView.scrollTime = 0.5;//默认为0.5
        bView.switchTimeInterval = 3;//默认为4秒
        [bView mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(self).offset(5);
//            make.left.equalTo(self).offset(15);
//            make.right.equalTo(self).offset(-15);
//            make.bottom.equalTo(self).offset(-14);
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
        bView;
    });
    
//    UIImageView *lineImgViewc = [FSViewHelper createImgViewWithImg:nil
//                                                           bgColor:[UIColor groupTableViewBackgroundColor]];
//    [self addSubview:lineImgViewc];
//    [lineImgViewc mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(self);
//        make.height.equalTo(@6);
//    }];
}

- (void)refreshViewWithArray:(NSArray *)ary {
    if (!ary || ary.count == 0) return;
    imgAry = ary;
    if (ary) {
        [bannerView reloadData];
    }
}

#pragma mark FSBannerViewDelegate

- (NSUInteger) countOfBanner {
    if (imgAry.count == 0) {
        return 1;
    }
    return  imgAry.count;
}

- (void)imageView:(UIImageView *)imageView didSelectAtIndex:(NSInteger)index {
    if (self.carouselViewClick) {
//        HomeCarouselModel *model = [imgAry objectAtIndex:index];
//        self.carouselViewClick(model.url);
    }
}

- (void)imageView:(UIImageView *)imageView imageViewAtIndex:(NSInteger)index {
    
    imageView.image = [UIImage imageNamed:@"piaoxinhui"];
//    HomeCarouselModel *model = [imgAry objectAtIndex:index];
//    [imageView sd_setImageWithURL:[NSURL URLWithString:model.image] placeholderImage:[UIImage imageNamed:@"rotationMapDefault"]];
}

@end
