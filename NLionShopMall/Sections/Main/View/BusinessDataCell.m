//
//  BusinessDataCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//


//height:240
#import "BusinessDataCell.h"

@interface BDSubView()

@property (nonatomic,strong) UILabel *textLbl;

@end

@implementation BDSubView

- (void)createViewWithTite:(NSString *)title text:(NSString *)text {
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:14];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x666666"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@25);
    }];
    
    _textLbl = [FSViewHelper createLblWithText:text
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:20];
    _textLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    _textLbl.font = [UIFont fontWithName:kFontName size:20];
    [self addSubview:_textLbl];
    [_textLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@35);
    }];
}

- (void)refreshViewWithTitle:(NSString *)title {
    _textLbl.text = title;
}

@end

@interface BusinessDataCell()

@property (nonatomic,strong) BDSubView *todayIncomeView;
//@property (nonatomic,strong) BDSubView *historyIncomeView;
@property (nonatomic,strong) BDSubView *tiXianView;

@end

@implementation BusinessDataCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:[ColorConverter colorWithHexString:@"0x4593E8"]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(3, 16)]);
        make.left.equalTo(self).offset(15);
        make.top.equalTo(self).offset(12);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"营业数据"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.equalTo(self);
        make.left.equalTo(lineImgView.mas_right).offset(8);
        make.height.equalTo(@40);
    }];
    
    UIImageView *lineImgViewTwo = [FSViewHelper createImgViewWithImg:nil
                                                             bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:lineImgViewTwo];
    [lineImgViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(@1);
    }];
    
    _todayIncomeView = [[BDSubView alloc] init];
    [self addSubview:_todayIncomeView];
    [_todayIncomeView createViewWithTite:@"今日收益(元)" text:@"0.00"];
    [_todayIncomeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgViewTwo.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [_todayIncomeView addGestureRecognizer:tapGR];

//    _historyIncomeView = [[BDSubView alloc] init];
//    [self addSubview:_historyIncomeView];
//    [_historyIncomeView createViewWithTite:@"历史收益" text:@"1500.00元"];
//    [_historyIncomeView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(titleLbl.mas_bottom).offset(25);
//        make.right.equalTo(self).offset(-16);
//        make.height.equalTo(@60);
//        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
//    }];
    
    _tiXianView = [[BDSubView alloc] init];
    [self addSubview:_tiXianView];
    [_tiXianView createViewWithTite:@"可提现鼓励金(元)" text:@"0.00"];
    [_tiXianView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgViewTwo.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@60);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-16]);
    }];
    
    UITapGestureRecognizer *tiXianTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tiXianTapAction:)];
    [_tiXianView addGestureRecognizer:tiXianTapGR];
    
    UIImageView *lineImgViewThree = [FSViewHelper createImgViewWithImg:nil
                                                               bgColor:kTableViewColor];
    [self addSubview:lineImgViewThree];
    [lineImgViewThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_tiXianView.mas_bottom).offset(9);
        make.right.equalTo(self);
        make.left.equalTo(self);
        make.height.equalTo(@6);
    }];
}

- (void)tapAction:(UITapGestureRecognizer *)tapGR {
    if (_todayIncomeAction) {
        _todayIncomeAction();
    }
}

- (void)tiXianTapAction:(UITapGestureRecognizer *)tapGR {
    if (_tiXianGuLiJinAction) {
        _tiXianGuLiJinAction();
    }
}

- (void)refreshViewWithModel:(MainDataModel *)model {
    if (!model) return;
    [_todayIncomeView refreshViewWithTitle:model.today.totalmoney];
    [_tiXianView refreshViewWithTitle:model.gulijin.cash_amount];
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
