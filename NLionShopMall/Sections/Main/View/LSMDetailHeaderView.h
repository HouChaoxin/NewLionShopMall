//
//  LSMDetailHeaderView.h
//  LionShopMall
//
//  Created by apple on 2018/5/7.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LSMDetailHeaderView : UIView

@property (nonatomic ,strong) UIImageView *icon;
@property (nonatomic ,strong) UILabel *phoneTitleLab;
@property (nonatomic ,strong) UILabel *phoneLab;
@property (nonatomic ,strong) UILabel *nameLab;

@property (nonatomic ,strong) UILabel *jifenLab;
@property (nonatomic ,strong) UILabel *jifenTitleLab;
@property (nonatomic ,strong) UILabel *jineLab;
@property (nonatomic ,strong) UILabel *jineTitleLab;
@property (nonatomic ,strong) UILabel *youhuiquanLab;
@property (nonatomic ,strong) UILabel *youhuiquanTitleLab;

//@property (nonatomic ,strong) LSMMemberModel *model;

@end
