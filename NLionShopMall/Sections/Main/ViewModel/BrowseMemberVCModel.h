//
//  BrowseMemberVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/25.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BrowseMemberVCModel : NSObject

- (void)getBrowseMemberDataWithPara:(NSDictionary *)dict
                            success:(SuccessBlock)successBlock
                            failure:(FailureBlock)failureBlock;

@end
