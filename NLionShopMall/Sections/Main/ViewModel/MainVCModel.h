//
//  MainVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MainDataModel.h"

@interface MainVCModel : NSObject

@property (nonatomic,strong) MainDataModel *mainDataModel;
- (void)getMainInfoWithPara:(NSDictionary *)dict
                    success:(SuccessBlock)successBlock
                    failure:(FailureBlock)failureBlock;

@end
