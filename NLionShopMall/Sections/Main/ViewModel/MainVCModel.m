//
//  MainVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainVCModel.h"

@implementation MainVCModel

//http://pd.lion-mall.com/index.php?r=pd-store-xin/get-store-money&store_id=15214574562913460000&uid=3136
- (void)getMainInfoWithPara:(NSDictionary *)dict
                               success:(SuccessBlock)successBlock
                               failure:(FailureBlock)failureBlock {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kMainInfoUrl];
    
    [NetHelper GET:urlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                _mainDataModel = [[MainDataModel alloc] initWithDict:rootDict[@"data"]];
                successBlock(nil);
            }else {
                failureBlock(nil);
            }
        }else {
            failureBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
    
}

@end
