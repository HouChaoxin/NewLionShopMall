//
//  BrowseNumVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/25.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BrowseNumVCModel.h"

@implementation BrowseNumVCModel

- (void)getBrowseNumDataWithPara:(NSDictionary *)dict
                            success:(SuccessBlock)successBlock
                            failure:(FailureBlock)failureBlock {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kIncomeUrl];
    
    [NetHelper GET:urlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                //todo...
                successBlock(nil);
            }else{
                failureBlock(nil);
            }
        }else {
            failureBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

@end
