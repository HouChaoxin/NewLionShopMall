//
//  MainUserViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainUserViewController.h"
#import "MainActivityViewController.h"
#import "ActivityManageViewController.h"
#import "MerchantProfileViewController.h"
#import "SettingViewController.h"
#import "QRcodeManageViewController.h"
#import "EditMerchantViewController.h"
#import "SubTableViewCell.h"
#import "StoreHeaderView.h"
#import "StoreModel.h"

@interface MainUserViewController ()

@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) StoreModel *model;
@property (nonatomic ,strong) StoreHeaderView *headerView;
@end

@implementation MainUserViewController

-(StoreHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[StoreHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenW, 116)];
        _headerView.addButton.hidden = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
        [_headerView addGestureRecognizer:tap];
        
    }
    return _headerView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:
     [UIImage imageNamed:@"mainBackground"] forBarMetrics:UIBarMetricsDefault];
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:@"编辑" forState:0];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    [button addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadBaseData];
    [self setTableView];
}

- (void)setTableView{
    
//    [self.view addSubview:self.headerView];
    self.titleArr = @[@{@"Image":@"ic_huodong",@"title":@"活动管理",@"subTitle":@"在线5个"},
                      @{@"Image":@"ic_erweima",@"title":@"二维码管理",@"subTitle":@"开通支付"},
                      @{@"Image":@"ic_shezhi",@"title":@"设置",@"subTitle":@""},];
    self.tableView.rowHeight = 65;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.tableFooterView = [UIView new];
    self.tableView.tableHeaderView = self.headerView;
    [self.tableView registerClass:[SubTableViewCell class] forCellReuseIdentifier:NSStringFromClass([SubTableViewCell class])];
}


- (void)rightAction{
    
    EditMerchantViewController *vc = [[EditMerchantViewController alloc] init];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tapAction{
    
    MerchantProfileViewController *vc = [[MerchantProfileViewController alloc] init];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.titleArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SubTableViewCell class]) forIndexPath:indexPath];
    cell.titleDic = self.titleArr[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 2) {
        SettingViewController *vc = [[SettingViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        QRcodeManageViewController *vc = [[QRcodeManageViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        MainActivityViewController *vc = [[MainActivityViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}


- (void)loadBaseData{
    
    NSString *url = [baseUrl stringByAppendingString:@"store/info-bussiness-store"];
    NSString *store_id = @"15269885400738030000";                      
    NSDictionary *body = @{@"store_id":store_id};
    [NetHelper GET:url parameters:body progressBlock:nil success:^(id responseObject) {
        
        NSDictionary *tempDic = responseObject[@"data"];
        self.model = [StoreModel mj_objectWithKeyValues:tempDic];
        self.headerView.model = self.model;
        
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
