//
//  StoreModel.h
//  NLionShopMall
//
//  Created by apple on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StoreModel : NSObject

@property (nonatomic ,strong) NSString *address;            //商家地址
@property (nonatomic ,strong) NSString *contacts_phones;    //联系电话
@property (nonatomic ,strong) NSString *store_tag;          //商家标签
@property (nonatomic ,strong) NSString *store_workingtime;  //营业时间
@property (nonatomic ,strong) NSString *store_logo;         //商家logo
@property (nonatomic ,strong) NSString *store_name;         //商家名字
@property (nonatomic ,strong) NSString *store_licence;      //商检证件（三证合一）

@property (nonatomic ,strong) NSString *store_tax;          //可能会有
@property (nonatomic ,strong) NSString *store_institutions; //可能会有

@property (nonatomic ,strong) NSString *store_environment;  //商家环境
@property (nonatomic ,strong) NSString *store_description;  //商家描述


@end

