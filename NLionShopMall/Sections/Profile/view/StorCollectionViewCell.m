//
//  StorCollectionViewCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "StorCollectionViewCell.h"
#import "LinkTool.h"

@implementation StorCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.font = [UIFont systemFontOfSize:14.0f];
        self.titleLab.textColor = [UIColor whiteColor];
        self.titleLab.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.titleLab];
        
        self.deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.deleteBtn setImage:[UIImage imageNamed:@"ic_biaoqianshanchu"] forState:0];
        self.deleteBtn.userInteractionEnabled = NO;//此时不需要点击按钮事件
        [self.contentView addSubview:self.deleteBtn];
    }
    return self;
}


-(void)setString:(NSString *)string{
    if (string) {
        self.titleLab.text = string;
        self.titleLab.frame = CGRectMake(0, 5, [[LinkTool alloc] CalculationWith:string]+10, 20);
        
        self.titleLab.layer.cornerRadius = 5;
        self.titleLab.layer.borderWidth = 1.0f;
        self.titleLab.layer.borderColor = [UIColor whiteColor].CGColor;
        self.titleLab.layer.masksToBounds = YES;
        
        [self.deleteBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLab).offset(-5);
            make.right.equalTo(self.titleLab).offset(5);
            make.width.equalTo(@10);
            make.height.equalTo(@10);
        }];
        
        if (self.isEdit) {
            self.deleteBtn.hidden = NO;
        }else{
            self.deleteBtn.hidden = YES;
        }
    }
}


@end
