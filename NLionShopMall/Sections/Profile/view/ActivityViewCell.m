//
//  activity  activity  ActivityViewCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ActivityViewCell.h"
#import "LinkTool.h"

@implementation ActivityViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        
        self.backgroundColor = [UIColor getColor:@"f3f3f3"];
        
        self.bgView = [[UIView alloc] init];
        self.bgView.backgroundColor = [UIColor whiteColor];
        [self.contentView  addSubview:self.bgView];
        
        self.mainTitleLab = [[UILabel alloc] init];
        self.mainTitleLab.font = [UIFont boldSystemFontOfSize:21];
        self.mainTitleLab.textAlignment = NSTextAlignmentCenter;
        self.mainTitleLab.text = @"满189立减5元";
        [self.bgView addSubview:self.mainTitleLab];
        
        self.typeLabel = [[UILabel alloc] init];
        self.typeLabel.font = [UIFont boldSystemFontOfSize:14];
        self.typeLabel.text = @"新客专享";
        self.typeLabel.textColor = [UIColor whiteColor];
        self.typeLabel.textAlignment = NSTextAlignmentCenter;
        self.typeLabel.backgroundColor = [UIColor getColor:@"56ad66"];
        [self.bgView addSubview:self.typeLabel];
        
        self.subTitleLab = [[UILabel alloc] init];
        self.subTitleLab.font = [UIFont systemFontOfSize:14];
        self.subTitleLab.textColor = [UIColor getColor:@"666666"];
        self.subTitleLab.text = @"满立减";
        [self.bgView addSubview:self.subTitleLab];
        
        self.linkView = [[UIImageView alloc] init];
        [self.bgView addSubview:self.linkView];
        
        self.timeTitleLab = [[UILabel alloc] init];
        self.timeTitleLab.font = [UIFont systemFontOfSize:13];
        self.timeTitleLab.text = @"活动时间";
        self.timeTitleLab.textColor = [UIColor getColor:@"999999"];
        [self.bgView addSubview:self.timeTitleLab];
        
        self.timeLabel = [[UILabel alloc] init];
        self.timeLabel.font = [UIFont systemFontOfSize:13];
        self.timeLabel.textColor = [UIColor getColor:@"999999"];
        self.timeLabel.text = @"2018-4-15至2018-5-24";
        [self.bgView addSubview:self.timeLabel];
        
        [self set_viewLayout];
    
    }
    return self;
}

- (void)set_viewLayout{
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView);
    }];
    self.bgView.layer.cornerRadius = 8.0f;
    self.bgView.layer.masksToBounds = YES;
    

    [self.mainTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bgView).offset(20);
        make.top.equalTo(self.bgView).offset(30);
    }];
    
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.bgView).offset(-15);
        make.centerY.equalTo(self.mainTitleLab);
        make.width.equalTo(@70);
        make.height.equalTo(@24);
    }];
    self.typeLabel.layer.cornerRadius = 12;
    self.typeLabel.layer.masksToBounds = YES;
    
    [self.subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.typeLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.typeLabel);
    }];
    
    self.linkView.frame = CGRectMake(15, 95, screenW - 60, 1);
    [[LinkTool alloc] drawHorDashLine:self.linkView lineLength:5 lineSpacing:3 lineColor:[UIColor lightGrayColor]];

    [self.timeTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bgView).offset(-10);
        make.left.equalTo(self.bgView).offset(15);
    }];
    
    [self.timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.timeTitleLab.mas_right);
        make.bottom.equalTo(self.timeTitleLab);
    }];
    
}

//- (void)setFrame:(CGRect)frame {
//    CGSize size = frame.size;
//    size.height -= 10;
//    frame.size = size;
//    [super setFrame:frame];
//}
@end
