//
//  activity  activity  ActivityViewCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityViewCell : UITableViewCell

@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic ,strong) UILabel *mainTitleLab;
@property (nonatomic ,strong) UILabel *typeLabel;
@property (nonatomic ,strong) UILabel *subTitleLab;
@property (nonatomic ,strong) UILabel *timeTitleLab;
@property (nonatomic ,strong) UILabel *timeLabel;
@property (nonatomic ,strong) UIImageView *linkView;
@end
