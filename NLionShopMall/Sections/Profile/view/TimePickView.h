//
//  TimePickView.h
//  NLionShopMall
//
//  Created by apple on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InputPickTimeDelegate <NSObject>

- (void)didComfirmClick:(NSString *)time;

@end

@interface TimePickView : UIView

@property (nonatomic, weak) id<InputPickTimeDelegate> delegate;
-(void)show;

@end
