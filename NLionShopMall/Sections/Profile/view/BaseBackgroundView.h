//
//  BaseBackgroundView.h
//  NLionShopMall
//
//  Created by apple on 2018/5/16.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseBackgroundView : UIView

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *subLabel;

- (instancetype)initWithFrame:(CGRect)frame setTitle:(NSString *)string;
@end
