//
//  ActivityView.h
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityView : UIView

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UITextField *textTF;
@property (nonatomic ,strong) UIView *link;

- (instancetype)initWithFrame:(CGRect)frame setTitle:(NSString *)string;
@end
