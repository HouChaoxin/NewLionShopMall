//
//  BaseBackgroundView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/16.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BaseBackgroundView.h"

@implementation BaseBackgroundView

- (instancetype)initWithFrame:(CGRect)frame setTitle:(NSString *)string
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.font = [UIFont boldSystemFontOfSize:17.0f];
        self.titleLab.text = string;
        [self addSubview:self.titleLab];
        
        self.subLabel = [[UILabel alloc] init];
        self.subLabel.font = [UIFont systemFontOfSize:14];
        self.subLabel.numberOfLines = 0;
        self.subLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:self.subLabel];
        
        UIView *bottonView = [[UIView alloc] init];
        bottonView.backgroundColor = [UIColor getColor:@"f3f3f3"];
        [self addSubview:bottonView];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.centerY.equalTo(self);
        }];
        
        [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self).offset(-15);
            make.centerY.equalTo(self);
            make.width.equalTo(@(screenW/2));
        }];
        
        [bottonView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self);
            make.height.equalTo(@1);
            make.left.equalTo(self);
            make.right.equalTo(self);
        }];
    }
    return self;
}

@end
