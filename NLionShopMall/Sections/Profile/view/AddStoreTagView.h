//
//  AddStoreTagView.h
//  NLionShopMall
//
//  Created by apple on 2018/5/25.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AddStoreTagDelegate <NSObject>

- (void)didSelectStoreTag:(NSString *)title;

@end

@interface AddStoreTagView : UIView

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UITextField *titleTF;
@property (nonatomic ,strong) UILabel *linkLab;
@property (nonatomic ,strong) UIButton *leftBtn;
@property (nonatomic ,strong) UIButton *rightBtn;
@property (nonatomic ,strong) UIView *bgView;

@property (nonatomic, weak) id<AddStoreTagDelegate> delegate;

-(void)show;

@end
