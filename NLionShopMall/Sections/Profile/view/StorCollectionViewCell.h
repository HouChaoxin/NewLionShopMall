//
//  StorCollectionViewCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StorCollectionViewCell : UICollectionViewCell

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UIButton *deleteBtn;
@property (nonatomic ,strong) NSString *string;
@property (nonatomic ,assign) BOOL isEdit;

@end
