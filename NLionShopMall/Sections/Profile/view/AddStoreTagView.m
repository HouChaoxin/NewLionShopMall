//
//  AddStoreTagView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/25.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "AddStoreTagView.h"
#import "IQKeyboardManager.h"

@interface AddStoreTagView ()<UITextFieldDelegate>

@end

@implementation AddStoreTagView

- (instancetype)initWithFrame:(CGRect)frame
{
    
//    [[IQKeyboardManager sharedManager] setEnable:NO];
//    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        
        self.bgView = [[UIView alloc] init];
        self.bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bgView];
        
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.text = @"添加标签";
        self.titleLab.font = [UIFont boldSystemFontOfSize:17];
        self.titleLab.textColor = [UIColor getColor:@"343434"];
        [self.bgView addSubview:self.titleLab];
        
        self.titleTF = [[UITextField alloc] init];
        self.titleTF.placeholder = @"请输入商家标签,最多五个字";
        self.titleTF.font = [UIFont systemFontOfSize:14];
        self.titleTF.textAlignment = NSTextAlignmentCenter;
        self.titleTF.delegate = self;
        [self.bgView addSubview:self.titleTF];
        
        self.linkLab = [[UILabel alloc] init];
        self.linkLab.backgroundColor = [UIColor getColor:@"cccccc"];
        [self.bgView addSubview:self.linkLab];
        
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.leftBtn setTitle:@"取消" forState:0];
        [self.leftBtn addTarget:self action:@selector(leftBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftBtn setTitleColor:[UIColor blackColor] forState:0];
        [self.bgView addSubview:self.leftBtn];
        
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.rightBtn setTitle:@"确定" forState:0];
        [self.rightBtn setTitleColor:mainColor forState:0];
        [self.bgView addSubview:self.rightBtn];
        [self.rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [self setSubView_layout];
    }
    return self;
}

- (void)setSubView_layout{
    
    [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_bgView);
        make.top.equalTo(_bgView).offset(20);
    }];
    
    [self.titleTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bgView).offset(20);
        make.right.equalTo(_bgView).offset(-20);
        make.top.equalTo(self.titleLab.mas_bottom).offset(30);
        make.height.equalTo(@35);
    }];
    
    [self.linkLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleTF);
        make.right.equalTo(self.titleTF);
        make.top.equalTo(self.titleTF.mas_bottom);
        make.height.equalTo(@1);
    }];
    
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bgView);
        make.top.equalTo(self.linkLab.mas_bottom).offset(20);
        make.right.equalTo(_bgView.mas_centerX);
        make.height.equalTo(@44);
    }];
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_bgView);
        make.top.equalTo(self.linkLab.mas_bottom).offset(20);
        make.left.equalTo(_bgView.mas_centerX);
        make.height.equalTo(@44);
        make.bottom.equalTo(_bgView);
    }];
    
    [self.bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self);
        make.width.equalTo(@(screenW - 60));
    }];
    self.bgView.layer.cornerRadius = 10.0f;
    self.bgView.layer.masksToBounds = YES;
}


- (void)leftBtnAction:(UIButton *)button{
    [self.titleTF resignFirstResponder];
    if (self.superview) {
        [self removeFromSuperview];
    }
}

- (void)rightBtnAction:(UIButton *)button{
    if (self.titleTF.text.length > 5) {
        [self makeToast:@"标签最多为5个字符" duration:1.5 position:CSToastPositionCenter];
    }else{
        
        __weak typeof(self) weakSelf = self;
        NSString *string = weakSelf.titleTF.text;
        weakSelf.titleTF.text = @"";
        if ([weakSelf.delegate respondsToSelector:@selector(didSelectStoreTag:)]) {
            [weakSelf.delegate didSelectStoreTag:string];
        }
        [weakSelf.titleTF resignFirstResponder];
        if (weakSelf.superview) {
            [weakSelf removeFromSuperview];
        }  
    }
}

- (void)show{
    
    [[UIApplication sharedApplication].keyWindow addSubview:self];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    
    if (textField.text.length > 5 ) {
        [self makeToast:@"标签最多为5个字符" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    return YES;
}


//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//    if (textField.text.length > 5 ) {
//        [self makeToast:@"标签最多为5个字符" duration:1.5 position:CSToastPositionCenter];
//        return NO;
//    }
//    return YES;
//}

@end






