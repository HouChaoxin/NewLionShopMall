//
//  StoreHeaderView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "StoreHeaderView.h"
#import "StorCollectionViewCell.h"
#import "LinkTool.h"

@interface StoreHeaderView ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    UIImageView *_bgView;
}
@property (nonatomic ,strong) UICollectionView *collectView;

@end

@implementation StoreHeaderView


- (UICollectionView *)collectView{
    if (!_collectView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 5;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectView.delegate = self;
        _collectView.dataSource = self;
        _collectView.backgroundColor = [UIColor clearColor];
        [_collectView registerClass:[StorCollectionViewCell class] forCellWithReuseIdentifier:NSStringFromClass([StorCollectionViewCell class])];
        [self addSubview:self.collectView];
    }
    return _collectView;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenW, 116)];
        _bgView.image = [UIImage imageNamed:@"mainBackground"];
        [self addSubview:_bgView];
        
        self.icon = [[UIImageView alloc] init];
        self.icon.image = [UIImage imageNamed:@"Image_demo"];
        [self addSubview:self.icon];
        
        self.nameLabel = [[UILabel alloc] init];
        self.nameLabel.text = @"山城客家王";
        self.nameLabel.textColor = [UIColor whiteColor];
        self.nameLabel.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.nameLabel];
        
        self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.addButton setTitle:@"添加" forState:0];
        self.addButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [self.collectView addSubview:self.addButton];
        [self.addButton addTarget:self action:@selector(addMarkClick:) forControlEvents:UIControlEventTouchUpInside];
        
        [self set_subViewLayout];
    }
    return self;
}

- (void)set_subViewLayout{
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(20);
        make.centerY.equalTo(self);
        make.width.equalTo(@86);
        make.height.equalTo(@86);
    }];
    self.icon.layer.cornerRadius = 43;
    self.icon.layer.borderWidth = 1.0f;
    self.icon.layer.borderColor = [UIColor whiteColor].CGColor;
    self.icon.layer.masksToBounds = YES;
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.icon.mas_right).offset(10);
        make.top.equalTo(self.icon);
    }];
    
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.mas_bottom).offset(5);
        make.right.equalTo(self).offset(-10);
        make.left.equalTo(self.icon.mas_right).offset(10);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    [self.addButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-10);
        make.bottom.equalTo(self).offset(-10);
    }];
}


- (void)addMarkClick:(UIButton *)button{
    
    
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.titleArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    StorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([StorCollectionViewCell class]) forIndexPath:indexPath];
    cell.isEdit = self.isEdit;
    cell.string = self.titleArr[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return CGSizeMake([[LinkTool alloc] CalculationWith:self.titleArr[indexPath.row]]+15,25);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (self.isEdit) {
        NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.titleArr];
        [tempArr removeObjectAtIndex:indexPath.row];
        self.titleArr = tempArr;
        [self.collectView deleteItemsAtIndexPaths:@[indexPath]];
    }
}

-(void)setModel:(StoreModel *)model{
    if (model) {
        NSArray *storeTag = [model.store_tag componentsSeparatedByString:@","];
        self.titleArr = storeTag;
        self.nameLabel.text = model.store_name;
        [self.icon sd_setImageWithURL:[NSURL URLWithString:model.store_logo] placeholderImage:[UIImage imageNamed:@"Image_demo"]];
        [self.collectView reloadData];
    }
}


- (void)refreshHeaderView{
    
    [self.collectView reloadData];
}
@end
