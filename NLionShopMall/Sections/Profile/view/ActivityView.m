//
//  ActivityView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ActivityView.h"

@implementation ActivityView

- (instancetype)initWithFrame:(CGRect)frame setTitle:(NSString *)string
{
    self = [super initWithFrame:frame];
    if (self) {
    
        UILabel *topLabel = [[UILabel alloc] init];
        topLabel.text = @"*";
        topLabel.textColor = [UIColor redColor];
        [self addSubview:topLabel];
        
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.text = string;
        [self addSubview:self.titleLab];
        
        self.textTF = [[UITextField alloc] init];
        self.textTF.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.textTF];
        
        self.link = [[UIView alloc] init];
        self.link.backgroundColor = [UIColor getColor:@"f3f3f3"];
        [self addSubview:self.link];
        
        [topLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.centerY.equalTo(self);
            make.width.equalTo(@7);
        }];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(topLabel.mas_right);
            make.width.equalTo(@120);
        }];
        
        [self.textTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.titleLab.mas_right).offset(10);
            make.right.equalTo(self).offset(-20);
            make.height.equalTo(@30);
        }];
        
        [self.link mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.right.equalTo(self).offset(-15);
            make.bottom.equalTo(self);
            make.height.equalTo(@1);
        }];
        
    }
    return self;
}

@end
