//
//  SubTableViewCell.m
//  LionShopMall
//
//  Created by apple on 2018/5/11.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "SubTableViewCell.h"

@implementation SubTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]){
        self.titleLab = [[UILabel alloc] init];
        [self.contentView addSubview:self.titleLab];
        
        self.subLabel = [[UILabel alloc] init];
        [self.contentView addSubview:self.subLabel];
        
        self.icon = [[UIImageView alloc] init];
        [self.contentView addSubview:self.icon];
        
        [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.contentView).offset(15);
            make.centerY.equalTo(self.contentView);
        }];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.icon.mas_right).offset(10);
            make.centerY.equalTo(self.contentView);
        }];
        [self.subLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView).offset(-15);
            make.centerY.equalTo(self.contentView);
        }];
    }
    return self;
}


-(void)setTitleDic:(NSDictionary *)titleDic{
    if (titleDic) {
        self.icon.image = [UIImage imageNamed:titleDic[@"Image"]];
        self.titleLab.text = titleDic[@"title"];
        self.subLabel.text = titleDic[@"subTitle"];
    }
}


//- (void)setFrame:(CGRect)frame {
//    CGSize size = frame.size;
//    size.height -= 5;
//    
//    CGFloat y = frame.origin.y;
//    y += 5;
//    frame.origin.y = y;
//    frame.size = size;
//    [super setFrame:frame];
//}

@end
