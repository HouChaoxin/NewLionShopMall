//
//  TimePickView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/23.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "TimePickView.h"

@interface TimePickView ()
{
    UIView *_bgView;
    UILabel *_topTitleLab;
    UILabel *_bottomTitleLab;
    UIButton *_leftBtn;
    UIButton *_rightBtn;
    UIView *_herLink;
    UIView *_verLink;
}
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic, strong) UIDatePicker *startPicker;
@property (nonatomic, strong) UIDatePicker *endPicker;

@property (nonatomic ,strong) NSString *starTimeStr;
@property (nonatomic ,strong) NSString *endTimeStr;

@end

@implementation TimePickView

- (NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter = [[NSDateFormatter alloc]init];
    }
    _formatter.dateFormat = @"HH:mm";
    return _formatter;
}

- (UIDatePicker *)startPicker
{
    if (!_startPicker) {
        _startPicker = [[UIDatePicker alloc] init];
        _startPicker.datePickerMode = UIDatePickerModeTime;
        [_startPicker addTarget:self action:@selector(startPickvalueChange) forControlEvents:UIControlEventValueChanged];
    }
    return _startPicker;
}

-(UIDatePicker *)endPicker{
    if (!_endPicker) {
        _endPicker = [[UIDatePicker alloc] init];
        _endPicker.datePickerMode = UIDatePickerModeTime;
        [_endPicker addTarget:self action:@selector(endPickvalueChange) forControlEvents:UIControlEventValueChanged];
    }
    return _endPicker;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        
        _bgView= [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_bgView];
        
        _topTitleLab = [[UILabel alloc] init];
        _topTitleLab.text = @"开始时间";
        [_bgView addSubview:_topTitleLab];

        _bottomTitleLab = [[UILabel alloc] init];
        _bottomTitleLab.text = @"结束时间";
        [_bgView addSubview:_bottomTitleLab];

        self.starTimeStr = [self.formatter stringFromDate:self.startPicker.date];
        [_bgView addSubview:self.startPicker];

        self.endTimeStr = [self.formatter stringFromDate:self.endPicker.date];
        [_bgView addSubview:self.endPicker];

        _herLink = [[UIView alloc] init];
        _herLink.backgroundColor = [UIColor getColor:@"e6e6e6"];
        [_bgView addSubview:_herLink];

        _verLink = [[UIView alloc] init];
        _verLink.backgroundColor = [UIColor getColor:@"e6e6e6"];
        [_bgView addSubview:_verLink];

        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftBtn setTitle:@"取消" forState:0];
        [_leftBtn setTitleColor:[UIColor blackColor] forState:0];
        _leftBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_leftBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_leftBtn];

        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setTitle:@"确认" forState:0];
        [_rightBtn setTitleColor:mainColor forState:0];
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_bgView addSubview:_rightBtn];
        [_rightBtn addTarget:self action:@selector(confirmClick:) forControlEvents:UIControlEventTouchUpInside];
    
    }
    return self;
}

- (void)startPickvalueChange{
    
    self.starTimeStr = [self.formatter stringFromDate:self.startPicker.date];
}

-(void)endPickvalueChange{
    
    self.endTimeStr = [self.formatter stringFromDate:self.endPicker.date];
}

-(void)cancelClick:(UIButton *)button{
    if (self.superview) {
        [self removeFromSuperview];
    }
}

-(void)confirmClick:(UIButton *)button{
   
    __weak typeof(self) weakSelf = self;
    
    NSString *string = [NSString stringWithFormat:@"%@-%@",self.starTimeStr,self.endTimeStr];
    if ([weakSelf.delegate respondsToSelector:@selector(didComfirmClick:)]) {
        [weakSelf.delegate didComfirmClick:string];
    }
    if (weakSelf.superview) {
        [weakSelf removeFromSuperview];
    }
}



- (void)setSubView_layout{
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(30);
        make.right.equalTo(self).offset(-30);
        make.height.equalTo(@380);
    }];
    _bgView.layer.cornerRadius = 6.0f;
    _bgView.layer.masksToBounds = YES;
    
    [_topTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgView).offset(15);
        make.centerX.equalTo(_bgView);
    }];

    [self.startPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_topTitleLab.mas_bottom);
        make.left.equalTo(_bgView);
        make.right.equalTo(_bgView);
        make.height.equalTo(@130);
    }];

    [_bottomTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.startPicker.mas_bottom).offset(10);
        make.centerX.equalTo(_bgView);
    }];

    [self.endPicker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bottomTitleLab.mas_bottom);
        make.left.equalTo(_bgView);
        make.right.equalTo(_bgView);
        make.height.equalTo(@130);
    }];

    [_herLink mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bgView).offset(10);
        make.right.equalTo(_bgView).offset(-10);
        make.height.equalTo(@(1));
        make.bottom.equalTo(_bgView).offset(-45);
    }];

    [_verLink mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_bgView);
        make.centerX.equalTo(_bgView);
        make.height.equalTo(@(45));
        make.width.equalTo(@(1));
    }];

    [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_bgView);
        make.left.equalTo(_bgView);
        make.width.equalTo(@(screenW/2 - 30));
        make.height.equalTo(@(45));
    }];

    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_bgView);
        make.right.equalTo(_bgView);
        make.width.equalTo(@(screenW/2 - 30));
        make.height.equalTo(@(45));
    }];
}

-(void)show{
    
    [self setSubView_layout];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
}
@end
