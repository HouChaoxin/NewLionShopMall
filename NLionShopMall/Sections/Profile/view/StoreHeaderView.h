//
//  StoreHeaderView.h
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"

@interface StoreHeaderView : UIView

@property (nonatomic ,strong) UIImageView *icon;
@property (nonatomic ,strong) UILabel *nameLabel;
@property (nonatomic ,strong) UIButton *addButton;
@property (nonatomic ,assign) BOOL isEdit;
@property (nonatomic ,strong) NSArray *titleArr;

@property (nonatomic ,strong) StoreModel *model;

- (void)refreshHeaderView;

@end
