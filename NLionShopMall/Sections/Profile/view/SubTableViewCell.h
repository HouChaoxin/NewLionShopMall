//
//  SubTableViewCell.h
//  LionShopMall
//
//  Created by apple on 2018/5/11.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubTableViewCell : UITableViewCell

@property (nonatomic ,strong) UILabel *titleLab;
@property (nonatomic ,strong) UILabel *subLabel;
@property (nonatomic ,strong) UIImageView *icon;

@property (nonatomic ,strong) NSDictionary *titleDic;

@end
