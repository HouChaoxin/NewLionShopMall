//
//  MainActivityViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainActivityViewController.h"
#import "ActivityManageViewController.h"
#import "MainSetingViewController.h"

@interface MainActivityViewController ()

@property(nonatomic,strong)NSArray *fenleiArr;
@end

@implementation MainActivityViewController

-(void)loadView
{
    [super loadView];
    self.menuViewStyle = WMMenuViewStyleLine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle];
    self.fenleiArr = @[@"在线",@"已下线"];
    self.automaticallyCalculatesItemWidths = YES;
    self.titleSizeSelected = 16;
    self.titleSizeNormal = 16;
    self.menuHeight = 40;
    self.titleColorSelected = [UIColor whiteColor];
    self.titleColorNormal = [UIColor colorWithWhite:1.0 alpha:0.4];
    self.viewFrame = CGRectMake(0, 0, screenW, screenH - 64);
    [self reloadData];
    
    
    
    for (UIView *view in self.view.subviews){
        NSLog(@"%@",NSStringFromClass([view class]));
        if ([NSStringFromClass([view class]) isEqualToString:@"WMMenuView"]){
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainBackground"]];
            [view addSubview:imgView];
            [view sendSubviewToBack:imgView];
            [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(view);
            }];
            break;
        }
    }
}

- (void)setNavTitle{
    
    self.navigationItem.title = @"活动管理";
    
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:@"添加" forState:0];
    [button addTarget:self action:@selector(addAction) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = buttonItem;
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)addAction{
    
    MainSetingViewController *vc = [[MainSetingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return _fenleiArr.count;
}
//此方法只有在加载某一界面时才会调用
- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index{
    
    ActivityManageViewController *vc = [[ActivityManageViewController alloc]init];
//    vc.VCType = index;
    return vc;
}


- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index{
    
    return _fenleiArr[index];
}

@end
