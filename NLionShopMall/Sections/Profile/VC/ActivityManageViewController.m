//
//  ActivityManageViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ActivityManageViewController.h"
#import "ActivitySetingViewController.h"
#import "ActivityViewCell.h"

@interface ActivityManageViewController ()

@property (nonatomic ,strong) UIButton *nextBtn;
@property (nonatomic ,strong) UIButton *leftBtn;
@property (nonatomic ,strong) UIButton *rightBtn;


@property (nonatomic ,strong) NSMutableArray *deleteArr;
@end

@implementation ActivityManageViewController

-(NSMutableArray *)deleteArr{
    if (!_deleteArr) {
        _deleteArr = [NSMutableArray array];
    }
    return _deleteArr;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"活动管理";
    
    [self.tableView registerClass:[ActivityViewCell class] forCellReuseIdentifier:NSStringFromClass([ActivityViewCell class])];
    self.tableView.rowHeight = 140;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self setTableFootView];
    
}

- (void)setTableFootView{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenW, 120)];
    view.backgroundColor = [UIColor getColor:@"f3f3f3"];
    self.nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextBtn.frame = CGRectMake(screenW/2 - 60, 40, 120, 40);
    self.nextBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    self.nextBtn.layer.cornerRadius = 5.0f;
    self.nextBtn.layer.masksToBounds = YES;
    [self.nextBtn setTitle:@"下架活动" forState:0];
    self.nextBtn.backgroundColor = [UIColor getColor:@"4e8ce9"];
    [self.nextBtn addTarget:self action:@selector(endActivity:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.nextBtn];
    
    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftBtn.frame = CGRectMake(15, 40, 120, 40);
    self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.leftBtn setTitle:@"取消" forState:0];
    [self.leftBtn setTitleColor:[UIColor blackColor] forState:0];
    self.leftBtn.layer.cornerRadius = 5.0f;
    self.leftBtn.layer.borderWidth = 1.0f;
    self.leftBtn.layer.borderColor = [UIColor getColor:@"4e8ce9"].CGColor;
    self.leftBtn.layer.masksToBounds = YES;
    [self.leftBtn addTarget:self action:@selector(leftAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.leftBtn];
    self.leftBtn.hidden = YES;
    
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn.frame = CGRectMake(screenW - 135, 40, 120, 40);
    self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:17];
    [self.rightBtn setTitle:@"确定" forState:0];
    self.rightBtn.backgroundColor = [UIColor getColor:@"4e8ce9"];
    self.rightBtn.layer.cornerRadius = 5.0f;
    self.rightBtn.layer.masksToBounds = YES;
    [self.rightBtn addTarget:self action:@selector(rightAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.rightBtn];
    self.rightBtn.hidden = YES;
    self.tableView.tableFooterView = view;
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ActivityViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ActivityViewCell class]) forIndexPath:indexPath];
    return cell;
    
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
//    UIView *view = [UIView new];
//    return view;
//}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [self.deleteArr addObject:[self.dataArr objectAtIndex:indexPath.row]];
//    self.deleteNum += 1;
//    [self.deleteBtn setTitle:[NSString stringWithFormat:@"删除(%lu)",self.deleteNum] forState:UIControlStateNormal];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
//    [self.deleteArr removeObject:[self.dataArr objectAtIndex:indexPath.row]];
//    self.deleteNum -= 1;
//    [self.deleteBtn setTitle:[NSString stringWithFormat:@"删除(%lu)",self.deleteNum] forState:UIControlStateNormal];
}


#pragma mark -- 下架活动
- (void)endActivity:(UIButton *)button{
    
    self.tableView.editing = YES;
    button.hidden = YES;
    self.leftBtn.hidden = NO;
    self.rightBtn.hidden = NO;
}

- (void)leftAction:(UIButton *)button{
    
    self.tableView.editing = NO;
    button.hidden = YES;
    self.nextBtn.hidden = NO;
    self.rightBtn.hidden = YES;
}

- (void)rightAction:(UIButton *)button{
    
    self.tableView.editing = NO;
    button.hidden = YES;
    self.nextBtn.hidden = NO;
    self.leftBtn.hidden = YES;
}
@end
