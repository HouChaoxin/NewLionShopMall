//
//  ActivitySetingViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ActivitySetingViewController.h"
#import "ActivityView.h"

@interface ActivitySetingViewController ()
{
    UITextField *_doorTF;
    UITextField *_reductionTF;
    UITextField *_startTimeTF;
    UITextField *_endTimeTF;
    
    UIButton *_newBtn;
    UIButton *_oldBtn;
    
    NSString *_startStr;
    NSString *_endStr;
}
@property (nonatomic, strong) NSDateFormatter *formatter;
@property (nonatomic ,strong) UIButton *nextBtn;
@property (nonatomic ,strong) UIDatePicker *startTime;
@property (nonatomic ,strong) UIDatePicker *endTime;

@end

@implementation ActivitySetingViewController

- (NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter = [[NSDateFormatter alloc]init];
    }
    _formatter.dateFormat = @"yyyy-mm-dd";
    return _formatter;
}

-(UIDatePicker *)startTime{
    if (!_startTime) {
        _startTime = [[UIDatePicker alloc] init];
        _startTime.datePickerMode = UIDatePickerModeDate;
        [_startTime addTarget:self action:@selector(startPickvalueChange) forControlEvents:UIControlEventValueChanged];
        _startTimeTF.text = [self.formatter stringFromDate:_startTime.date];
    }
    return _startTime;
}

-(UIDatePicker *)endTime{
    if (!_endTime) {
        _endTime = [[UIDatePicker alloc] init];
        _endTime.datePickerMode = UIDatePickerModeDate;
        [_endTime addTarget:self action:@selector(endPickvalueChange) forControlEvents:UIControlEventValueChanged];
        _endTimeTF.text = [self.formatter stringFromDate:_endTime.date];
    }
    return _endTime;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"活动设置";
    self.view.backgroundColor = [UIColor whiteColor];
    [self set_subView];
}

- (void)set_subView{
    
    NSArray *titleArr;
    NSArray *placeholderArr;
    if (self.VCType == 0) {
        titleArr = @[@"减免金额（元):",@"活动开始时间:",@"活动结束时间:"];
        placeholderArr = @[@"输入减免的金额",@"活动开始的时间",@"活动结束的时间"];
    }else{
        titleArr = @[@"折扣幅度(折):",@"活动开始时间:",@"活动结束时间:"];
        placeholderArr = @[@"输入折扣幅度",@"活动开始的时间",@"活动结束的时间"];
    }
    
    ActivityView *objView = [[ActivityView alloc] initWithFrame:CGRectMake(0, 0, screenW, 60) setTitle:@"活动对象:"];
    objView.textTF.hidden = YES;
    [self.view addSubview:objView];
    
    _newBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _newBtn.frame = CGRectMake((screenW - 340)/2 + 130, 10, 80, 40);
    _newBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 0);
    _newBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [_newBtn setTitle:@"新客" forState:0];
    [_newBtn setImage:[UIImage imageNamed:@"noSelect"] forState:0];
    [_newBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateSelected];
    [_newBtn setTitleColor:[UIColor blackColor] forState:0];
    [_newBtn addTarget:self action:@selector(newAction:) forControlEvents:UIControlEventTouchUpInside];
    [objView addSubview:_newBtn];
    
    _oldBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _oldBtn.frame = CGRectMake(CGRectGetMaxX(_newBtn.frame), 10, 100, 40);
    _oldBtn.imageEdgeInsets = UIEdgeInsetsMake(10, 0, 10, 10);
    _oldBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    [_oldBtn setTitle:@"回头客" forState:0];
    [_oldBtn setImage:[UIImage imageNamed:@"noSelect"] forState:0];
    [_oldBtn setImage:[UIImage imageNamed:@"select"] forState:UIControlStateSelected];
    [_oldBtn setTitleColor:[UIColor blackColor] forState:0];
    [_oldBtn addTarget:self action:@selector(oldAction:) forControlEvents:UIControlEventTouchUpInside];
    [objView addSubview:_oldBtn];
    
    ActivityView *doorView = [[ActivityView alloc] initWithFrame:CGRectMake(0, 60, screenW, 60) setTitle:@"使用门槛（元):"];
    doorView.textTF.placeholder = @"享受优惠的消费金额";
    _doorTF = doorView.textTF;
    [self.view addSubview:doorView];
    
    UILabel *infoLab = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, screenW, 25)];
    infoLab.backgroundColor = [UIColor getColor:@"e5efff"];
    infoLab.font = [UIFont systemFontOfSize:13.0f];
    infoLab.textColor = [UIColor getColor:@"4e8ce9"];
    infoLab.text = @"满*元减*元；填0表示无门槛，到店立减*元";
    infoLab.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:infoLab];
    
    for (int i=0; i<titleArr.count; i++) {
        ActivityView *avtivityView = [[ActivityView alloc] initWithFrame:CGRectMake(0, 145+60*i, screenW, 60) setTitle:titleArr[i]];
        avtivityView.textTF.placeholder = placeholderArr[i];
        avtivityView.textTF.tag = 100+i;
        [self.view addSubview:avtivityView];
        
        if (i == 0) {
            
            _reductionTF = avtivityView.textTF;
        }else if (i == 1){
            
            _startTimeTF = avtivityView.textTF;
            _startTimeTF.inputView = self.startTime;
        }else{
            
            _endTimeTF = avtivityView.textTF;
            _endTimeTF.inputView = self.endTime;
            
            self.nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            self.nextBtn.frame = CGRectMake(screenW/2 - 80, CGRectGetMaxY(avtivityView.frame)+50, 120, 40);
            [self.nextBtn setTitle:@"确定" forState:0];
            self.nextBtn.layer.cornerRadius = 5.0f;
            self.nextBtn.layer.masksToBounds = YES;
            self.nextBtn.backgroundColor = [UIColor getColor:@"4e8ce9"];
            [self.nextBtn addTarget:self action:@selector(nextClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:self.nextBtn];
        }
    }
    
    
}

- (void)newAction:(UIButton *)button{
    
    button.selected = !button.selected;
}

- (void)oldAction:(UIButton *)button{
    
    button.selected = !button.selected;
}

- (void)startPickvalueChange{
    
    _startTimeTF.text = [self.formatter stringFromDate:_startTime.date];
}

- (void)endPickvalueChange{
    
    _endTimeTF.text = [self.formatter stringFromDate:_endTime.date];
}

-(void)nextClick:(UIButton *)button{
   
    if (_newBtn.selected == NO && _oldBtn.selected == NO) {
        [self.view makeToast:@"请选择活动对象" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    if (!_doorTF.text.length || !_reductionTF.text.length || !_startTimeTF.text.length || !_endTimeTF.text.length) {
        [self.view makeToast:@"请填写完整的资料" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    
    NSString *url = [baseUrl stringByAppendingString:@"activitystore/add"];
    NSString *storeID = @"15270428988477720000";
    NSString *amount;
    NSString *discount;
    if (self.VCType == 0) {
        amount = _reductionTF.text;
        discount = @"1";
    }else{
        amount = @"0";
        discount = _reductionTF.text;
    }
    
    NSString *member_type;
    if (_newBtn.selected == YES  && _oldBtn.selected == NO) {
        member_type = @"1";
    }else if (_newBtn.selected == NO  && _oldBtn.selected == YES){
        member_type = @"2";
    }else{
        member_type = @"3";
    }
    
    NSDictionary *body = @{@"store_id":storeID,
                           @"type":@(self.VCType + 1),
                           @"member_type":member_type,
                           @"term":_doorTF.text,
                           @"amount":amount,
                           @"discount":discount,
                           @"start_time":_startTimeTF.text,
                           @"end_time":_endTimeTF.text,};
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}
@end
