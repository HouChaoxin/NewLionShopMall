//
//  MainSetingViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/26.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainSetingViewController.h"
#import "ActivitySetingViewController.h"

@interface MainSetingViewController ()

@property(nonatomic,strong)NSArray *fenleiArr;
@end

@implementation MainSetingViewController

-(void)loadView
{
    [super loadView];
    self.menuViewStyle = WMMenuViewStyleLine;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle];
    self.fenleiArr = @[@"满减活动",@"折扣活动"];
    self.automaticallyCalculatesItemWidths = YES;
    self.titleSizeSelected = 16;
    self.titleSizeNormal = 16;
    self.menuHeight = 40;
    self.titleColorSelected = [UIColor whiteColor];
    self.titleColorNormal = [UIColor colorWithWhite:1.0 alpha:0.4];
    self.viewFrame = CGRectMake(0, 0, screenW, screenH - 64);
    [self reloadData];
    
    for (UIView *view in self.view.subviews){
        NSLog(@"%@",NSStringFromClass([view class]));
        if ([NSStringFromClass([view class]) isEqualToString:@"WMMenuView"]){
            UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainBackground"]];
            [view addSubview:imgView];
            [view sendSubviewToBack:imgView];
            [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(view);
            }];
            break;
        }
    }
}

- (void)setNavTitle{
    
    self.navigationItem.title = @"活动设置";
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return _fenleiArr.count;
}
//此方法只有在加载某一界面时才会调用
- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index{
    
    ActivitySetingViewController *vc = [[ActivitySetingViewController alloc]init];
    vc.VCType = index;
    return vc;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index{
    
    return _fenleiArr[index];
}

@end
