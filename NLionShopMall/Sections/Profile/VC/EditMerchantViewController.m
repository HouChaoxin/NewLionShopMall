//
//  EditMerchantViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "EditMerchantViewController.h"
#import "StoreHeaderView.h"
#import "TZImagePickerController.h"
#import <CommonCrypto/CommonDigest.h>
#import <Photos/Photos.h>
#import "CollectionViewCell.h"
#import "BaseBackgroundView.h"

#import "TimePickView.h"
#import "AddStoreTagView.h"

@interface EditMerchantViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate,UIScrollViewDelegate,UITextFieldDelegate,InputPickTimeDelegate,UITextViewDelegate,AddStoreTagDelegate>
{
    CGFloat _collectView_H;
    
    UIView *_contentView;
    UILabel *_huanjingTitleLab;
    UILabel *_huanjingSubLab;
    UILabel *_jianjieTitleLab;
    UILabel *_jianjieSubLab;
    UITextView *_detailStoreView;
    UILabel *_cardTitleLab;
    UILabel *_cardSubLab;
    UIImageView *_cardImg;
    UIImageView *_store_taxImg;
    UIImageView *_store_institutionsImg;
    
    BaseBackgroundView *_storeAddressView;
    BaseBackgroundView *_storePhoneView;
    BaseBackgroundView *_storeTimeView;
    
    UITextField *_textField;
    
    
    UILabel *_kindLabel;
    UITextView *_addressTF;
    UITextField *_phoneTF;
    UITextField *_timeTF;
    
    UIView *_firstBgView;
    UIView *_seconBgView;
    
}

@property (nonatomic ,strong) TimePickView *timePick;
@property (nonatomic ,strong) AddStoreTagView *addTagView;

@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) StoreHeaderView *headerView;

@property (nonatomic ,strong) UICollectionView *collectView;
@property (nonatomic ,strong) NSMutableArray *photosArray;
@property (nonatomic ,strong) NSMutableArray *assestArray;
@property BOOL isSelectOriginalPhoto;
@property (nonatomic ,strong) NSMutableArray *imageArr;//模型图片

@property (nonatomic ,strong) NSMutableArray *imagePath;//上传图片路径集合

@property (nonatomic ,assign) NSInteger maxCount;
@end

@implementation EditMerchantViewController

-(NSMutableArray *)imagePath{
    if (!_imagePath) {
        _imagePath = [NSMutableArray array];
    }
    return _imagePath;
}

- (NSMutableArray *)photosArray{
    if (!_photosArray) {
        _photosArray = [NSMutableArray array];
    }
    return _photosArray;
}

- (NSMutableArray *)assestArray{
    if (!_assestArray) {
        self.assestArray = [NSMutableArray array];
    }
    return _assestArray;
}

-(AddStoreTagView *)addTagView{
    if (!_addTagView) {
        _addTagView = [[AddStoreTagView alloc] init];
        _addTagView.delegate = self;
    }
    return _addTagView;
}

-(TimePickView *)timePick{
    
    if (!_timePick) {
        _timePick = [[TimePickView alloc] init];
        _timePick.delegate = self;
    }
    return _timePick;
}

-(UICollectionView *)collectView{
    if (!_collectView) {
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc] init];
        flowLayOut.itemSize = CGSizeMake((screenW - 50)/3, 0.85*(screenW - 50)/3);
        flowLayOut.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
        flowLayOut.minimumLineSpacing = 5;
        flowLayOut.minimumInteritemSpacing = 5;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayOut];
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.delegate = self;
        _collectView.dataSource = self;
        [_collectView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _collectView.scrollEnabled = NO;
    }
    return _collectView;
}

-(StoreHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[StoreHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenW, 116)];
        _headerView.isEdit = YES;
        _headerView.model = self.model;
        [_headerView.addButton addTarget:self action:@selector(begainEdit:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _headerView;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardNotification:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

- (void)keyboardNotification:(NSNotification *)notification{
    
    NSDictionary *dict = notification.userInfo;
    CGRect rect = [dict[@"UIKeyboardFrameEndUserInfoKey"] CGRectValue];
    CGFloat botton = self.addTagView.bgView.fh_bottom;
    if (rect.origin.y < botton ) {
        [UIView animateWithDuration:0.25 animations:^{
            self.addTagView.bgView.fh_bottom = rect.origin.y;
        }];
    }else{
        [UIView animateWithDuration:0.25 animations:^{
            self.addTagView.bgView.fh_centerY = screenH/2;
        }];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSArray *tempArr = [self.model.store_environment componentsSeparatedByString:@";"];
    self.imageArr = [NSMutableArray arrayWithArray:tempArr];
    [self calculateHeight:self.imageArr.count];
    [self setNavView];
    [self creatSubView];
}

- (void)setNavView{
    self.navigationItem.title = @"编辑详情";
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:@"完成" forState:0];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    [button addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightAction{
    
    [self upDataImage];
    
}


- (void)begainEdit:(UIButton *)button{
    
    if (_headerView.titleArr.count < 5) {
        
        [self.addTagView show];
        
    }else{
        [self.view makeToast:@"已有五个标签，如需添加请先删除部分" duration:1.5 position:CSToastPositionCenter];
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    if (_timeTF == textField) {
        
        [self.timePick show];
        return NO;
    }
    return YES;
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField{
//
//    if (textField.text.length > 5) {
//
//        [self.view makeToast:@"标签最多为5个字符" duration:1.5 position:CSToastPositionTop];
//        return NO;
//    }
//
//    [textField resignFirstResponder];
//    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:_headerView.titleArr];
//    [tempArr addObject:textField.text];
//    self.headerView.titleArr = tempArr;
//    [self.headerView refreshHeaderView];
//    textField.text = @"";
//    return YES;
//}

- (void)didSelectStoreTag:(NSString *)title{
    
    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:_headerView.titleArr];
    [tempArr addObject:title];
    self.headerView.titleArr = tempArr;
    [self.headerView refreshHeaderView];
}

- (void)creatSubView{

    [self.view addSubview:self.scrollView];
    _contentView = [[UIView alloc] init];
    [self.scrollView addSubview:_contentView];
    
    [_contentView addSubview:self.headerView];
    
    _huanjingTitleLab = [[UILabel alloc] init];
    _huanjingTitleLab.text = @"商家环境";
    _huanjingTitleLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_huanjingTitleLab];
    
    _huanjingSubLab = [[UILabel alloc] init];
    _huanjingSubLab.text = @"(如需替换请先删除部分照片)";
    _huanjingSubLab.font = [UIFont systemFontOfSize:13];
    _huanjingSubLab.textColor = [UIColor getColor:@"999999"];
    [_contentView addSubview:_huanjingSubLab];
    
    [_contentView addSubview:self.collectView];
   
    _jianjieTitleLab = [[UILabel alloc] init];
    _jianjieTitleLab.text = @"商家简介";
    _jianjieTitleLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_jianjieTitleLab];
    
    _jianjieSubLab = [[UILabel alloc] init];
    _jianjieSubLab.text = [NSString stringWithFormat:@"%ld/300",self.model.store_description.length];
    _jianjieSubLab.font = [UIFont systemFontOfSize:13];
    _jianjieSubLab.textColor = [UIColor getColor:@"999999"];
    [_contentView addSubview:_jianjieSubLab];
    
    _detailStoreView = [[UITextView alloc] init];
    _detailStoreView.font = [UIFont systemFontOfSize:15];
    _detailStoreView.text = self.model.store_description;
    _detailStoreView.delegate = self;
    [_contentView addSubview:_detailStoreView];
    
    _firstBgView = [[UIView alloc] init];
    _firstBgView.backgroundColor = [UIColor getColor:@"e6e6e6"];
    [_contentView addSubview:_firstBgView];
    
    _cardTitleLab = [[UILabel alloc] init];
    _cardTitleLab.text = @"营业资质";
    _cardTitleLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_cardTitleLab];
    
    _cardSubLab = [[UILabel alloc] init];
    _cardSubLab.text = @"营业资质不可修改，如需变更请与平台客服联系";
    _cardSubLab.font = [UIFont systemFontOfSize:13];
    _cardSubLab.textColor = [UIColor getColor:@"999999"];
    [_contentView addSubview:_cardSubLab];
    
    _cardImg = [[UIImageView alloc] init];
    [_cardImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_licence] placeholderImage:[UIImage imageNamed:@"Image_demo"]];
    [_contentView addSubview:_cardImg];
    
    _store_taxImg = [[UIImageView alloc] init];
    [_store_taxImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_tax]];
    [_contentView addSubview:_store_taxImg];
    
    _store_institutionsImg = [[UIImageView alloc] init];
    [_store_institutionsImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_tax]];
    [_contentView addSubview:_store_institutionsImg];
    
    _seconBgView = [[UIView alloc] init];
    _seconBgView.backgroundColor = [UIColor getColor:@"e6e6e6"];
    [_contentView addSubview:_seconBgView];
    
    _storeAddressView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"商家地址"];
    [_contentView addSubview:_storeAddressView];
    
    _addressTF = [[UITextView alloc] init];
    _addressTF.text = self.model.address;
    _addressTF.textAlignment = NSTextAlignmentRight;
    _addressTF.delegate = self;
    _addressTF.font = [UIFont systemFontOfSize:14.0f];
    [_storeAddressView addSubview:_addressTF];
    
    _storePhoneView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"商家电话"];
    [_contentView addSubview:_storePhoneView];
    
    _phoneTF = [[UITextField alloc] init];
    _phoneTF.text = self.model.contacts_phones;
    _phoneTF.textAlignment = NSTextAlignmentRight;
    _phoneTF.font = [UIFont systemFontOfSize:14.0f];
    [_storePhoneView addSubview:_phoneTF];

    _storeTimeView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"营业时间"];
    [_contentView addSubview:_storeTimeView];
    
    _timeTF = [[UITextField alloc] init];
    _timeTF.text = self.model.store_workingtime;
    _timeTF.textAlignment = NSTextAlignmentRight;
    _timeTF.font = [UIFont systemFontOfSize:14.0f];
    _timeTF.delegate = self;
    [_storeTimeView addSubview:_timeTF];
    
    [self setSubView_layout];
}



- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if (textView == _detailStoreView) {
        if (textView.text.length + text.length > 300) {
            [self.view makeToast:@"最多只能输入300个字符" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
    }
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView{
    if (textView == _detailStoreView) {
        _jianjieSubLab.text = [NSString stringWithFormat:@"%ld/300",textView.text.length];
    }
    
}

#pragma mark -- 多选图片代理方法
- (void)checkLocalPhoto{
    
    TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:6-self.imageArr.count delegate:self];
    [imagePicker setSortAscendingByModificationDate:NO];
    imagePicker.isSelectOriginalPhoto = _isSelectOriginalPhoto;
    imagePicker.selectedAssets = _assestArray;
    imagePicker.allowPickingVideo = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
    
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    
    self.photosArray = [NSMutableArray arrayWithArray:photos];
    self.assestArray = [NSMutableArray arrayWithArray:assets];
    _isSelectOriginalPhoto = isSelectOriginalPhoto;
    [self calculateHeight:self.photosArray.count+self.imageArr.count];
    [self.collectView reloadData];
    
}

- (void)calculateHeight:(NSInteger)count{
    
    NSInteger minIndex;//需要多少行显示全部的item
    int yushu = (count + 1)%3;
    if (yushu > 0) {
        minIndex = (count + 1)/3 + 1;
    }else{
        minIndex = (count + 1)/3;
    }
    _collectView_H = ((screenW - 50)/3*0.85 + 5)*(minIndex - 1)+(screenW - 50)/3*0.85+20;
    [self.collectView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(_collectView_H));
    }];
    [self.view updateConstraintsIfNeeded];
    [self.view layoutIfNeeded];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    self.maxCount = self.photosArray.count+self.imageArr.count;
    if (self.maxCount == 6) {
        return self.maxCount;
    }
    return self.maxCount+1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    if (self.maxCount == 6) {
        cell.deleteButton.hidden = NO;
        if (indexPath.row < self.imageArr.count){
            [cell.imagev sd_setImageWithURL:[NSURL URLWithString:self.imageArr[indexPath.row]] placeholderImage:[UIImage imageNamed:@"moren"]];
        }else{
            cell.imagev.image = self.photosArray[indexPath.row - self.imageArr.count];
        }
    }else{
        if (indexPath.row == self.maxCount){
            cell.imagev.image = [UIImage imageNamed:@"tianjiatupian"];
            cell.deleteButton.hidden = YES;
        }else if (indexPath.row < self.imageArr.count){
            
            [cell.imagev sd_setImageWithURL:[NSURL URLWithString:self.imageArr[indexPath.row]] placeholderImage:[UIImage imageNamed:@"moren"]];
            cell.deleteButton.hidden = NO;
        }else{
            
            cell.imagev.image = self.photosArray[indexPath.row - self.imageArr.count];
            cell.deleteButton.hidden = NO;
        }
    }
    cell.deleteButton.tag = 100 + indexPath.row;
    [cell.deleteButton addTarget:self action:@selector(deletePhotos:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == self.photosArray.count + self.imageArr.count) {
        [self checkLocalPhoto];
    }else if (indexPath.row >= self.imageArr.count){
        TZImagePickerController *imagePickerVc = [[TZImagePickerController alloc] initWithSelectedAssets:self.assestArray selectedPhotos:self.photosArray index:indexPath.row - self.imageArr.count];
        imagePickerVc.isSelectOriginalPhoto = _isSelectOriginalPhoto;
        [imagePickerVc setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
            self.photosArray = [NSMutableArray arrayWithArray:photos];
            _assestArray = [NSMutableArray arrayWithArray:assets];
            _isSelectOriginalPhoto = isSelectOriginalPhoto;
            [self.collectView reloadData];
        }];
        [self presentViewController:imagePickerVc animated:YES completion:nil];
    }
}

- (void)deletePhotos:(UIButton *)sender{
    
    NSInteger index = sender.tag - 100;
    if (index < self.imageArr.count) {
        [self.imageArr removeObjectAtIndex:index];
    }else{
        [self.photosArray removeObjectAtIndex:index - self.imageArr.count];
        [self.assestArray removeObjectAtIndex:index - self.imageArr.count];
    }
    
    NSInteger temp = self.photosArray.count + self.imageArr.count;
    if (temp == 5) {
        [self calculateHeight:temp];
        [self.collectView reloadData];
    }else{
        [self.collectView performBatchUpdates:^{
            NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
            [self.collectView deleteItemsAtIndexPaths:@[indexPath]];
        } completion:^(BOOL finished) {
            [self calculateHeight:_photosArray.count+self.imageArr.count];
            [self.collectView reloadData];
        }];
    }
}

- (void)didComfirmClick:(NSString *)time{
    
    _timeTF.text = time;
}




#pragma mark -- 上传图片
- (void)upDataImage{
    
    dispatch_group_t group = dispatch_group_create();
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    for (UIImage *image in self.photosArray) {
        dispatch_group_enter(group);
        NSString *url = [imgBaseUrl stringByAppendingString:@"upimgs/goods-up-iphone"];
        NSData *data = UIImageJPEGRepresentation(image, 1.0f);
        NSString *imageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        NSDictionary *body = @{@"goods_imgs":imageStr};
        [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                BOOL success = [rootDict[@"status"] boolValue];
                if (success) {
                    NSString *url = [self isNil:rootDict[@"data"][@"file"]];
                    [self.imagePath addObject:url];
                }
                else {
                    [self.view makeToast:rootDict[@"msg"] duration:1.5 position:CSToastPositionCenter];
                }
            }
            dispatch_group_leave(group);
        } failure:^(NSError *error) {
            dispatch_group_leave(group);
        }];
    }
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (self.imagePath.count == self.photosArray.count) {
            [self.imagePath addObjectsFromArray:self.imageArr];
            [self editStoreInfo];
        }else{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self.view makeToast:@"上传图片失败" duration:1.5 position:CSToastPositionCenter];
        }
        
    });
}

- (void)editStoreInfo{
    
    NSString *url = [baseUrl stringByAppendingString:@"store/edit-business-store"];
    NSString *store_id = @"15269885400738030000";
    NSString *store_tag = [self.headerView.titleArr componentsJoinedByString:@","];
    NSString *store_environment = [self.imagePath componentsJoinedByString:@";"];
    NSDictionary *body = @{@"store_id":store_id,
                           @"store_tag":store_tag,
                           @"store_environment":store_environment,
                           @"store_description":_detailStoreView.text,
                           @"store_workingtime":_timeTF.text,};
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (void)setSubView_layout{
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_scrollView);
        make.width.equalTo(_scrollView);
        make.height.greaterThanOrEqualTo(@0.f);
    }];
    
    [_huanjingTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(self.headerView.mas_bottom).offset(15);
    }];
    
    [_huanjingSubLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(_huanjingTitleLab.mas_bottom).offset(10);
    }];
    
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.top.equalTo(_huanjingSubLab.mas_bottom).offset(10);
        make.right.equalTo(_contentView);
        make.height.equalTo(@(_collectView_H));
    }];
    
    [_jianjieTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(self.collectView.mas_bottom).offset(5);
    }];
    
    [_jianjieSubLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(_jianjieTitleLab.mas_bottom).offset(5);
    }];
    
    [_detailStoreView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(10);
        make.top.equalTo(_jianjieSubLab.mas_bottom).offset(12);
        make.width.equalTo(@(screenW - 20));
        make.height.equalTo(@60);
    }];
    _detailStoreView.layer.borderWidth = 1.0;
    _detailStoreView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [_firstBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.right.equalTo(_contentView);
        make.top.equalTo(_detailStoreView.mas_bottom).offset(15);
        make.height.equalTo(@8);
    }];
    
    
    [_cardTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_firstBgView.mas_bottom).offset(15);
        make.left.equalTo(_contentView).offset(15);
    }];
    
    [_cardSubLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardTitleLab.mas_bottom).offset(10);
        make.left.equalTo(_contentView).offset(15);
    }];
    
    [_cardImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardSubLab.mas_bottom).offset(10);
        make.left.equalTo(_contentView).offset(15);
        make.width.equalTo(@(screenW/4));
        make.height.equalTo(@(screenW/4));
    }];
    
    [_store_taxImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardImg);
        make.left.equalTo(_cardImg.mas_right).offset(10);
    }];
    
    [_store_institutionsImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardImg);
        make.left.equalTo(_store_taxImg.mas_right).offset(10);
    }];
    
    [_seconBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.right.equalTo(_contentView);
        make.top.equalTo(_cardImg.mas_bottom).offset(15);
        make.height.equalTo(@8);
    }];
   
    [_storeAddressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_seconBgView.mas_bottom).offset(10);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
    }];
    
    [_addressTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_storeAddressView).offset(-10);
        make.centerY.equalTo(_storeAddressView);
        make.width.equalTo(@200);
        make.height.equalTo(@40);
    }];
    
    [_storePhoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_storeAddressView.mas_bottom).offset(5);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
    }];
    
    [_phoneTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_storePhoneView).offset(-10);
        make.centerY.equalTo(_storePhoneView);
        make.width.equalTo(@200);
        make.height.equalTo(@20);
    }];
    
    [_storeTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_storePhoneView.mas_bottom).offset(5);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
        make.bottom.equalTo(_contentView).offset(-50);
    }];
    
    [_timeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_storeTimeView);
        make.right.equalTo(_storeTimeView).offset(-10);
    }];
}
- (NSString *)isNil:(NSString *)obj {
    
    return obj? obj:@"";
}

@end
