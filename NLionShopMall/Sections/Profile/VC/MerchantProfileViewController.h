//
//  MerchantProfileViewController.h
//  NLionShopMall
//
//  Created by apple on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StoreModel.h"

@interface MerchantProfileViewController : UIViewController

@property (nonatomic ,strong) StoreModel *model;

@end
