//
//  ActivitySetingViewController.h
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ActivitySetingViewControllerType) {
    FullActivityControllerType  = 0,    //满减活动
    DiscountActivityControllerType = 1,   //折扣活动
};

@interface ActivitySetingViewController : UIViewController

@property (nonatomic, assign) ActivitySetingViewControllerType VCType;
@end
