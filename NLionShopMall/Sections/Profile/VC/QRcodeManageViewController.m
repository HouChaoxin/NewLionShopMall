//
//  QRcodeManageViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "QRcodeManageViewController.h"
#import "QRcodeViewController.h"
#import "SubTableViewCell.h"

@interface QRcodeManageViewController ()

@property (nonatomic ,strong) NSArray *dataArr;
@end

@implementation QRcodeManageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle];
    self.navigationItem.title = @"二维码管理";
    self.dataArr = @[@{@"title":@"店铺二维码",@"subTitle":@"微信扫码关注店铺小程序"},
                     @{@"title":@"收款二维码",@"subTitle":@"扫码收款"}];
    self.tableView.rowHeight = 65;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.tableView registerClass:[SubTableViewCell class] forCellReuseIdentifier:NSStringFromClass([SubTableViewCell class])];
    self.tableView.tableFooterView = [UIView new];
}

- (void)setNavTitle{
    
    self.navigationItem.title = @"二维码管理";
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SubTableViewCell class]) forIndexPath:indexPath];
    cell.titleLab.text = self.dataArr[indexPath.row][@"title"];
    cell.subLabel.text = self.dataArr[indexPath.row][@"subTitle"];
    cell.subLabel.font = [UIFont systemFontOfSize:14];
    cell.subLabel.textColor = [UIColor getColor:@"999999"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    QRcodeViewController *vc = [[QRcodeViewController alloc] init];
    if (indexPath.row == 0) {
        vc.VCType = StoreControllerType;
    }else{
        vc.VCType = ReceivablesControllerType;
    }
    [self.navigationController pushViewController:vc animated:YES];
    
}


@end
