//
//  EditMerchantViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MerchantProfileViewController.h"
#import "EditMerchantViewController.h"
#import "StoreHeaderView.h"
#import "CollectionViewCell.h"
#import "BaseBackgroundView.h"


@interface MerchantProfileViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    CGFloat _collectView_H;
    UIView *_contentView;
    UILabel *_huanjingLab;
    UILabel *_jianjieLab;
    UILabel *_jianjieSubLab;
    UILabel *_cardTileLab;
    UIImageView *_cardImg;
    UIImageView *_store_taxImg;
    UIImageView *_store_institutionsImg;
    BaseBackgroundView *_storeAddressView;
    BaseBackgroundView *_storePhoneView;
    BaseBackgroundView *_storeTimeView;
    
    UIView *_firstBgView;
    UIView *_seconBgView;

}
@property (nonatomic ,strong) UIScrollView *scrollView;
@property (nonatomic ,strong) StoreHeaderView *headerView;
@property (nonatomic ,strong) UICollectionView *collectView;
@property (nonatomic ,strong) NSMutableArray *imageArr;//模型图片
@end

@implementation MerchantProfileViewController


-(UICollectionView *)collectView{
    if (!_collectView) {
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc] init];
        flowLayOut.itemSize = CGSizeMake((screenW - 50)/3, 0.85*(screenW - 50)/3);
        flowLayOut.sectionInset = UIEdgeInsetsMake(10, 15, 10, 15);
        flowLayOut.minimumLineSpacing = 5;
        flowLayOut.minimumInteritemSpacing = 5;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayOut];
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.delegate = self;
        _collectView.dataSource = self;
        [_collectView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _collectView.scrollEnabled = NO;
    }
    return _collectView;
}

-(StoreHeaderView *)headerView{
    if (!_headerView) {
        _headerView = [[StoreHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenW, 116)];
        _headerView.addButton.hidden = YES;
        _headerView.model = self.model;
    }
    return _headerView;
}

-(UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
        _scrollView.backgroundColor = [UIColor whiteColor];
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setNavView];
    NSArray *tempArr = [self.model.store_environment componentsSeparatedByString:@";"];
    self.imageArr = [NSMutableArray arrayWithArray:tempArr];
    [self calculateHeight:self.imageArr.count];
    [self creatSubView];
}

- (void)setNavView{
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:
     [UIImage imageNamed:@"mainBackground"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.title = @"商家详情";
    UIButton *button = [[UIButton alloc] init];
    [button setTitle:@"编辑" forState:0];
    button.titleLabel.font = [UIFont systemFontOfSize:15];
    [button addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [button sizeToFit];
    UIBarButtonItem *rightBtn = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightBtn;
    
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
}
- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightAction{
    
    EditMerchantViewController *vc = [[EditMerchantViewController alloc] init];
    vc.model = self.model;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)creatSubView{
    
    [self.view addSubview:self.scrollView];
    _contentView = [[UIView alloc] init];
    [self.scrollView addSubview:_contentView];
    [_contentView addSubview:self.headerView];
    
    _huanjingLab = [[UILabel alloc] init];
    _huanjingLab.text = @"商家环境";
    _huanjingLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_huanjingLab];
    
    [_contentView addSubview:self.collectView];
    
    _jianjieLab = [[UILabel alloc] init];
    _jianjieLab.text = @"商家简介";
    _jianjieLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_jianjieLab];
    
    _jianjieSubLab = [[UILabel alloc] init];
    _jianjieSubLab.font = [UIFont systemFontOfSize:13];
    _jianjieSubLab.text = self.model.store_description;
    _jianjieSubLab.numberOfLines = 0;
    [_contentView addSubview:_jianjieSubLab];
    
    _firstBgView = [[UIView alloc] init];
    _firstBgView.backgroundColor = [UIColor getColor:@"e6e6e6"];
    [_contentView addSubview:_firstBgView];
    
    _cardTileLab = [[UILabel alloc] init];
    _cardTileLab.text = @"营业资质";
    _cardTileLab.font = [UIFont boldSystemFontOfSize:17];
    [_contentView addSubview:_cardTileLab];
    
    _cardImg = [[UIImageView alloc] init];
    [_cardImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_licence] placeholderImage:[UIImage imageNamed:@"Image_demo"]];
    [_contentView addSubview:_cardImg];
    
    _store_taxImg = [[UIImageView alloc] init];
    [_store_taxImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_tax]];
    [_contentView addSubview:_store_taxImg];
    
    _store_institutionsImg = [[UIImageView alloc] init];
    [_store_institutionsImg sd_setImageWithURL:[NSURL URLWithString:self.model.store_tax]];
    [_contentView addSubview:_store_institutionsImg];
    
    _seconBgView = [[UIView alloc] init];
    _seconBgView.backgroundColor = [UIColor getColor:@"e6e6e6"];
    [_contentView addSubview:_seconBgView];
    
    _storeAddressView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"商家地址"];
    _storeAddressView.subLabel.text = self.model.address;
    [_contentView addSubview:_storeAddressView];
    
    _storePhoneView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"商家电话"];
    _storePhoneView.subLabel.text = self.model.contacts_phones;
    [_contentView addSubview:_storePhoneView];
    
    _storeTimeView = [[BaseBackgroundView alloc] initWithFrame:CGRectZero setTitle:@"营业时间"];
    _storeTimeView.subLabel.text = self.model.store_workingtime;
    [_contentView addSubview:_storeTimeView];
  
    [self setSubView_layout];
}

- (void)calculateHeight:(NSInteger)count{
    NSInteger minIndex;//需要多少行显示全部的item
    int yushu = count%3;
    if (yushu > 0) {
        minIndex = count/4 + 1;
    }else{
        minIndex = count/4;
    }
    _collectView_H = ((screenW - 50)/3*0.85 + 5)*(minIndex - 1)+(screenW - 50)/3*0.85+20;
    [self.collectView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(_collectView_H));
    }];
    [self.view updateConstraintsIfNeeded];
    [self.view layoutIfNeeded];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.imageArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.deleteButton.hidden = YES;
    [cell.imagev sd_setImageWithURL:[NSURL URLWithString:self.imageArr[indexPath.row]] placeholderImage:[UIImage imageNamed:@"Image_demo"]];
//    cell.imagev.image = [UIImage imageNamed:@"Image_demo"];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)setSubView_layout{
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    [_contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(_scrollView);
        make.width.equalTo(_scrollView);
        make.height.greaterThanOrEqualTo(@0.f);
    }];
    
    [_huanjingLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(self.headerView.mas_bottom).offset(15);
    }];
    
    [self.collectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.top.equalTo(_huanjingLab.mas_bottom).offset(5);
        make.right.equalTo(_contentView);
        make.height.equalTo(@(_collectView_H));
    }];
    
    [_jianjieLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(self.collectView.mas_bottom).offset(5);
    }];
    
    [_jianjieSubLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView).offset(15);
        make.top.equalTo(_jianjieLab.mas_bottom).offset(10);
        make.right.equalTo(_contentView).offset(-15);
    }];
  
    [_firstBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.right.equalTo(_contentView);
        make.top.equalTo(_jianjieSubLab.mas_bottom).offset(15);
        make.height.equalTo(@8);
    }];
    
    [_cardTileLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_firstBgView.mas_bottom).offset(10);
        make.left.equalTo(_contentView).offset(15);
    }];
    
    [_cardImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardTileLab.mas_bottom).offset(10);
        make.left.equalTo(_contentView).offset(15);
        make.width.equalTo(@(screenW/4));
        make.height.equalTo(@(screenW/4));
    }];
    
    [_store_taxImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardImg);
        make.left.equalTo(_cardImg.mas_right).offset(10);
    }];
    
    [_store_institutionsImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_cardImg);
        make.left.equalTo(_store_taxImg.mas_right).offset(10);
    }];
    
    
    [_seconBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_contentView);
        make.right.equalTo(_contentView);
        make.top.equalTo(_cardImg.mas_bottom).offset(15);
        make.height.equalTo(@8);
    }];
    
    [_storeAddressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_seconBgView.mas_bottom).offset(10);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
    }];
  
    [_storePhoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_storeAddressView.mas_bottom).offset(5);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
    }];
    
    [_storeTimeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_storePhoneView.mas_bottom).offset(5);
        make.left.equalTo(_contentView);
        make.width.equalTo(@(screenW));
        make.height.equalTo(@60);
        make.bottom.equalTo(_contentView).offset(-50);
    }];
}
- (NSString *)isNil:(NSString *)obj {
    
    return obj? obj:@"";
}

@end
