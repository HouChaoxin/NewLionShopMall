//
//  QRcodeViewController.h
//  NLionShopMall
//
//  Created by apple on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, QRcedeViewControllerType) {
    StoreControllerType  = 0,        //商铺二维码
    ReceivablesControllerType = 1,   //
};

@interface QRcodeViewController : UIViewController

@property (nonatomic, assign) QRcedeViewControllerType VCType;

@end
