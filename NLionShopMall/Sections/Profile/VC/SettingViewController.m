//
//  SettingViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "SettingViewController.h"
#import "MemberListViewController.h"
#import "OrderManageListViewController.h"
#import "SubTableViewCell.h"
#import "HLoginVC.h"

@interface SettingViewController ()

@property (nonatomic ,strong) NSArray *dataArr;
@property (nonatomic ,strong) UIButton *nextBtn;
@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle];
    self.dataArr = @[@"关于我们",@"用户协议",@"意见反馈"];
    self.tableView.rowHeight = 65;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.sectionFooterHeight = 100;
    self.tableView.sectionHeaderHeight = 0;
    [self.tableView registerClass:[SubTableViewCell class] forCellReuseIdentifier:NSStringFromClass([SubTableViewCell class])];
}

- (void)setNavTitle{
    self.navigationItem.title = @"设置";
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SubTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SubTableViewCell class]) forIndexPath:indexPath];
    cell.titleLab.text = self.dataArr[indexPath.row];
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, screenW, 100)];
    self.nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.nextBtn.frame = CGRectMake(screenW/2 - 80, 40, 160, 40);
    self.nextBtn.layer.cornerRadius = 5.0f;
    self.nextBtn.layer.masksToBounds = YES;
    [self.nextBtn setTitle:@"退出登录" forState:0];
    self.nextBtn.backgroundColor = mainColor;
    [self.nextBtn addTarget:self action:@selector(exitLogon) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.nextBtn];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        MemberListViewController *vc = [[MemberListViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 1){
        OrderManageListViewController *vc = [[OrderManageListViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark -- 退出登录
- (void)exitLogon{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMemberId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kStoreName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMainStoreId];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kMemberToken];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kGuLiJinNum];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserName];
    
    HLoginVC *loginVC = [[HLoginVC alloc] init];
    UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:loginVC];
    [UIApplication sharedApplication].keyWindow.rootViewController = nv;
}



@end
