//
//  QRcodeViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "QRcodeViewController.h"
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDKUI.h>
@interface QRcodeViewController ()
{
    UIImageView *_QRimageView;
    UILabel     *_titleLab;
}
@end

@implementation QRcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavTitle];
    [self setSubView];
    
    NSString *store_id = @"15249077146730960000";
    NSString *string = [NSString stringWithFormat:@"m2.lion-mall.com/yfpay/yfpayurl?storeid=%@",store_id];
    [self generatorQRCodeWithValue:string];
}

- (void)setSubView {
    
    _titleLab = [[UILabel alloc] init];
    if (self.VCType == 0) {
        _titleLab.text = @"扫码关注商户店铺二维码";
    }else{
        _titleLab.text = @"扫码付款";
    }
    [self.view addSubview:_titleLab];
    
    _QRimageView = [[UIImageView alloc] init];
    _QRimageView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_QRimageView];
    [self setSubView_layout];
}

- (void)setSubView_layout{
    [_QRimageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.width.equalTo(@200);
        make.height.equalTo(@200);
    }];
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(_QRimageView.mas_bottom).offset(20);
    }];
}


- (void)setNavTitle{
    
    if (self.VCType == 0) {
        self.navigationItem.title = @"商铺二维码";
    }else{
        self.navigationItem.title = @"商铺收款二维码";
    }
    self.view.backgroundColor = [UIColor whiteColor];
    UIButton *leftBtn = [[UIButton alloc] init];
    [leftBtn setImage:[UIImage imageNamed:@"back"] forState:0];
    [leftBtn addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn sizeToFit];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [[UIButton alloc] init];
    [rightBtn setTitle:@"分享" forState:0];
    [rightBtn addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn sizeToFit];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)leftAction{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)rightAction{
    
    [self shareFountion];
}

- (void)generatorQRCodeWithValue:(NSString *)value
{
    
    CIFilter *fiter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    [fiter setDefaults];
    
    // 设置输入数据
    NSString *inputData = value;
    NSData *data = [inputData dataUsingEncoding:NSUTF8StringEncoding];
    [fiter setValue:data forKeyPath:@"inputMessage"];
    
    
    CIImage *outputImage = [fiter outputImage];
    UIImage *qrCodeImage = [self createNonInterpolatedUIImageFormCIImage:outputImage withSize:200];
    
    //设置中间小图片
//    NSString *imgUrl = [picBaseUrl stringByAppendingString:_dic[@"member_avatar"]];
//    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
//    UIImage *logoImage =[UIImage imageWithData:imgData];//imageNamed:@"logo"
    UIImage *logoImage = [UIImage imageNamed:@"logo"];
    
    CGFloat logoW = logoImage.size.width;
    CGFloat logoH = logoImage.size.height;
    CGFloat boardW = 5;
    CGRect bigRect = CGRectMake(0, 0, logoW + boardW * 2, logoH + boardW * 2);
    CGRect smallRect = CGRectMake(boardW, boardW, logoImage.size.width, logoImage.size.height);
    
    UIGraphicsBeginImageContextWithOptions(bigRect.size, 0, [UIScreen mainScreen].scale);
    
    // 大圆填充
    UIBezierPath *bigPath = [UIBezierPath bezierPathWithRoundedRect:bigRect cornerRadius:10];
    [[UIColor whiteColor] set];
    [bigPath fill];
    
    // 小圆剪切
    UIBezierPath *smallPath = [UIBezierPath bezierPathWithRoundedRect:smallRect cornerRadius:10];
    [smallPath addClip];
    
    [logoImage drawInRect:smallRect];
    logoImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(qrCodeImage.size, NO, [UIScreen mainScreen].scale);
    [qrCodeImage drawAtPoint:CGPointZero];
    [logoImage drawInRect:CGRectMake(85, 85, 30, 30)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    _QRimageView.image = newImage;
    
}


/**
 *  根据CIImage生成指定大小的UIImage
 *
 *  @param image CIImage
 *  @param size  图片宽度
 */
- (UIImage *)createNonInterpolatedUIImageFormCIImage:(CIImage *)image withSize:(CGFloat) size
{
    CGRect extent = CGRectIntegral(image.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    
    // 1.创建bitmap;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:image fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    
    // 2.保存bitmap到图片
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}


#pragma mark -- 分享功能
- (void)shareFountion{
    NSArray* imageArray = @[[UIImage imageNamed:@"ic_nanxing"]];
    //    （注意：图片必须要在Xcode左边目录里面，名称必须要传正确，如果要分享网络图片，可以这样传image参数 images:@[@"http://mob.com/Assets/images/logo.png?v=20150320"]）
    if (imageArray) {
        
        NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
        [shareParams SSDKSetupShareParamsByText:@"分享内容"
                                         images:imageArray
                                            url:[NSURL URLWithString:@"http://mob.com"]
                                          title:@"分享标题"
                                           type:SSDKContentTypeAuto];
        //有的平台要客户端分享需要加此方法，例如微博
        [shareParams SSDKEnableUseClientShare];
        //2、分享（可以弹出我们的分享菜单和编辑界面）
        [ShareSDK showShareActionSheet:nil //要显示菜单的视图, iPad版中此参数作为弹出菜单的参照视图，只有传这个才可以弹出我们的分享菜单，可以传分享的按钮对象或者自己创建小的view 对象，iPhone可以传nil不会影响
                                 items:nil
                           shareParams:shareParams
                   onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                       
                       switch (state) {
                           case SSDKResponseStateSuccess:
                           {
                               UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"分享成功"
                                                                                   message:nil
                                                                                  delegate:nil
                                                                         cancelButtonTitle:@"确定"
                                                                         otherButtonTitles:nil];
                               [alertView show];
                               break;
                           }
                           case SSDKResponseStateFail:
                           {
                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"分享失败"
                                                                               message:[NSString stringWithFormat:@"%@",error]
                                                                              delegate:nil
                                                                     cancelButtonTitle:@"OK"
                                                                     otherButtonTitles:nil, nil];
                               [alert show];
                               break;
                           }
                           default:
                               break;
                       }
                   }
         ];}
}
@end
