//
//  LinkTool.h
//  NLionShopMall
//
//  Created by apple on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LinkTool : NSObject

/*
 横虚线
 */
-(void)drawHorDashLine:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor;

/*
 竖虚线
 */
-(void)drawVerDashLine:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor;

/*
 计算一段字符串的长度
 */
-(CGFloat)CalculationWith:(NSString *)str;

@end
