//
//  GuLiJinDetailsVC.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GuLiJinDetailsVC.h"
#import "GLJDeatilCell.h"
#import "GLJDeatilSection.h"
#import "GLJDetailVCModel.h"

static NSString *cellIdentify = @"cellIdentify";
static NSString *sectionIdentify = @"sectionIdentify";

@interface GuLiJinDetailsVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView_;
@property (nonatomic,strong) GLJDetailVCModel *vcModel;
@property (nonatomic,assign) NSInteger pageNum;

@end

@implementation GuLiJinDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _pageNum = 1;
    _vcModel = [[GLJDetailVCModel alloc] init];
}

- (void)requestData {
    __weak typeof(self) weakSelf = self;
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSString stringWithFormat:@"%ld",_pageNum] forKey:@"page"];
    [dict setObject:@"20" forKey:@"pagesize"];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kMemberId] forKey:@"member_id"];
    [dict setObject:@"2" forKey:@"type"];
    [_vcModel getGuLiJinDetailRecordWithPara:dict success:^(id responseObject) {
        
        [[FSProgressViewTool shareInstance] removeProgressView];
        [[ErrorView shareInstance] removeErrorView];
        [[NoDataView shareInstance] removeNoDataView];
        [weakSelf.tableView_.mj_header endRefreshing];
        [weakSelf.tableView_.mj_footer endRefreshing];
        
        if (weakSelf.vcModel.guLiJinListAry.count == 0) {
            [[NoDataView shareInstance] showNoDataView:weakSelf.view];
        }
        [weakSelf.tableView_ reloadData];
        
    } failure:^(NSError *error) {
//        [self.view makeToast:@"鼓励金明细加载失败，请刷新重试" duration:1.5 position:CSToastPositionCenter];
        [[FSProgressViewTool shareInstance] removeProgressView];
        [[ErrorView shareInstance] removeErrorView];
        [[NoDataView shareInstance] removeNoDataView];
        [weakSelf.tableView_.mj_header endRefreshing];
        [weakSelf.tableView_.mj_footer endRefreshing];
        
        if (weakSelf.vcModel.guLiJinListAry.count == 0) {
            [[ErrorView shareInstance] showErrorView:self.view reloadAction:^{
                [weakSelf requestData];
            }];
        }
    }];
}

- (void)initView {
    
    [self setNavigationBarTitleViewWithTitle:@"鼓励金明细"];
    [self hideBackButtonTitle];
    
    self.tableView_ = [self creatTableView];
    self.tableView_.frame = CGRectMake(0, 0, screenW, screenH-64);
    [self.view addSubview:self.tableView_];
    [self addRefreshFunction];
}

- (UITableView *)creatTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = kWhiteColor;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView registerClass:[GLJDeatilCell class] forCellReuseIdentifier:cellIdentify];
    [tableView registerClass:[GLJDeatilSection class] forHeaderFooterViewReuseIdentifier:sectionIdentify];
    return tableView;
}


#pragma mark - 上拉下拉刷新数据
- (void)addRefreshFunction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefreshData)];
    self.tableView_.mj_header = header;
    self.tableView_.mj_footer = footer;
    
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松手刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新数据" forState:MJRefreshStateRefreshing];
    
    [footer setTitle:@"上拉刷新" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载数据" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"松开加载更多数据" forState:MJRefreshStatePulling];
    [footer setTitle:@"无数据" forState:MJRefreshStateNoMoreData];
}

- (void)upRefreshData{
    _pageNum++;
    if(_pageNum <= _vcModel.totalPageNum) {
        [self requestData];
    }else{
        [self.tableView_.mj_footer endRefreshing];
    }
}

- (void)downRefreshData {
    if (_vcModel.guLiJinListAry) {
        [_vcModel.guLiJinListAry removeAllObjects];
    }
    _pageNum = 1;
    [self requestData];
}


#pragma mark - <UITableViewDelegate,UITableViewDataSource>

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    GLJDeatilSection *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentify];
    return sectionView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _vcModel.guLiJinListAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 54;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GLJDeatilCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    GLJDetailListModel *model = [_vcModel.guLiJinListAry objectAtIndex:indexPath.row];
    [cell refreshCellWithModel:model];
    return cell;
}

@end
