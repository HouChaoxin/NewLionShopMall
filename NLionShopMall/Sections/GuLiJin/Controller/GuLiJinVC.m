//
//  MyWalletVC.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/24.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GuLiJinVC.h"
#import "GuLiJinCashVC.h"
#import "GuLiJinDetailsVC.h"
#import "MyWalletVCModel.h"
#import "GuLiJinRuleVC.h"
#import "GuLiJinHeaderView.h"
#import "GuLiJinRecordCell.h"
#import "GuLiJinSection.h"
#import "GLJDetailModel.h"

static NSString *cellIdentify = @"cellIdentify";
static NSString *sectionIdentify = @"sectionIdentify";

@interface GuLiJinVC ()<UITableViewDelegate,UITableViewDataSource>

//@property (nonatomic,strong) UILabel *guLiJinNumLbl;
@property (nonatomic,strong) MyWalletVCModel *vcModel;
@property (nonatomic,strong) UITableView *tableView_;
@property (nonatomic,strong) GuLiJinHeaderView *headView;
@property (nonatomic,assign) NSInteger pageNum;

@end

@implementation GuLiJinVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    UIImage *image = [[ColorConverter colorWithHexString:@"0x58A4F8"] toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:image];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    UIImage *image = [kWhiteColor toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _pageNum = 1;
    _vcModel = [[MyWalletVCModel alloc] init];
}

- (void)requestData {
    __weak typeof(self) weakSelf = self;
    
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    NSString *memberId = [[NSUserDefaults standardUserDefaults] objectForKey:kMemberId];
    if (!memberId || [memberId isEqualToString:@""]) return;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)_pageNum] forKey:@"page"];
    [dict setObject:@"20" forKey:@"pagesize"];
    [dict setObject:memberId forKey:@"member_id"];
    [dict setObject:@"2" forKey:@"type"];
    
    [_vcModel getGuLiJinDetailRecordWithPara:dict success:^(id responseObject) {
        
        [[FSProgressViewTool shareInstance] removeProgressView];
        [weakSelf.tableView_.mj_header endRefreshing];
        [weakSelf.tableView_.mj_footer endRefreshing];
        [weakSelf.headView refreshViewWithNumStr:weakSelf.vcModel.walletModel.cash_amount];
        [weakSelf.tableView_ reloadData];
    } failure:^(NSError *error) {
        
        [weakSelf.tableView_.mj_header endRefreshing];
        [weakSelf.tableView_.mj_footer endRefreshing];
        [[FSProgressViewTool shareInstance] removeProgressView];
    }];
}

- (void)createNavigationBarRightBtn {
    UIButton *rightBtn = [FSViewHelper createBtnWithTitle:@"提现"
                                                  bgColor:[UIColor clearColor]
                                               titleColor:kWhiteColor];
    [rightBtn addTarget:self action:@selector(navigationRightBtnClick) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [self.navigationItem setRightBarButtonItem:rightButton];
}

- (void)navigationRightBtnClick {
    GuLiJinCashVC *vc = [[GuLiJinCashVC alloc] init];
    vc.guLiJinNum = _vcModel.walletModel.cash_amount;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)setNavigationBarTitleViewWithTitle:(NSString *)title{
    UILabel * titleView = [UILabel new];
    titleView.text = title;
    [titleView setTextColor:kWhiteColor];
    [titleView setFont:[UIFont systemFontOfSize:20]];
    [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
}

- (void)hideBackButtonTitle {
    UIView * itmeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 13,17, 17)];
    imageView.image = [UIImage imageNamed:@"ic_fanhui"];
    [itmeView addSubview:imageView];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popView)];
    [itmeView addGestureRecognizer:tap];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:itmeView];
    [self.navigationItem setLeftBarButtonItem:leftButton];
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"可提现鼓励金"];
    [self hideBackButtonTitle];
    [self createNavigationBarRightBtn];
    
    __weak typeof(self) weakSelf = self;
    _headView = [[GuLiJinHeaderView alloc] initWithFrame:CGRectMake(0, 0, screenW, 134)];
    _headView.ruleTapAction = ^{
        GuLiJinRuleVC *vc = [[GuLiJinRuleVC alloc] init];
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    
    self.tableView_ = [self creatTableView];
    self.tableView_.frame = CGRectMake(0, 0, screenW, screenH-64);
    [self.view addSubview:self.tableView_];
    self.tableView_.tableHeaderView = _headView;
    [self addRefreshFunction];
}

- (UITableView *)creatTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = kWhiteColor;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView registerClass:[GuLiJinRecordCell class] forCellReuseIdentifier:cellIdentify];
    [tableView registerClass:[GuLiJinSection class] forHeaderFooterViewReuseIdentifier:sectionIdentify];
    return tableView;
}


#pragma mark - 上拉下拉刷新数据
- (void)addRefreshFunction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefreshData)];
    self.tableView_.mj_header = header;
    self.tableView_.mj_footer = footer;
    
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松手刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新数据" forState:MJRefreshStateRefreshing];
    
    [footer setTitle:@"上拉刷新" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载数据" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"松开加载更多数据" forState:MJRefreshStatePulling];
    [footer setTitle:@"无数据" forState:MJRefreshStateNoMoreData];
}

- (void)upRefreshData{
    _pageNum++;
    if(_pageNum <= _vcModel.totalPageNum) {
        [self requestData];
    }else{
        [self.tableView_.mj_footer endRefreshing];
    }
}

- (void)downRefreshData {
    if (_vcModel.guLiJinListAry) {
        [_vcModel.guLiJinListAry removeAllObjects];
    }
    _pageNum = 1;
    [self requestData];
}


#pragma mark - <UITableViewDelegate,UITableViewDataSource>

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 46;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    GuLiJinSection *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentify];
    return sectionView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _vcModel.guLiJinListAry.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 66;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    GuLiJinRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    GLJDetailListModel *model = [_vcModel.guLiJinListAry objectAtIndex:indexPath.row];
    [cell refreshCellWithModel:model];
    return cell;
}

//- (void)mingXiBtnAction:(UIButton *)sender {
//    NSLog(@"明细被点击");
//    GuLiJinDetailsVC *detailVC = [[GuLiJinDetailsVC alloc] init];
//    [self.navigationController pushViewController:detailVC animated:YES];
//}

@end
