//
//  AddBCVC.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "AddBCVC.h"
#import "BCCell.h"
#import "UIView+Toast.h"
#import "AddBCVCModel.h"

static NSInteger count;
@interface AddBCVC ()<UITextFieldDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;

@property (nonatomic,strong) BCCell *nameCell;
@property (nonatomic,strong) BCCell *bankNumCell;
@property (nonatomic,strong) BCCell *IDCardCell;
@property (nonatomic,strong) BCCell *bankNameCell;
@property (nonatomic,strong) BCCell *phoneNumCell;
//@property (nonatomic,strong) BCCell *imgCodeCell;
@property (nonatomic,strong) BCCell *codeCell;

@property (nonatomic,strong) UIButton *nextBtn;

@property (nonatomic, copy) NSString *bankLogo;
@property (nonatomic,copy) AddBCVCModel *vcModel;
@property (nonatomic,strong) NSTimer *timer;
@end

@implementation AddBCVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _vcModel = [[AddBCVCModel alloc] init];
}

- (void)requestData {
    [self getYanZhengMaPic];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"添加银行卡"];
    [self hideBackButtonTitle];
    
    _scrollView = [[UIScrollView alloc] init];
    _scrollView.frame = CGRectMake(0, 0, screenW, screenH);
    _scrollView.backgroundColor = kWhiteColor;
    [self.view addSubview:_scrollView];
    
    UIView *layoutView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH)];
    layoutView.backgroundColor = kWhiteColor;
    [_scrollView addSubview:layoutView];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"请绑定银行卡"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [layoutView addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(layoutView).offset(16);
        make.left.equalTo(layoutView).offset(12);
        make.right.equalTo(layoutView).offset(-12);
        make.height.equalTo(@25);
    }];
    
    //bgView的布局
    UIImageView *bgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"juxing"]
                                                     bgColor:kWhiteColor];
    bgView.userInteractionEnabled = YES;
    [layoutView addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@390);
        make.left.equalTo(layoutView).offset(8);
        make.right.equalTo(layoutView).offset(-8);
        make.top.equalTo(titleLbl.mas_bottom).offset(16);
    }];
    
    _nameCell = [[BCCell alloc] initCellWithTitle:@"持卡人"
                                      placeHolder:@"请输入持卡人姓名"
                                             type:DefaultType];
//    _nameCell.textField.userInteractionEnabled = NO;
//    _nameCell.textField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kUserName];
    _nameCell.textField.delegate = self;
    [bgView addSubview:_nameCell];
    [_nameCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(bgView).offset(4);
        make.right.equalTo(bgView).offset(-4);
        make.height.equalTo(@54);
    }];
    
    UIButton *alertBtn = [FSViewHelper createBtnWithTitle:@""
                                                  bgColor:kWhiteColor
                                               titleColor:kWhiteColor];
    [alertBtn setBackgroundImage:[UIImage imageNamed:@"ic_shuoming"] forState:UIControlStateNormal];
    [_nameCell addSubview:alertBtn];
    [alertBtn addTarget:self action:@selector(alertBtnClick:) forControlEvents:UIControlEventTouchDown];
    [alertBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_nameCell).offset(-6);
        make.centerY.equalTo(_nameCell);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(17, 17)]);
    }];
    
    _bankNumCell = [[BCCell alloc] initCellWithTitle:@"卡号"
                                         placeHolder:@"请输入卡号"
                                                type:DefaultType];
    _bankNumCell.textField.delegate = self;
    _bankNumCell.textField.tag = 1001;
    [bgView addSubview:_bankNumCell];
    [_bankNumCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(6);
        make.right.equalTo(bgView).offset(-6);
        make.top.equalTo(_nameCell.mas_bottom);
        make.height.equalTo(@54);
    }];
    
    _IDCardCell = [[BCCell alloc] initCellWithTitle:@"身份证号"
                                        placeHolder:@"请输入身份证号"
                                               type:DefaultType];
    _IDCardCell.textField.delegate = self;
    [bgView addSubview:_IDCardCell];
    [_IDCardCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(6);
        make.right.equalTo(bgView).offset(-6);
        make.top.equalTo(_bankNumCell.mas_bottom);
        make.height.equalTo(@54);
    }];
    
    _bankNameCell = [[BCCell alloc] initCellWithTitle:@"银行"
                                        placeHolder:@"请输入银行后自动判断"
                                               type:DefaultType];
    _bankNameCell.textField.delegate = self;
    _bankNameCell.textField.enabled = NO;
    [bgView addSubview:_bankNameCell];
    [_bankNameCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(6);
        make.right.equalTo(bgView).offset(-6);
        make.top.equalTo(_IDCardCell.mas_bottom);
        make.height.equalTo(@54);
    }];
    
    _phoneNumCell = [[BCCell alloc] initCellWithTitle:@"手机号"
                                          placeHolder:@"请输入手机号码"
                                                 type:DefaultType];
    _phoneNumCell.textField.delegate = self;
    [bgView addSubview:_phoneNumCell];
    [_phoneNumCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(6);
        make.right.equalTo(bgView).offset(-6);
        make.top.equalTo(_bankNameCell.mas_bottom);
        make.height.equalTo(@54);
    }];
    
//    _imgCodeCell = [[BCCell alloc] initCellWithTitle:@"验证码"
//                                          placeHolder:@"请输入图片验证码"
//                                                 type:ImageType];
//    _imgCodeCell.textField.delegate = self;
//    [bgView addSubview:_imgCodeCell];
//    [_imgCodeCell mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(bgView).offset(6);
//        make.right.equalTo(bgView).offset(-6);
//        make.top.equalTo(_phoneNumCell.mas_bottom);
//        make.height.equalTo(@54);
//    }];
//
//    _imgCodeCell.imgView.userInteractionEnabled = YES;
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getYanZhengMaPic)];
//    [_imgCodeCell.imgView addGestureRecognizer:tap];
    
    _codeCell = [[BCCell alloc] initCellWithTitle:@"验证码"
                                         placeHolder:@"请输入短信验证码"
                                                type:CodeType];
    _codeCell.textField.delegate = self;
    [_codeCell.codeBtn addTarget:self action:@selector(getYanZhengCode:) forControlEvents:UIControlEventTouchDown];
    [bgView addSubview:_codeCell];
    [_codeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(6);
        make.right.equalTo(bgView).offset(-6);
        make.top.equalTo(_phoneNumCell.mas_bottom);
        make.height.equalTo(@54);
    }];
    
    UILabel *alertLbl = [FSViewHelper createLblWithText:@"信息加密处理，仅用于银行验证"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:14];
    [layoutView addSubview:alertLbl];
    [alertLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView.mas_bottom).offset(24);
        make.left.equalTo(layoutView).offset(16);
        make.right.equalTo(layoutView).offset(-16);
        make.height.equalTo(@25);
    }];
    
    _nextBtn = [FSViewHelper createBtnWithTitle:@"保存"
                                         bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"] titleColor:kWhiteColor];
    _nextBtn.layer.cornerRadius = 5.0f;
    [_nextBtn addTarget:self action:@selector(saveAction:) forControlEvents:UIControlEventTouchDown];
    [_scrollView addSubview:_nextBtn];
    [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(layoutView).offset(16);
        make.right.equalTo(layoutView).offset(-16);
        make.top.equalTo(alertLbl.mas_bottom).offset(8);
        make.height.equalTo(@50);
    }];
}

- (void)getBankCardInfo:(NSString *)str {
    
    [_vcModel getBandCardInfoWithBankCard:str success:^(id responseObject) {
        if (responseObject && [responseObject isKindOfClass:[NSDictionary class]]) {

            NSDictionary *dict = (NSDictionary *)responseObject;
            if (dict && dict.count > 0) {
                _bankNameCell.textField.text = dict[@"bankname"];
                self.bankLogo = dict[@"bankimage"];
            }
        }else if([responseObject isKindOfClass:[NSString class]]) {
            [self.view makeToast:responseObject duration:1.5 position:CSToastPositionCenter];
        }
    } failure:^(NSError *error) {
        NSLog(@"error:%@",[error description]);
        [self.view makeToast:@"网络请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
    }];
}

- (void)alertBtnClick:(UIButton *)sender {
    [[FSAlertTools sharedInstance] showAlert:self title:@"持卡人说明" message:@"为了您的资金安全，只能绑定与商户开户名一致的持卡人银行卡。" cancleTitle:@"确定" confirmTitle:nil cancleBlock:^{
        
    } confirmBlock:^{
        
    }];
}

// 获取验证码
- (IBAction)getYanZhengCode:(id)sender {
    
    NSString *phone = _phoneNumCell.textField.text;
    
    if(phone && ![phone isEqualToString:@""]) {
        
        _codeCell.codeBtn.enabled = NO;
        __weak typeof(self) weakSelf = self;
        [_vcModel getYanZhengMaPhoneCodeWithYanZhengMa:@""
                                                 phone:phone
                                               success:^(id responseObject) {
                                                   if (!responseObject) {
                                                       
                                                       [weakSelf.view makeToast:@"验证码获取失败" duration:1.5 position:CSToastPositionCenter];
                                                       _codeCell.codeBtn.enabled = YES;
                                                   }else if([responseObject isKindOfClass:[NSString class]]) {
                                                       
                                                       [weakSelf.view makeToast:responseObject duration:1.5 position:CSToastPositionCenter];
                                                       if ([responseObject isEqualToString:@"发送成功"]) {
                                                           
                                                           [weakSelf startCountTime];
                                                       }else{
                                                           _codeCell.codeBtn.enabled = YES;
                                                       }
                                                   }else{
                                                       _codeCell.codeBtn.enabled = YES;
                                                   }
                                               }
                                               failure:^(NSError *error) {
                                                   _codeCell.codeBtn.enabled = YES;
                                                   [weakSelf.view makeToast:@"验证码获取失败" duration:1.5 position:CSToastPositionCenter];
                                               }];
    }else {
        [self.view makeToast:@"请输入电话号码" duration:1.5 position:CSToastPositionCenter];
    }
}

// 开始倒计时
//- (void)startCountTime {
//
//    count = 60;
//    [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
//
//        count--;
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            NSString *str = [NSString stringWithFormat:@"%ld",count];
//            [_codeCell.codeBtn setTitle:str forState:UIControlStateNormal];
//            if (count < 1) {
//                _codeCell.codeBtn.enabled = YES;
//                [_codeCell.codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
//            }
//        });
//        if (count < 1) {
//            [timer invalidate];
//        }
//    }];
//}


- (void)startCountTime {
    count = 60;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownTime) userInfo:nil repeats:YES];
}

- (void)countDownTime {
    count--;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *str = [NSString stringWithFormat:@"%ld",count];
        [_codeCell.codeBtn setTitle:str forState:UIControlStateNormal];
        if (count < 1) {
            _codeCell.codeBtn.enabled = YES;
            [_codeCell.codeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
        }
    });
    if (count < 1) {
        [_timer invalidate];
        _timer = nil;
    }
}

// 获取验证码图片
- (void)getYanZhengMaPic {
//    [_vcModel getYanZhengMaPicSuccess:^(id responseObject) {
//        if (!responseObject) {
//
//        }else if([responseObject isKindOfClass:[NSString class]]) {
//
//        }else if([responseObject isKindOfClass:[NSDictionary class]]) {
//
//            NSDictionary *dict = (NSDictionary *)responseObject;
//            if (dict && dict.count > 0) {
//
//                NSString *urlStr = dict[@"url"];
//                [_imgCodeCell.imgView sd_setImageWithURL:[NSURL URLWithString:urlStr]];
//            }
//        }
//    } failure:^(NSError *error) {
//        NSLog(@"error:%@",[error description]);
//    }];
}


/**
 提交银行卡信息
 
 @param sender 按钮
 */
- (IBAction)saveAction:(id)sender {
    
    NSString *name = _nameCell.textField.text;
    NSString *bankNo = _bankNumCell.textField.text;
    NSString *idCard = _IDCardCell.textField.text;
    NSString *bankName = _bankNameCell.textField.text;
    NSString *phone = _phoneNumCell.textField.text;
    NSString *yanzhenCode = _codeCell.textField.text;
    __weak typeof(self) weakSelf = self;
    
    if (name && ![name isEqualToString:@""] && bankNo && ![bankNo isEqualToString:@""] && idCard && ![idCard isEqualToString:@""] && bankName && ![bankName isEqualToString:@""] && phone && ![phone isEqualToString:@""] && yanzhenCode && ![yanzhenCode isEqualToString:@""]) {
        
        [_vcModel submitBankCardWithPhoneNum:phone acct_name:name acct_pan:bankNo code:yanzhenCode bankName:bankName bank_logo:self.bankLogo bank_owner_no:idCard success:^(id responseObject) {
            if (!responseObject) {
                
                [weakSelf.view makeToast:@"添加失败" duration:1.5 position:CSToastPositionCenter];
            }else if([responseObject isKindOfClass:[NSString class]]) {
                
                [weakSelf.view makeToast:responseObject duration:1.5 position:CSToastPositionCenter];
                if (_backAction) {
                    _backAction();
                }
                [weakSelf.navigationController popViewControllerAnimated:YES];
            }
        } failure:^(NSError *error) {
            NSLog(@"error:%@",[error description]);
            [weakSelf.view makeToast:@"添加失败" duration:1.5 position:CSToastPositionCenter];
        }];
    }else {
        
        [weakSelf.view makeToast:@"请填写完整的数据" duration:1.5 position:CSToastPositionCenter];
    }
}

#pragma mark - textFieldDelegate
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
    
    NSString *str = textField.text;
    NSInteger tag = textField.tag;
    if (tag == 1001) {
        if (str.length == 19 || str.length == 18 || str.length == 16) {
            [self getBankCardInfo:str];
        }else {
            [self.view makeToast:@"请输入正确的银行卡号" duration:1.5 position:CSToastPositionCenter];
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.view endEditing:YES];
    [self.view resignFirstResponder];
    return YES;
}

@end
