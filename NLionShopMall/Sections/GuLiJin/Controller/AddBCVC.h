//
//  AddBCVC.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BackAction)(void);

@interface AddBCVC : BaseVC

@property (nonatomic,copy) BackAction backAction;

@end
