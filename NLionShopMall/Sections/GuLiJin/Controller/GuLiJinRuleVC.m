//
//  GuLiJinRuleVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "GuLiJinRuleVC.h"

@interface GuLiJinRuleVC ()

@end

@implementation GuLiJinRuleVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO];
    UIImage *image = [[ColorConverter colorWithHexString:@"0x58A4F8"] toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:image];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setNavigationBarTitleViewWithTitle:(NSString *)title{
    UILabel * titleView = [UILabel new];
    titleView.text = title;
    [titleView setTextColor:kWhiteColor];
    [titleView setFont:[UIFont systemFontOfSize:20]];
    [titleView sizeToFit];
    self.navigationItem.titleView = titleView;
}

- (void)hideBackButtonTitle {
    UIView * itmeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 44)];
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 13,17, 17)];
    imageView.image = [UIImage imageNamed:@"ic_fanhui"];
    [itmeView addSubview:imageView];
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popView)];
    [itmeView addGestureRecognizer:tap];
    UIBarButtonItem * leftButton = [[UIBarButtonItem alloc] initWithCustomView:itmeView];
    [self.navigationItem setLeftBarButtonItem:leftButton];
}

- (void)popView {
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"鼓励金规则"];
    [self hideBackButtonTitle];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"鼓励金规则"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:20];
    titleLbl.font = [UIFont boldSystemFontOfSize:20];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self.view addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20);
        make.left.equalTo(self.view).offset(16);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(@28);
    }];
    
    NSString *cntStr = @"1.鼓励金单笔最多提现5000元\n2.鼓励金允许任意时间提现，每日限提1笔\n3.鼓励金提现每笔需为100的倍数\n4.鼓励金提现到账时间为:T+1,周末节假日顺延.(如有调整请以APP相关通知为准)";
    
    UILabel *cntLbl = [FSViewHelper createLblWithText:cntStr
                                        textAlignment:NSTextAlignmentLeft
                                             fontSize:16];
    cntLbl.numberOfLines = 0;
    cntLbl.textColor = [ColorConverter colorWithHexString:@"0x666666"];
    [self.view addSubview:cntLbl];
    [cntLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(16);
        make.left.equalTo(self.view).offset(12);
        make.right.equalTo(self.view).offset(-12);
    }];
}

@end
