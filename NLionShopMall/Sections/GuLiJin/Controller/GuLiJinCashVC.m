//
//  GuLiJinCashVC.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/8.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "GuLiJinCashVC.h"
#import "SelectBankCardView.h"
#import "AddBCVC.h"
#import "GuLiJinCashVCModel.h"
#import "BankModal.h"

@interface GuLiJinCashVC ()<UITextFieldDelegate>
{
    UILabel *selectBankCardLbl;
    UITextField *cashTextField;
    UIButton *cashBtn;
    NSMutableArray *bankCardArray;
    NSString *selectBankId;
    SelectBankCardView *selectBankView;
}

@property (nonatomic,strong) GuLiJinCashVCModel *vcModel;
@end

@implementation GuLiJinCashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    bankCardArray = [[NSMutableArray alloc] init];
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    __weak typeof(self) weakSelf = self;
    _vcModel = [[GuLiJinCashVCModel alloc] init];
    [_vcModel getMyBankCardListSuccess:^{
        [[FSProgressViewTool shareInstance] removeProgressView];
        if (weakSelf.vcModel.bankCardAry && weakSelf.vcModel.bankCardAry.count > 0) {
            BankModal *model = [weakSelf.vcModel.bankCardAry firstObject];
            NSString *numStr = [model.bank_no substringFromIndex:model.bank_no.length-4];
            selectBankCardLbl.text = [NSString stringWithFormat:@"%@(%@)",model.bank_name,numStr];
        }
        for (NSInteger i = 0; i < weakSelf.vcModel.bankCardAry.count; i++) {
            BankModal *model = [weakSelf.vcModel.bankCardAry objectAtIndex:i];
            NSString *numStr = [model.bank_no substringFromIndex:model.bank_no.length-4];
            NSString *str = [NSString stringWithFormat:@"%@(%@)",model.bank_name,numStr];
            BankCardVM *m = [[BankCardVM alloc] initVMWithImg:model.bank_logo
                                                     bankName:str
                                                   isSelected:NO
                                                       bankId:model.bank_id];
            if (i == 0){
                m.isSelected = YES;
                selectBankId = model.bank_id;
            }
            [bankCardArray addObject:m];
        }
        if (selectBankView) {
            [selectBankView refreshView];
        }
    } failure:^(NSError *error) {
        [[FSProgressViewTool shareInstance] removeProgressView];
    }];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"鼓励金"];
    [self hideBackButtonTitle];
//    [self createNavigationBarRightBtn];
    
    UIImageView *bgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"juxing"]
                                                     bgColor:kWhiteColor];
    bgView.userInteractionEnabled = YES;
    [self.view addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.view).offset(12);
        make.right.equalTo(self.view).offset(-12);
        make.height.equalTo(@380);
    }];
    
    UIView *titleView = [[UIView alloc] init];
    titleView.backgroundColor = [ColorConverter colorWithHexString:@"0xFAFAFA"];
    [bgView addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(bgView);
        make.height.equalTo(@80);
    }];
    
    UITapGestureRecognizer *selectBankCardGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(selectBankCard)];
    [titleView addGestureRecognizer:selectBankCardGR];
    
    UILabel *cardTypeLbl = [FSViewHelper createLblWithText:@"到账银行卡"
                                             textAlignment:NSTextAlignmentLeft
                                                  fontSize:18];
    [titleView addSubview:cardTypeLbl];
    [cardTypeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgView).offset(24);
        make.centerY.equalTo(titleView);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(120, 30)]);
    }];
    
    selectBankCardLbl = [FSViewHelper createLblWithText:@"请选择到账银行卡号"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:18];
    [titleView addSubview:selectBankCardLbl];
    [selectBankCardLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cardTypeLbl.mas_right).offset(16);
        make.right.equalTo(titleView).offset(-24);
        make.top.equalTo(titleView).offset(25);
        make.bottom.equalTo(titleView).offset(-25);
    }];
    
    UILabel *cashNumLbl = [FSViewHelper createLblWithText:@"提现鼓励金金额"
                                            textAlignment:NSTextAlignmentLeft
                                                 fontSize:15];
    cashNumLbl.textColor = [ColorConverter colorWithHexString:@"0x313131"];
    [bgView addSubview:cashNumLbl];
    [cashNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleView.mas_bottom).offset(24);
        make.left.equalTo(bgView).offset(24);
        make.right.equalTo(bgView).offset(-24);
        make.height.equalTo(@25);
    }];
    
    UILabel *cashIconLbl = [FSViewHelper createLblWithText:@"¥"
                                            textAlignment:NSTextAlignmentLeft
                                                 fontSize:30];
    cashIconLbl.textColor = [ColorConverter colorWithHexString:@"0x0D0D0D"];
    [bgView addSubview:cashIconLbl];
    [cashIconLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cashNumLbl.mas_bottom).offset(24);
        make.left.equalTo(bgView).offset(24);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(35, 35)]);
    }];
    
    cashTextField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                         fontSize:30
                                                        textColor:[ColorConverter colorWithHexString:@"0x0D0D0D"]];
    cashTextField.keyboardType = UIKeyboardTypeDecimalPad;
    cashTextField.delegate = self;
    [bgView addSubview:cashTextField];
    [cashTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cashNumLbl.mas_bottom).offset(24);
        make.left.equalTo(cashIconLbl.mas_right).offset(8);
        make.right.equalTo(bgView).offset(-24);
        make.height.equalTo(@35);
    }];
    
    UIImageView *lineImg = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[UIColor groupTableViewBackgroundColor]];
    [bgView addSubview:lineImg];
    [lineImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(cashTextField.mas_bottom).offset(24);
        make.left.equalTo(bgView).offset(24);
        make.right.equalTo(bgView).offset(-24);
        make.height.equalTo(@1);
    }];
    
    UIButton *tiXianAllBtn = [FSViewHelper createBtnWithTitle:@"全部提现"
                                                      bgColor:[UIColor clearColor]
                                                   titleColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    tiXianAllBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [tiXianAllBtn addTarget:self action:@selector(allCashAction)
           forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:tiXianAllBtn];
    [tiXianAllBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImg.mas_bottom).offset(12);
        make.right.equalTo(bgView).offset(-24);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(100, 25)]);
    }];
    
    NSString *currentCash = [NSString stringWithFormat:@"当前可提现鼓励金额为%@元",self.guLiJinNum?self.guLiJinNum:@"0"];
    UILabel *canCashNumLbl = [FSViewHelper createLblWithText:currentCash
                                               textAlignment:NSTextAlignmentLeft
                                                    fontSize:14];
    canCashNumLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [bgView addSubview:canCashNumLbl];
    [canCashNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImg.mas_bottom).offset(12);
        make.left.equalTo(bgView).offset(24);
        make.height.equalTo(@25);
        make.right.equalTo(tiXianAllBtn.mas_left);
    }];
    
    cashBtn = [FSViewHelper createBtnWithTitle:@"我要提现"
                                       bgColor:[ColorConverter colorWithHexString:@"0x96C3E3"]
                                    titleColor:[ColorConverter colorWithHexString:@"0xFFFFFF"]];
    cashBtn.layer.cornerRadius = 5.0f;
    cashBtn.userInteractionEnabled = NO;
    [cashBtn addTarget:self action:@selector(tiXianAction:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:cashBtn];
    [cashBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(canCashNumLbl.mas_bottom).offset(32);
        make.left.equalTo(bgView).offset(24);
        make.right.equalTo(bgView).offset(-24);
        make.height.equalTo(@45);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tapGR];
}

- (void)createNavigationBarRightBtn {
    UIButton *rightBtn = [FSViewHelper createBtnWithTitle:@"提现记录"
                                                  bgColor:[UIColor clearColor]
                                               titleColor:kBlackColor];
    [rightBtn addTarget:self action:@selector(navigationRightBtnClick) forControlEvents:UIControlEventTouchDown];
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [self.navigationItem setRightBarButtonItem:rightButton];
}

- (void)navigationRightBtnClick {
    
}

- (void)tapAction:(UITapGestureRecognizer *)tapGR {
    [self.view endEditing:YES];
}

- (void)selectBankCard {
    NSLog(@"选择银行卡");
    selectBankView = [[SelectBankCardView alloc] init];
    selectBankView.bankCardList = bankCardArray;
    [selectBankView createSelectData];
    __weak typeof(selectBankView) weakView = selectBankView;
    __weak typeof(self) weakSelf = self;
    selectBankView.addBankCard = ^{
        AddBCVC *vc = [[AddBCVC alloc] init];
        vc.backAction = ^{
            [weakSelf initData];
            [weakView refreshView];
        };
        [weakSelf.navigationController pushViewController:vc animated:YES];
    };
    selectBankView.hasSelecte = ^(BankCardVM *vm) {
        if (vm.bankName && ![vm.bankName isEqualToString:@""]) {
            selectBankCardLbl.text = [NSString stringWithFormat:@"%@",vm.bankName];
        }else {
            selectBankCardLbl.text = @"请选择到账银行卡";
        }
        selectBankId = vm.bankId;
    };
    [self.view addSubview:selectBankView];
    [selectBankView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (void)allCashAction {
    NSLog(@"全部提现被点击");
    cashTextField.text = self.guLiJinNum?self.guLiJinNum:@"0";
    if (cashTextField.text.floatValue > 0) {
        cashBtn.userInteractionEnabled = YES;
        [cashBtn setBackgroundColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    }
}

- (void)tiXianAction:(UIButton *)sender {
    NSLog(@"我要提现");
    if (!selectBankId) {
        [self.view makeToast:@"请选择您的到账银行卡" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    if (cashTextField.text.floatValue > _guLiJinNum.floatValue) {
        [self.view makeToast:@"抱歉，您的提现金额已超过您的鼓励金总额，请重新输入" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    if (cashTextField.text.integerValue%100 > 0 ) {
        [self.view makeToast:@"抱歉，鼓励金的提取必须为100的整数倍" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:cashTextField.text forKey:@"withdraw_amount"];
    [dict setObject:selectBankId forKey:@"bank_id"];
    [_vcModel tiXianGuLiJinWithPara:dict success:^{
        
        [self.view makeToast:@"您的提现请求已提交，请耐心等待审核通过" duration:1.5 position:CSToastPositionCenter];
        [[FSProgressViewTool shareInstance] removeProgressView];
    } failure:^(NSError *error) {
        
        [self.view makeToast:@"您的提现请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        [[FSProgressViewTool shareInstance] removeProgressView];
    }];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.floatValue > 0) {
        cashBtn.userInteractionEnabled = YES;
        [cashBtn setBackgroundColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    }else {
        cashBtn.userInteractionEnabled = NO;
        [cashBtn setBackgroundColor:[ColorConverter colorWithHexString:@"0x96C3E3"]];
    }
}

@end
