//
//  BCCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DefaultType,
    ImageType,
    CodeType
} CellType;

@interface BCCell : BaseView

@property (nonatomic,strong) UITextField *textField;
@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UIButton *codeBtn;

-(instancetype)initCellWithTitle:(NSString *)title
                     placeHolder:(NSString *)placeHolder
                            type:(CellType)type;
@end
