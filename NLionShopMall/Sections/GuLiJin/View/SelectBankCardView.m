//
//  SelectBankCardView.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "SelectBankCardView.h"
#import "AddBankCardCell.h"
#import "BankModal.h"

static NSString *selectCellIdentify = @"selectCellIdentify";
static NSString *addCellIdentify = @"addCellIdentify";

@interface SelectBankCardView()<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView;
    BankCardVM *selectBankCardModel;
}

@end

@implementation SelectBankCardView

-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4];
    UIView *topView = [[UIView alloc] init];
    topView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4];
    [self addSubview:topView];
    [topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.equalTo(self);
        make.bottom.equalTo(self).offset(-screenH/2+15);
    }];
    
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction)];
    [topView addGestureRecognizer:tapGr];
    
    [self createTitleView];
    
    tableView = [self creatTableView];
    [self addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(screenH/2);
        make.left.right.bottom.equalTo(self);
    }];
}

- (void)createSelectData {
    for (BankCardVM *model in _bankCardList) {
        if (model.isSelected) {
            selectBankCardModel = model;
        }
    }
}

- (void)createTitleView {
    UIView *titleView = [[UIView alloc] init];
    titleView.backgroundColor = kWhiteColor;
    [self addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(screenH/2-50);
        make.left.right.equalTo(self);
        make.height.equalTo(@50);
    }];
    
    UILabel *lbl = [FSViewHelper createLblWithText:@"选择到账银行"
                                     textAlignment:NSTextAlignmentLeft
                                          fontSize:17];
    [titleView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(titleView).offset(16);
        make.right.bottom.equalTo(titleView).offset(-16);
    }];
    
    UIButton *sureBtn = [FSViewHelper createBtnWithTitle:@"确定"
                                                 bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"]
                                              titleColor:kWhiteColor];
    sureBtn.layer.cornerRadius = 5.0f;
    [titleView addSubview:sureBtn];
    [sureBtn addTarget:self action:@selector(sureBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleView).offset(8);
        make.width.equalTo(@60);
        make.right.equalTo(titleView).offset(-16);
        make.bottom.equalTo(titleView).offset(-8);
    }];
    
    UIImageView *imgLine = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [titleView addSubview:imgLine];
    [imgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(titleView);
        make.height.equalTo(@1);
    }];
}

- (UITableView *)creatTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = kWhiteColor;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    [tableView registerClass:[SelectBankCardCell class] forCellReuseIdentifier:selectCellIdentify];
//    [tableView registerClass:[AddBankCardCell class] forCellReuseIdentifier:addCellIdentify];
    return tableView;
}

- (void)tapAction {
    
}

- (void)sureBtnAction {
    [self removeFromSuperview];
    if (_hasSelecte) {
        _hasSelecte(selectBankCardModel);
    }
}

- (void)refreshView {
    [tableView reloadData];
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _bankCardList.count+1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 58;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == _bankCardList.count) {
        AddBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:addCellIdentify];
        if (!cell) {
            cell = [[AddBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:addCellIdentify];
            cell.addBankCardAction = ^{
                _addBankCard();
            };
        }
        return cell;
    }else {
        SelectBankCardCell *cell = [tableView dequeueReusableCellWithIdentifier:selectCellIdentify];
        if (!cell) {
            cell = [[SelectBankCardCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:selectCellIdentify];
            cell.selectBankAction = ^{
                for (BankCardVM *model in _bankCardList) {
                    model.isSelected = NO;
                }
                BankCardVM *model = [_bankCardList objectAtIndex:indexPath.row];
                model.isSelected = YES;
                [tableView reloadData];
                selectBankCardModel = model;
            };
        }
        if (_bankCardList && _bankCardList.count != 0) {
            BankCardVM *model = [_bankCardList objectAtIndex:indexPath.row];
            [cell refreshCellWithModel:model];
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 2) {
        if (_addBankCard) {
            _addBankCard();
        }
    }
}

@end
