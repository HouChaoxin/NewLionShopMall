//
//  AddBankCardCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "AddBankCardCell.h"

@implementation AddBankCardCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UILabel *addBankCardLbl = [FSViewHelper createLblWithText:@"添加新卡提现"
                                    textAlignment:NSTextAlignmentLeft
                                         fontSize:14];
    addBankCardLbl.textColor = [ColorConverter colorWithHexString:@"0x595959"];
    [self addSubview:addBankCardLbl];
    [addBankCardLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(65);
        make.right.equalTo(self).offset(-16);
        make.top.equalTo(self).offset(16);
        make.bottom.equalTo(self).offset(-16);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@1);
    }];
    
    UIButton *btn = [FSViewHelper createBtnWithTitle:@""
                                             bgColor:[UIColor clearColor]
                                          titleColor:kWhiteColor];
    [btn addTarget:self action:@selector(btnAction) forControlEvents:UIControlEventTouchDown];
    [self addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self);
    }];
}

- (void)btnAction {
    NSLog(@"添加新卡");
    if (_addBankCardAction) {
        _addBankCardAction();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
