//
//  GuLiJinHeaderView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "GuLiJinHeaderView.h"

@interface GuLiJinHeaderView()

@property (nonatomic,strong) UILabel *guLiJinNumLbl;
@end

@implementation GuLiJinHeaderView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = [ColorConverter colorWithHexString:@"0x58A4F8"];
    
    UIView *ruleView = [self createRuleView];
    [self addSubview:ruleView];
    [ruleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(50, 25)]);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ruleClickAction:)];
    [ruleView addGestureRecognizer:tapGR];
    
    UILabel *markLbl = [FSViewHelper createLblWithText:@"可提现鼓励金(元)"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:17];
    markLbl.textColor = kWhiteColor;
    [self addSubview:markLbl];
    [markLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(36);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 25)]);
    }];
    
    _guLiJinNumLbl = [FSViewHelper createLblWithText:@"鼓励金金额"
                                       textAlignment:NSTextAlignmentCenter
                                            fontSize:28];
    _guLiJinNumLbl.font = [UIFont fontWithName:kFontName size:28];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:kGuLiJinNum]) {
        NSString *moneyNum = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:kGuLiJinNum]];
        _guLiJinNumLbl.text = moneyNum;
    }
    _guLiJinNumLbl.textColor = kWhiteColor;
    _guLiJinNumLbl.font = [UIFont boldSystemFontOfSize:28];
    [self addSubview:_guLiJinNumLbl];
    [_guLiJinNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(markLbl.mas_bottom).offset(12);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(200, 40)]);
    }];
}

- (UIView *)createRuleView {
    UIView *ruleView = [[UIView alloc] init];
    
    UIImageView *imgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"introduce"]
                                                      bgColor:kClearColor];
    [ruleView addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(15, 15)]);
        make.top.equalTo(ruleView).offset(5);
        make.left.equalTo(ruleView);
    }];
    
    UILabel *ruleLbl = [FSViewHelper createLblWithText:@"规则"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:14];
    ruleLbl.textColor = kWhiteColor;
    [ruleView addSubview:ruleLbl];
    [ruleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(ruleView);
        make.right.equalTo(ruleView);
        make.bottom.equalTo(ruleView);
        make.left.equalTo(imgView.mas_right);
    }];
    
    return ruleView;
}

- (void)refreshViewWithNumStr:(NSString *)numStr {
    if (!numStr || [numStr isEqualToString:@""]) return;
    _guLiJinNumLbl.text = numStr;
}

- (void)ruleClickAction:(UITapGestureRecognizer *)tapGR {
    if (_ruleTapAction) {
        _ruleTapAction();
    }
}

@end
