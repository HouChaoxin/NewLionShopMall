//
//  BCCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "BCCell.h"

@interface BCCell()

@end

@implementation BCCell

-(instancetype)initCellWithTitle:(NSString *)title
                     placeHolder:(NSString *)placeHolder
                            type:(CellType)type {
    self = [super init];
    if (self) {
        if (type == 0) {
            [self initViewWithTitle:title placeHolder:placeHolder];
        }else if (type == 1){
            [self initImageCellWithTitle:title placeHolder:placeHolder];
        }else if (type == 2){
            [self initCodeCellWithTitle:title placeHolder:placeHolder];
        }
    }
    return self;
}

- (void)initViewWithTitle:(NSString *)title
              placeHolder:(NSString *)placeHolder {
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                           textAlignment:NSTextAlignmentLeft
                                                fontSize:17];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.top.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(80, 22)]);
    }];
    
    _textField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                      fontSize:17
                                                     textColor:[ColorConverter colorWithHexString:@"0x131313"]];
    _textField.placeholder = placeHolder;
    [self addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLbl.mas_right).offset(8);
        make.top.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-8);
        make.bottom.equalTo(self).offset(-16);
    }];
    
    UIImageView *imgView = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.bottom.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (void)initImageCellWithTitle:(NSString *)title
              placeHolder:(NSString *)placeHolder {
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:17];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.top.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(80, 22)]);
    }];
    
    _textField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                      fontSize:17
                                                     textColor:[ColorConverter colorWithHexString:@"0x131313"]];
    _textField.placeholder = placeHolder;
    [self addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLbl.mas_right).offset(8);
        make.top.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-144);
        make.bottom.equalTo(self).offset(-16);
    }];
    
    _imgView = [FSViewHelper createImgViewWithImg:nil
                                          bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-8);
        make.top.equalTo(self).offset(4);
        make.bottom.equalTo(self).offset(-4);
        make.width.equalTo(@100);
    }];
    
    UIImageView *imgView = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.bottom.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (void)initCodeCellWithTitle:(NSString *)title
                   placeHolder:(NSString *)placeHolder {
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:17];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.top.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(80, 22)]);
    }];
    
    _textField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                      fontSize:17
                                                     textColor:[ColorConverter colorWithHexString:@"0x131313"]];
    _textField.placeholder = placeHolder;
    [self addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(titleLbl.mas_right).offset(8);
        make.top.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-144);
        make.bottom.equalTo(self).offset(-16);
    }];
    
    _codeBtn = [FSViewHelper createBtnWithTitle:@"获取验证码"
                                        bgColor:kWhiteColor
                                     titleColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    [self addSubview:_codeBtn];
    [_codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-8);
        make.top.equalTo(self).offset(16);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo(@100);
    }];
    
//    UIImageView *imgView = [FSViewHelper createImgViewWithImg:nil
//                                                      bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
//    [self addSubview:imgView];
//    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).offset(8);
//        make.right.equalTo(self).offset(-8);
//        make.bottom.equalTo(self);
//        make.height.equalTo(@1);
//    }];
}


@end
