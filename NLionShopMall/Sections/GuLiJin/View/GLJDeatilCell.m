//
//  GLJDeatilCell.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GLJDeatilCell.h"

@interface GLJDeatilCell()

@property (nonatomic,strong) UILabel *nickNameLbl;
@property (nonatomic,strong) UILabel *orderNumLbl;
@property (nonatomic,strong) UILabel *guLiJinNumLbl;
@property (nonatomic,strong) UILabel *timeLbl;

@end

@implementation GLJDeatilCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.backgroundColor = kWhiteColor;
    self.backgroundView = view;
    
    _nickNameLbl = [FSViewHelper createLblWithText:@"会员昵称"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    _nickNameLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:_nickNameLbl];
    [_nickNameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    _orderNumLbl = [FSViewHelper createLblWithText:@"订单金额"
                                           textAlignment:NSTextAlignmentCenter
                                                fontSize:16];
    _orderNumLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:_orderNumLbl];
    [_orderNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(_nickNameLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    _guLiJinNumLbl = [FSViewHelper createLblWithText:@"获得鼓励金"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    _guLiJinNumLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:_guLiJinNumLbl];
    [_guLiJinNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(_orderNumLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    _timeLbl = [FSViewHelper createLblWithText:@"时间"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    _timeLbl.numberOfLines = 2;
    _timeLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:_timeLbl];
    [_timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(4);
        make.left.equalTo(_guLiJinNumLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-4);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    UIImageView *imgLine = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:imgLine];
    [imgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (void)refreshCellWithModel:(GLJDetailListModel *)model {
    if (!model) return;
    _nickNameLbl.text = model.member_name;
    _orderNumLbl.text = [NSString stringWithFormat:@"%@元",model.order_amount];
    _guLiJinNumLbl.text = [NSString stringWithFormat:@"%@元",model.cash];
    _timeLbl.text = model.create_time;
}

@end
