//
//  AddBankCardCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^AddBankCardAction)(void);

@interface AddBankCardCell : UITableViewCell

@property (nonatomic,copy) AddBankCardAction addBankCardAction;

@end
