//
//  GuLiJinCashCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/8.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "GuLiJinCashCell.h"

@implementation GuLiJinCashCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"提现须知"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:17];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(40);
        make.left.equalTo(self).offset(25);
        make.right.equalTo(self).offset(-25);
        make.height.equalTo(@25);
    }];
    
    NSString *cntStr = @"1.目前鼓励金提现支持任一家银行\n\n2.鼓励金单笔最多提现5000元\n\n3.鼓励金允许任意时间提现，每日限提1笔\n\n4.鼓励金提现每笔需为10的倍数\n\n5.鼓励金提现到账时间为:T+1,周末节假日顺延.(如有调整请以APP相关通知为准)";
    
    UILabel *cntLbl = [FSViewHelper createLblWithText:cntStr
                                        textAlignment:NSTextAlignmentLeft
                                             fontSize:15];
    cntLbl.numberOfLines = 0;
    cntLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [self addSubview:cntLbl];
    [cntLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(16);
        make.left.equalTo(self).offset(12);
        make.right.equalTo(self).offset(-12);
    }];
    
    UIButton *tiXianBtn = [FSViewHelper createBtnWithTitle:@"提现"
                                                   bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"]
                                                titleColor:kWhiteColor];
    [tiXianBtn addTarget:self action:@selector(tiXianBtnClick) forControlEvents:UIControlEventTouchUpInside];
    tiXianBtn.layer.cornerRadius = 5.0f;
    [self addSubview:tiXianBtn];
    [tiXianBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(128, 40)]);
        make.top.equalTo(cntLbl.mas_bottom).offset(50);
        make.centerX.equalTo(self);
    }];
    
    UIButton *tiXianRecordBtn = [FSViewHelper createBtnWithTitle:@"查看提现记录"
                                                   bgColor:[UIColor clearColor]
                                                titleColor:[ColorConverter colorWithHexString:@"0x808080"]];
    [tiXianRecordBtn addTarget:self action:@selector(tiXianRecordClick) forControlEvents:UIControlEventTouchUpInside];
    tiXianRecordBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:tiXianRecordBtn];
    [tiXianRecordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(140, 20)]);
        make.top.equalTo(tiXianBtn.mas_bottom).offset(24);
        make.centerX.equalTo(self);
    }];
}

- (void)tiXianBtnClick {
    if (self.tiXianAction) {
        self.tiXianAction();
    }
}

- (void)tiXianRecordClick {
    if (_tiXianRecordAction) {
        _tiXianRecordAction();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
