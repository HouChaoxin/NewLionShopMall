//
//  GuLiJinHeaderView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/18.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GuLiJinTapAction)(void);
@interface GuLiJinHeaderView : BaseView

@property (nonatomic,copy) GuLiJinTapAction ruleTapAction;
- (void)refreshViewWithNumStr:(NSString *)numStr;

@end
