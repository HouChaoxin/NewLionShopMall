//
//  GuLiJinRecordCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GLJDetailModel.h"

@interface GuLiJinRecordCell : UITableViewCell

- (void)refreshCellWithModel:(GLJDetailListModel *)model;

@end
