//
//  GuLiJinRecordCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "GuLiJinRecordCell.h"

//height=66

@interface GuLiJinRecordCell(){
    UILabel *cashLbl;
//    UILabel *bankLbl;
    UILabel *timeLbl;
    UILabel *stateLbl;
}

@end

@implementation GuLiJinRecordCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.backgroundColor = kWhiteColor;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cashLbl = [FSViewHelper createLblWithText:@"+0.00元"
                                textAlignment:NSTextAlignmentRight
                                     fontSize:16];
    cashLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self addSubview:cashLbl];
    [cashLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-16);
        make.centerY.equalTo(self);
        make.width.equalTo(@100);
        make.height.equalTo(@25);
    }];
    
    stateLbl = [FSViewHelper createLblWithText:@"审核中"
                                 textAlignment:NSTextAlignmentLeft
                                      fontSize:14];
    stateLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self addSubview:stateLbl];
    [stateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-120);
        make.height.equalTo(@25);
    }];
    
//    bankLbl = [FSViewHelper createLblWithText:@"中国建设银行-王小蛋-6228*********88"
//                                textAlignment:NSTextAlignmentLeft
//                                     fontSize:14];
//    bankLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
//    [bgView addSubview:bankLbl];
//    [bankLbl mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(cashLbl.mas_bottom).offset(16);
//        make.left.equalTo(bgView).offset(16);
//        make.right.equalTo(bgView).offset(-16);
//        make.height.equalTo(@25);
//    }];
    
    timeLbl = [FSViewHelper createLblWithText:@"时间"
                                textAlignment:NSTextAlignmentLeft
                                     fontSize:14];
    timeLbl.textColor = [ColorConverter colorWithHexString:@"0x666666"];
    [self addSubview:timeLbl];
    [timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(stateLbl.mas_bottom);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@25);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.bottom.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (void)refreshCellWithModel:(GLJDetailListModel *)model {
    if (!model) return;
    stateLbl.text = model.order_from;
    cashLbl.text = [NSString stringWithFormat:@"%@元",model.cash];
    timeLbl.text = [NSString stringWithFormat:@"%@",model.create_time];
//    NSString *bankNameStr = [NSString stringWithFormat:@"%@-%@-%@",model.withdraw_bank_name,model.withdraw_bank_user,model.withdraw_bank_no];
//    bankLbl.text = [NSString stringWithFormat:@"%@",bankNameStr];
//    if ([model.withdraw_payment_state isEqualToString:@"0"]) {
//        stateLbl.text = @"审核中";
//    }if ([model.withdraw_payment_state isEqualToString:@"-1"]) {
//        stateLbl.text = @"驳回";
//    }if ([model.withdraw_payment_state isEqualToString:@"1"]) {
//        stateLbl.text = @"支付成功";
//    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
