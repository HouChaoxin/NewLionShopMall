//
//  SelectBankCardCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankModal.h"

@interface BankCardVM : NSObject

@property (nonatomic,copy) NSString *logoImg;
@property (nonatomic,copy) NSString *bankName;
@property (nonatomic,copy) NSString *bankId;
@property (nonatomic,assign) BOOL isSelected;

-(instancetype)initVMWithImg:(NSString *)logoImg
                    bankName:(NSString *)bankName
                  isSelected:(BOOL)isSelected
                      bankId:(NSString *)bankId;
@end

typedef void(^SelectBankAction)(void);

@interface SelectBankCardCell : BaseTableViewCell

@property (nonatomic,copy) SelectBankAction selectBankAction;
- (void)refreshCellWithModel:(BankCardVM *)model;
@end
