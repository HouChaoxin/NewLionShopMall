//
//  GuLiJinRecordSectionView.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "GuLiJinRecordSectionView.h"

@interface GuLiJinRecordSectionView()
{
    UILabel *checkLbl;
    UILabel *rejectLbl;
    UILabel *successLbl;
    UIImageView *imgLine;
}

@end

@implementation GuLiJinRecordSectionView

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.backgroundColor = kWhiteColor;
    self.backgroundView = view;
    
    checkLbl = [FSViewHelper createLblWithText:@"审核中"
                                  textAlignment:NSTextAlignmentCenter
                                       fontSize:16];
    checkLbl.textColor = [ColorConverter colorWithHexString:@"0x36A4F3"];
    checkLbl.userInteractionEnabled = YES;
    [self addSubview:checkLbl];
    [checkLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self).offset(16);
        make.bottom.equalTo(self).offset(-24);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-16]);
    }];
    UITapGestureRecognizer *timeGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(checkAction)];
    [checkLbl addGestureRecognizer:timeGR];
    
    rejectLbl = [FSViewHelper createLblWithText:@"驳回"
                                textAlignment:NSTextAlignmentCenter
                                     fontSize:16];
    rejectLbl.userInteractionEnabled = YES;
    rejectLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [self addSubview:rejectLbl];
    [rejectLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(checkLbl.mas_right).offset(16);
        make.bottom.equalTo(self).offset(-24);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-16]);
    }];
    UITapGestureRecognizer *sourceGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rejectAction)];
    [rejectLbl addGestureRecognizer:sourceGR];
    
    successLbl = [FSViewHelper createLblWithText:@"支付成功"
                                   textAlignment:NSTextAlignmentCenter
                                        fontSize:16];
    successLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    successLbl.userInteractionEnabled = YES;
    [self addSubview:successLbl];
    [successLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(rejectLbl.mas_right).offset(16);
        make.bottom.equalTo(self).offset(-24);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-16]);
    }];
    UITapGestureRecognizer *cashGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(successAction)];
    [successLbl addGestureRecognizer:cashGR];
    
    imgLine = [FSViewHelper createImgViewWithImg:nil
                                         bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    [self addSubview:imgLine];
    [imgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-8);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(30, 1)]);
        make.centerX.equalTo(checkLbl);
    }];
    
    UIImageView *bottomLine = [FSViewHelper createImgViewWithImg:nil
                                                         bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@8);
    }];
}

- (void)checkAction {
    checkLbl.textColor = [ColorConverter colorWithHexString:@"0x36A4F3"];
    rejectLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    successLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [UIView animateWithDuration:0.5 animations:^{
        [imgLine mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).offset(-8);
            make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(30, 1)]);
            make.centerX.equalTo(checkLbl);
        }];
        [imgLine layoutIfNeeded];
    }];
    
    if (_processAction) {
        _processAction();
    }
}

- (void)rejectAction {
    checkLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    rejectLbl.textColor = [ColorConverter colorWithHexString:@"0x36A4F3"];
    successLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    [imgLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-8);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(30, 1)]);
        make.centerX.equalTo(rejectLbl);
    }];
    [imgLine layoutIfNeeded];
    if (_failAction) {
        _failAction();
    }
}

- (void)successAction {
    checkLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    rejectLbl.textColor = [ColorConverter colorWithHexString:@"0x808080"];
    successLbl.textColor = [ColorConverter colorWithHexString:@"0x36A4F3"];
    [imgLine mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-8);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(30, 1)]);
        make.centerX.equalTo(successLbl);
    }];
    [imgLine layoutIfNeeded];
    if (_successAction) {
        _successAction();
    }
}


@end
