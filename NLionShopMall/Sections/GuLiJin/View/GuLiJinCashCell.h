//
//  GuLiJinCashCell.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/8.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef void(^ClickAction)(void);
@interface GuLiJinCashCell : BaseTableViewCell

@property (nonatomic,copy) ClickAction tiXianAction;
@property (nonatomic,copy) ClickAction tiXianRecordAction;
@end
