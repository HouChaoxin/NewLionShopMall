//
//  SelectBankCardCell.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "SelectBankCardCell.h"

@implementation BankCardVM

-(instancetype)initVMWithImg:(NSString *)logoImg
                    bankName:(NSString *)bankName
                  isSelected:(BOOL)isSelected
                      bankId:(NSString *)bankId {
    self = [super init];
    if (self) {
        self.logoImg = logoImg;
        self.bankName = bankName;
        self.bankId = bankId;
        self.isSelected = isSelected;
    }
    return self;
}

@end

@interface SelectBankCardCell() {
    
    UIImageView *imgView;
    UILabel *bankCardLbl;
    UIButton *selectBtn;
}

@end

@implementation SelectBankCardCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    imgView = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:imgView];
    [imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(25, 25)]);
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(16);
    }];
    
    bankCardLbl = [FSViewHelper createLblWithText:@"银行储蓄卡"
                                    textAlignment:NSTextAlignmentLeft
                                         fontSize:14];
    bankCardLbl.textColor = [ColorConverter colorWithHexString:@"0x595959"];
    [self addSubview:bankCardLbl];
    [bankCardLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(imgView.mas_right).offset(24);
        make.right.equalTo(self).offset(-55);
        make.top.equalTo(self).offset(16);
        make.bottom.equalTo(self).offset(-16);
    }];
    
    selectBtn = [FSViewHelper createBtnWithTitle:@""
                                         bgColor:kWhiteColor
                                      titleColor:kWhiteColor];
    [selectBtn setBackgroundImage:[UIImage imageNamed:@"weixuanzhongkuang"] forState:UIControlStateNormal];
    [selectBtn addTarget:self action:@selector(selectedBtnClick) forControlEvents:UIControlEventTouchDown];
    [self addSubview:selectBtn];
    [selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(20, 20)]);
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-16);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:[UIColor groupTableViewBackgroundColor]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(@1);
    }];
}

- (void)selectedBtnClick {
    if (_selectBankAction) {
        _selectBankAction();
    }
}

- (void)refreshCellWithModel:(BankCardVM *)model {
    [imgView sd_setImageWithURL:[NSURL URLWithString:model.logoImg]];
    bankCardLbl.text = [NSString stringWithFormat:@"%@",model.bankName];
    if (!model.isSelected) {
        [selectBtn setBackgroundImage:[UIImage imageNamed:@"weixuanzhongkuang"] forState:UIControlStateNormal];
    }else {
        [selectBtn setBackgroundImage:[UIImage imageNamed:@"xuanzhongkuang"] forState:UIControlStateNormal];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
