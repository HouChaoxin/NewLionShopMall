//
//  SelectBankCardView.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectBankCardCell.h"

typedef void(^AddBankCardBlock)(void);
typedef void(^HasSelectedBlock)(BankCardVM *vm);

@interface SelectBankCardView : UIView

@property (nonatomic,copy) AddBankCardBlock addBankCard;
@property (nonatomic,copy) HasSelectedBlock hasSelecte;
@property (nonatomic,copy) NSArray *bankCardList;

- (void)refreshView;
- (void)createSelectData;
@end
