//
//  GLJDeatilSection.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GLJDeatilSection.h"

@implementation GLJDeatilSection

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.backgroundColor = kWhiteColor;
    self.backgroundView = view;
    
    UILabel *timeLbl = [FSViewHelper createLblWithText:@"会员昵称"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    timeLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:timeLbl];
    [timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(self).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    UILabel *sourceLbl = [FSViewHelper createLblWithText:@"订单金额"
                                           textAlignment:NSTextAlignmentCenter
                                                fontSize:16];
    sourceLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:sourceLbl];
    [sourceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(timeLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    UILabel *cashLbl = [FSViewHelper createLblWithText:@"获得鼓励金"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    cashLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:cashLbl];
    [cashLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(sourceLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    UILabel *dateLbl = [FSViewHelper createLblWithText:@"时间"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
    dateLbl.textColor = [ColorConverter colorWithHexString:@"0x131313"];
    [self addSubview:dateLbl];
    [dateLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(16);
        make.left.equalTo(cashLbl.mas_right).offset(8);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/4-10]);
    }];
    
    UIImageView *imgLine = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:imgLine];
    [imgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.equalTo(@1);
    }];
}


@end
