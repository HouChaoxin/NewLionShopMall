//
//  GLJDeatilCell.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GLJDetailModel.h"

@interface GLJDeatilCell : UITableViewCell
- (void)refreshCellWithModel:(GLJDetailListModel *)model;
@end
