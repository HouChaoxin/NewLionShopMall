//
//  GuLiJinRecordSectionView.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/9.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectAction)(void);

@interface GuLiJinRecordSectionView : UITableViewHeaderFooterView

@property (nonatomic,copy) SelectAction processAction;
@property (nonatomic,copy) SelectAction failAction;
@property (nonatomic,copy) SelectAction successAction;
@end
