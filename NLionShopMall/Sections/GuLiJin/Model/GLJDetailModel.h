//
//  GLJDetailModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GLJDetailListModel : BaseObject

@property (nonatomic,copy) NSString *member_id;
@property (nonatomic,copy) NSString *cash;
@property (nonatomic,copy) NSString *create_time;
@property (nonatomic,copy) NSString *order_id;
@property (nonatomic,copy) NSString *type;
@property (nonatomic,copy) NSString *order_amount;
@property (nonatomic,copy) NSString *order_from;
@property (nonatomic,copy) NSString *member_name;

-(instancetype)initWithDict:(NSDictionary *)dict;

@end

@interface GLJDetailModel : BaseObject

@property (nonatomic,copy) NSString *count;
@property (nonatomic,copy) NSString *page;
@property (nonatomic,copy) NSString *totalPages;
@property (nonatomic,strong) NSMutableArray *list;

-(instancetype)initWithDict:(NSDictionary *)dict;

@end
