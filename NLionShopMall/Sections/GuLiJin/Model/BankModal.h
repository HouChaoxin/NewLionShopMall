//
//  BankModal.h
//  LionMall
//
//  Created by Casanova.Z on 2017/11/4.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankModal : BaseObject

@property (nonatomic, copy) NSString *bank_id;
@property (nonatomic, copy) NSString *bank_name;
@property (nonatomic, copy) NSString *bank_logo;
@property (nonatomic, copy) NSString *bank_no;
@property (nonatomic, copy) NSString *bank_owner_name;
@property (nonatomic, copy) NSString *bank_owner_no;
@property (nonatomic, copy) NSString *bank_reserved_mobile;
@property (nonatomic, copy) NSString *member_id;
@property (nonatomic, copy) NSString *add_time;
@property (nonatomic, copy) NSString *is_default;
@property (nonatomic, copy) NSString *is_delete;

+ (instancetype)modalWithDict:(NSDictionary *)dict;

@end
