//
//  GLJDetailModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GLJDetailModel.h"

@implementation GLJDetailListModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.member_id = [self isNil:dict[@"member_id"]];
        self.cash = [self isNil:dict[@"cash"]];
        self.create_time = [self isNil:dict[@"create_time"]];
        self.order_id = [self isNil:dict[@"order_id"]];
        self.type = [self isNil:dict[@"type"]];
        self.order_amount = [self isNil:dict[@"order_amount"]];
        self.order_from = [self isNil:dict[@"order_from"]];
        self.member_name = [self isNil:dict[@"member_name"]];
    }
    return self;
}

@end

@implementation GLJDetailModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.count = [self isNil:dict[@"count"]];
        self.totalPages = [self isNil:dict[@"totalPages"]];
        self.page = [self isNil:dict[@"page"]];
        self.list = [[NSMutableArray alloc] init];
        NSArray *ary = dict[@"list"];
        for (NSDictionary *dict in ary) {
            GLJDetailListModel *model = [[GLJDetailListModel alloc] initWithDict:dict];
            [self.list addObject:model];
        }
    }
    return self;
}

@end
