//
//  BankModal.m
//  LionMall
//
//  Created by Casanova.Z on 2017/11/4.
//  Copyright © 2017年 lionmall. All rights reserved.
//

#import "BankModal.h"

@implementation BankModal
//{
//    "bank_id": "15097607498987630000",
//    "bank_name": "平安银行",
//    "bank_logo": "http://api.jisuapi.com/bankcard/upload/80/19.png",
//    "bank_no": "6230***********7492",
//    "bank_owner_name": "朱静宁",
//    "bank_owner_no": "640322********0611",
//    "bank_reserved_mobile": "134****7708",
//    "member_id": "14907593311911500000",
//    "add_time": "1509760749",
//    "is_default": "0",
//    "is_delete": "0"
//}
+ (instancetype)modalWithDict:(NSDictionary *)dict {
    
    BankModal *modal = [[BankModal alloc] init];
    if (dict) {
     
        modal.bank_id = [self ISNil:dict[@"bank_id"]];
        modal.bank_name = [self ISNil:dict[@"bank_name"]];
        modal.bank_logo = [self ISNil:dict[@"bank_logo"]];
        modal.bank_no = [self ISNil:dict[@"bank_no"]];
        modal.bank_owner_name = [self ISNil:dict[@"bank_owner_name"]];
        modal.bank_reserved_mobile = [self ISNil:dict[@"bank_reserved_mobile"]];
        modal.member_id = [self ISNil:dict[@"member_id"]];
        modal.add_time = [self ISNil:dict[@"add_time"]];
        modal.is_default = [self ISNil:dict[@"is_default"]];
        modal.is_delete = [self ISNil:dict[@"is_delete"]];
    }
    return modal;
}

+ (NSString *)ISNil:(NSString *)obj {
    
    return obj?[NSString stringWithFormat:@"%@",obj]:@"";
}

@end
