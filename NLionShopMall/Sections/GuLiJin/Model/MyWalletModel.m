//
//  MyWalletModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "MyWalletModel.h"

@implementation MyWalletModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.all_amount = [self isNil:dict[@"all_amount"]];
        self.cash_amount = [self isNil:dict[@"cash_amount"]];
        self.freeze_meney = [self isNil:dict[@"freeze_meney"]];
        self.reflect_amount = [self isNil:dict[@"reflect_amount"]];
    }
    return self;
}

@end
