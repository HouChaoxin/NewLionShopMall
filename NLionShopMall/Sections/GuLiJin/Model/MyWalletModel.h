//
//  MyWalletModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyWalletModel : BaseObject

@property (nonatomic,copy) NSString *all_amount;
@property (nonatomic,copy) NSString *cash_amount;
@property (nonatomic,copy) NSString *freeze_meney;
@property (nonatomic,copy) NSString *reflect_amount;
-(instancetype)initWithDict:(NSDictionary *)dict;
@end
