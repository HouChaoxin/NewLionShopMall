//
//  AddBCVCModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "AddBCVCModel.h"

@implementation AddBCVCModel

- (void)getBandCardInfoWithBankCard:(NSString *)bankNo
                            success:(SuccessBlock)successBlock
                            failure:(FailureBlock)failureBlock  {
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kGetBankCardInfoUrl];
    NSDictionary *param = @{@"bankCard":bankNo};
    [NetHelper GET:urlStr parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        if (responseObject) {
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                BOOL status = [rootDict[@"status"] boolValue];
                if (status) {
                    
                    NSDictionary *data = rootDict[@"data"];
                    successBlock(data);
                }else{
                    NSString *msg = rootDict[@"msg"];
                    successBlock(msg);
                }
            }else{
                successBlock(@"请求失败,请稍后再试");
            }
        }else {
            successBlock(@"请求失败,请稍后再试");
        }
    } failure:^(NSError *error) {
        
        failureBlock(error);
    }];
}

- (void)getYanZhengMaPhoneCodeWithYanZhengMa:(NSString *)yanZhenMa
                                       phone:(NSString *)phone
                                     success:(SuccessBlock)successBlock
                                     failure:(FailureBlock)failureBlock {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kGetPhoneCodeUrl];
    NSDictionary *param = @{@"phone":phone};
    
    [NetHelper POST:urlStr parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        if (responseObject) {
            
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                NSString *msg = rootDict[@"msg"];
                successBlock(msg);
            }else {
                successBlock(nil);
            }
        }else {
            successBlock(nil);
        }
        
    } failure:^(NSError *error) {
        
        failureBlock(error);
    }];
}

- (void)getYanZhengMaPicSuccess:(SuccessBlock)successBlock
                        failure:(FailureBlock)failureBlock {
    
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kGetPhoneCodeUrl];
    
    NSDictionary *param = @{@"r":@"index/get-captcha"};
    [NetHelper GET:urlStr parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        if (responseObject) {
            
            NSDictionary *dict = (NSDictionary *)responseObject;
            if (dict && dict.count > 0) {
                
                BOOL status = [dict[@"status"] boolValue];
                if (status) {
                    
                    NSDictionary *data = dict[@"data"];
                    successBlock(data);
                }else{
                    NSString *msg = dict[@"msg"];
                    successBlock(msg);
                }
            }else{
                successBlock(nil);
            }
        }else{
            successBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

- (void)submitBankCardWithPhoneNum:(NSString *)phone acct_name:(NSString *)acct_name acct_pan:(NSString *)acct_pan code:(NSString *)code bankName:(NSString *)bank_name bank_logo:(NSString *)logo bank_owner_no:(NSString *)bank_owner_no success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock {
    
    
    
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:kMemberToken];
    if (!token || [token isEqualToString:@""]) return;
    NSString *urlStr = [NSString stringWithFormat:@"%@%@&token=%@",baseUrl,kAddBankCardUrl,token];
    NSDictionary *param = @{@"phone_num":phone,@"acct_name":acct_name,@"acct_pan":acct_pan,@"code":code,@"bank_name":bank_name,@"bank_logo":logo,@"bank_owner_no":bank_owner_no,@"is_receive_card":@"0",@"sub_bank_code":@"0"};
    [NetHelper POST:urlStr parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"res:%@",responseObject);
        
        if (responseObject) {
            
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                NSString *msg = rootDict[@"msg"];
                successBlock(msg);
            }else{
                successBlock(nil);
            }
        }else {
            successBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

@end
