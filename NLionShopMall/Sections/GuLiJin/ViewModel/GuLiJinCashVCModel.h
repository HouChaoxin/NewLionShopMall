//
//  GuLiJinCashVCModel.h
//  LionMall
//
//  Created by hcx_ios on 2018/4/11.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^DataSuccessBlock)();
typedef void (^FailureBlock)(NSError *error);

@interface GuLiJinCashVCModel : NSObject

@property (nonatomic,strong) NSMutableArray *bankCardAry;

- (void)tiXianGuLiJinWithPara:(NSDictionary *)dict
                      success:(DataSuccessBlock)successBlock
                      failure:(FailureBlock)failureBlock;

- (void)getMyBankCardListSuccess:(DataSuccessBlock)successBlock
                         failure:(FailureBlock)failureBlock;
@end
