//
//  GuLiJinCashVCModel.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/11.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "GuLiJinCashVCModel.h"
//#import "NetHelper.h"
//#import "MineNetAction.h"
#import "BankModal.h"

@implementation GuLiJinCashVCModel

-(instancetype)init {
    self = [super init];
    if (self) {
        _bankCardAry = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)tiXianGuLiJinWithPara:(NSDictionary *)dict
                      success:(DataSuccessBlock)successBlock
                      failure:(FailureBlock)failureBlock {
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:kMemberToken];
    if (!token || [token isEqualToString:@""]) return;
    NSString *urlStr = [NSString stringWithFormat:@"%@%@&token=%@",baseUrl,kTiXianUrl,token];
    
    [NetHelper POST:urlStr parameters:dict progressBlock:nil success:^(id responseObject) {
        NSDictionary *rootDict = responseObject;
        if (rootDict && rootDict.count > 0) {

            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {

                successBlock();
            }else{
                
                NSString *msg = rootDict[@"msg"]?rootDict[@"msg"]:@"数据错误";
                successBlock(msg);
            }
        }else{
            NSError *error = [[NSError alloc] init];
            failureBlock(error);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

- (void)getMyBankCardListSuccess:(DataSuccessBlock)successBlock
                      failure:(FailureBlock)failureBlock {
    
    NSString *bankListUrl = [NSString stringWithFormat:@"%@%@",baseUrl,kGetBankCardListUrl];
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:kMemberToken];
    NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:token,@"token", nil];
    [NetHelper GET:bankListUrl parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {

        if (responseObject) {

            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {

                BOOL status = [rootDict[@"status"] boolValue];
                if (status) {

                    NSArray *arr = rootDict[@"data"];
                    if (arr && arr.count > 0) {

                        for (NSInteger i = 0; i<arr.count; i++) {
                            
                            NSDictionary *oneDict = [arr objectAtIndex:i];
                            BankModal *modal = [BankModal modalWithDict:oneDict];
                            [_bankCardAry addObject:modal];
                        }
                    }
                }
            }
        }
        successBlock();
    } failure:^(NSError *error) {

        failureBlock(error);
    }];
}

@end
