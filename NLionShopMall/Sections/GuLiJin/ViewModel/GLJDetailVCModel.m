//
//  GLJDetailVCModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "GLJDetailVCModel.h"

@interface GLJDetailVCModel()

@end

@implementation GLJDetailVCModel

-(instancetype)init {
    self = [super init];
    if (self) {
        _guLiJinListAry = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)getGuLiJinDetailRecordWithPara:(NSDictionary *)dict
                               success:(SuccessBlock)successBlock
                               failure:(FailureBlock)failureBlock {
    
    NSString *gljDetailUrlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kGuLiJinDetailUrl];
    
    [NetHelper GET:gljDetailUrlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                NSDictionary *dictionary = [[rootDict objectForKey:@"data"] objectForKey:@"detail"];
                GLJDetailModel *model = [[GLJDetailModel alloc] initWithDict:dictionary];
                [self.guLiJinListAry addObjectsFromArray:model.list];
                self.totalPageNum = model.totalPages.integerValue;
            }
        }
        successBlock(nil);
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
    
}

@end
