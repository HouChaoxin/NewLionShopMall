//
//  GLJDetailVCModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetHelper.h"
#import "GLJDetailModel.h"

@interface GLJDetailVCModel : NSObject

@property (nonatomic,strong) NSMutableArray *guLiJinListAry;
@property (nonatomic,assign) NSInteger totalPageNum;

- (void)getGuLiJinDetailRecordWithPara:(NSDictionary *)dict
                               success:(SuccessBlock)successBlock
                               failure:(FailureBlock)failureBlock;

@end
