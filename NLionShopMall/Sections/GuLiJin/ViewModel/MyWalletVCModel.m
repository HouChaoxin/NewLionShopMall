//
//  MyWalletVCModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "MyWalletVCModel.h"



@implementation MyWalletVCModel

-(instancetype)init {
    self = [super init];
    if (self) {
        _guLiJinListAry = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)getGuLiJinDetailRecordWithPara:(NSDictionary *)dict
                               success:(SuccessBlock)successBlock
                               failure:(FailureBlock)failureBlock {
    
    NSString *gljDetailUrlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kGuLiJinDetailUrl];
    
    [NetHelper GET:gljDetailUrlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                NSDictionary *dictionary = [[rootDict objectForKey:@"data"] objectForKey:@"amount"];
                _walletModel = [[MyWalletModel alloc] initWithDict:dictionary];
                GLJDetailModel *model = [[GLJDetailModel alloc] initWithDict:[[rootDict objectForKey:@"data"] objectForKey:@"detail"]];
                [_guLiJinListAry addObjectsFromArray:model.list];
                _totalPageNum = model.count.integerValue;
                successBlock(nil);
            }else {
                failureBlock(nil);
            }
        }else{
            failureBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

@end
