//
//  AddBCVCModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/27.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddBCVCModel : NSObject

- (void)getBandCardInfoWithBankCard:(NSString *)bankNo
                            success:(SuccessBlock)successBlock
                            failure:(FailureBlock)failureBlock;

- (void)getYanZhengMaPhoneCodeWithYanZhengMa:(NSString *)yanZhenMa
                                       phone:(NSString *)phone
                                     success:(SuccessBlock)successBlock
                                     failure:(FailureBlock)failureBlock;

//- (void)getYanZhengMaPicSuccess:(SuccessBlock)successBlock
//                        failure:(FailureBlock)failureBlock;

- (void)submitBankCardWithPhoneNum:(NSString *)phone acct_name:(NSString *)acct_name acct_pan:(NSString *)acct_pan code:(NSString *)code bankName:(NSString *)bank_name bank_logo:(NSString *)logo bank_owner_no:(NSString *)bank_owner_no success:(SuccessBlock)successBlock failure:(FailureBlock)failureBlock;
@end
