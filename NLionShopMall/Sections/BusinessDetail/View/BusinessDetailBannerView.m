//
//  BusinessDetailBannerView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailBannerView.h"

@interface BusinessDetailBannerView()

@property (nonatomic,strong) UILabel *todayIncomeLbl;
@property (nonatomic,strong) UILabel *recordLbl;
@property (nonatomic,strong) UILabel *increseLbl;

@end

@implementation BusinessDetailBannerView


- (instancetype)init{
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    UIImageView *calendarImgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"ic_riqi"]
                                                              bgColor:kClearColor];
    calendarImgView.userInteractionEnabled = YES;
    [self addSubview:calendarImgView];
    [calendarImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(12);
        make.right.equalTo(self).offset(-12);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(16, 16)]);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calendarTapAction:)];
    [calendarImgView addGestureRecognizer:tapGR];
    
    _titleLbl = [FSViewHelper createLblWithText:@"今日收益(元)"
                                  textAlignment:NSTextAlignmentCenter
                                       fontSize:14];
    _titleLbl.textColor = kWhiteColor;
    [self addSubview:_titleLbl];
    [_titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(20);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@20);
    }];
    
    _todayIncomeLbl = [FSViewHelper createLblWithText:@"0.00"
                                        textAlignment:NSTextAlignmentCenter
                                             fontSize:28];
    _todayIncomeLbl.textColor = kWhiteColor;
    _todayIncomeLbl.font = [UIFont boldSystemFontOfSize:28];
    [self addSubview:_todayIncomeLbl];
    [_todayIncomeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@35);
    }];
    
    UILabel *recordLbl = [FSViewHelper createLblWithText:@"交易记录(条)"
                                           textAlignment:NSTextAlignmentCenter
                                                fontSize:14];
    recordLbl.textColor = kWhiteColor;
    [self addSubview:recordLbl];
    [recordLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_todayIncomeLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    _recordLbl = [FSViewHelper createLblWithText:@"0"
                                        textAlignment:NSTextAlignmentCenter
                                             fontSize:18];
    _recordLbl.textColor = kWhiteColor;
    [self addSubview:_recordLbl];
    [_recordLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(recordLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    UILabel *incomeTitleLbl = [FSViewHelper createLblWithText:@"同比收益"
                                                textAlignment:NSTextAlignmentCenter
                                                     fontSize:14];
    incomeTitleLbl.textColor = kWhiteColor;
    [self addSubview:incomeTitleLbl];
    [incomeTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_todayIncomeLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
    
    _increseLbl = [FSViewHelper createLblWithText:@"0.00%"
                                    textAlignment:NSTextAlignmentCenter
                                         fontSize:18];
    _increseLbl.textColor = kWhiteColor;
    [self addSubview:_increseLbl];
    [_increseLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(incomeTitleLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-30]);
        make.height.equalTo(@20);
    }];
}

- (void)calendarTapAction:(UITapGestureRecognizer *)tapGR {
    if (_calendarTapAct) {
        _calendarTapAct();
    }
}

- (void)refreshViewWithIncome:(NSString *)income recordNum:(NSString *)recordNum increse:(NSString *)increse isTodayIncome:(BOOL)isTodayIncome periodStr:(NSString *)periodStr{

    _todayIncomeLbl.text = [NSString stringWithFormat:@"%0.2f",income.floatValue];
    _recordLbl.text = recordNum;
    _increseLbl.text = [NSString stringWithFormat:@"%0.2f",increse.floatValue];
    if (isTodayIncome) {
        _titleLbl.text = @"今日收益";
        self.mainImageView.image = [UIImage imageNamed:@"todayIncome"];
    }else {
        _titleLbl.text = periodStr;
        self.mainImageView.image = [UIImage imageNamed:@"noTodayIncome"];
    }
}

@end
