//
//  BusinessDetailCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailCell.h"

@interface BusinessDetailCell()

@property (nonatomic,strong) UILabel *nameLbl;
@property (nonatomic,strong) UIImageView *payStyleImg;
@property (nonatomic,strong) UILabel *consumptionStyleLbl;
@property (nonatomic,strong) UILabel *moneyLbl;
@property (nonatomic,strong) UILabel *timeLbl;

@end

@implementation BusinessDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _payStyleImg = [FSViewHelper createImgViewWithImg:nil bgColor:kWhiteColor];
    [self addSubview:_payStyleImg];
    [_payStyleImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(16, 16)]);
    }];
    
    _nameLbl = [FSViewHelper createLblWithText:@"姓名"
                                 textAlignment:NSTextAlignmentLeft
                                      fontSize:16];
    _nameLbl.textColor = kWordColor;
    [self addSubview:_nameLbl];
    [_nameLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-16);
        make.left.equalTo(_payStyleImg.mas_right).offset(6);
        make.height.equalTo(@20);
    }];
    
    _moneyLbl = [FSViewHelper createLblWithText:@"+¥0"
                                  textAlignment:NSTextAlignmentRight
                                       fontSize:16];
    _moneyLbl.textColor = kWordColor;
    [self addSubview:_moneyLbl];
    [_moneyLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(100, 20)]);
    }];
    
    _consumptionStyleLbl = [FSViewHelper createLblWithText:@"购买方式"
                                             textAlignment:NSTextAlignmentLeft
                                                  fontSize:14];
    _consumptionStyleLbl.textColor = kLightGrayColor;
    [self addSubview:_consumptionStyleLbl];
    [_consumptionStyleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLbl.mas_bottom).offset(8);
        make.left.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    _timeLbl = [FSViewHelper createLblWithText:@"时间"
                                 textAlignment:NSTextAlignmentRight
                                      fontSize:14];
    _timeLbl.textColor = kLightGrayColor;
    [self addSubview:_timeLbl];
    [_timeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil bgColor:kLineColor];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_timeLbl.mas_bottom).offset(8);
        make.right.equalTo(self).offset(-16);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@1);
    }];
}

- (void)refreshViewWithModel:(BusinessDetailListModel *)model {
    if (!model) return;
    _nameLbl.text = model.member_name;
    _consumptionStyleLbl.text = model.typess;
    _timeLbl.text = model.payment_time;
    _moneyLbl.text = model.order_amount;
    
    if ([model.pay_type isEqualToString:@"1"]) {
        _payStyleImg.image = [UIImage imageNamed:@"ic_weixin"];
    }else if ([model.pay_type isEqualToString:@"2"]) {
        _payStyleImg.image = [UIImage imageNamed:@"ic_zhifubao"];
    }else {
        _payStyleImg.image = [UIImage imageNamed:@"ic_xianjin"];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
