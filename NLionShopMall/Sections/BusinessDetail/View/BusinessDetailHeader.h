//
//  BusinessDetailHeader.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BusinessDetailBannerView.h"

typedef void(^BDHCalendarTapAction)(UILabel *titleLbl);
typedef void(^RefreshViewAction)(BusinessDetailBannerView *view);

typedef void(^SlideAction)(void);

@interface BusinessDetailHeader : BaseView

@property (nonatomic,copy) BDHCalendarTapAction bDCalendarTapAction;
@property (nonatomic,copy) SlideAction leftSlideAction;
@property (nonatomic,copy) SlideAction rightSlideAction;
@property (nonatomic,copy) RefreshViewAction refreshViewAction;

- (void)refreshViewWithIncome:(NSString *)income
                    recordNum:(NSString *)recordNum
                      increse:(NSString *)increse
                jieSuanIncome:(NSString *)jieSuanIncome
                    sumIncome:(NSString *)sumIncome
                isTodayIncome:(BOOL)isTodayIncome
                    periodStr:(NSString *)periodStr;

@end
