//
//  BusinessDetailHeader.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailHeader.h"

#import "NewPagedFlowView.h"
#import "SelectDateView.h"

@interface BusinessDetailHeader()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>

@property (nonatomic, strong) NewPagedFlowView *pageFlowView;

@property (nonatomic,strong) UILabel *jieSuanIncomeLbl;
@property (nonatomic,strong) UILabel *sumIncomeLbl;

@property (nonatomic,assign) NSInteger currentIndex;

@property (nonatomic,copy) NSString *incomeStr;
@property (nonatomic,copy) NSString *recordNumStr;
@property (nonatomic,copy) NSString *increaseStr;

@property (nonatomic,strong) BusinessDetailBannerView *currentView;
@end

@implementation BusinessDetailHeader

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
        [self initView];
    }
    return self;
}

- (void)initData {
    _currentIndex = 0;
    _incomeStr = @"0.00";
    _recordNumStr = @"0";
    _increaseStr = @"0.00%";
}

- (void)initView {
    [self setupFlowUI];
    [self createBottomView];
}

- (void)setupFlowUI {
    NewPagedFlowView *pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 0, screenW, 160)];
    pageFlowView.backgroundColor = kWhiteColor;
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 0.4;
    pageFlowView.leftRightMargin = 30;
    
    pageFlowView.topBottomMargin = 0;
    pageFlowView.orginPageCount = 3;
    pageFlowView.isOpenAutoScroll = NO;
    [self addSubview:pageFlowView];
    [pageFlowView reloadData];
    self.pageFlowView = pageFlowView;
}

- (void)createBottomView {
    _jieSuanIncomeLbl = [FSViewHelper createLblWithText:@"0.00"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:17];
    _jieSuanIncomeLbl.textColor = kWordColor;
    [self addSubview:_jieSuanIncomeLbl];
    [_jieSuanIncomeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-52);
        make.left.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    _sumIncomeLbl = [FSViewHelper createLblWithText:@"0.00"
                                      textAlignment:NSTextAlignmentCenter
                                           fontSize:17];
    _sumIncomeLbl.textColor = kWordColor;
    [self addSubview:_sumIncomeLbl];
    [_sumIncomeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-52);
        make.right.equalTo(self).offset(-16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    UILabel *jieSuanTitleLbl = [FSViewHelper createLblWithText:@"结算中的收益(元)"
                                                 textAlignment:NSTextAlignmentCenter
                                                      fontSize:15];
    jieSuanTitleLbl.textColor = kLightWordColor;
    [self addSubview:jieSuanTitleLbl];
    [jieSuanTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-22);
        make.left.equalTo(self).offset(16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    UILabel *sumTitleLbl = [FSViewHelper createLblWithText:@"累积收益(元)"
                                                 textAlignment:NSTextAlignmentCenter
                                                      fontSize:15];
    sumTitleLbl.textColor = kLightWordColor;
    [self addSubview:sumTitleLbl];
    [sumTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-22);
        make.right.equalTo(self).offset(-16);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/2-16, 20)]);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                                          bgColor:kLineColor];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.right.left.equalTo(self);
        make.height.equalTo(@6);
    }];
}

#pragma mark --NewPagedFlowView Delegate
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@";点击了第%ld张图",(long)subIndex + 1);
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
//    NSLog(@"CustomViewController 滚动到了第%ld页",pageNumber);
    if (((pageNumber > _currentIndex) && (pageNumber - _currentIndex == 1)) ||
        ((pageNumber < _currentIndex) && (_currentIndex - pageNumber == 2))){
        NSLog(@"***右滑***");
        if (_rightSlideAction) {
            _rightSlideAction();
        }
    }else if (((pageNumber < _currentIndex) && (_currentIndex - pageNumber == 1)) ||
              ((pageNumber > _currentIndex) && (pageNumber - _currentIndex == 2))) {
        NSLog(@"***左滑***");
        if (_leftSlideAction) {
            _leftSlideAction();
        }
    }
    _currentIndex = pageNumber;
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(screenW-50, 164);
}

#pragma mark --NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    
    return 3;
}

- (PGIndexBannerSubiew *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    
    BusinessDetailBannerView *bannerView = (BusinessDetailBannerView *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[BusinessDetailBannerView alloc] init];
        bannerView.layer.cornerRadius = 4;
        bannerView.layer.masksToBounds = YES;
        
        __weak typeof(bannerView) weakBannerView = bannerView;
        bannerView.calendarTapAct = ^{
            //日历被点击
            NSLog(@"日历被点击");
            if (self.bDCalendarTapAction) {
                _bDCalendarTapAction(weakBannerView.titleLbl);
            }
        };
    }
    bannerView.mainImageView.image = [UIImage imageNamed:@"noTodayIncome"];
    return bannerView;
}

- (void)refreshViewWithIncome:(NSString *)income
                    recordNum:(NSString *)recordNum
                      increse:(NSString *)increse
                jieSuanIncome:(NSString *)jieSuanIncome
                    sumIncome:(NSString *)sumIncome
                isTodayIncome:(BOOL)isTodayIncome
                    periodStr:(NSString *)periodStr {
    
    NSLog(@"income = %@",income);
    NSLog(@"recordNum = %@",recordNum);
    
    for (id view in _pageFlowView.cells) {
        if ([view isKindOfClass:[BusinessDetailBannerView class]]) {
            BusinessDetailBannerView *bview = (BusinessDetailBannerView *)view;
            [bview refreshViewWithIncome:income recordNum:recordNum increse:increse isTodayIncome:isTodayIncome periodStr:periodStr];
        }
    }
    for (id view in _pageFlowView.reusableCells) {
        if ([view isKindOfClass:[BusinessDetailBannerView class]]) {
            BusinessDetailBannerView *bview = (BusinessDetailBannerView *)view;
            [bview refreshViewWithIncome:income recordNum:recordNum increse:increse isTodayIncome:isTodayIncome periodStr:periodStr];
        }
    }
    _jieSuanIncomeLbl.text = jieSuanIncome;
    _sumIncomeLbl.text = sumIncome;
}

@end
