//
//  BusinessDetailSection.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailSection.h"

@implementation BusinessDetailSection

-(instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    view.backgroundColor = kWhiteColor;
    self.backgroundView = view;
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"交易记录"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:15];
    titleLbl.textColor = [ColorConverter colorWithHexString:@"0x333333"];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(95, 30)]);
    }];
    
    UIImageView *imgLine = [FSViewHelper createImgViewWithImg:nil
                                                      bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:imgLine];
    [imgLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.left.equalTo(self).offset(30);
        make.right.equalTo(titleLbl.mas_left).offset(-12);
        make.height.equalTo(@1);
    }];
    
    UIImageView *imgLine1 = [FSViewHelper createImgViewWithImg:nil
                                                       bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:imgLine1];
    [imgLine1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.right.equalTo(self).offset(-30);
        make.left.equalTo(titleLbl.mas_right).offset(30);
        make.height.equalTo(@1);
    }];
}


@end
