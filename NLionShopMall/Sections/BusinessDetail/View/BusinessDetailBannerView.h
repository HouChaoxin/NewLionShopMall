//
//  BusinessDetailBannerView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PGIndexBannerSubiew.h"

typedef void(^BDCalendarTapAction)(void);

@interface BusinessDetailBannerView : PGIndexBannerSubiew

@property (nonatomic,strong) UILabel *titleLbl;
@property (nonatomic,copy) BDCalendarTapAction calendarTapAct;

- (void)refreshViewWithIncome:(NSString *)income recordNum:(NSString *)recordNum increse:(NSString *)increse isTodayIncome:(BOOL)isTodayIncome periodStr:(NSString *)periodStr;
@end
