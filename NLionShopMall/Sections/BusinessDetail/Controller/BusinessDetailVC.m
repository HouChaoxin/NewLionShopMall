//
//  BusinessDetailVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailVC.h"
#import "BusinessDetailHeader.h"
#import "BusinessDetailCell.h"
#import "BusinessDetailSection.h"
#import "SelectTwoDateView.h"
#import "BusinessDetailVCModel.h"

static NSString *cellIdentify = @"cellIdentify";
static NSString *sectionIdentify = @"sectionIdentify";

@interface BusinessDetailVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong) UITableView *tableView_;
@property (nonatomic,strong) BusinessDetailHeader *headerView;
@property (nonatomic,strong) BusinessDetailVCModel *vcModel;

@property (nonatomic,strong) NSString *startDateStr;
@property (nonatomic,strong) NSString *endDateStr;

@end

@implementation BusinessDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)initData {
    _vcModel = [[BusinessDetailVCModel alloc] init];
    _startDateStr = [self getCurrentDate];
    _endDateStr = [self getCurrentDate];
}

- (void)requestData {
    __weak typeof(self) weakSelf = self;
    [[FSProgressViewTool shareInstance] showProgressView:self.view message:nil];
    
    BOOL isToday = NO;
    NSString *periodStr = [NSString stringWithFormat:@"%@~%@",_startDateStr,_endDateStr];
    if ([_startDateStr isEqualToString:_endDateStr]) {
        periodStr = _startDateStr;
        if ([_startDateStr isEqualToString:[self getCurrentDate]]) {
            isToday = YES;
        }
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[[NSUserDefaults standardUserDefaults] objectForKey:kMainStoreId] forKey:@"store_id"];
    [dict setObject:_startDateStr forKey:@"start_time"];
    [dict setObject:_endDateStr forKey:@"end_time"];
    [_vcModel getBusinessDetailWithPara:dict success:^(id responseObject) {
        
        [[FSProgressViewTool shareInstance] removeProgressView];
        [weakSelf.headerView refreshViewWithIncome:weakSelf.vcModel.incomeMoney
                                         recordNum:weakSelf.vcModel.incomeNum
                                           increse:weakSelf.vcModel.radio
                                     jieSuanIncome:weakSelf.vcModel.sumMoney
                                         sumIncome:weakSelf.vcModel.sumNum
                                     isTodayIncome:isToday
                                         periodStr:periodStr];
        [weakSelf.tableView_ reloadData];
    } failure:^(NSError *error) {
        [[FSProgressViewTool shareInstance] removeProgressView];
        [self.view makeToast:@"抱歉，数据请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
    }];
}

- (NSString *)getCurrentDate {
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    fmt.dateFormat = @"yyyy-MM-dd";
    NSString *dateStr = [fmt stringFromDate:[NSDate date]];
    return dateStr;
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"营业详情"];
    [self hideBackButtonTitle];
    UIImage *image = [kWhiteColor toImage];
    [self.navigationController.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:image];
    
    self.tableView_ = [self creatTableView];
    [self.view addSubview:self.tableView_];
    __weak typeof(self) weakSelf = self;
    [self.tableView_ mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weakSelf.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    _headerView = [[BusinessDetailHeader alloc] initWithFrame:CGRectMake(0, 0, screenW, 250)];
    self.tableView_.tableHeaderView = _headerView;
    _headerView.bDCalendarTapAction = ^(UILabel *titleLbl) {
        SelectTwoDateView *view = [[SelectTwoDateView alloc] init];
        view.sureBtnAction = ^(NSString *startDateStr, NSString *endDateStr) {
            if ([startDateStr isEqualToString:endDateStr]) {
                titleLbl.text = [NSString stringWithFormat:@"%@",startDateStr];
            }else {
                titleLbl.text = [NSString stringWithFormat:@"%@~%@",startDateStr,endDateStr];
            }
            weakSelf.startDateStr = startDateStr;
            weakSelf.endDateStr = endDateStr;
            [weakSelf requestData];
        };
        [weakSelf.view addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakSelf.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    };
    _headerView.leftSlideAction = ^{
        NSInteger interval = [weakSelf numberOfDaysWithTwoDay];
        weakSelf.startDateStr = [weakSelf dateWithFromDate:weakSelf.startDateStr days:-interval-1];
        weakSelf.endDateStr = [weakSelf dateWithFromDate:weakSelf.endDateStr days:-interval-1];
        NSLog(@"startDateStr = %@",weakSelf.startDateStr);
        NSLog(@"endDateStr = %@",weakSelf.endDateStr);
        [weakSelf requestData];
    };
    _headerView.rightSlideAction = ^{
        NSInteger interval = [weakSelf numberOfDaysWithTwoDay];
        weakSelf.startDateStr = [weakSelf dateWithFromDate:weakSelf.startDateStr days:interval+1];
        weakSelf.endDateStr = [weakSelf dateWithFromDate:weakSelf.endDateStr days:interval+1];
        NSLog(@"dateStr = %@",weakSelf.startDateStr);
        [weakSelf requestData];
    };
//    [self addRefreshFunction];
}

- (NSInteger)numberOfDaysWithTwoDay{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *fromDate = [dateFormatter dateFromString:_startDateStr];
    NSDate *endDate = [dateFormatter dateFromString:_endDateStr];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comp = [calendar components:NSCalendarUnitDay
                                         fromDate:fromDate
                                           toDate:endDate
                                          options:NSCalendarWrapComponents];
    return comp.day;
}

- (NSString *)dateWithFromDate:(NSString *)dateStr days:(NSInteger)days{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *latterDate = [dateFormatter dateFromString:dateStr];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *comps = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:latterDate];

    [comps setYear:0];
    [comps setMonth:0];
    [comps setDay:days];
    
    NSDate *getDate = [calendar dateByAddingComponents:comps toDate:latterDate options:0];
    return [dateFormatter stringFromDate:getDate];
}

- (UITableView *)creatTableView {
    UITableView *tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.backgroundColor = kWhiteColor;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [tableView registerClass:[BusinessDetailSection class] forHeaderFooterViewReuseIdentifier:sectionIdentify];
    return tableView;
}

#pragma mark - 上拉下拉刷新数据
- (void)addRefreshFunction{
    MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(downRefreshData)];
    header.lastUpdatedTimeLabel.hidden = YES;
    MJRefreshBackNormalFooter *footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(upRefreshData)];
    self.tableView_.mj_header = header;
    self.tableView_.mj_footer = footer;
    
    [header setTitle:@"下拉刷新" forState:MJRefreshStateIdle];
    [header setTitle:@"松手刷新" forState:MJRefreshStatePulling];
    [header setTitle:@"正在刷新数据" forState:MJRefreshStateRefreshing];
    
    [footer setTitle:@"上拉刷新" forState:MJRefreshStateIdle];
    [footer setTitle:@"正在加载数据" forState:MJRefreshStateRefreshing];
    [footer setTitle:@"松开加载更多数据" forState:MJRefreshStatePulling];
    [footer setTitle:@"无数据" forState:MJRefreshStateNoMoreData];
}

- (void)upRefreshData{
//    _pageNum++;
//    [self requestData];
}

- (void)downRefreshData {
//    if (_vcModel.listAry) {
//        [_vcModel.listAry removeAllObjects];
//    }
//    _pageNum = 0;
//    [self requestData];
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 46;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    BusinessDetailSection *sectionView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:sectionIdentify];
    return sectionView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _vcModel.incomeList.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    BusinessDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cell) {
        cell = [[BusinessDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
    }
    BusinessDetailListModel *model = [_vcModel.incomeList objectAtIndex:indexPath.row];
    [cell refreshViewWithModel:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
