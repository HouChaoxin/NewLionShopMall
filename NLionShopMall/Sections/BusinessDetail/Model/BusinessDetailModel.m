
//
//  BusinessDetailModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailModel.h"

@implementation BusinessDetailListModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.payment_time = [self isNil:dict[@"payment_time"]];
        self.order_amount = [self isNil:dict[@"order_amount"]];
        self.member_name = [self isNil:dict[@"member_name"]];
        self.typess = [self isNil:dict[@"typess"]];
        self.order_points = [self isNil:dict[@"order_points"]];
        self.pay_type = [self isNil:dict[@"pay_type"]];
    }
    return self;
}

@end


@implementation AllPayModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.moneys = [self isNil:dict[@"moneys"]];
        self.nums = [self isNil:dict[@"nums"]];
    }
    return self;
}

@end

@implementation BusinessDetailModel

-(instancetype)initWithDict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.moneys = [self isNil:dict[@"moneys"]];
        self.nums = [self isNil:dict[@"nums"]];
        self.allpay = [[AllPayModel alloc] initWithDict:[dict objectForKey:@"allpay"]];
        self.list = [[NSMutableArray alloc] init];
        NSArray *ary = dict[@"list"];
        for (NSDictionary *dict in ary) {
            BusinessDetailListModel *model = [[BusinessDetailListModel alloc] initWithDict:dict];
            [self.list addObject:model];
        }
    }
    return self;
}

@end
