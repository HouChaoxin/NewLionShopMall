//
//  BusinessDetailModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessDetailListModel : BaseObject

@property (nonatomic,copy) NSString *payment_time;
@property (nonatomic,copy) NSString *order_amount;
@property (nonatomic,copy) NSString *member_name;
@property (nonatomic,copy) NSString *typess;
@property (nonatomic,copy) NSString *order_points;
@property (nonatomic,copy) NSString *pay_type;

@end

@interface AllPayModel : BaseObject

@property (nonatomic,copy) NSString *moneys;
@property (nonatomic,copy) NSString *nums;

@end

@interface BusinessDetailModel : BaseObject

@property (nonatomic,copy) NSString *moneys;
@property (nonatomic,copy) NSString *nums;
@property (nonatomic,copy) NSString *ratio;
@property (nonatomic,strong) NSMutableArray *list;
@property (nonatomic,strong) AllPayModel *allpay;

-(instancetype)initWithDict:(NSDictionary *)dict;
@end
