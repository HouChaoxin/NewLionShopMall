//
//  BusinessDetailVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessDetailVCModel.h"
#import "BusinessDetailModel.h"

@implementation BusinessDetailVCModel

-(instancetype)init {
    self = [super init];
    if (self) {
        _incomeList = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)getBusinessDetailWithPara:(NSDictionary *)dict
                               success:(SuccessBlock)successBlock
                               failure:(FailureBlock)failureBlock {
    if (_incomeList) {
        [_incomeList removeAllObjects];
    }
    NSString *urlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kIncomeUrl];
    
    [NetHelper GET:urlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                BusinessDetailModel *model = [[BusinessDetailModel alloc] initWithDict:[rootDict objectForKey:@"data"]];
                [_incomeList addObjectsFromArray:model.list];
                _incomeMoney = model.moneys;
                _incomeNum = model.nums;
                _radio = model.ratio;
                
                AllPayModel *mod = model.allpay;
                _sumNum = mod.nums;
                _sumMoney = mod.moneys;
                
                successBlock(nil);
            }else{
                failureBlock(nil);
            }
        }else {
            failureBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(nil);
    }];
}

@end
