//
//  BusinessDetailVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/24.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusinessDetailVCModel : NSObject

@property (nonatomic,copy) NSString *incomeMoney;
@property (nonatomic,copy) NSString *incomeNum;
@property (nonatomic,copy) NSString *sumMoney;
@property (nonatomic,copy) NSString *sumNum;
@property (nonatomic,copy) NSString *radio;
@property (nonatomic,strong) NSMutableArray *incomeList;

- (void)getBusinessDetailWithPara:(NSDictionary *)dict
                          success:(SuccessBlock)successBlock
                          failure:(FailureBlock)failureBlock;
@end
