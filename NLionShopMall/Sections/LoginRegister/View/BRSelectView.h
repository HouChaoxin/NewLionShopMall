//
//  BRSelectView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BRSelectAction)(UIButton *sender);

@interface BRSelectView : UIView

@property (nonatomic,strong) UIButton *shBtn;
@property (nonatomic,strong) UIButton *careerBtn;
@property (nonatomic,strong) UIButton *careerBtnTwo;
@property (nonatomic,strong) UIButton *careerBtnThree;
@property (nonatomic,strong) UIButton *locationBtnOne;
@property (nonatomic,strong) UIButton *locationBtnTwo;
@property (nonatomic,strong) UIButton *locationBtnThree;
@property (nonatomic,strong) UITextField *addressTF;

@property (nonatomic,copy) BRSelectAction businessAttributeSelectAction;
@property (nonatomic,copy) BRSelectAction careerOneSelectAction;
@property (nonatomic,copy) BRSelectAction careerTwoSelectAction;
@property (nonatomic,copy) BRSelectAction careerThreeSelectAction;
@property (nonatomic,copy) BRSelectAction locationAction;
@property (nonatomic,copy) BRSelectAction provinceSelectAction;
@property (nonatomic,copy) BRSelectAction citySelectAction;
@property (nonatomic,copy) BRSelectAction quxianSelectAction;

@end
