//
//  JieSuanRenView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "JieSuanRenView.h"

@interface JieSuanRenView()

@end

@implementation JieSuanRenView


-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    _jieSuanRenIDCell = [[BRTextFieldCell alloc] initWithTitle:@"结算人证件号"
                                              placeHolder:@"请输入结算人身份证号"
                                                    isRequired:YES];
    [self addSubview:_jieSuanRenIDCell];
    [_jieSuanRenIDCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self);
        make.height.equalTo(@53);
    }];

    _iDCardDateCell = [[BRTextFieldCell alloc] initWithTitle:@"身份证有效期"
                                                 placeHolder:@"年/月/日"
                                                  isRequired:YES];
    [self addSubview:_iDCardDateCell];
    [_iDCardDateCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_jieSuanRenIDCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _accountNameCell = [[BRTextFieldCell alloc] initWithTitle:@"账户开户名"
                                                  placeHolder:@"请输入结算账户开户名，同公司注册名称" isRequired:YES];
    [self addSubview:_accountNameCell];
    [_accountNameCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_iDCardDateCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _accountNumCell = [[BRTextFieldCell alloc] initWithTitle:@"账户开户号"
                                                 placeHolder:@"请输入对公账号"
                                                  isRequired:YES];
    [self addSubview:_accountNumCell];
    [_accountNumCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_accountNameCell.mas_bottom);
        make.height.equalTo(@53);
    }];
    
    _phoneCell = [[BRTextFieldCell alloc] initWithTitle:@"预留手机号"
                                            placeHolder:@"请输入银行预留手机号"
                                             isRequired:NO];
    [self addSubview:_phoneCell];
    [_phoneCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_accountNumCell.mas_bottom);
        make.height.equalTo(@53);
    }];
}



@end
