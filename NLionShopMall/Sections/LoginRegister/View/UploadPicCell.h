//
//  UploadPicCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^UploadPicAction)(UIImageView *imgView,NSInteger index);

@interface PicUploadMod : NSObject

@property (nonatomic,copy) NSString *titleStr;
@property (nonatomic,copy) NSString *alertStr;
@property (nonatomic,assign) NSInteger index;
@property (nonatomic,copy) NSString *picUrl;
@property (nonatomic,copy) NSString *key;
@property (nonatomic,copy) NSString *errorAlert;

- (instancetype)initWithTitle:(NSString *)title
                        alert:(NSString *)alert
                        index:(NSInteger)index
                       picUrl:(NSString *)picUrl
                          key:(NSString *)key
                   errorAlert:(NSString *)errorAlert;

@end

@interface UploadPicCell : BaseView

-(instancetype)initWithTitle:(NSString *)title
                       alert:(NSString *)alert
                       index:(NSInteger)index;
@property (nonatomic,copy) UploadPicAction uploadPicAction;

@end
