//
//  JiSuanSelectCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^SelectBtnAction)(UIButton *sender);

@interface JiSuanSelectCell : UIView

@property (nonatomic,strong) UIButton *selectBtn;
@property (nonatomic,copy) SelectBtnAction selectBtnAction;

-(instancetype)initWithTitle:(NSString *)title isRequired:(BOOL)isRequired;

@end
