//
//  JiSuanSelectCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "JiSuanSelectCell.h"

@implementation JiSuanSelectCell

-(instancetype)initWithTitle:(NSString *)title
                  isRequired:(BOOL)isRequired {
    self = [super init];
    if (self) {
        [self initViewWithTitle:title isRequired:isRequired];
    }
    return self;
}

- (void)initViewWithTitle:(NSString *)title
               isRequired:(BOOL)isRequired {
    
    UIView *bgView = [[UIView alloc] init];
    bgView.layer.cornerRadius = 5.0f;
    bgView.layer.borderWidth = 1.0f;
    bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(self).offset(11);
        make.bottom.equalTo(self);
        make.right.equalTo(self).offset(-14);
    }];
    
    if (isRequired) {
        UILabel *lbl = [FSViewHelper createLblWithText:@"*"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
        lbl.textColor = kRedColor;
        [bgView addSubview:lbl];
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bgView).offset(16);
            make.left.equalTo(bgView).offset(6);
            make.bottom.equalTo(bgView).offset(-8);
            make.width.equalTo(@8);
        }];
    }
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:title textAlignment:NSTextAlignmentLeft fontSize:16];
    [bgView addSubview:titleLbl];
    CGFloat titleWidth = [FSViewHelper caculateLblWidthWithString:title andFontSize:17];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(bgView).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:titleWidth]);
    }];
    
    _selectBtn = [FSViewHelper createBtnWithTitle:@"请选择"
                                             bgColor:kWhiteColor
                                          titleColor:colorOf(215, 215, 215)];
    _selectBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _selectBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_selectBtn addTarget:self action:@selector(selectBtnClick:) forControlEvents:UIControlEventTouchDown];
    [bgView addSubview:_selectBtn];
    [_selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(titleLbl.mas_right).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.right.equalTo(bgView).offset(-16);
    }];
    
    UIImageView *pullArrow = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgView addSubview:pullArrow];
    [pullArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(13);
        make.right.equalTo(_selectBtn.mas_right).offset(-8);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
}

- (void)selectBtnClick:(UIButton *)sender {
    if (_selectBtnAction) {
        _selectBtnAction(sender);
    }
}

@end
