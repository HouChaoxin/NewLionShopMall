//
//  CreditalView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "CreditalView.h"

@interface CreditalView()

@end

@implementation CreditalView

-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    _creditalNumCell = [[BRTextFieldCell alloc] initWithTitle:@"执照证件号码:"
                                                  placeHolder:@"执照注册号或统一社会信用代码"
                                                   isRequired:YES];
    [self addSubview:_creditalNumCell];
    [_creditalNumCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.equalTo(@53);
    }];

    _creditalDateCell = [[BRTextFieldCell alloc] initWithTitle:@"证件到期日期:"
                                                  placeHolder:@"年/月/日"
                                                    isRequired:YES];
    [self addSubview:_creditalDateCell];
    [_creditalDateCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_creditalNumCell.mas_bottom);
        make.height.equalTo(@53);
    }];
    __weak typeof(self) weakSelf = self;
    _creditalTypeCell = [[JiSuanSelectCell alloc] initWithTitle:@"企业证件类型:" isRequired:YES];
    [self addSubview:_creditalTypeCell];
    _creditalTypeCell.selectBtnAction = ^(UIButton *sender) {
        if (weakSelf.qiYeZhengJianAction) {
            weakSelf.qiYeZhengJianAction(sender);
        }
    };
    [_creditalTypeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_creditalDateCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _jisuanAccountCell = [[JiSuanSelectCell alloc] initWithTitle:@"结算账户类型:" isRequired:YES];
    [self addSubview:_jisuanAccountCell];
    _jisuanAccountCell.selectBtnAction = ^(UIButton *sender) {
        if (weakSelf.jieSuanAccountAction) {
            weakSelf.jieSuanAccountAction(sender);
        }
    };
    [_jisuanAccountCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_creditalTypeCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _ruZhangFormCell = [[JiSuanSelectCell alloc] initWithTitle:@"入账形式:" isRequired:YES];
    _ruZhangFormCell.hidden = YES;
    [self addSubview:_ruZhangFormCell];
    _ruZhangFormCell.selectBtnAction = ^(UIButton *sender) {
        if (weakSelf.ruZhangAction) {
            weakSelf.ruZhangAction(sender);
        }
    };
    [_ruZhangFormCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_jisuanAccountCell.mas_bottom);
        make.height.equalTo(@53);
    }];
}

@end
