//
//  BusinessHoursCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BHTimeSelectAction)(UIButton *sender);
@interface BusinessHoursCell : UIView

@property (nonatomic,strong) UIButton *timeBtnOne;
@property (nonatomic,strong) UIButton *timeBtnTwo;

@property (nonatomic,copy) BHTimeSelectAction startTimeAction;
@property (nonatomic,copy) BHTimeSelectAction endTimeAction;

@end
