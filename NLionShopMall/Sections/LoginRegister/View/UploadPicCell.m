//
//  UploadPicCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "UploadPicCell.h"

@implementation PicUploadMod

- (instancetype)initWithTitle:(NSString *)title
                        alert:(NSString *)alert
                        index:(NSInteger)index
                       picUrl:(NSString *)picUrl
                          key:(NSString *)key
                   errorAlert:(NSString *)errorAlert {
    self = [super init];
    if (self) {
        self.titleStr = title;
        self.alertStr = alert;
        self.index = index;
        self.picUrl = picUrl;
        self.key = key;
        self.errorAlert = errorAlert;
    }
    return self;
}
@end

//height:168
@interface UploadPicCell()

@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,assign) NSInteger index;
@end

@implementation UploadPicCell

-(instancetype)initWithTitle:(NSString *)title
                       alert:(NSString *)alert
                       index:(NSInteger)index {
    self = [super init];
    if (self) {
        self.index = index;
        [self initViewWithTitle:title alert:alert];
    }
    return self;
}

- (void)initViewWithTitle:(NSString *)title alert:(NSString *)alert {
    
    UILabel *lbl = [FSViewHelper createLblWithText:@"*"
                                     textAlignment:NSTextAlignmentCenter
                                          fontSize:16];
    lbl.textColor = kRedColor;
    [self addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(30);
        make.left.equalTo(self).offset(16);
        make.height.equalTo(@8);
        make.width.equalTo(@8);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:title
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    [self addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(21);
        make.left.equalTo(self).offset(24);
        make.height.equalTo(@28);
        make.width.equalTo(@200);
    }];
    
    _imgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"addImg"]
                                          bgColor:kWhiteColor];
    _imgView.userInteractionEnabled = YES;
    [self addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(16);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(60, 60)]);
    }];
    
    UITapGestureRecognizer *tapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(uploadPicAction:)];
    [_imgView addGestureRecognizer:tapGR];
    
    UILabel *alertLbl = [FSViewHelper createLblWithText:alert
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:16];
    alertLbl.textColor = [ColorConverter colorWithHexString:@"0x999999"];
    [self addSubview:alertLbl];
    [alertLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imgView.mas_bottom).offset(16);
        make.centerX.equalTo(self);
        make.height.equalTo(@28);
        make.width.equalTo(@320);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil
                                          bgColor:[ColorConverter colorWithHexString:@"0xE6E6E6"]];
    [self addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(alertLbl.mas_bottom).offset(16);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@1);
    }];
}

- (void)uploadPicAction:(UITapGestureRecognizer *)tap {
    UIImageView *imgView = (UIImageView *)tap.view;
    if (_uploadPicAction) {
        _uploadPicAction(imgView,_index);
    }
}

@end
