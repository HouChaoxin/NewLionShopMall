//
//  JieSuanRenView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BRTextFieldCell.h"

@interface JieSuanRenView : UIView

@property (nonatomic,strong) BRTextFieldCell *jieSuanRenIDCell;
@property (nonatomic,strong) BRTextFieldCell *iDCardDateCell;
@property (nonatomic,strong) BRTextFieldCell *accountNameCell;
@property (nonatomic,strong) BRTextFieldCell *accountNumCell;
@property (nonatomic,strong) BRTextFieldCell *phoneCell;

@end
