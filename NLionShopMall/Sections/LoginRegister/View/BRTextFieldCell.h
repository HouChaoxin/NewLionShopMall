//
//  BRTextFieldCell.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRTextFieldCell : BaseView<UITextFieldDelegate>

@property (nonatomic,strong) UITextField *textField;

-(instancetype)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder isRequired:(BOOL)isRequired;

@end
