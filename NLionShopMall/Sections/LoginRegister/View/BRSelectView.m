//
//  BRSelectView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BRSelectView.h"

@implementation BRSelectView

-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    UIView *bgViewOne = [[UIView alloc] init];
    bgViewOne.layer.cornerRadius = 5.0f;
    bgViewOne.layer.borderWidth = 1.0f;
    bgViewOne.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgViewOne];
    [bgViewOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(self).offset(11);
        make.height.equalTo(@42);
        make.right.equalTo(self).offset(-14);
    }];
    
    UILabel *lbl0 = [FSViewHelper createLblWithText:@"*"
                                     textAlignment:NSTextAlignmentCenter
                                          fontSize:16];
    lbl0.textColor = kRedColor;
    [bgViewOne addSubview:lbl0];
    [lbl0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewOne).offset(16);
        make.left.equalTo(bgViewOne).offset(6);
        make.width.equalTo(@8);
        make.height.equalTo(@8);
    }];
    
    UILabel *shTitleLbl = [FSViewHelper createLblWithText:@"商户属性:"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    [bgViewOne addSubview:shTitleLbl];
    CGFloat aWidth = [FSViewHelper caculateLblWidthWithString:@"商户属性:" andFontSize:17];
    [shTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(bgViewOne).offset(16);
        make.top.equalTo(bgViewOne).offset(8);
        make.height.equalTo(@26);
        make.width.equalTo([NSNumber numberWithFloat:aWidth]);
    }];
    
    _shBtn = [FSViewHelper createBtnWithTitle:@"请选择"
                                               bgColor:kWhiteColor
                                            titleColor:colorOf(215, 215, 215)];
    [_shBtn addTarget:self action:@selector(businessAttributeSelectClick:) forControlEvents:UIControlEventTouchDown];
    _shBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _shBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [bgViewOne addSubview:_shBtn];
    [_shBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewOne).offset(8);
        make.left.equalTo(shTitleLbl.mas_right).offset(16);
        make.right.equalTo(bgViewOne).offset(-16);
        make.height.equalTo(@26);
    }];
    
    UIImageView *pullArrow = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewOne addSubview:pullArrow];
    [pullArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewOne).offset(13);
        make.right.equalTo(bgViewOne).offset(-16);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    //行业类别
    UIView *bgViewTwo = [[UIView alloc] init];
    bgViewTwo.layer.cornerRadius = 5.0f;
    bgViewTwo.layer.borderWidth = 1.0f;
    bgViewTwo.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgViewTwo];
    [bgViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(bgViewOne.mas_bottom).offset(11);
        make.height.equalTo(@85);
        make.right.equalTo(self).offset(-14);
    }];
    
    UILabel *lbl1 = [FSViewHelper createLblWithText:@"*"
                                      textAlignment:NSTextAlignmentCenter
                                           fontSize:16];
    lbl1.textColor = kRedColor;
    [bgViewTwo addSubview:lbl1];
    [lbl1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewTwo).offset(16);
        make.left.equalTo(bgViewTwo).offset(6);
        make.width.equalTo(@8);
        make.height.equalTo(@8);
    }];
    
    UILabel *careerTitleLbl = [FSViewHelper createLblWithText:@"行业类别:"
                                            textAlignment:NSTextAlignmentLeft
                                                 fontSize:16];
    [bgViewTwo addSubview:careerTitleLbl];
    CGFloat cWidth = [FSViewHelper caculateLblWidthWithString:@"行业类别:" andFontSize:17];
    [careerTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewTwo).offset(8);
        make.left.equalTo(bgViewTwo).offset(16);
        make.height.equalTo(@26);
        make.width.equalTo([NSNumber numberWithFloat:cWidth]);
    }];
    
    _careerBtn = [FSViewHelper createBtnWithTitle:@"请选择"
                                               bgColor:kWhiteColor
                                            titleColor:colorOf(215, 215, 215)];
    _careerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _careerBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [_careerBtn addTarget:self action:@selector(careerOneSelectClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewTwo addSubview:_careerBtn];
    CGFloat btnWidth = screenW/3-104/3;
    [_careerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(careerTitleLbl.mas_bottom).offset(12);
        make.left.equalTo(bgViewTwo).offset(16);
        make.height.equalTo(@26);
        make.width.equalTo([NSNumber numberWithFloat:btnWidth]);
    }];
    
    UIImageView *pullArrowTwo = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewTwo addSubview:pullArrowTwo];
    [pullArrowTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_careerBtn);
        make.right.equalTo(_careerBtn.mas_right).offset(-4);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _careerBtnTwo = [FSViewHelper createBtnWithTitle:@"请选择"
                                                   bgColor:kWhiteColor
                                                titleColor:colorOf(215, 215, 215)];
    _careerBtnTwo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _careerBtnTwo.titleLabel.font = [UIFont systemFontOfSize:16];
    [_careerBtnTwo addTarget:self action:@selector(careerTwoSelectClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewTwo addSubview:_careerBtnTwo];
    [_careerBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(careerTitleLbl.mas_bottom).offset(12);
        make.left.equalTo(_careerBtn.mas_right).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:btnWidth]);
        make.height.equalTo(@26);
    }];
    
    UIImageView *pullArrowThree = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewTwo addSubview:pullArrowThree];
    [pullArrowThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_careerBtnTwo);
        make.right.equalTo(_careerBtnTwo.mas_right).offset(-4);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _careerBtnThree = [FSViewHelper createBtnWithTitle:@"请选择"
                                             bgColor:kWhiteColor
                                          titleColor:colorOf(215, 215, 215)];
    _careerBtnThree.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _careerBtnThree.titleLabel.font = [UIFont systemFontOfSize:16];
    [_careerBtnThree addTarget:self action:@selector(careerThreeSelectClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewTwo addSubview:_careerBtnThree];
    [_careerBtnThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(careerTitleLbl.mas_bottom).offset(12);
        make.left.equalTo(_careerBtnTwo.mas_right).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:btnWidth]);
        make.height.equalTo(@26);
    }];
    
    UIImageView *pullArwThree = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"]
                                                           bgColor:kWhiteColor];
    [bgViewTwo addSubview:pullArwThree];
    [pullArwThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_careerBtnThree);
        make.right.equalTo(_careerBtnThree.mas_right).offset(-4);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    //商户地址
    UIView *bgViewThree = [[UIView alloc] init];
    bgViewThree.layer.cornerRadius = 5.0f;
    bgViewThree.layer.borderWidth = 1.0f;
    bgViewThree.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgViewThree];
    [bgViewThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(bgViewTwo.mas_bottom).offset(11);
        make.height.equalTo(@164);
        make.right.equalTo(self).offset(-14);
    }];
    
    UILabel *lbl2 = [FSViewHelper createLblWithText:@"*"
                                      textAlignment:NSTextAlignmentCenter
                                           fontSize:16];
    lbl2.textColor = kRedColor;
    [bgViewThree addSubview:lbl2];
    [lbl2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewThree).offset(16);
        make.left.equalTo(bgViewThree).offset(6);
        make.width.equalTo(@8);
        make.height.equalTo(@8);
    }];
    
    UILabel *addressTitleLbl = [FSViewHelper createLblWithText:@"商户地址:"
                                                textAlignment:NSTextAlignmentLeft
                                                     fontSize:16];
    CGFloat addWidth = [FSViewHelper caculateLblWidthWithString:@"商户地址:" andFontSize:17];
    [bgViewThree addSubview:addressTitleLbl];
    [addressTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewThree).offset(8);
        make.left.equalTo(bgViewThree).offset(16);
        make.height.equalTo(@28);
        make.width.equalTo([NSNumber numberWithFloat:addWidth]);
    }];
    
    UILabel *locationLbl = [FSViewHelper createLblWithText:@""
                                                 textAlignment:NSTextAlignmentLeft
                                                      fontSize:16];
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@"点击使用GPS辅助定位"
                                                                                attributes:@{NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)}];
    locationLbl.attributedText = attrStr;
    
    [bgViewThree addSubview:locationLbl];
    [locationLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgViewThree).offset(8);
        make.left.equalTo(addressTitleLbl.mas_right).offset(16);
        make.right.equalTo(bgViewThree).offset(-16);
        make.height.equalTo(@28);
    }];
    
    UITapGestureRecognizer *locationTapGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(locationClick:)];
    [locationLbl addGestureRecognizer:locationTapGR];
    
    CGFloat w = screenW/3-104/3;
    
    _locationBtnOne = [FSViewHelper createBtnWithTitle:@" 省份"
                                                      bgColor:kWhiteColor
                                                   titleColor:colorOf(215, 215, 215)];
    _locationBtnOne.layer.cornerRadius = 5.0f;
    _locationBtnOne.layer.borderWidth = 1.0f;
    _locationBtnOne.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    _locationBtnOne.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _locationBtnOne.titleLabel.font = [UIFont systemFontOfSize:16];
    [_locationBtnOne addTarget:self action:@selector(provinceBtnClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewThree addSubview:_locationBtnOne];
    [_locationBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addressTitleLbl.mas_bottom).offset(8);
        make.left.equalTo(bgViewThree).offset(16);
        make.height.equalTo(@30);
        make.width.equalTo([NSNumber numberWithFloat:w]);
    }];
    
    UIImageView *pullArrowFour = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewThree addSubview:pullArrowFour];
    [pullArrowFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_locationBtnOne).offset(7);
        make.right.equalTo(_locationBtnOne.mas_right);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _locationBtnTwo = [FSViewHelper createBtnWithTitle:@" 市"
                                                        bgColor:kWhiteColor
                                                     titleColor:colorOf(215, 215, 215)];
    _locationBtnTwo.layer.cornerRadius = 5.0f;
    _locationBtnTwo.layer.borderWidth = 1.0f;
    _locationBtnTwo.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    _locationBtnTwo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _locationBtnTwo.titleLabel.font = [UIFont systemFontOfSize:16];
    [_locationBtnTwo addTarget:self action:@selector(cityBtnClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewThree addSubview:_locationBtnTwo];
    [_locationBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addressTitleLbl.mas_bottom).offset(8);
        make.left.equalTo(_locationBtnOne.mas_right).offset(16);
        make.height.equalTo(@30);
        make.width.equalTo([NSNumber numberWithFloat:w]);
    }];
    
    UIImageView *pullArrowFive = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewThree addSubview:pullArrowFive];
    [pullArrowFive mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_locationBtnTwo).offset(7);
        make.right.equalTo(_locationBtnTwo.mas_right);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _locationBtnThree = [FSViewHelper createBtnWithTitle:@" 区/县"
                                                        bgColor:kWhiteColor
                                                     titleColor:colorOf(215, 215, 215)];
    _locationBtnThree.layer.cornerRadius = 5.0f;
    _locationBtnThree.layer.borderWidth = 1.0f;
    _locationBtnThree.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    _locationBtnThree.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _locationBtnThree.titleLabel.font = [UIFont systemFontOfSize:16];
    [_locationBtnThree addTarget:self action:@selector(countryBtnClick:) forControlEvents:UIControlEventTouchDown];
    [bgViewThree addSubview:_locationBtnThree];
    [_locationBtnThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addressTitleLbl.mas_bottom).offset(8);
        make.left.equalTo(_locationBtnTwo.mas_right).offset(16);
        make.height.equalTo(@30);
        make.width.equalTo([NSNumber numberWithFloat:w]);
    }];
    
    UIImageView *pullArrowSix = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgViewThree addSubview:pullArrowSix];
    [pullArrowSix mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_locationBtnThree).offset(7);
        make.right.equalTo(_locationBtnThree.mas_right);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    UILabel *lbl3 = [FSViewHelper createLblWithText:@"*"
                                      textAlignment:NSTextAlignmentCenter
                                           fontSize:16];
    lbl3.textColor = kRedColor;
    [bgViewThree addSubview:lbl3];
    [lbl3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_locationBtnThree.mas_bottom).offset(20);
        make.left.equalTo(bgViewThree).offset(6);
        make.width.equalTo(@8);
        make.height.equalTo(@8);
    }];
    
    UILabel *addressTitleLblTwo = [FSViewHelper createLblWithText:@"商户详细地址:"
                                                 textAlignment:NSTextAlignmentLeft
                                                      fontSize:16];
    CGFloat addWidth1 = [FSViewHelper caculateLblWidthWithString:@"商户详细地址:" andFontSize:17];
    [bgViewThree addSubview:addressTitleLblTwo];
    [addressTitleLblTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_locationBtnThree.mas_bottom).offset(12);
        make.left.equalTo(bgViewThree).offset(16);
        make.height.equalTo(@28);
        make.width.equalTo([NSNumber numberWithFloat:addWidth1]);
    }];
    
    _addressTF = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                                  fontSize:16
                                                                 textColor:kBlackColor];
    _addressTF.placeholder = @"商户详细地址";
    [bgViewThree addSubview:_addressTF];
    [_addressTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(addressTitleLblTwo.mas_bottom).offset(12);
        make.left.equalTo(bgViewThree).offset(16);
        make.right.equalTo(bgViewThree).offset(-16);
        make.height.equalTo(@28);
    }];
}

- (void)businessAttributeSelectClick:(UIButton *)sender {
    if (_businessAttributeSelectAction) {
        _businessAttributeSelectAction(sender);
    }
}

- (void)careerOneSelectClick:(UIButton *)sender {
    if (_careerOneSelectAction) {
        _careerOneSelectAction(sender);
    }
}

- (void)careerTwoSelectClick:(UIButton *)sender {
    if (_careerTwoSelectAction) {
        _careerTwoSelectAction(sender);
    }
}

- (void)careerThreeSelectClick:(UIButton *)sender {
    if (_careerThreeSelectAction) {
        _careerThreeSelectAction(sender);
    }
}

- (void)locationClick:(UIButton *)btn {
    if (_locationAction) {
        _locationAction(btn);
    }
}

- (void)provinceBtnClick:(UIButton *)sender {
    
    if (_provinceSelectAction) {
        _provinceSelectAction(sender);
    }
}

- (void)cityBtnClick:(UIButton *)sender {
    
    if (_citySelectAction) {
        _citySelectAction(sender);
    }
}

- (void)countryBtnClick:(UIButton *)sender {
    if (_quxianSelectAction) {
        _quxianSelectAction(sender);
    }
}

@end
