//
//  BRTextFieldCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BRTextFieldCell.h"

@interface BRTextFieldCell()

@property (nonatomic,strong) UIView *bgView;
@end

@implementation BRTextFieldCell

-(instancetype)initWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder isRequired:(BOOL)isRequired {
    self = [super init];
    if (self) {
        [self initViewWithTitle:title placeHolder:placeHolder isRequired:isRequired];
    }
    return self;
}

- (void)initViewWithTitle:(NSString *)title placeHolder:(NSString *)placeHolder isRequired:(BOOL)isRequired {
    
    _bgView = [[UIView alloc] init];
    _bgView.layer.cornerRadius = 5.0f;
    _bgView.layer.borderWidth = 1.0f;
    _bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:_bgView];
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(self).offset(11);
        make.bottom.equalTo(self);
        make.right.equalTo(self).offset(-14);
    }];
    
    if (isRequired) {
        UILabel *lbl = [FSViewHelper createLblWithText:@"*"
                                         textAlignment:NSTextAlignmentCenter
                                              fontSize:16];
        lbl.textColor = kRedColor;
        [_bgView addSubview:lbl];
        [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_bgView).offset(16);
            make.left.equalTo(_bgView).offset(6);
            make.bottom.equalTo(_bgView).offset(-8);
            make.width.equalTo(@8);
        }];
    }
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:title textAlignment:NSTextAlignmentLeft fontSize:16];
    [_bgView addSubview:titleLbl];
    CGFloat titleWidth = [FSViewHelper caculateLblWidthWithString:title andFontSize:17];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgView).offset(8);
        make.left.equalTo(_bgView).offset(16);
        make.bottom.equalTo(_bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:titleWidth]);
    }];
    
    _textField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                                  fontSize:16
                                                                 textColor:kBlackColor];
    _textField.delegate = self;
    _textField.placeholder = placeHolder;
    [_bgView addSubview:_textField];
    [_textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgView).offset(8);
        make.left.equalTo(titleLbl.mas_right).offset(16);
        make.bottom.equalTo(_bgView).offset(-8);
        make.right.equalTo(_bgView).offset(-16);
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    _bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0x4593E8"].CGColor;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    _bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
}

@end
