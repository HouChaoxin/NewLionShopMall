//
//  BRHeaderView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BRHeaderView.h"

@interface BRHeaderView(){
    
    UIImageView *imgOne;
    UIImageView *imgTwo;
    UIImageView *imgThree;
    UIImageView *imgFour;
    UIImageView *imgFive;
    UIImageView *imgSix;
    UIImageView *imgSeven;
    
    UILabel *baseResourceLbl;
    UILabel *billAccountLbl;
    UILabel *picUplodeLbl;
}

@end

@implementation BRHeaderView


-(instancetype)init{
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    imgOne = [FSViewHelper createImgViewWithImg:nil
                                        bgColor:[ColorConverter colorWithHexString:@"0x4593E8"]];
    imgOne.layer.cornerRadius = 9.0f;
    [self addSubview:imgOne];
    [imgOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.left.equalTo(self).offset(48);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(18, 18)]);
    }];
    
    imgTwo = [FSViewHelper createImgViewWithImg:nil
                                        bgColor:[ColorConverter colorWithHexString:@"0x4593E8"]];
    [self addSubview:imgTwo];
    [imgTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.left.equalTo(imgOne.mas_right);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/4-37.5, 2)]);
    }];
    
    imgThree = [FSViewHelper createImgViewWithImg:nil
                                        bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]];
    [self addSubview:imgThree];
    [imgThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.left.equalTo(imgTwo.mas_right);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/4-37.5, 2)]);
    }];
    
    imgFour = [FSViewHelper createImgViewWithImg:nil
                                        bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]];
    imgFour.layer.cornerRadius = 9.0f;
    [self addSubview:imgFour];
    [imgFour mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.centerX.equalTo(self);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(18, 18)]);
    }];
    
    imgFive = [FSViewHelper createImgViewWithImg:nil
                                          bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]];
    [self addSubview:imgFive];
    [imgFive mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.left.equalTo(imgFour.mas_right);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/4-37.5, 2)]);
    }];
    
    imgSix = [FSViewHelper createImgViewWithImg:nil
                                         bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]];
    [self addSubview:imgSix];
    [imgSix mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(23);
        make.left.equalTo(imgFive.mas_right);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(screenW/4-37.5, 2)]);
    }];
    
    imgSeven = [FSViewHelper createImgViewWithImg:nil
                                         bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]];
    imgSeven.layer.cornerRadius = 9.0f;
    [self addSubview:imgSeven];
    [imgSeven mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.right.equalTo(self).offset(-48);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(18, 18)]);
    }];
    
    baseResourceLbl = [FSViewHelper createLblWithText:@"基础资料"
                                       textAlignment:NSTextAlignmentCenter
                                            fontSize:16];
    baseResourceLbl.textColor = kBlackColor;
    [self addSubview:baseResourceLbl];
    [baseResourceLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgOne.mas_bottom).offset(8);
        make.centerX.equalTo(imgOne);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-21]);
    }];
    
    billAccountLbl = [FSViewHelper createLblWithText:@"结算账号"
                                  textAlignment:NSTextAlignmentCenter
                                       fontSize:16];
    billAccountLbl.textColor = kBlackColor;
    [self addSubview:billAccountLbl];
    [billAccountLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgOne.mas_bottom).offset(8);
        make.centerX.equalTo(self);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-21]);
    }];
    
    picUplodeLbl = [FSViewHelper createLblWithText:@"证照上传"
                                     textAlignment:NSTextAlignmentCenter
                                          fontSize:16];
    picUplodeLbl.textColor = kBlackColor;
    [self addSubview:picUplodeLbl];
    [picUplodeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(imgOne.mas_bottom).offset(8);
        make.centerX.equalTo(imgSeven);
        make.bottom.equalTo(self).offset(-16);
        make.width.equalTo([NSNumber numberWithFloat:screenW/3-21]);
    }];
}

- (void)refreshViewWithIndex:(NSInteger)index {
    
    switch (index) {
        case 1:{
            imgOne.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgTwo.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgThree.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            imgFour.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            imgFive.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            imgSix.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            imgSeven.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            break;
        }case 2:{
            imgOne.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgTwo.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgThree.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgFour.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgFive.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgSix.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            imgSeven.backgroundColor = [ColorConverter colorWithHexString:@"0xC3E0FF"];
            break;
        }case 3:{
            imgOne.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgTwo.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgThree.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgFour.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgFive.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgSix.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            imgSeven.backgroundColor = [ColorConverter colorWithHexString:@"0x4593E8"];
            break;
        }
        default:
            break;
    }
}

@end
