//
//  AccountBankView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JiSuanSelectCell.h"

typedef void(^BankSelectAction)(UIButton *sender);
@interface AccountBankView : UIView

@property (nonatomic,strong) JiSuanSelectCell *accountBankCell;
@property (nonatomic,strong) JiSuanSelectCell *accountBranchCell;
@property (nonatomic,strong) UIButton *selectBtnOne;//省市
@property (nonatomic,strong) UIButton *selectBtnTwo;//市/区

@property (nonatomic,copy) BankSelectAction bankSelecAction;
@property (nonatomic,copy) BankSelectAction provinceSelectAction;
@property (nonatomic,copy) BankSelectAction citySelectAction;
@property (nonatomic,copy) BankSelectAction branchSelectAction;

@end
