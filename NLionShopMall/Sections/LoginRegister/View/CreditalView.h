//
//  CreditalView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BRTextFieldCell.h"
#import "JiSuanSelectCell.h"

typedef void(^CreditalSelectAction)(UIButton *sender);

@interface CreditalView : UIView

@property (nonatomic,strong) BRTextFieldCell *creditalNumCell;
@property (nonatomic,strong) BRTextFieldCell *creditalDateCell;
@property (nonatomic,strong) JiSuanSelectCell *creditalTypeCell;
@property (nonatomic,strong) JiSuanSelectCell *jisuanAccountCell;
@property (nonatomic,strong) JiSuanSelectCell *ruZhangFormCell;

@property (nonatomic,copy) CreditalSelectAction qiYeZhengJianAction;
@property (nonatomic,copy) CreditalSelectAction jieSuanAccountAction;
@property (nonatomic,copy) CreditalSelectAction ruZhangAction;

@end
