//
//  StoreDescriptionView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StoreDescriptionView : BaseView

@property (nonatomic,strong) UITextView *textView;

@end
