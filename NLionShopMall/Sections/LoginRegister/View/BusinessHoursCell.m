//
//  BusinessHoursCell.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BusinessHoursCell.h"

@implementation BusinessHoursCell


-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UIView *bgView = [[UIView alloc] init];
    bgView.layer.cornerRadius = 5.0f;
    bgView.layer.borderWidth = 1.0f;
    bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(self).offset(11);
        make.bottom.equalTo(self);
        make.right.equalTo(self).offset(-14);
    }];
    
    UILabel *lbl = [FSViewHelper createLblWithText:@"*"
                                     textAlignment:NSTextAlignmentCenter
                                          fontSize:16];
    lbl.textColor = kRedColor;
    [bgView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(16);
        make.left.equalTo(bgView).offset(6);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo(@8);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"营业时间"
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:16];
    CGFloat width = [FSViewHelper caculateLblWidthWithString:@"营业时间" andFontSize:17];
    [bgView addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(bgView).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:width]);
    }];
    
    _timeBtnOne = [FSViewHelper createBtnWithTitle:@"请选择"
                                                   bgColor:kWhiteColor
                                                titleColor:colorOf(215, 215, 215)];
    _timeBtnOne.titleLabel.font = [UIFont systemFontOfSize:16];
    [_timeBtnOne addTarget:self action:@selector(startTimeClick:) forControlEvents:UIControlEventTouchDown];
    [bgView addSubview:_timeBtnOne];
    [_timeBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(titleLbl.mas_right).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:(screenW-80-width)/2]);
        make.bottom.equalTo(bgView).offset(-8);
    }];
    
    
    UILabel *zhiLbl = [FSViewHelper createLblWithText:@"至"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:16];
    [bgView addSubview:zhiLbl];
    [zhiLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(_timeBtnOne.mas_right);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo(@20);
    }];
    
    _timeBtnTwo = [FSViewHelper createBtnWithTitle:@"请选择"
                                                      bgColor:kWhiteColor
                                                   titleColor:colorOf(215, 215, 215)];
    _timeBtnTwo.titleLabel.font = [UIFont systemFontOfSize:16];
    [_timeBtnTwo addTarget:self action:@selector(endTimeClick:) forControlEvents:UIControlEventTouchDown];
    [bgView addSubview:_timeBtnTwo];
    [_timeBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(zhiLbl.mas_right);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:(screenW-80-width)/2]);
    }];
}

- (void)startTimeClick:(UIButton *)sender {
    if (_startTimeAction) {
        _startTimeAction(sender);
    }
}

- (void)endTimeClick:(UIButton *)sender {
    if (_endTimeAction) {
        _endTimeAction(sender);
    }
}
@end
