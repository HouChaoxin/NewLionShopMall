//
//  BRHeaderView.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BRHeaderView : UIView

- (void)refreshViewWithIndex:(NSInteger)index;

@end
