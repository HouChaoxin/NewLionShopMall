//
//  StoreDescriptionView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "StoreDescriptionView.h"

static NSInteger textViewTag = 2048;

@interface StoreDescriptionView()<UITextViewDelegate>{
    UILabel *wordNumLbl;
}

@end

@implementation StoreDescriptionView

-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    UILabel *lbl0 = [FSViewHelper createLblWithText:@"*"
                                      textAlignment:NSTextAlignmentCenter
                                           fontSize:16];
    lbl0.textColor = kRedColor;
    [self addSubview:lbl0];
    [lbl0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(6);
        make.width.equalTo(@8);
        make.height.equalTo(@8);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"商户描述"
                                          textAlignment:NSTextAlignmentLeft fontSize:16];
    [self addSubview:titleLbl];
    CGFloat width = [FSViewHelper caculateLblWidthWithString:@"商户描述" andFontSize:17];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(16);
        make.width.equalTo([NSNumber numberWithFloat:width]);
        make.height.equalTo(@28);
    }];
    
    wordNumLbl = [FSViewHelper createLblWithText:@"0/300" textAlignment:NSTextAlignmentCenter fontSize:16];
    [self addSubview:wordNumLbl];
    [wordNumLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(titleLbl.mas_right).offset(8);
        make.width.equalTo(@80);
        make.height.equalTo(@28);
    }];
    
    
    _textView = [[UITextView alloc] init];
    _textView.text = @"完善的商户描述有助于消费者更好的了解您的店铺，吸引更多的用户到店消费；300字以内";
    _textView.textColor = [UIColor lightGrayColor];
    _textView.font = [UIFont systemFontOfSize:16];
    _textView.tag = textViewTag;
    _textView.delegate = self;
    _textView.layer.cornerRadius = 5.0f;
    _textView.layer.borderWidth = 1.0f;
    _textView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:_textView];
    [_textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(16);
        make.left.equalTo(self).offset(16);
        make.right.equalTo(self).offset(-16);
        make.height.equalTo(@149);
    }];
}


#pragma mark - UITextViewDelegate
-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (textView.tag == 2048) {
        textView.text = @"";
        textView.textColor = [UIColor blackColor];
        textView.tag = 2049;
    }
    return YES;
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSInteger length;
    if (range.length > 0) {
        length = textView.text.length-range.length;
    }else {
        length = textView.text.length+text.length;
    }
    
    if (length > 150) {
        textView.text = [textView.text substringToIndex:150];
    }
    wordNumLbl.text = [NSString stringWithFormat:@"%ld/150",textView.text.length];
    return YES;
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    if (textView.tag == 2049 && [textView.text isEqualToString:@""]) {
        textView.text = @"完善的商户描述有助于消费者更好的了解您的店铺，吸引更多的用户到店消费；300字以内";
        textView.textColor = [UIColor lightGrayColor];
        textView.tag = 2048;
    }
}


@end
