//
//  AccountBankView.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "AccountBankView.h"

@interface AccountBankView()

@end

@implementation AccountBankView

-(instancetype)init {
    self = [super init];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    __weak typeof(self) weakSelf = self;
    _accountBankCell = [[JiSuanSelectCell alloc] initWithTitle:@"账户开户行"
                                                    isRequired:YES];
    _accountBankCell.selectBtnAction = ^(UIButton *sender) {
        if (weakSelf.bankSelecAction) {
            weakSelf.bankSelecAction(sender);
        }
    };
    [self addSubview:_accountBankCell];
    [_accountBankCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self);
        make.height.equalTo(@53);
    }];
    
    UIView *bgView = [[UIView alloc] init];
    bgView.layer.cornerRadius = 5.0f;
    bgView.layer.borderWidth = 1.0f;
    bgView.layer.borderColor = [ColorConverter colorWithHexString:@"0xb2b2b2"].CGColor;
    [self addSubview:bgView];
    [bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(14);
        make.top.equalTo(_accountBankCell.mas_bottom).offset(11);
        make.height.equalTo(@42);
        make.right.equalTo(self).offset(-14);
    }];
    
    UILabel *lbl = [FSViewHelper createLblWithText:@"*"
                                     textAlignment:NSTextAlignmentCenter
                                          fontSize:16];
    lbl.textColor = kRedColor;
    [bgView addSubview:lbl];
    [lbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(16);
        make.left.equalTo(bgView).offset(6);
        make.height.equalTo(@8);
        make.width.equalTo(@8);
    }];
    
    
    UILabel *addressTitleLbl = [FSViewHelper createLblWithText:@"账户支行地址"
                                                textAlignment:NSTextAlignmentLeft
                                                     fontSize:16];
    CGFloat width = [FSViewHelper caculateLblWidthWithString:@"账户支行地址" andFontSize:17];
    [bgView addSubview:addressTitleLbl];
    [addressTitleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(bgView).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:width]);
    }];
    
    _selectBtnOne = [FSViewHelper createBtnWithTitle:@"省市"
                                                   bgColor:kWhiteColor
                                                titleColor:colorOf(215, 215, 215)];
    _selectBtnOne.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    _selectBtnOne.titleLabel.font = [UIFont systemFontOfSize:16];
    [_selectBtnOne addTarget:self action:@selector(provinceBtnClick:) forControlEvents:UIControlEventTouchDown];
    [bgView addSubview:_selectBtnOne];
    [_selectBtnOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(addressTitleLbl.mas_right).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:(screenW-76-width)/2]);
    }];
    
    UIImageView *pullArrow = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgView addSubview:pullArrow];
    [pullArrow mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(13);
        make.right.equalTo(_selectBtnOne.mas_right).offset(-8);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _selectBtnTwo = [FSViewHelper createBtnWithTitle:@"市/区"
                                                      bgColor:kWhiteColor
                                                   titleColor:colorOf(215, 215, 215)];
    _selectBtnTwo.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_selectBtnTwo addTarget:self action:@selector(cityBtnClick:) forControlEvents:UIControlEventTouchDown];
    _selectBtnTwo.titleLabel.font = [UIFont systemFontOfSize:16];
    [bgView addSubview:_selectBtnTwo];
    [_selectBtnTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(8);
        make.left.equalTo(_selectBtnOne.mas_right).offset(16);
        make.bottom.equalTo(bgView).offset(-8);
        make.width.equalTo([NSNumber numberWithFloat:(screenW-76-width)/2]);
    }];
    
    UIImageView *pullArrowTwo = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"pullArrow"] bgColor:kWhiteColor];
    [bgView addSubview:pullArrowTwo];
    [pullArrowTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(13);
        make.right.equalTo(_selectBtnTwo.mas_right).offset(-8);
        make.height.equalTo(@15);
        make.width.equalTo(@15);
    }];
    
    _accountBranchCell = [[JiSuanSelectCell alloc] initWithTitle:@"账户开户支行" isRequired:YES];
    [self addSubview:_accountBranchCell];
    _accountBranchCell.selectBtnAction = ^(UIButton *sender) {
        if (weakSelf.branchSelectAction) {
            weakSelf.branchSelectAction(sender);
        }
    };
    [_accountBranchCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(_selectBtnTwo.mas_bottom).offset(8);
        make.height.equalTo(@53);
    }];
}

- (void)provinceBtnClick:(UIButton *)sender {
    if (_provinceSelectAction) {
        _provinceSelectAction(sender);
    }
}

- (void)cityBtnClick:(UIButton *)sender {
    if (_citySelectAction) {
        _citySelectAction(sender);
    }
}

@end
