//
//  PicUploadVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/16.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "PicUploadVC.h"
#import "BRHeaderView.h"
#import "UploadPicCell.h"
#import "RootViewController.h"
#import "PicUploadVCModel.h"

@interface PicUploadVC ()<UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,strong) PicUploadVCModel *vcModel;
@property (nonatomic,copy)   NSArray *uploadAry;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) BRHeaderView *headView;
@property (nonatomic,strong) UIImageView *currentImgView;
@property (nonatomic,assign) NSInteger currentIndex;
@end

@implementation PicUploadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
//    NSDictionary *dict = [self getDataFromSaveFileWithPath];
    
    _vcModel = [[PicUploadVCModel alloc] init];
    PicUploadMod *shangHuLogoMod = [[PicUploadMod alloc] initWithTitle:@"商户logo"
                                                                 alert:@"点击上传商户logo"
                                                                 index:1 picUrl:@"" key:@"store_logo"
                                                            errorAlert:@"您的商户logo照未上传，请点击上传"];
    PicUploadMod *yingYeZhiZhaoMod = [[PicUploadMod alloc] initWithTitle:@"营业执照"
                                                                   alert:@"点击上传商户营业执照" index:2 picUrl:@"" key:@"store_licence" errorAlert:@"您的商户营业执照未上传，请点击上传"];
    PicUploadMod *kaiHuXuKeMod = [[PicUploadMod alloc] initWithTitle:@"开户许可证照"
                                                               alert:@"点击上传商户银行开户许可证" index:3 picUrl:@"" key:@"store_bank_enabe" errorAlert:@"您的开户许可证照未上传，请点击上传"];
    PicUploadMod *jiGouDaiMaMod = [[PicUploadMod alloc] initWithTitle:@"机构代码证照"
                                                                alert:@"点击上传商户机构代码证照" index:4 picUrl:@"" key:@"store_institutions" errorAlert:@"您的商户机构代码证照未上传，请点击上传"];
    PicUploadMod *shuiWuDengJiMod = [[PicUploadMod alloc] initWithTitle:@"税务登记证照"
                                                                  alert:@"点击上传商户税务登记证照" index:5 picUrl:@"" key:@"store_tax" errorAlert:@"您的商户税务登记证照未上传，请点击上传"];
    PicUploadMod *shenFenZhengZhengMod = [[PicUploadMod alloc] initWithTitle:@"身份证正面"
                                                                       alert:@"点击上传法人身份证正面" index:6 picUrl:@"" key:@"store_identity_correct" errorAlert:@"您的法人身份证正面照未上传，请点击上传"];
    PicUploadMod *shenFenZhengFanMod = [[PicUploadMod alloc] initWithTitle:@"身份证反面"
                                                                     alert:@"点击上传法人身份证反面" index:7 picUrl:@"" key:@"store_identity_opposite" errorAlert:@"您的法人身份证反面照未上传，请点击上传"];
    PicUploadMod *shenFenZhengZhengFeiFaRenMod = [[PicUploadMod alloc] initWithTitle:@"身份证正面"
                                                                       alert:@"点击上传结算人身份证正面" index:8 picUrl:@"" key:@"store_settlement_correct" errorAlert:@"您的结算人身份证正面照未上传，请点击上传"];
    PicUploadMod *shenFenZhengFanFeiFeiFaRenMod = [[PicUploadMod alloc] initWithTitle:@"身份证反面"
                                                                     alert:@"点击上传结算人身份证反面" index:9 picUrl:@"" key:@"store_settlement_opposite" errorAlert:@"您的身份证反面照未上传，请点击上传"];
    PicUploadMod *shangHuMenTouMod = [[PicUploadMod alloc] initWithTitle:@"商户门头照"
                                                                   alert:@"点击上传商户门头照片" index:10 picUrl:@"" key:@"store_heard" errorAlert:@"您的商户门头照未上传，请点击上传"];
    PicUploadMod *neiBuQianTaiMod = [[PicUploadMod alloc] initWithTitle:@"内部前台照"
                                                                  alert:@"点击上传商户门头图" index:11 picUrl:@"" key:@"store_desk_img" errorAlert:@"您的内部前台照未上传，请点击上传"];
    PicUploadMod *dianPuHuanJingMod = [[PicUploadMod alloc] initWithTitle:@"店铺环境图"
                                                                    alert:@"最多六张，可在商户详情中添加" index:12 picUrl:@"" key:@"store_environment" errorAlert:@"您的店铺环境图未上传，请点击上传"];
    PicUploadMod *yinHangKaZhengMod = [[PicUploadMod alloc] initWithTitle:@"银行卡正面"
                                                                    alert:@"点击上传银行卡正面" index:13 picUrl:@"" key:@"banke_img_correct" errorAlert:@"您的银行卡正面照未上传，请点击上传"];
    PicUploadMod *yinHangKaFanMod = [[PicUploadMod alloc] initWithTitle:@"银行卡反面"
                                                                  alert:@"点击上传银行卡反面" index:14 picUrl:@"" key:@"banke_img_opposite" errorAlert:@"您的银行卡反面照未上传，请点击上传"];
    PicUploadMod *jieSuanShouQuanMod = [[PicUploadMod alloc] initWithTitle:@"结算授权书"
                                                                     alert:@"点击上传结算非法人授权书" index:15 picUrl:@"" key:@"authorization_img" errorAlert:@"您的结算非法人授权书未上传，请点击上传"];
    PicUploadMod *shouChiShenFenZhengMod = [[PicUploadMod alloc] initWithTitle:@"手持身份证照"
                                                                     alert:@"点击上传商户老板手持身份证照" index:16 picUrl:@"" key:@"store_boss_idcar" errorAlert:@"您的商户老板手持身份证照未上传，请点击上传"];
    if (_uploadType == QiYeDuiGongType) {
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,kaiHuXuKeMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == QiYeDuiSiFaRen){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,kaiHuXuKeMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,yinHangKaZhengMod,yinHangKaFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == QiYeDuiSiFeiFaRen){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,kaiHuXuKeMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,jieSuanShouQuanMod,shenFenZhengZhengFeiFaRenMod,shenFenZhengFanFeiFeiFaRenMod,yinHangKaZhengMod,yinHangKaFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == GeTiDuiGong){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,kaiHuXuKeMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == GeTiDuiSiFaRen){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,yinHangKaZhengMod,yinHangKaFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == GeTiDuiSiFeiFaRen){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,yingYeZhiZhaoMod,jiGouDaiMaMod,shuiWuDengJiMod,shenFenZhengZhengMod,shenFenZhengFanMod,jieSuanShouQuanMod,shenFenZhengZhengFeiFaRenMod,shenFenZhengFanFeiFeiFaRenMod,yinHangKaZhengMod,yinHangKaFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }else if (_uploadType == XiaoWeiShangHu){
        self.uploadAry = [NSArray arrayWithObjects:shangHuLogoMod,shouChiShenFenZhengMod,shenFenZhengZhengMod,shenFenZhengFanMod,shangHuMenTouMod,neiBuQianTaiMod,dianPuHuanJingMod, nil];
    }
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"证照上传"];
    [self hideBackButtonTitle];
    
    _headView = [[BRHeaderView alloc] init];
    [_headView refreshViewWithIndex:3];
    [self.view addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@80);
    }];
    
    UIView *btmView = [self createBottomView];
    [self.view addSubview:btmView];
    [btmView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.equalTo(@102);
    }];
    
    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(btmView.mas_top);
    }];
    
    CGFloat height = self.uploadAry.count*168;
    UIView *sBgView = [self createScrollViewSubViews];
    sBgView.frame = CGRectMake(0, 0, screenW, height);
    [_scrollView addSubview:sBgView];
    _scrollView.contentSize = CGSizeMake(screenW, height);
}

- (UIView *)createScrollViewSubViews {
    UIView *bgView = [[UIView alloc] init];
    
    for (NSInteger i = 0; i < self.uploadAry.count; i++) {
        PicUploadMod *model = [self.uploadAry objectAtIndex:i];
        UploadPicCell *cell = [[UploadPicCell alloc] initWithTitle:model.titleStr alert:model.alertStr index:model.index];
        cell.uploadPicAction = ^(UIImageView *imgView,NSInteger index) {
            NSLog(@"上传图片被点击了！！！");
            self.currentIndex = index;
            self.currentImgView = imgView;
            [self showSelectedCamera];
        };
        [bgView addSubview:cell];
        [cell mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bgView).offset(168*i);
            make.left.right.equalTo(bgView);
            make.height.equalTo(@168);
        }];
    }
    
    return bgView;
}

- (UIView *)createBottomView {
    
    UIView *btmView = [[UIView alloc] init];
    
    UIButton *previousBtn = [FSViewHelper createBtnWithTitle:@"上一步"
                                                     bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                                  titleColor:kWhiteColor];
    [previousBtn addTarget:self action:@selector(previousBtnClick) forControlEvents:UIControlEventTouchDown];
    previousBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:previousBtn];
    [previousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.left.equalTo(btmView).offset(40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    UIButton *nextBtn = [FSViewHelper createBtnWithTitle:@"提交"
                                                 bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                              titleColor:kWhiteColor];
    [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchDown];
    nextBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:nextBtn];
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.right.equalTo(btmView).offset(-40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    return btmView;
}

- (void)previousBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)nextBtnClick {
    
    [self removeLocalData];
    if (![self judgeNumericField]) return;
    NSMutableDictionary *picDict = [[NSMutableDictionary alloc] init];
    for (PicUploadMod *model in _uploadAry) {
        [picDict setObject:model.picUrl forKey:model.key];
    }
    [self saveLocalDataWithDict:picDict];
    
    NSMutableDictionary *para = [[NSMutableDictionary alloc] init];
    NSDictionary *brDict = [self getDataFromSaveFileWithPath:@"baseResource.archive"];
    NSDictionary *jieSuanDict = [self getDataFromSaveFileWithPath:@"jieSuanAccount.archive"];
    [para addEntriesFromDictionary:brDict];
    [para addEntriesFromDictionary:jieSuanDict];
    [para addEntriesFromDictionary:picDict];
    [_vcModel addBusinessStoreWithPara:para success:^(id responseObject) {
        NSLog(@"资料上传成功");
    } failure:^{
        NSLog(@"资料上传失败");
    }];
    
//    RootViewController *mainVC = [[RootViewController alloc] init];
//    UINavigationController *nv = [[UINavigationController alloc] initWithRootViewController:mainVC];
//    [UIApplication sharedApplication].keyWindow.rootViewController = nv;
}

- (void)saveLocalDataWithDict:(NSDictionary *)dict {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:@"picUpload.archive"];
    [NSKeyedArchiver archiveRootObject:dict toFile:path];
}

- (NSDictionary *)getDataFromSaveFileWithPath:(NSString *)pathStr {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:pathStr];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    return dict;
}

- (void)removeLocalData {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:@"picUpload.archive"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:path];
    if (bRet) {
        NSError *err;
        [fileMgr removeItemAtPath:path error:&err];
    }
}

- (BOOL)judgeNumericField {
    for (PicUploadMod *model in _uploadAry) {
        if (!model.picUrl || [model.picUrl isEqualToString:@""]) {
            [self.view makeToast:model.errorAlert duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
    }
    return YES;
}


#pragma mark - 上传图片处理
- (void)showSelectedCamera {
    
    __weak typeof(self) weakSelf = self;
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:nil message:@"请选择获取图片途径" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"照相机" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf openCamera];
    }];
    UIAlertAction *libraryAction = [UIAlertAction actionWithTitle:@"图库" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf openPhotoLibrary];
    }];
    [alertVC addAction:cameraAction];
    [alertVC addAction:libraryAction];
    [alertVC addAction:cancel];
    [self presentViewController:alertVC animated:YES completion:^{
    }];
}

- (void)openCamera{
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:nil];
    }else{
        NSLog(@"没有摄像头");
    }
}

-(void)openPhotoLibrary{
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]){
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        imagePicker.delegate = self;
        [self presentViewController:imagePicker animated:YES completion:^{
            NSLog(@"打开相册");
        }];
    }else{
        NSLog(@"不能打开相册");
    }
}

#pragma mark - UIImagePickerControllerDelegate
// 拍照完成回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo NS_DEPRECATED_IOS(2_0, 3_0){
    NSLog(@"finish..");
    //    if(picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    //    {
    //        //图片存入相册
    //        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
    //    }
    
//    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    NSData *imgData = UIImagePNGRepresentation(image);
    [self uploadAvatarWithImageData:imgData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    
    //原生图片
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    //修改过的图片
    //    UIImage *EditedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.4);
    [self uploadAvatarWithImageData:imageData];
    [self dismissViewControllerAnimated:YES completion:nil];
}

//进入拍摄页面点击取消按钮
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 上传头像
- (void)uploadAvatarWithImageData:(NSData *)imageData {
    __weak typeof(self) weakSelf = self;
    NSData *database64 = [imageData base64EncodedDataWithOptions:0];
    NSString *dataStr = [[NSString alloc] initWithData:database64 encoding:NSUTF8StringEncoding];
    [_vcModel uploadImageWithAvatar:dataStr directory:@"" success:^(id responseObject) {
        if (responseObject) {
            NSDictionary *dict = (NSDictionary *)responseObject;
            BOOL status = [dict[@"status"] boolValue];
            if (status) {

                NSDictionary *data = dict[@"data"];
                if (data && data.count > 0) {

                    NSString *url = data[@"file"]?data[@"file"]:@"";
                    url = [NSString stringWithFormat:@"http://img.lion-mall.com/%@",url];
                    
                    for (PicUploadMod *model in _uploadAry) {
                        if (model.index == self.currentIndex) {
                            model.picUrl = url;
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf.currentImgView sd_setImageWithURL:[NSURL URLWithString:url]
                                                placeholderImage:[UIImage imageNamed:@"defaultImage.png"]];
                    });
                }else {
                    NSString *msg = dict[@"msg"];
                    [weakSelf.view makeToast:msg duration:1.5 position:CSToastPositionCenter];
                }
            }else {
                NSString *msg = dict[@"msg"];
                [weakSelf.view makeToast:msg duration:1.5 position:CSToastPositionCenter];
            }
        }else {
            [weakSelf.view makeToast:@"图片上传失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }

    } failure:^(NSError *error) {
        NSLog(@"error:%@",[error description]);
        [weakSelf.view makeToast:@"图片上传失败，请检查您的网络状态" duration:1.5 position:CSToastPositionCenter];
    }];
}

@end
