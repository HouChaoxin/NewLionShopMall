//
//  JoinProtocalVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/12.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "JoinProtocalVC.h"
#import "BaseResourceVC.h"


#import "PicUploadVC.h"
#import "JieSuanAccountVC.h"

@interface JoinProtocalVC ()

@property (nonatomic,strong) UIButton *agreeBtn;
@property (nonatomic,strong) UIButton *nextBtn;

@end

@implementation JoinProtocalVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"入驻协议"];
    [self hideBackButtonTitle];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"联盟商家入驻协议书"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:16];
    titleLbl.font = [UIFont boldSystemFontOfSize:16];
    [self.view addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20);
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.height.equalTo(@36);
    }];
    
    UIScrollView *scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.bottom.equalTo(self.view).offset(-160);
    }];
    
    NSString *protacalText = @"联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议书联盟协议";
    
    CGFloat height = [FSViewHelper caculateHeightWithString:protacalText
                                                andFontSize:17
                                                      width:screenW-32];
    scrollView.contentSize = CGSizeMake(screenW-32, height);
    UILabel *protacalLbl = [FSViewHelper createLblWithText:protacalText
                                             textAlignment:NSTextAlignmentCenter
                                                  fontSize:16];
    protacalLbl.numberOfLines = 0;
    [scrollView addSubview:protacalLbl];
    [protacalLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(titleLbl.mas_bottom).offset(20);
        make.left.equalTo(self.view).offset(24);
        make.right.equalTo(self.view).offset(-24);
        make.height.equalTo([NSNumber numberWithFloat:height]);
    }];
    
    UIView *btmView = [self createBottomView];
    [self.view addSubview:btmView];
    [btmView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.equalTo(@160);
    }];
}

- (UIView *)createBottomView {
    
    UIView *bottomView = [[UIView alloc] init];
    
    NSString *agreeStr = @"同意并了解协议内容";
    CGFloat width = [FSViewHelper caculateLblWidthWithString:agreeStr andFontSize:15];
    UIView *agreeView = [self createAgreeViewWithWidth:width text:agreeStr];
    [bottomView addSubview:agreeView];
    [agreeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bottomView);
        make.top.equalTo(bottomView).offset(30);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(width+25, 20)]);
    }];
    
    _nextBtn = [FSViewHelper createBtnWithTitle:@"下一步"
                                        bgColor:[ColorConverter colorWithHexString:@"0x96C3E3"]
                                     titleColor:kWhiteColor];
    _nextBtn.layer.cornerRadius = 5.0f;
    [bottomView addSubview:_nextBtn];
    [_nextBtn addTarget:self action:@selector(nextBtnAction) forControlEvents:UIControlEventTouchDown];
    [_nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bottomView);
        make.top.equalTo(agreeView.mas_bottom).offset(30);
        make.height.equalTo(@41);
        make.width.equalTo(@130);
    }];
    
    return bottomView;
}

- (UIView *)createAgreeViewWithWidth:(CGFloat)whidth text:(NSString *)text {
    UIView *agreeView = [[UIView alloc] init];
    
    _agreeBtn = [FSViewHelper createBtnWithTitle:@""
                                         bgColor:kWhiteColor
                                      titleColor:kWhiteColor];
    [_agreeBtn setImage:[UIImage imageNamed:@"duiHao"] forState:UIControlStateSelected];
    [_agreeBtn setImage:[UIImage imageNamed:@"noDuiHao"] forState:UIControlStateNormal];
    [_agreeBtn addTarget:self action:@selector(agreeBtnAction:) forControlEvents:UIControlEventTouchDown];
    [agreeView addSubview:_agreeBtn];
    [_agreeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(agreeView);
        make.top.equalTo(agreeView);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(20, 20)]);
    }];
    
    UILabel *agreeLbl = [FSViewHelper createLblWithText:text
                                          textAlignment:NSTextAlignmentLeft
                                               fontSize:14];
    [agreeView addSubview:agreeLbl];
    [agreeLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(agreeView);
        make.right.equalTo(agreeView);
        make.height.equalTo(@20);
        make.width.equalTo([NSNumber numberWithFloat:whidth]);
    }];
    return agreeView;
}

- (void)nextBtnAction {
    
    if (!_agreeBtn.selected) {
        [self.view makeToast:@"请同意并了解协议内容后再进行下一步" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    BaseResourceVC *vc = [[BaseResourceVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    
//    PicUploadVC *vc = [[PicUploadVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
//    JieSuanAccountVC *vc = [[JieSuanAccountVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (void)agreeBtnAction:(UIButton *)sender {
    if (!sender.selected) {
        sender.selected = YES;
        _nextBtn.backgroundColor = [ColorConverter colorWithHexString:@"0x36A4F3"];
    }else {
        sender.selected = NO;
        _nextBtn.backgroundColor = [ColorConverter colorWithHexString:@"0x96C3E3"];
    }
}

@end
