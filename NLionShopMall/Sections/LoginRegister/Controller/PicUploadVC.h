//
//  PicUploadVC.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/16.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    QiYeDuiGongType,//企业对公结算
    QiYeDuiSiFaRen,//企业对私结算（法人入账）
    QiYeDuiSiFeiFaRen,//企业对私结算（非法人入账）
    GeTiDuiGong,//个体对公结算
    GeTiDuiSiFaRen,//个体对私结算（法人入账）
    GeTiDuiSiFeiFaRen,//个体对私结算（非法人入账）
    XiaoWeiShangHu,//小微商户
} PicUploadType;



@interface PicUploadVC : BaseVC

@property (nonatomic,assign) PicUploadType uploadType;

@end
