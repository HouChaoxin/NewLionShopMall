//
//  GaoDeMapVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "GaoDeMapVC.h"

@interface GaoDeMapVC ()<UIWebViewDelegate>

@property (nonatomic,strong) UIWebView *webView;

@end

@implementation GaoDeMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initView {
    
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = colorOf(247, 247, 247);
    view.frame = CGRectMake(0, 0, screenW, 20);
    [self.view addSubview:view];
    
    self.webView = ({
        UIWebView *uiWebView = [[UIWebView alloc] init];
        uiWebView.frame = CGRectMake(0, 20, screenW, screenH-20);
        uiWebView.delegate = self;
        uiWebView.scrollView.bounces = YES;
        [self.view addSubview:uiWebView];
        uiWebView;
    });
    
    UIButton *closeBtn = [FSViewHelper createBtnWithTitle:@""
                                                  bgColor:colorOf(247, 247, 247)
                                               titleColor:kBlackColor];
    closeBtn.titleLabel.font = [UIFont systemFontOfSize:12];
    closeBtn.frame = CGRectMake(screenW-30, 32, 20, 20);
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"红包关闭按钮"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(closeBtnAction) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:closeBtn];
    
    if (@available(iOS 11.0, *)) {
        _webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    
    if (self.webUrlStr) {
        NSURL *url = [[NSURL alloc] initWithString:self.webUrlStr];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)closeBtnAction {
    [self webViewClearCache];
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)webViewClearCache {
    //清除cookies
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]){
        [storage deleteCookie:cookie];
    }
    
    //清除UIWebView的缓存
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    NSURLCache * cache = [NSURLCache sharedURLCache];
    [cache removeAllCachedResponses];
    [cache setDiskCapacity:0];
    [cache setMemoryCapacity:0];
}


#pragma mark - WebView Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString* url = request.URL.absoluteString;
    NSLog(@"url = %@",url);
    NSString *backMark = [url substringFromIndex:7];
    NSArray *ary = [backMark componentsSeparatedByString:@"/"];
    if ([[ary firstObject] isEqualToString:@"backhome"]) {
        [self webViewClearCache];
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }else if ([[ary firstObject] isEqualToString:@"commodityinfo"]){
        
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    if([[error domain] isEqualToString:NSURLErrorDomain] && [error code] == NSURLErrorUserCancelledAuthentication){
        return;
    }
    
    if([error code] == NSURLErrorNotConnectedToInternet || [error code] == NSURLErrorCannotConnectToHost){
        //        [self displayGenericErrorPage];
        return;
    }
    
    // Ignore this error when the page is instantly
    // redirected via javascript or somehow else
    if ([error code] == NSURLErrorCancelled)
        return;
    
    if(error.code == 102 && [[error domain] isEqual:@"WebKitErrorDomain"]) return;
    
    if ([[error domain] isEqualToString:NSURLErrorDomain] &&
        ([error code] == -1003 || [error code] == -1001)
        ){
        //-1003 -- A server with the specified hostname could not be found.
        //-1001 -- The request timed out
        
        //        [self displayGenericErrorPage];
        return;
    }
    
    int status = [[[webView request] valueForHTTPHeaderField:@"Status"] intValue];
    if (status == 404 || status == 500){
        //        [self displayGenericErrorPage];
    }
    
}


@end
