//
//  BaseResourceVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/14.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BaseResourceVC.h"
#import "BRTextFieldCell.h"
#import "BRHeaderView.h"
#import "BRSelectView.h"
#import "StoreDescriptionView.h"
#import "JieSuanAccountVC.h"
#import "FSPickViewContainerView.h"
#import "AreaModel.h"
#import "BaseResourceVCModel.h"

@interface BaseResourceVC ()<FSPickViewContainerViewDelegate>

@property (nonatomic,copy) BaseResourceVCModel *vcModel;

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) BRHeaderView *headView;
@property (nonatomic, strong) FSPickViewContainerView *pickViewContainerView;
@property (nonatomic, strong) FSPickViewModel *pickViewModel;

@property (nonatomic,strong) BRTextFieldCell *serviceIDCell;
@property (nonatomic,strong) BRTextFieldCell *storeNameCell;
@property (nonatomic,strong) BRTextFieldCell *storeSimpleCell;
@property (nonatomic,strong) BRTextFieldCell *registerNameCell;
@property (nonatomic,strong) BRTextFieldCell *storeContactCell;
@property (nonatomic,strong) BRTextFieldCell *contactPhoneCell;
@property (nonatomic,strong) BRTextFieldCell *contactMailCell;
@property (nonatomic,strong) BRTextFieldCell *storeContactPhoneCell;
@property (nonatomic,strong) BRTextFieldCell *shJiFenRuleCell;
@property (nonatomic,strong) BRTextFieldCell *jiFenExchangeCell;
@property (nonatomic,strong) BRSelectView *selectView;
@property (nonatomic,strong) StoreDescriptionView *descriptionView;

@property (nonatomic,copy) NSArray *busnessAttributeAry;
@property (nonatomic,strong) NSMutableDictionary *busnessAttributeDict;
@property (nonatomic,strong) NSMutableDictionary *careerOneDict;
@property (nonatomic,strong) NSMutableDictionary *careerTwoDict;
@property (nonatomic,copy) NSArray *careerOneAry;
@property (nonatomic,strong) NSMutableDictionary *careerDict;
@property (nonatomic,strong) NSMutableArray *provinceAry;
@property (nonatomic,strong) NSMutableDictionary *provinceDict;
@property (nonatomic,strong) NSMutableDictionary *cityDict;
@property (nonatomic,strong) NSMutableDictionary *countryDict;
@property (nonatomic,copy) NSArray *areaAry;
@property (nonatomic,strong) NSMutableDictionary *quXianDict;
@end

@implementation BaseResourceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self requestData];
    [self initializationViewData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _vcModel = [[BaseResourceVCModel alloc] init];
    //商业属性
    _busnessAttributeDict = [[NSMutableDictionary alloc] init];
    [_busnessAttributeDict setObject:@"1" forKey:@"企业"];
    [_busnessAttributeDict setObject:@"2" forKey:@"个体工商户"];
    [_busnessAttributeDict setObject:@"3" forKey:@"小微商户"];
    FSPickViewShowModel *showmodel0 = [[FSPickViewShowModel alloc] initWitShowName:@"企业" nextLevelArray:nil];
    FSPickViewShowModel *showmodel1 = [[FSPickViewShowModel alloc] initWitShowName:@"个体工商户" nextLevelArray:nil];
    FSPickViewShowModel *showmodel2 = [[FSPickViewShowModel alloc] initWitShowName:@"小微商户" nextLevelArray:nil];
    self.busnessAttributeAry = [[NSArray alloc] initWithObjects:showmodel0,showmodel1,showmodel2, nil];
    //行业类别
    FSPickViewShowModel *careerModel0 = [[FSPickViewShowModel alloc] initWitShowName:@"企业" nextLevelArray:nil];
    FSPickViewShowModel *careerModel1 = [[FSPickViewShowModel alloc] initWitShowName:@"个体工商户" nextLevelArray:nil];
    FSPickViewShowModel *careerModel2 = [[FSPickViewShowModel alloc] initWitShowName:@"事业单位" nextLevelArray:nil];
    self.careerOneAry = [NSArray arrayWithObjects:careerModel0,careerModel1,careerModel2,nil];
    self.careerOneDict = [[NSMutableDictionary alloc] init];
    [_careerOneDict setObject:@"1" forKey:@"企业"];
    [_careerOneDict setObject:@"2" forKey:@"个体工商户"];
    [_careerOneDict setObject:@"3" forKey:@"事业单位"];
    self.careerDict = [[NSMutableDictionary alloc] init];
    self.careerTwoDict = [[NSMutableDictionary alloc] init];
    
    _provinceDict = [[NSMutableDictionary alloc] init];
    [_provinceDict setObject:@"1" forKey:@"北京市"];
    [_provinceDict setObject:@"2" forKey:@"天津市"];
    [_provinceDict setObject:@"3" forKey:@"河北省"];
    [_provinceDict setObject:@"4" forKey:@"山西省"];
    [_provinceDict setObject:@"5" forKey:@"内蒙古自治区"];
    [_provinceDict setObject:@"6" forKey:@"辽宁省"];
    [_provinceDict setObject:@"7" forKey:@"吉林省"];
    [_provinceDict setObject:@"8" forKey:@"黑龙江省"];
    [_provinceDict setObject:@"9" forKey:@"上海市"];
    [_provinceDict setObject:@"10" forKey:@"江苏省"];
    [_provinceDict setObject:@"11" forKey:@"浙江省"];
    [_provinceDict setObject:@"12" forKey:@"安徽省"];
    [_provinceDict setObject:@"13" forKey:@"福建省"];
    [_provinceDict setObject:@"14" forKey:@"江西省"];
    [_provinceDict setObject:@"15" forKey:@"山东省"];
    [_provinceDict setObject:@"16" forKey:@"河南省"];
    [_provinceDict setObject:@"17" forKey:@"湖北省"];
    [_provinceDict setObject:@"18" forKey:@"湖南省"];
    [_provinceDict setObject:@"19" forKey:@"广东省"];
    [_provinceDict setObject:@"20" forKey:@"广西省"];
    [_provinceDict setObject:@"21" forKey:@"海南省"];
    [_provinceDict setObject:@"22" forKey:@"重庆市"];
    [_provinceDict setObject:@"23" forKey:@"四川省"];
    [_provinceDict setObject:@"24" forKey:@"贵州省"];
    [_provinceDict setObject:@"25" forKey:@"云南省"];
    [_provinceDict setObject:@"26" forKey:@"西藏自治区"];
    [_provinceDict setObject:@"27" forKey:@"陕西省"];
    [_provinceDict setObject:@"28" forKey:@"甘肃省"];
    [_provinceDict setObject:@"29" forKey:@"青海省"];
    [_provinceDict setObject:@"30" forKey:@"宁夏回族自治区"];
    [_provinceDict setObject:@"31" forKey:@"新疆维吾尔自治区"];
    [_provinceDict setObject:@"32" forKey:@"台湾省"];
    [_provinceDict setObject:@"33" forKey:@"香港特别行政区"];
    [_provinceDict setObject:@"34" forKey:@"澳门特别行政区"];
    _provinceAry = [[NSMutableArray alloc] init];
    for (NSString *str in [_provinceDict allKeys]) {
        FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:str nextLevelArray:nil];
        [_provinceAry addObject:showmodel];
    }
    _cityDict = [[NSMutableDictionary alloc] init];
    _countryDict = [[NSMutableDictionary alloc] init];
}

- (void)requestData {
    
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"基础资料"];
    [self hideBackButtonTitle];
    
    _headView = [[BRHeaderView alloc] init];
    [self.view addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@80);
    }];
    
    UIView *btmView = [self createBottomView];
    [self.view addSubview:btmView];
    [btmView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.equalTo(@102);
    }];
    
    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(btmView.mas_top);
    }];
    
    UIView *sBgView = [self createScrollViewSubViews];
    sBgView.frame = CGRectMake(0, 0, screenW, 1100);
    [_scrollView addSubview:sBgView];
    _scrollView.contentSize = CGSizeMake(screenW, 1100);
    
    [self initPickerView];
}

- (UIView *)createScrollViewSubViews {
    
    UIView *bgView = [[UIView alloc] init];
    
    _serviceIDCell = [[BRTextFieldCell alloc] initWithTitle:@"服务商ID:" placeHolder:@"请输入所属服务商ID/推荐码" isRequired:NO];
    [bgView addSubview:_serviceIDCell];
    [_serviceIDCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(bgView);
        make.height.equalTo(@53);
    }];
    
    _storeNameCell = [[BRTextFieldCell alloc] initWithTitle:@"商户名称:" placeHolder:@"须与营业执照保持一致" isRequired:YES];
    [bgView addSubview:_storeNameCell];
    [_storeNameCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_serviceIDCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _storeSimpleCell = [[BRTextFieldCell alloc] initWithTitle:@"商户简称:" placeHolder:@"请输入商户简称" isRequired:YES];
    [bgView addSubview:_storeSimpleCell];
    [_storeSimpleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_storeNameCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _registerNameCell = [[BRTextFieldCell alloc] initWithTitle:@"注册名称:" placeHolder:@"须与营业执照保持一致" isRequired:YES];
    [bgView addSubview:_registerNameCell];
    [_registerNameCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_storeSimpleCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _storeContactCell = [[BRTextFieldCell alloc] initWithTitle:@"商户联系人:" placeHolder:@"请输入商户联系人姓名" isRequired:YES];
    [bgView addSubview:_storeContactCell];
    [_storeContactCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_registerNameCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _contactPhoneCell = [[BRTextFieldCell alloc] initWithTitle:@"联系人电话:" placeHolder:@"请输入商户联系人手机号码" isRequired:YES];
    [bgView addSubview:_contactPhoneCell];
    [_contactPhoneCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_storeContactCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _contactMailCell = [[BRTextFieldCell alloc] initWithTitle:@"联系人邮箱:" placeHolder:@"请输入商户联系人邮箱" isRequired:NO];
    [bgView addSubview:_contactMailCell];
    [_contactMailCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_contactPhoneCell.mas_bottom);
        make.height.equalTo(@53);
    }];

    _storeContactPhoneCell = [[BRTextFieldCell alloc] initWithTitle:@"商户联系电话:" placeHolder:@"请输入商户联系电话" isRequired:YES];
    [bgView addSubview:_storeContactPhoneCell];
    [_storeContactPhoneCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_contactMailCell.mas_bottom);
        make.height.equalTo(@53);
    }];
    
    _shJiFenRuleCell = [[BRTextFieldCell alloc] initWithTitle:@"商户积分规则:" placeHolder:@"每消费多少钱积商户1分" isRequired:YES];
    [bgView addSubview:_shJiFenRuleCell];
    [_shJiFenRuleCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_storeContactPhoneCell.mas_bottom);
        make.height.equalTo(@53);
    }];
    
    _jiFenExchangeCell = [[BRTextFieldCell alloc] initWithTitle:@"商户兑换规则:" placeHolder:@"商户多少分可以兑换1元消费" isRequired:YES];
    [bgView addSubview:_jiFenExchangeCell];
    [_jiFenExchangeCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_shJiFenRuleCell.mas_bottom);
        make.height.equalTo(@53);
    }];
    
    __weak typeof(self) weakSelf = self;
    _selectView = [[BRSelectView alloc] init];
    [bgView addSubview:_selectView];
    [_selectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_jiFenExchangeCell.mas_bottom);
        make.height.equalTo(@353);
    }];
    _selectView.businessAttributeSelectAction = ^(UIButton *sender){
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.busnessAttributeAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    _selectView.careerOneSelectAction = ^(UIButton *sender) {
        [weakSelf.selectView.careerBtnTwo setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                 forState:UIControlStateNormal];
        [weakSelf.selectView.careerBtnTwo setTitle:@"请选择" forState:UIControlStateNormal];
        [weakSelf.selectView.careerBtnThree setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                   forState:UIControlStateNormal];
        [weakSelf.selectView.careerBtnThree setTitle:@"请选择" forState:UIControlStateNormal];
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.careerOneAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    __weak typeof(_selectView) weakSelectView = _selectView;
    _selectView.careerTwoSelectAction = ^(UIButton *sender) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"1" forKey:@"type"];
        [dict setObject:[weakSelf.careerOneDict objectForKey:weakSelectView.careerBtn.titleLabel.text] forKey:@"mccwxid"];
        [weakSelf.vcModel getCareerTypeWithParameter:dict success:^(id responseObject) {
            [weakSelf.selectView.careerBtnThree setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                     forState:UIControlStateNormal];
            [weakSelf.selectView.careerBtnThree setTitle:@"请选择" forState:UIControlStateNormal];
            NSMutableArray *cityAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"secondname"] nextLevelArray:nil];
                [cityAry addObject:showmodel];
                [weakSelf.careerDict setObject:[dict objectForKey:@"id"] forKey:[dict objectForKey:@"secondname"]];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:cityAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"行业数据请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    _selectView.careerThreeSelectAction = ^(UIButton *sender) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:@"2" forKey:@"type"];
        [dict setObject:[weakSelf.careerDict objectForKey:weakSelectView.careerBtnTwo.titleLabel.text] forKey:@"mccwxid"];
        [weakSelf.vcModel getCareerTypeWithParameter:dict success:^(id responseObject) {
            NSMutableArray *careerAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"thirdname"] nextLevelArray:nil];
                [careerAry addObject:showmodel];
                [weakSelf.careerTwoDict setObject:[dict objectForKey:@"id"] forKey:[dict objectForKey:@"thirdname"]];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:careerAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"行业数据请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    _selectView.locationAction = ^(UIButton *sender) {
        //todo...[使用GPS定位]
    };
    _selectView.provinceSelectAction = ^(UIButton *sender) {
        [weakSelf.selectView.locationBtnTwo setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                 forState:UIControlStateNormal];
        [weakSelf.selectView.locationBtnTwo setTitle:@"市" forState:UIControlStateNormal];
        [weakSelf.selectView.locationBtnThree setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                 forState:UIControlStateNormal];
        [weakSelf.selectView.locationBtnThree setTitle:@"区/县" forState:UIControlStateNormal];
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.provinceAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    _selectView.citySelectAction = ^(UIButton *sender) {
        [weakSelf.selectView.locationBtnThree setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                   forState:UIControlStateNormal];
        [weakSelf.selectView.locationBtnThree setTitle:@"区/县" forState:UIControlStateNormal];
        [weakSelf.cityDict removeAllObjects];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[weakSelf.provinceDict objectForKey:weakSelectView.locationBtnOne.titleLabel.text] forKey:@"parent"];
        [weakSelf.vcModel getCityWithParameter:dict success:^(id responseObject) {
            NSMutableArray *cityAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"area_name"] nextLevelArray:nil];
                [cityAry addObject:showmodel];
                [weakSelf.cityDict setObject:[dict objectForKey:@"area_id"] forKey:[dict objectForKey:@"area_name"]];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:cityAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"城市列表请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    
    _selectView.quxianSelectAction = ^(UIButton *sender) {
        [weakSelf.countryDict removeAllObjects];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[weakSelf.cityDict objectForKey:weakSelectView.locationBtnTwo.titleLabel.text] forKey:@"parent"];
        [weakSelf.vcModel getCountryWithParameter:dict success:^(id responseObject) {
            NSMutableArray *countryAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"area_name"] nextLevelArray:nil];
                [countryAry addObject:showmodel];
                [weakSelf.countryDict setObject:[dict objectForKey:@"area_id"] forKey:[dict objectForKey:@"area_name"]];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:countryAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"市县列表请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    
    _descriptionView = [[StoreDescriptionView alloc] init];
    [bgView addSubview:_descriptionView];
    [_descriptionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(bgView);
        make.top.equalTo(_selectView.mas_bottom);
        make.height.equalTo(@210);
    }];
    
    return bgView;
}

- (void)initPickerView {
    _pickViewContainerView = [[FSPickViewContainerView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH-44)];
    _pickViewContainerView.delegate = self;
    [self.view addSubview:_pickViewContainerView];
    _pickViewContainerView.hidden = YES;
}

- (UIView *)createBottomView {
    
    UIView *btmView = [[UIView alloc] init];
    
    UIButton *previousBtn = [FSViewHelper createBtnWithTitle:@"上一步"
                                                     bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                                  titleColor:kWhiteColor];
    [previousBtn addTarget:self action:@selector(previousBtnClick) forControlEvents:UIControlEventTouchDown];
    previousBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:previousBtn];
    [previousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.left.equalTo(btmView).offset(40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    UIButton *nextBtn = [FSViewHelper createBtnWithTitle:@"下一步"
                                                     bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                                  titleColor:kWhiteColor];
    [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchDown];
    nextBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:nextBtn];
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.right.equalTo(btmView).offset(-40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    return btmView;
}

- (void)previousBtnClick {
    
}

- (void)initializationViewData {
    NSDictionary *dict = [self getDataFromSaveFileWithPath:@"baseResource.archive"];
    if (!dict || dict.count == 0) return;
    _serviceIDCell.textField.text = [dict objectForKey:@"inviter_manage_id"];
    _storeNameCell.textField.text = [dict objectForKey:@"store_name"];
    _storeSimpleCell.textField.text = [dict objectForKey:@"store_nickname"];
    _registerNameCell.textField.text = [dict objectForKey:@"store_resiger_name"];
    _storeContactCell.textField.text = [dict objectForKey:@"contacts_name"];
    _contactPhoneCell.textField.text = [dict objectForKey:@"contacts_phone"];
    _contactMailCell.textField.text = [dict objectForKey:@"contacts_email"];
    _storeContactPhoneCell.textField.text = [dict objectForKey:@"contacts_phones"];
    _shJiFenRuleCell.textField.text = [dict objectForKey:@"jf_role"];
    _jiFenExchangeCell.textField.text = [dict objectForKey:@"jf_dh_role"];
    _selectView.addressTF.text = [dict objectForKey:@"store_address"];
    _descriptionView.textView.text = [dict objectForKey:@"store_description"];
}

- (void)nextBtnClick {
    if (![self judgeNumericField]) return;
    
    NSMutableDictionary *baseResourceDict = [[NSMutableDictionary alloc] init];
    //todo...
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:kMemberId];
    [baseResourceDict setObject:userID forKey:@"userid"];
    [baseResourceDict setObject:_serviceIDCell.textField.text?_serviceIDCell.textField.text:@"" forKey:@"inviter_manage_id"];//服务商ID
    [baseResourceDict setObject:_storeNameCell.textField.text forKey:@"store_name"];//商户名称
    [baseResourceDict setObject:_storeSimpleCell.textField.text forKey:@"store_nickname"];//商户简称
    [baseResourceDict setObject:_registerNameCell.textField.text forKey:@"store_resiger_name"];//注册名称
    [baseResourceDict setObject:_storeContactCell.textField.text forKey:@"contacts_name"];//商户联系人
    [baseResourceDict setObject:_contactPhoneCell.textField.text forKey:@"contacts_phone"];//联系人电话
    [baseResourceDict setObject:_contactMailCell.textField.text?_contactMailCell.textField.text:@"" forKey:@"contacts_email"];//联系人邮箱
    [baseResourceDict setObject:_storeContactPhoneCell.textField.text forKey:@"contacts_phones"];//商户联系电话
    [baseResourceDict setObject:_shJiFenRuleCell.textField.text forKey:@"jf_role"];//商户积分规则
    [baseResourceDict setObject:_jiFenExchangeCell.textField.text forKey:@"jf_dh_role"];//商户兑换规则
    [baseResourceDict setObject:[_careerOneDict objectForKey:_selectView.careerBtn.titleLabel.text] forKey:@"store_industry_one"];//行业类别 一类
    [baseResourceDict setObject:[_careerDict objectForKey:_selectView.careerBtnTwo.titleLabel.text] forKey:@"store_industry_two"];//行业类别 二类
    [baseResourceDict setObject:[_careerTwoDict objectForKey:_selectView.careerBtnThree.titleLabel.text] forKey:@"store_industry_three"];//行业类别 三类
    [baseResourceDict setObject:[_busnessAttributeDict objectForKey:_selectView.shBtn.titleLabel.text] forKey:@"store_type"];//商户属性
    [baseResourceDict setObject:[_provinceDict objectForKey:_selectView.locationBtnOne.titleLabel.text] forKey:@"province_id"];//省ID
    [baseResourceDict setObject:[_cityDict objectForKey:_selectView.locationBtnTwo.titleLabel.text] forKey:@"city_id"];//市ID
    [baseResourceDict setObject:[_countryDict objectForKey:_selectView.locationBtnThree.titleLabel.text] forKey:@"area_id"];//县ID
    NSString *address = [NSString stringWithFormat:@"%@%@%@",_selectView.locationBtnOne.titleLabel.text,_selectView.locationBtnTwo.titleLabel.text,_selectView.locationBtnThree.titleLabel.text];
    [baseResourceDict setObject:address forKey:@"address_info"];//商户地址
    [baseResourceDict setObject:_selectView.addressTF.text forKey:@"store_address"];//商户详细地址
    [baseResourceDict setObject:_descriptionView.textView.text forKey:@"store_description"];//商户描述
    //todo...
    [baseResourceDict setObject:@"" forKey:@"store_lng"];//商户经度
    [baseResourceDict setObject:@"" forKey:@"store_lat"];//商户纬度
    [self saveLocalDataWithDict:baseResourceDict];
    
    JieSuanAccountVC *vc = [[JieSuanAccountVC alloc] init];
    if ([_selectView.shBtn.titleLabel.text isEqualToString:@"小微商户"]) {
        vc.shangHuType = 3;
    }else if ([_selectView.shBtn.titleLabel.text isEqualToString:@"企业"]){
        vc.shangHuType = 1;
    }else {
        vc.shangHuType = 2;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)saveLocalDataWithDict:(NSDictionary *)dict {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:@"baseResource.archive"];
    [NSKeyedArchiver archiveRootObject:dict toFile:path];
}

- (NSDictionary *)getDataFromSaveFileWithPath:(NSString *)pathStr {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:pathStr];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    return dict;
}

- (BOOL)judgeNumericField {
    if (!_storeNameCell.textField.text || [_storeNameCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户名称" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_storeSimpleCell.textField.text || [_storeSimpleCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户简称" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_registerNameCell.textField.text || [_registerNameCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入注册名称" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_registerNameCell.textField.text || [_registerNameCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入注册名称" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_storeContactCell.textField.text || [_storeContactCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户联系人" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_contactPhoneCell.textField.text || [_contactPhoneCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入联系人电话" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_storeContactPhoneCell.textField.text || [_storeContactPhoneCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户联系电话" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_shJiFenRuleCell.textField.text || [_shJiFenRuleCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户积分规则" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_jiFenExchangeCell.textField.text || [_jiFenExchangeCell.textField.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入积分兑换规则" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.shBtn.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择商户属性" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.careerBtn.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择行业类别" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.careerBtnTwo.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择行业类别" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.locationBtnOne.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择商户所在的省份" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.locationBtnTwo.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择商户所在的市区" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_selectView.locationBtnThree.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择商户所在的区县" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if (!_selectView.addressTF.text || [_selectView.addressTF.text isEqualToString:@""]) {
        [self.view makeToast:@"请输入商户详细地址" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_descriptionView.textView.text isEqualToString:@""] ||
        [_descriptionView.textView.text isEqualToString:@"完善的商户描述有助于消费者更好的了解您的店铺，吸引更多的用户到店消费；300字以内"]) {
        [self.view makeToast:@"请输入您的商户描述" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    return YES;
}

#pragma mark - 实现FSPickerViewContrller的delegate
- (UIView *)buttonContainerView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor = kWhiteColor;
    
    UIButton *cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 56, 33)];
    [cancleBtn setTitle:@"取  消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:kBlackColor forState:UIControlStateNormal];
    cancleBtn.layer.cornerRadius = 5.0;
    
    [cancleBtn addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancleBtn];
    
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 66, 10, 56, 33)];
    [sureBtn setTitle:@"确  定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:kBlackColor forState:UIControlStateNormal];
    sureBtn.layer.cornerRadius = 5.0;
    [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:sureBtn];
    return view;
}

- (void) cancleAction:(UIButton *)button {
    [_pickViewContainerView hiddenByChangeSelectedValue:YES];
}

- (void) sureAction:(UIButton *)button {
    [_pickViewContainerView hiddenByChangeSelectedValue:YES];
    NSLog(@"%@",_pickViewContainerView.showString);
}

- (CGFloat)buttonContainerViewHeight {
    return 50;
}

- (CGFloat)pickViewHeight {
    return 216;
}

- (void)clickTopBlankViewAction {
    
}

- (void)didSelectWithString:(NSString *)string {
}

@end
