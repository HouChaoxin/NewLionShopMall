//
//  HLoginVC.m
//  LionMall
//
//  Created by hcx_ios on 2018/4/14.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "HLoginVC.h"
#import "HLoginVCModel.h"
#import "RootViewController.h"
#import "JoinProtocalVC.h"

@interface HLoginVC ()<UITextFieldDelegate>

@property (nonatomic,strong) UIImageView *imgView;
@property (nonatomic,strong) UITextField *accountTextField;
@property (nonatomic,strong) UITextField *codeTextField;
@property (nonatomic,strong) UIButton *sendBtn;
@property (nonatomic,strong) UIButton *loginBtn;
@property (nonatomic,strong) HLoginVCModel *loginVCModel;

@property (nonatomic,strong) NSTimer *timer;

@end

@implementation HLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _loginVCModel = [[HLoginVCModel alloc] init];
}

- (void)initView {
    
    [self setDismissKeyboradWhenBackgroundTouched:self.view];
    
    _imgView = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"logo"]
                                          bgColor:[UIColor groupTableViewBackgroundColor]];
    NSString *urlStr = [[NSUserDefaults standardUserDefaults] objectForKey:kMemberHeadImg];
    if (urlStr && ![urlStr isEqualToString:@""]) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:urlStr]
                    placeholderImage:[UIImage imageNamed:@"logo"]];
    }
    _imgView.layer.cornerRadius = 5.0f;
    _imgView.layer.masksToBounds = YES;
    [self.view addSubview:_imgView];
    [_imgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(72, 72)]);
        make.top.equalTo(self.view).offset(90);
        make.centerX.equalTo(self.view);
    }];
    
    UILabel *titleLbl = [FSViewHelper createLblWithText:@"您的智能门店管家"
                                          textAlignment:NSTextAlignmentCenter
                                               fontSize:22];
    [self.view addSubview:titleLbl];
    [titleLbl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_imgView.mas_bottom).offset(24);
        make.left.right.equalTo(self.view);
        make.height.equalTo(@30);
    }];
    
    UIImageView *accountImg = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"ic_shouji"]
                                                         bgColor:kWhiteColor];
    [self.view addSubview:accountImg];
    [accountImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.top.equalTo(titleLbl.mas_bottom).offset(48);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(24, 24)]);
    }];
    
    _accountTextField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                             fontSize:16
                                                            textColor:[ColorConverter colorWithHexString:@"0x333333"]];
    _accountTextField.delegate = self;
    _accountTextField.placeholder = @"请输入您的手机号码";
    _accountTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:kMemberPhone];
    if (userName) {
        _accountTextField.text = userName;
    }
    [self.view addSubview:_accountTextField];
    [_accountTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(accountImg.mas_right).offset(16);
        make.right.equalTo(self.view).offset(-24);
        make.top.equalTo(titleLbl.mas_bottom).offset(45);
        make.height.equalTo(@30);
    }];
    
    UIImageView *lineImgView = [FSViewHelper createImgViewWithImg:nil bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    [self.view addSubview:lineImgView];
    [lineImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_accountTextField.mas_bottom).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.left.equalTo(self.view).offset(16);
        make.height.equalTo(@1);
    }];
    
    UIImageView *codeImg = [FSViewHelper createImgViewWithImg:[UIImage imageNamed:@"ic_duanxin"]
                                                         bgColor:kWhiteColor];
    [self.view addSubview:codeImg];
    [codeImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(24);
        make.top.equalTo(lineImgView.mas_bottom).offset(18);
        make.size.equalTo([NSValue valueWithCGSize:CGSizeMake(24, 24)]);
    }];
    
    _codeTextField = [FSViewHelper creatTextFieldWithTextAlignment:NSTextAlignmentLeft
                                                             fontSize:16
                                                            textColor:[ColorConverter colorWithHexString:@"0x333333"]];
    _codeTextField.secureTextEntry = YES;
    _codeTextField.delegate = self;
    _codeTextField.placeholder = @"请输入短信验证码";
    [self.view addSubview:_codeTextField];
    [_codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(codeImg.mas_right).offset(24);
        make.right.equalTo(self.view).offset(-115);
        make.top.equalTo(lineImgView.mas_bottom).offset(16);
        make.height.equalTo(@30);
    }];
    
    _sendBtn = [FSViewHelper createBtnWithTitle:@"获取验证码"
                                                 bgColor:kWhiteColor
                                              titleColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    _sendBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    _sendBtn.layer.cornerRadius = 5.0f;
    [_sendBtn addTarget:self action:@selector(getYanZhengCode:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_sendBtn];
    [_sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(lineImgView.mas_bottom).offset(16);
        make.right.equalTo(self.view).offset(-15);
        make.width.equalTo(@85);
        make.height.equalTo(@30);
    }];
    
    UIImageView *lineImgViewTwo = [FSViewHelper createImgViewWithImg:nil
                                                             bgColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    [self.view addSubview:lineImgViewTwo];
    [lineImgViewTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_codeTextField.mas_bottom).offset(16);
        make.right.equalTo(self.view).offset(-16);
        make.left.equalTo(self.view).offset(16);
        make.height.equalTo(@1);
    }];
    
    _loginBtn = [FSViewHelper createBtnWithTitle:@"登录"
                                                  bgColor:[ColorConverter colorWithHexString:@"0x96C3E3"]
                                               titleColor:kWhiteColor];
    _loginBtn.userInteractionEnabled = NO;
    _loginBtn.layer.cornerRadius = 5.0f;
    [_loginBtn addTarget:self action:@selector(loginBtnAction:) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:_loginBtn];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(14);
        make.right.equalTo(self.view).offset(-14);
        make.top.equalTo(_codeTextField.mas_bottom).offset(80);
        make.height.equalTo(@50);
    }];
}

- (void)eyeBtnAction:(UIButton *)sender {
    if (_codeTextField.secureTextEntry) {
        _codeTextField.secureTextEntry = NO;
        [sender setImage:[UIImage imageNamed:@"ic_xianshimima"] forState:UIControlStateNormal];
    }else {
        _codeTextField.secureTextEntry = YES;
        [sender setImage:[UIImage imageNamed:@"ic_guanbixianshi"] forState:UIControlStateNormal];
    }
}

- (void)loginBtnAction:(UIButton *)sender {
    NSLog(@"登录按钮被点击");
    [self loginAction:sender];
//    JoinProtocalVC *vc = [[JoinProtocalVC alloc] init];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (![_accountTextField.text isEqualToString:@""] ||
        ![_codeTextField.text isEqualToString:@""]) {
        _loginBtn.userInteractionEnabled = YES;
        [_loginBtn setBackgroundColor:[ColorConverter colorWithHexString:@"0x36A4F3"]];
    }else {
        _loginBtn.userInteractionEnabled = NO;
        [_loginBtn setBackgroundColor:[ColorConverter colorWithHexString:@"0x96C3E3"]];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

// 登录点击
- (IBAction)loginAction:(id)sender {
    
    NSString *username = _accountTextField.text;
    NSString *password = _codeTextField.text;
    if ([username isEqualToString:@""]) {

        [self.view makeToast:@"请输入您的手机号" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    if ([password isEqualToString:@""]) {

        [self.view makeToast:@"请输入您的验证码" duration:1.5 position:CSToastPositionCenter];
        return;
    }

    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:_accountTextField.text forKey:@"member_mobile"];
    [dict setObject:_codeTextField.text forKey:@"sms_code"];

    [_loginVCModel loginRequestWithParameter:dict success:^(NSString *state) {
        if ([state isEqualToString:@"1"]) {
            RootViewController *mainVC = [[RootViewController alloc] init];
            [UIApplication sharedApplication].keyWindow.rootViewController = mainVC;
        }else if([state isEqualToString:@"5"]){
            JoinProtocalVC *vc = [[JoinProtocalVC alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }else if([state isEqualToString:@"0"]){
            [self.view makeToast:@"抱歉，该商户已关闭" duration:1.5 position:CSToastPositionCenter];
        }else if([state isEqualToString:@"2"]){
            [self.view makeToast:@"抱歉，该商户在审核中，请耐心等待审核完毕" duration:1.5 position:CSToastPositionCenter];
        }else if([state isEqualToString:@"3"]){
            [self.view makeToast:@"抱歉，该商户审核未通过，请修改并完善资料" duration:1.5 position:CSToastPositionCenter];
            //todo...
        }
    } fail:^{
        [self.view makeToast:@"登录失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
    }];
}


// 获取验证码
- (IBAction)getYanZhengCode:(id)sender {
    
    NSString *phone = self.accountTextField.text;
    
    if(phone && ![phone isEqualToString:@""]) {
        self.sendBtn.enabled = NO;
        __weak typeof(self) weakSelf = self;
         //发送验证码
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:phone forKey:@"phone"];
        [_loginVCModel getVertifyCodeWithParameter:dict success:^(id responseObject) {
            if([responseObject isKindOfClass:[NSString class]]) {
                
                [weakSelf.view makeToast:responseObject duration:1.5 position:CSToastPositionCenter];
                if ([responseObject isEqualToString:@"发送成功"]) {
                    
                    [weakSelf startCountTime];
                }
            }else{
                [weakSelf.view makeToast:@"验证码获取失败" duration:1.5 position:CSToastPositionCenter];
                _sendBtn.enabled = YES;
            }
        } fail:^{
            _sendBtn.enabled = YES;
            [weakSelf.view makeToast:@"验证码获取失败" duration:1.5 position:CSToastPositionCenter];
        }];
    }else {
        [self.view makeToast:@"请输入电话号码" duration:1.5 position:CSToastPositionCenter];
    }
}

static NSInteger count = 60;
// 开始倒计时
//- (void)startCountTime {
//
//    count = 60;
//    __weak typeof(self) weakSelf = self;
//    [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
//
//        count--;
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            NSString *str = [NSString stringWithFormat:@"%ld",count];
//            [weakSelf.sendBtn setTitle:str forState:UIControlStateNormal];
//            if (count < 1) {
//
//                weakSelf.sendBtn.enabled = YES;
//                [weakSelf.sendBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
//            }
//        });
//        if (count < 1) {
//            [timer invalidate];
//            timer = nil;
//        }
//    }];
//}

- (void)startCountTime {
    count = 60;
    _timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDownTime) userInfo:nil repeats:YES];
}

- (void)countDownTime {
    count--;
    dispatch_async(dispatch_get_main_queue(), ^{

        NSString *str = [NSString stringWithFormat:@"%ld",count];
        [self.sendBtn setTitle:str forState:UIControlStateNormal];
        if (count < 1) {

            self.sendBtn.enabled = YES;
            [self.sendBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
        }
    });
    if (count < 1) {
        [_timer invalidate];
        _timer = nil;
    }
}


@end
