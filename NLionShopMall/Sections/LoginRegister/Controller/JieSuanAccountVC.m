//
//  JieSuanAccountVC.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/16.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "JieSuanAccountVC.h"
#import "BRHeaderView.h"
#import "BusinessHoursCell.h"
#import "CreditalView.h"
#import "FaRenView.h"
#import "AccountBankView.h"
#import "JieSuanRenView.h"
#import "FaRenJieSuanRenView.h"
#import "FSPickViewContainerView.h"
#import "AreaModel.h"
#import "SelectTimeView.h"
#import "JiSuanAccountVCModel.h"
#import "PicUploadVC.h"

static CGFloat hourCellHeight = 53;
static CGFloat creditalViewHeight = 212;
static CGFloat faRenViewHeight = 318;
static CGFloat accountBankViewHeight = 159;
static CGFloat jieSuanRenViewHeight = 265;
static CGFloat faRenJieSuanRenViewHeight = 424;

@interface JieSuanAccountVC ()<FSPickViewContainerViewDelegate>

@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic,strong) UIView *sBgView;
@property (nonatomic,strong) BRHeaderView *headView;
@property (nonatomic, strong) FSPickViewContainerView *pickViewContainerView;
@property (nonatomic, strong) FSPickViewModel *pickViewModel;
@property (nonatomic,strong) JiSuanAccountVCModel *vcModel;

@property (nonatomic,strong) BusinessHoursCell *hourCell;
@property (nonatomic,strong) CreditalView *creditalView;
@property (nonatomic,strong) FaRenView *faRenView;
@property (nonatomic,strong) AccountBankView *accountBankView;
@property (nonatomic,strong) JieSuanRenView *jieSuanRenView;
@property (nonatomic,strong) FaRenJieSuanRenView *faRenJieSuanRenView;

@property (nonatomic,copy) NSArray *qiYeZhangJianAry;
@property (nonatomic,copy) NSArray *jieSuanZhangHuAry;
@property (nonatomic,copy) NSArray *ruZhangAry;
@property (nonatomic,strong) NSMutableDictionary *provinceDict;
@property (nonatomic,strong) NSMutableArray *provinceAry;
@property (nonatomic,strong) NSMutableDictionary *cityDict;
@property (nonatomic,strong) NSMutableDictionary *bankDict;
@property (nonatomic,strong) NSMutableArray *bankAry;

@property (nonatomic,assign) PicUploadType uploadType;

@end

@implementation JieSuanAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initData];
    [self initView];
    [self initializationViewData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)initData {
    _vcModel = [[JiSuanAccountVCModel alloc] init];
    //企业证件类型
    FSPickViewShowModel *qiYeModel0 = [[FSPickViewShowModel alloc] initWitShowName:@"三证合一营业执照" nextLevelArray:nil];
    FSPickViewShowModel *qiYeModel1 = [[FSPickViewShowModel alloc] initWitShowName:@"营业执照" nextLevelArray:nil];
    self.qiYeZhangJianAry = [[NSArray alloc] initWithObjects:qiYeModel0,qiYeModel1, nil];
    
    //结算账户类型
    FSPickViewShowModel *jieSuanModel0 = [[FSPickViewShowModel alloc] initWitShowName:@"对公结算" nextLevelArray:nil];
    FSPickViewShowModel *jieSuanModel1 = [[FSPickViewShowModel alloc] initWitShowName:@"对私结算" nextLevelArray:nil];
    self.jieSuanZhangHuAry = [[NSArray alloc] initWithObjects:jieSuanModel0,jieSuanModel1, nil];
    
    //入账形式
    FSPickViewShowModel *ruZhangModel0 = [[FSPickViewShowModel alloc] initWitShowName:@"法人入账" nextLevelArray:nil];
    FSPickViewShowModel *ruZhangModel1 = [[FSPickViewShowModel alloc] initWitShowName:@"非法人入账" nextLevelArray:nil];
    self.ruZhangAry = [[NSArray alloc] initWithObjects:ruZhangModel0,ruZhangModel1, nil];
    
    //账户开户行
    _bankDict = [[NSMutableDictionary alloc] init];
    [_bankDict setObject:@"63" forKey:@"中国工商银行"];
    [_bankDict setObject:@"62" forKey:@"中国农业银行"];
    [_bankDict setObject:@"61" forKey:@"中国银行"];
    [_bankDict setObject:@"60" forKey:@"中国建设银行"];
    [_bankDict setObject:@"69" forKey:@"国家开发银行"];
    [_bankDict setObject:@"64" forKey:@"中国进出口银行"];
    [_bankDict setObject:@"1" forKey:@"中国农业发展银行"];
    [_bankDict setObject:@"67" forKey:@"交通银行"];
    [_bankDict setObject:@"4" forKey:@"中信银行"];
    [_bankDict setObject:@"70" forKey:@"中国光大银行"];
    [_bankDict setObject:@"68" forKey:@"华夏银行"];
    [_bankDict setObject:@"72" forKey:@"中国民生银行"];
    [_bankDict setObject:@"71" forKey:@"广发银行"];
    [_bankDict setObject:@"74" forKey:@"平安银行"];
    [_bankDict setObject:@"73" forKey:@"招商银行"];
    [_bankDict setObject:@"5" forKey:@"兴业银行"];
    [_bankDict setObject:@"59" forKey:@"上海浦东发展银行"];
    [_bankDict setObject:@"58" forKey:@"城市商业银行"];
    [_bankDict setObject:@"56" forKey:@"农村商业银行"];
    [_bankDict setObject:@"57" forKey:@"恒丰银行"];
    [_bankDict setObject:@"55" forKey:@"浙商银行"];
    [_bankDict setObject:@"12" forKey:@"农村合作银行"];
    [_bankDict setObject:@"54" forKey:@"渤海银行"];
    [_bankDict setObject:@"11" forKey:@"徽商银行"];
    [_bankDict setObject:@"77" forKey:@"地方银行"];
    [_bankDict setObject:@"13" forKey:@"重庆三峡银行"];
    [_bankDict setObject:@"6" forKey:@"上海农商银行"];
    [_bankDict setObject:@"14" forKey:@"城市信用社"];
    [_bankDict setObject:@"9" forKey:@"农村信用合作联社"];
    [_bankDict setObject:@"8" forKey:@"中国邮政储蓄银行"];
    _bankAry = [[NSMutableArray alloc] init];
    for (NSString *str in [_bankDict allKeys]) {
        FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:str nextLevelArray:nil];
        [_bankAry addObject:showmodel];
    }
    
//    FSPickViewShowModel *bankModel0 = [[FSPickViewShowModel alloc] initWitShowName:@"中国平安银行" nextLevelArray:nil];
//    FSPickViewShowModel *bankModel1 = [[FSPickViewShowModel alloc] initWitShowName:@"中国建设银行" nextLevelArray:nil];
//    FSPickViewShowModel *bankModel2 = [[FSPickViewShowModel alloc] initWitShowName:@"中国工商银行" nextLevelArray:nil];
//    FSPickViewShowModel *bankModel3 = [[FSPickViewShowModel alloc] initWitShowName:@"中国农业银行" nextLevelArray:nil];
//    FSPickViewShowModel *bankModel4 = [[FSPickViewShowModel alloc] initWitShowName:@"中国交通银行" nextLevelArray:nil];
//    FSPickViewShowModel *bankModel5 = [[FSPickViewShowModel alloc] initWitShowName:@"招商银行" nextLevelArray:nil];
//    self.bankAry = [[NSArray alloc] initWithObjects:bankModel0,bankModel1,bankModel2,bankModel3,bankModel4,bankModel5, nil];
    
    //省／市／区
//    NSDictionary *dic = [[NSDictionary alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area" ofType:@"plist"]];
//    AreaModel *areaModel = [[AreaModel alloc] init];
//    [areaModel analysisWithDictionary:dic];
//
//    NSMutableArray *tempArray = [[NSMutableArray alloc] init];
//
//    for (int i=0 ; i < areaModel.provinceArray.count; i++) {
//
//        CityModel *citymodel = [areaModel.provinceArray objectAtIndex:i];
//        NSMutableArray *array = [[NSMutableArray alloc] init];
//
//        for (int j=0; j < citymodel.cityArray.count; j++) {
//
//            CountyModel *countymodel = [citymodel.cityArray objectAtIndex:j];
//
//            NSMutableArray *array2 = [[NSMutableArray alloc] init];
//            for (NSString *str  in countymodel.countyArray) {
//                FSPickViewShowModel *tempPickmodel = [[FSPickViewShowModel alloc] initWitShowName:str nextLevelArray:nil];
//                [array2 addObject:tempPickmodel];
//            }
//            FSPickViewShowModel *pickModel = [[FSPickViewShowModel alloc] initWitShowName:countymodel.city nextLevelArray:nil];
//            [array addObject:pickModel];
//        }
//        [self.cityDict setObject:array forKey:citymodel.province];
//        FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:citymodel.province nextLevelArray:nil];
//
//        [tempArray addObject:showmodel];
//    }
//    self.provinceAry = [NSArray arrayWithArray:tempArray];
    _provinceDict = [[NSMutableDictionary alloc] init];
    [_provinceDict setObject:@"1" forKey:@"北京市"];
    [_provinceDict setObject:@"2" forKey:@"天津市"];
    [_provinceDict setObject:@"3" forKey:@"河北省"];
    [_provinceDict setObject:@"4" forKey:@"山西省"];
    [_provinceDict setObject:@"5" forKey:@"内蒙古自治区"];
    [_provinceDict setObject:@"6" forKey:@"辽宁省"];
    [_provinceDict setObject:@"7" forKey:@"吉林省"];
    [_provinceDict setObject:@"8" forKey:@"黑龙江省"];
    [_provinceDict setObject:@"9" forKey:@"上海市"];
    [_provinceDict setObject:@"10" forKey:@"江苏省"];
    [_provinceDict setObject:@"11" forKey:@"浙江省"];
    [_provinceDict setObject:@"12" forKey:@"安徽省"];
    [_provinceDict setObject:@"13" forKey:@"福建省"];
    [_provinceDict setObject:@"14" forKey:@"江西省"];
    [_provinceDict setObject:@"15" forKey:@"山东省"];
    [_provinceDict setObject:@"16" forKey:@"河南省"];
    [_provinceDict setObject:@"17" forKey:@"湖北省"];
    [_provinceDict setObject:@"18" forKey:@"湖南省"];
    [_provinceDict setObject:@"19" forKey:@"广东省"];
    [_provinceDict setObject:@"20" forKey:@"广西省"];
    [_provinceDict setObject:@"21" forKey:@"海南省"];
    [_provinceDict setObject:@"22" forKey:@"重庆市"];
    [_provinceDict setObject:@"23" forKey:@"四川省"];
    [_provinceDict setObject:@"24" forKey:@"贵州省"];
    [_provinceDict setObject:@"25" forKey:@"云南省"];
    [_provinceDict setObject:@"26" forKey:@"西藏自治区"];
    [_provinceDict setObject:@"27" forKey:@"陕西省"];
    [_provinceDict setObject:@"28" forKey:@"甘肃省"];
    [_provinceDict setObject:@"29" forKey:@"青海省"];
    [_provinceDict setObject:@"30" forKey:@"宁夏回族自治区"];
    [_provinceDict setObject:@"31" forKey:@"新疆维吾尔自治区"];
    [_provinceDict setObject:@"32" forKey:@"台湾省"];
    [_provinceDict setObject:@"33" forKey:@"香港特别行政区"];
    [_provinceDict setObject:@"34" forKey:@"澳门特别行政区"];
    _provinceAry = [[NSMutableArray alloc] init];
    for (NSString *str in [_provinceDict allKeys]) {
        FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:str nextLevelArray:nil];
        [_provinceAry addObject:showmodel];
    }
    self.cityDict = [[NSMutableDictionary alloc] init];
}

- (void)initView {
    [self setNavigationBarTitleViewWithTitle:@"结算账户"];
    [self hideBackButtonTitle];
    
    _headView = [[BRHeaderView alloc] init];
    [_headView refreshViewWithIndex:2];
    [self.view addSubview:_headView];
    [_headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@80);
    }];
    
    UIView *btmView = [self createBottomView];
    [self.view addSubview:btmView];
    [btmView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.equalTo(@102);
    }];
    
    _scrollView = [[UIScrollView alloc] init];
    [self.view addSubview:_scrollView];
    [_scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_headView.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(btmView.mas_top);
    }];
    
    _sBgView = [self createScrollViewSubViews];
    [_scrollView addSubview:_sBgView];
    
    [self initPickerView];
}

- (UIView *)createScrollViewSubViews {
    UIView *bgView = [[UIView alloc] init];
    __weak typeof(self) weakSelf = self;
    _hourCell = [[BusinessHoursCell alloc] init];
    [bgView addSubview:_hourCell];
    
    _hourCell.startTimeAction = ^(UIButton *sender) {
        SelectTimeView *view = [[SelectTimeView alloc] init];
        view.sureBtnAction = ^(NSString *dateStr) {
            [sender setTitle:dateStr forState:UIControlStateNormal];
            [sender setTitleColor:kBlackColor forState:UIControlStateNormal];
        };
        [weakSelf.view addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakSelf.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    };
    _hourCell.endTimeAction = ^(UIButton *sender) {
        SelectTimeView *view = [[SelectTimeView alloc] init];
        view.sureBtnAction = ^(NSString *dateStr) {
            [sender setTitle:dateStr forState:UIControlStateNormal];
            [sender setTitleColor:kBlackColor forState:UIControlStateNormal];
        };
        [weakSelf.view addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(weakSelf.view).with.insets(UIEdgeInsetsMake(0, 0, 0, 0));
        }];
    };
    
    _creditalView = [[CreditalView alloc] init];
    [bgView addSubview:_creditalView];
    
    weakSelf.creditalView.qiYeZhengJianAction = ^(UIButton *sender) {
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.qiYeZhangJianAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    _creditalView.jieSuanAccountAction = ^(UIButton *sender) {
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.jieSuanZhangHuAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    _creditalView.ruZhangAction = ^(UIButton *sender) {
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.ruZhangAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    _faRenView = [[FaRenView alloc] init];
    [bgView addSubview:_faRenView];
    
    _accountBankView = [[AccountBankView alloc] init];
    [bgView addSubview:_accountBankView];
    
    _accountBankView.bankSelecAction = ^(UIButton *sender) {
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.bankAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    _accountBankView.provinceSelectAction = ^(UIButton *sender) {
        [weakSelf.accountBankView.selectBtnTwo setTitleColor:[ColorConverter colorWithHexString:@"0x999999"]
                                                 forState:UIControlStateNormal];
        [weakSelf.accountBankView.selectBtnTwo setTitle:@"请选择" forState:UIControlStateNormal];
        weakSelf.pickViewContainerView.hidden = NO;
        weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:weakSelf.provinceAry selectedStringArray:nil];
        [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
    };
    
    __weak typeof(_accountBankView) weakBankView = _accountBankView;
    _accountBankView.citySelectAction = ^(UIButton *sender) {
        [weakSelf.cityDict removeAllObjects];
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[weakSelf.provinceDict objectForKey:weakBankView.selectBtnOne.titleLabel.text] forKey:@"provinceid"];
        [weakSelf.vcModel getBankCityWithParameter:dict success:^(id responseObject) {
            NSMutableArray *cityAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"name"] nextLevelArray:nil];
                [cityAry addObject:showmodel];
                [weakSelf.cityDict setObject:[dict objectForKey:@"id"] forKey:[dict objectForKey:@"name"]];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:cityAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"城市列表请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    _accountBankView.branchSelectAction = ^(UIButton *sender) {
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[weakSelf.bankDict objectForKey:weakBankView.accountBankCell.selectBtn.titleLabel.text] forKey:@"bankparentid"];
        [dict setObject:[weakSelf.cityDict objectForKey:weakBankView.selectBtnTwo.titleLabel.text] forKey:@"cityid"];
        [weakSelf.vcModel getBranchWithParameter:dict success:^(id responseObject) {
            NSMutableArray *branchAry = [[NSMutableArray alloc] init];
            NSArray *ary = (NSArray *)responseObject;
            for (NSDictionary *dict in ary) {
                FSPickViewShowModel *showmodel = [[FSPickViewShowModel alloc] initWitShowName:[dict objectForKey:@"bankName"] nextLevelArray:nil];
                [branchAry addObject:showmodel];
            }
            weakSelf.pickViewContainerView.hidden = NO;
            weakSelf.pickViewModel = [[FSPickViewModel alloc] initWithRelateView:sender showModelArray:branchAry selectedStringArray:nil];
            [weakSelf.pickViewContainerView showWithModel:weakSelf.pickViewModel];
        } fail:^{
            [weakSelf.view makeToast:@"账户开户支行列表请求失败，请稍后再试" duration:1.5 position:CSToastPositionCenter];
        }];
    };
    
    _jieSuanRenView = [[JieSuanRenView alloc] init];
    [bgView addSubview:_jieSuanRenView];
    
    _faRenJieSuanRenView = [[FaRenJieSuanRenView alloc] init];
    [bgView addSubview:_faRenJieSuanRenView];
    
    //布局
    [_hourCell mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(bgView);
        make.height.equalTo([NSNumber numberWithFloat:hourCellHeight]);
    }];
    
    if (_shangHuType == 3) {
        _faRenView.hidden = YES;
        _creditalView.hidden = YES;
        _faRenJieSuanRenView.hidden = YES;
        [_jieSuanRenView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(bgView);
            make.height.equalTo([NSNumber numberWithFloat:jieSuanRenViewHeight]);
        }];
        
        [_accountBankView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_jieSuanRenView.mas_bottom);
            make.left.right.equalTo(bgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+jieSuanRenViewHeight+accountBankViewHeight;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        bgView.frame = CGRectMake(0, 0, screenW, height);
    }else {
        _faRenView.hidden = YES;
        _jieSuanRenView.hidden = YES;
        _faRenJieSuanRenView.hidden = YES;
        [_creditalView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(bgView);
            make.height.equalTo([NSNumber numberWithFloat:creditalViewHeight]);
        }];
        
        [_accountBankView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_creditalView.mas_bottom);
            make.left.right.equalTo(bgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+creditalViewHeight+accountBankViewHeight;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        bgView.frame = CGRectMake(0, 0, screenW, height);
    }
    
    return bgView;
}

- (void)initPickerView {
    _pickViewContainerView = [[FSPickViewContainerView alloc] initWithFrame:CGRectMake(0, 0, screenW, screenH-44)];
    _pickViewContainerView.delegate = self;
    [self.view addSubview:_pickViewContainerView];
    _pickViewContainerView.hidden = YES;
}

- (UIView *)createBottomView {
    
    UIView *btmView = [[UIView alloc] init];
    
    UIButton *previousBtn = [FSViewHelper createBtnWithTitle:@"上一步"
                                                     bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                                  titleColor:kWhiteColor];
    [previousBtn addTarget:self action:@selector(previousBtnClick) forControlEvents:UIControlEventTouchDown];
    previousBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:previousBtn];
    [previousBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.left.equalTo(btmView).offset(40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    UIButton *nextBtn = [FSViewHelper createBtnWithTitle:@"下一步"
                                                 bgColor:[ColorConverter colorWithHexString:@"0xC3E0FF"]
                                              titleColor:kWhiteColor];
    [nextBtn addTarget:self action:@selector(nextBtnClick) forControlEvents:UIControlEventTouchDown];
    nextBtn.layer.cornerRadius = 5.0f;
    [btmView addSubview:nextBtn];
    [nextBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(btmView).offset(32);
        make.bottom.equalTo(btmView).offset(-32);
        make.right.equalTo(btmView).offset(-40);
        make.width.equalTo([NSNumber numberWithFloat:screenW/2-80]);
    }];
    
    return btmView;
}

- (void)previousBtnClick {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)initializationViewData {
    NSDictionary *dict = [self getDataFromSaveFileWithPath:@"jieSuanAccount.archive"];
    if (!dict || dict.count == 0) return;
    NSString *periodStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"store_workingtime"];
    NSArray *hourAry = [periodStr componentsSeparatedByString:@"-"];
    _hourCell.timeBtnOne = [hourAry objectAtIndex:0];
    _hourCell.timeBtnTwo = [hourAry objectAtIndex:1];
    _accountBankView.accountBankCell.selectBtn.titleLabel.text = [dict objectForKey:@"settlement_name"];
    if (_shangHuType == 3) {
       _jieSuanRenView.jieSuanRenIDCell.textField.text = [dict objectForKey:@"settlement_idcar"];
       _jieSuanRenView.iDCardDateCell.textField.text = [dict objectForKey:@"settlement_idcar_time"];
       _jieSuanRenView.accountNameCell.textField.text = [dict objectForKey:@"settlement_bank_account_name"];
       _jieSuanRenView.accountNumCell.textField.text = [dict objectForKey:@"settlement_bank_account_number"];
        _jieSuanRenView.phoneCell.textField.text = [dict objectForKey:@"settlement_bank_account_phone"];
    }else {
       _creditalView.creditalNumCell.textField.text = [dict objectForKey:@"licence_number"];
       _creditalView.creditalDateCell.textField.text = [dict objectForKey:@"licence_time"];
       _creditalView.creditalTypeCell.selectBtn.titleLabel.text = [dict objectForKey:@"licence_type"];
       _creditalView.jisuanAccountCell.selectBtn.titleLabel.text = [dict objectForKey:@"settlement_bank_type"];
        if (_uploadType == QiYeDuiGongType || _uploadType == GeTiDuiGong) {
            
            _faRenView.farenNameCell.textField.text = [dict objectForKey:@"store_legal"];
            _faRenView.farenIDCell.textField.text = [dict objectForKey:@"legal_idcar"];
            _faRenView.iDCardDateCell.textField.text = [dict objectForKey:@"legal_idcar_time"];
            _faRenView.accountNameCell.textField.text = [dict objectForKey:@"settlement_bank_account_name"];
            _faRenView.accountNumCell.textField.text = [dict objectForKey:@"settlement_bank_account_number"];
            _faRenView.phoneCell.textField.text = [dict objectForKey:@"settlement_bank_account_phone"];
        }else if (_uploadType == QiYeDuiSiFaRen || _uploadType == GeTiDuiSiFaRen) {
            
            _jieSuanRenView.jieSuanRenIDCell.textField.text = [dict objectForKey:@"settlement_idcar"];
            _jieSuanRenView.iDCardDateCell.textField.text = [dict objectForKey:@"settlement_idcar_time"];
            _jieSuanRenView.accountNameCell.textField.text = [dict objectForKey:@"settlement_bank_account_name"];
            _jieSuanRenView.accountNumCell.textField.text = [dict objectForKey:@"settlement_bank_account_number"];
            _jieSuanRenView.phoneCell.textField.text = [dict objectForKey:@"settlement_bank_account_phone"];
            
        }else if (_uploadType == QiYeDuiSiFeiFaRen || _uploadType == GeTiDuiSiFeiFaRen) {
            
            _faRenJieSuanRenView.farenNameCell.textField.text = [dict objectForKey:@"store_legal"];
            _faRenJieSuanRenView.farenIDCell.textField.text = [dict objectForKey:@"legal_idcar"];
            _faRenJieSuanRenView.iDCardDateCell.textField.text = [dict objectForKey:@"legal_idcar_time"];
            _faRenJieSuanRenView.jieSuanRenIDCell.textField.text = [dict objectForKey:@"settlement_idcar"];
            _faRenJieSuanRenView.jieSuanRenIDCardDateCell.textField.text = [dict objectForKey:@"settlement_idcar_time"];
            _faRenJieSuanRenView.accountNameCell.textField.text = [dict objectForKey:@"settlement_bank_account_name"];
            _faRenJieSuanRenView.accountNumCell.textField.text = [dict objectForKey:@"settlement_bank_account_number"];
            _faRenJieSuanRenView.phoneCell.textField.text = [dict objectForKey:@"settlement_bank_account_phone"];
        }
    }
}

- (void)nextBtnClick {
    
    //todo...[点击next之前应该把之前保存的数据删除]
    [self removeLocalData];
    if (![self judgeNumericField]) return;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSString *period = [NSString stringWithFormat:@"%@-%@",_hourCell.timeBtnOne,_hourCell.timeBtnTwo];
    [dict setObject:period forKey:@"store_workingtime"];//营业时间
    [dict setObject:_accountBankView.accountBankCell.selectBtn.titleLabel.text forKey:@"settlement_name"];//账户开户行
    //todo...
    [dict setObject:@"" forKey:@"settlement_bank_address"];//账户支行地址
    [dict setObject:_accountBankView.accountBranchCell.selectBtn.titleLabel.text forKey:@"settlement_bank_name"];//账户开户支行
    
    if (_shangHuType == 3) {
        [dict setObject:_jieSuanRenView.jieSuanRenIDCell.textField.text forKey:@"settlement_idcar"];//结算人证件号
        [dict setObject:_jieSuanRenView.iDCardDateCell.textField.text forKey:@"settlement_idcar_time"];//身份证有效期
        [dict setObject:_jieSuanRenView.accountNameCell.textField.text forKey:@"settlement_bank_account_name"];//账户开户名
        [dict setObject:_jieSuanRenView.accountNumCell.textField.text forKey:@"settlement_bank_account_number"];//账户开户号
        NSString *phoneStr = _jieSuanRenView.phoneCell.textField.text?_jieSuanRenView.phoneCell.textField.text:@"";
        [dict setObject:phoneStr forKey:@"settlement_bank_account_phone"];//预留手机号
    }else {
        [dict setObject:_creditalView.creditalNumCell.textField.text forKey:@"licence_number"];//执照证件号码
        //todo...[格式不对]
        [dict setObject:_creditalView.creditalDateCell.textField.text forKey:@"licence_time"];//证件到期日期
        [dict setObject:_creditalView.creditalTypeCell.selectBtn.titleLabel.text forKey:@"licence_type"];//企业证件类型
        [dict setObject:_creditalView.jisuanAccountCell.selectBtn.titleLabel.text forKey:@"settlement_bank_type"];//结算账户类型
        if (_uploadType == QiYeDuiGongType || _uploadType == GeTiDuiGong) {
            [dict setObject:_faRenView.farenNameCell.textField.text forKey:@"store_legal"];//法人姓名
            [dict setObject:_faRenView.farenIDCell.textField.text forKey:@"legal_idcar"];//法人身份证号
            [dict setObject:_faRenView.iDCardDateCell.textField.text forKey:@"legal_idcar_time"];//身份证有效期
            [dict setObject:_faRenView.accountNameCell.textField.text forKey:@"settlement_bank_account_name"];//账户开户名
            [dict setObject:_faRenView.accountNumCell.textField.text forKey:@"settlement_bank_account_number"];//账户开户号
            NSString *phoneStr = _faRenView.phoneCell.textField.text?_faRenView.phoneCell.textField.text:@"";
            [dict setObject:phoneStr forKey:@"settlement_bank_account_phone"];//预留手机号
        }else if (_uploadType == QiYeDuiSiFaRen || _uploadType == GeTiDuiSiFaRen) {
            [dict setObject:_jieSuanRenView.jieSuanRenIDCell.textField.text forKey:@"settlement_idcar"];//结算人证件号
            [dict setObject:_jieSuanRenView.iDCardDateCell.textField.text forKey:@"settlement_idcar_time"];//身份证有效期
            [dict setObject:_jieSuanRenView.accountNameCell.textField.text forKey:@"settlement_bank_account_name"];//账户开户名
            [dict setObject:_jieSuanRenView.accountNumCell.textField.text forKey:@"settlement_bank_account_number"];//账户开户号
            NSString *phoneStr = _jieSuanRenView.phoneCell.textField.text?_jieSuanRenView.phoneCell.textField.text:@"";
            [dict setObject:phoneStr forKey:@"settlement_bank_account_phone"];//预留手机号
        }else if (_uploadType == QiYeDuiSiFeiFaRen || _uploadType == GeTiDuiSiFeiFaRen) {
            [dict setObject:_faRenJieSuanRenView.farenNameCell.textField.text forKey:@"store_legal"];//法人姓名
            [dict setObject:_faRenJieSuanRenView.farenIDCell.textField.text forKey:@"legal_idcar"];//法人身份证
            [dict setObject:_faRenJieSuanRenView.iDCardDateCell.textField.text forKey:@"legal_idcar_time"];//身份证有效期
            [dict setObject:_faRenJieSuanRenView.jieSuanRenIDCell.textField.text forKey:@"settlement_idcar"];//结算人证件号
            [dict setObject:_faRenJieSuanRenView.jieSuanRenIDCardDateCell.textField.text forKey:@"settlement_idcar_time"];//身份证有效期
            [dict setObject:_faRenJieSuanRenView.accountNameCell.textField.text forKey:@"settlement_bank_account_name"];//账户开户名
            [dict setObject:_faRenJieSuanRenView.accountNumCell.textField.text forKey:@"settlement_bank_account_number"];//账户开户号
            NSString *phoneStr = _faRenJieSuanRenView.phoneCell.textField.text?_faRenJieSuanRenView.phoneCell.textField.text:@"";
            [dict setObject:phoneStr forKey:@"settlement_bank_account_phone"];//预留手机号
        }
    }
    [self saveLocalDataWithDict:dict];
    
    
    PicUploadVC *vc = [[PicUploadVC alloc] init];
    if (_shangHuType == 3) {
        vc.uploadType = XiaoWeiShangHu;
    }else {
        vc.uploadType = _uploadType;
    }
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)saveLocalDataWithDict:(NSDictionary *)dict {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:@"jieSuanAccount.archive"];
    [NSKeyedArchiver archiveRootObject:dict toFile:path];
}

- (NSDictionary *)getDataFromSaveFileWithPath:(NSString *)pathStr {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:pathStr];
    NSDictionary *dict = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    return dict;
}

- (void)removeLocalData {
    NSArray *array =  NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documents = [array objectAtIndex:0];
    NSString *path = [documents stringByAppendingPathComponent:@"jieSuanAccount.archive"];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL bRet = [fileMgr fileExistsAtPath:path];
    if (bRet) {
        NSError *err;
        [fileMgr removeItemAtPath:path error:&err];
    }
}

- (BOOL)judgeNumericField {
    if ([_hourCell.timeBtnOne.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择起始营业时间" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_hourCell.timeBtnTwo.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择结束营业时间" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    
    if (_shangHuType == 3) {
        
        if (!_jieSuanRenView.jieSuanRenIDCell.textField.text ||
            [_jieSuanRenView.jieSuanRenIDCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入结算人证件号" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if (!_jieSuanRenView.iDCardDateCell.textField.text ||
            [_jieSuanRenView.iDCardDateCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入身份证有效期" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if (!_jieSuanRenView.accountNameCell.textField.text ||
            [_jieSuanRenView.accountNameCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入账户开户名" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if (!_jieSuanRenView.accountNumCell.textField.text ||
            [_jieSuanRenView.accountNumCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入账户开户号" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        
    }else {
        if (!_creditalView.creditalNumCell.textField.text ||
            [_creditalView.creditalNumCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入执照证件号码" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if (!_creditalView.creditalDateCell.textField.text ||
            [_creditalView.creditalDateCell.textField.text isEqualToString:@""]) {
            [self.view makeToast:@"请输入证件到期日期" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if ([_creditalView.creditalTypeCell.selectBtn.titleLabel.text isEqualToString:@"请选择"]) {
            [self.view makeToast:@"请选择企业证件类型" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if ([_creditalView.jisuanAccountCell.selectBtn.titleLabel.text isEqualToString:@"请选择"]) {
            [self.view makeToast:@"请选择结算账户类型" duration:1.5 position:CSToastPositionCenter];
            return NO;
        }
        if (_uploadType == QiYeDuiGongType || _uploadType == GeTiDuiGong) {
            if (!_faRenView.farenNameCell.textField.text ||
                [_faRenView.farenNameCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人姓名" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenView.farenIDCell.textField.text ||
                [_faRenView.farenIDCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人身份证号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenView.iDCardDateCell.textField.text ||
                [_faRenView.iDCardDateCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人身份证有效期" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenView.accountNameCell.textField.text ||
                [_faRenView.accountNameCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户名" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenView.accountNumCell.textField.text ||
                [_faRenView.accountNumCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenView.phoneCell.textField.text ||
                [_faRenView.phoneCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入预留手机号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
        }else if (_uploadType == QiYeDuiSiFaRen || _uploadType == GeTiDuiSiFaRen) {
            if (!_jieSuanRenView.jieSuanRenIDCell.textField.text ||
                [_jieSuanRenView.jieSuanRenIDCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入结算人证件号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_jieSuanRenView.iDCardDateCell.textField.text ||
                [_jieSuanRenView.iDCardDateCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入身份证有效期" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_jieSuanRenView.accountNameCell.textField.text ||
                [_jieSuanRenView.accountNameCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户名" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_jieSuanRenView.accountNumCell.textField.text ||
                [_jieSuanRenView.accountNumCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
        }else if (_uploadType == QiYeDuiSiFeiFaRen || _uploadType == GeTiDuiSiFeiFaRen) {
            if (!_faRenJieSuanRenView.farenNameCell.textField.text ||
                [_faRenJieSuanRenView.farenNameCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人姓名" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.farenIDCell.textField.text ||
                [_faRenJieSuanRenView.farenIDCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人身份证号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.iDCardDateCell.textField.text ||
                [_faRenJieSuanRenView.iDCardDateCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入法人身份证有效期" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.jieSuanRenIDCell.textField.text ||
                [_faRenJieSuanRenView.jieSuanRenIDCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入结算人证件号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.jieSuanRenIDCardDateCell.textField.text ||
                [_faRenJieSuanRenView.jieSuanRenIDCardDateCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入结算人身份证有效期" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.accountNameCell.textField.text ||
                [_faRenJieSuanRenView.accountNameCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户名" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
            if (!_faRenJieSuanRenView.accountNumCell.textField.text ||
                [_faRenJieSuanRenView.accountNumCell.textField.text isEqualToString:@""]) {
                [self.view makeToast:@"请输入账户开户号" duration:1.5 position:CSToastPositionCenter];
                return NO;
            }
        }
    }
    if ([_accountBankView.accountBankCell.selectBtn.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择账户开户行" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_accountBankView.selectBtnOne.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择账户开户行地址" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_accountBankView.selectBtnTwo.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择账户开户行地址" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    if ([_accountBankView.accountBranchCell.selectBtn.titleLabel.text isEqualToString:@"请选择"]) {
        [self.view makeToast:@"请选择账户开户支行" duration:1.5 position:CSToastPositionCenter];
        return NO;
    }
    return YES;
}

#pragma mark - 实现FSPickerViewContrller的delegate
- (UIView *)buttonContainerView {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    view.backgroundColor = kWhiteColor;
    
    UIButton *cancleBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, 10, 56, 33)];
    [cancleBtn setTitle:@"取  消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:kBlackColor forState:UIControlStateNormal];
    cancleBtn.layer.cornerRadius = 5.0;
    
    [cancleBtn addTarget:self action:@selector(cancleAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:cancleBtn];
    
    UIButton *sureBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 66, 10, 56, 33)];
    [sureBtn setTitle:@"确  定" forState:UIControlStateNormal];
    [sureBtn setTitleColor:kBlackColor forState:UIControlStateNormal];
    sureBtn.layer.cornerRadius = 5.0;
    [sureBtn addTarget:self action:@selector(sureAction:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:sureBtn];
    return view;
}

- (void) cancleAction:(UIButton *)button {
    [_pickViewContainerView hiddenByChangeSelectedValue:YES];
}

- (void) sureAction:(UIButton *)button {
    [_pickViewContainerView hiddenByChangeSelectedValue:YES];
    NSLog(@"%@",_pickViewContainerView.showString);
    if ([_pickViewContainerView.showString isEqualToString:@"对公结算"]) {
        
        _faRenView.hidden = NO;
        _jieSuanRenView.hidden = YES;
        _faRenJieSuanRenView.hidden = YES;
        
        [_creditalView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:creditalViewHeight]);
        }];
        _creditalView.ruZhangFormCell.hidden = YES;
        
        [_faRenView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_creditalView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:faRenViewHeight]);
        }];
        
        [_accountBankView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_faRenView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+creditalViewHeight+faRenViewHeight+accountBankViewHeight;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        _sBgView.frame = CGRectMake(0, 0, screenW, height);
        
        if (_shangHuType == 1) {
            _uploadType = QiYeDuiGongType;
        }else if (_shangHuType == 2) {
            _uploadType = GeTiDuiGong;
        }
    }else if ([_pickViewContainerView.showString isEqualToString:@"对私结算"]) {
        
        _faRenView.hidden = YES;
        _jieSuanRenView.hidden = YES;
        _faRenJieSuanRenView.hidden = YES;
        
        [_creditalView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:creditalViewHeight+53]);
        }];
        _creditalView.ruZhangFormCell.hidden = NO;
        
        [_accountBankView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_creditalView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+creditalViewHeight+accountBankViewHeight+53;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        _sBgView.frame = CGRectMake(0, 0, screenW, height);
    }else if ([_pickViewContainerView.showString isEqualToString:@"法人入账"]) {
        
        _faRenView.hidden = YES;
        _jieSuanRenView.hidden = NO;
        _faRenJieSuanRenView.hidden = YES;
        
        [_creditalView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:creditalViewHeight+53]);
        }];
        _creditalView.ruZhangFormCell.hidden = NO;
        
        [_jieSuanRenView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_creditalView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:jieSuanRenViewHeight]);
        }];
        
        [_accountBankView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_jieSuanRenView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+creditalViewHeight+jieSuanRenViewHeight+accountBankViewHeight+53;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        _sBgView.frame = CGRectMake(0, 0, screenW, height);
        
        if (_shangHuType == 1) {
            _uploadType = QiYeDuiSiFaRen;
        }else if (_shangHuType == 2) {
            _uploadType = GeTiDuiSiFaRen;
        }
        
    }else if ([_pickViewContainerView.showString isEqualToString:@"非法人入账"]) {
        
        _faRenView.hidden = YES;
        _jieSuanRenView.hidden = YES;
        _faRenJieSuanRenView.hidden = NO;
        
        [_creditalView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_hourCell.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:creditalViewHeight+53]);
        }];
        _creditalView.ruZhangFormCell.hidden = NO;
        
        [_faRenJieSuanRenView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_creditalView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:faRenJieSuanRenViewHeight]);
        }];
        
        [_accountBankView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_faRenJieSuanRenView.mas_bottom);
            make.left.right.equalTo(_sBgView);
            make.height.equalTo([NSNumber numberWithFloat:accountBankViewHeight]);
        }];
        
        CGFloat height = hourCellHeight+creditalViewHeight+faRenJieSuanRenViewHeight+accountBankViewHeight+53;
        _scrollView.contentSize = CGSizeMake(screenW, height);
        _sBgView.frame = CGRectMake(0, 0, screenW, height);
        
        if (_shangHuType == 1) {
            _uploadType = QiYeDuiSiFeiFaRen;
        }else if (_shangHuType == 2) {
            _uploadType = GeTiDuiSiFeiFaRen;
        }
    }
}

- (CGFloat)buttonContainerViewHeight {
    return 50;
}

- (CGFloat)pickViewHeight {
    return 216;
}

- (void)clickTopBlankViewAction {
    
}

- (void)didSelectWithString:(NSString *)string {
}

@end
