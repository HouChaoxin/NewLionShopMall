//
//  JiSuanAccountVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "JiSuanAccountVCModel.h"

@implementation JiSuanAccountVCModel

- (void)getBankCityWithParameter:(NSDictionary *)dict
                     success:(SuccessBlock)success
                        fail:(FailBlock)fail {
    NSString *url = [NSString stringWithFormat:@"%@",kGetBankCityUrl];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSArray *rootAry = (NSArray *)responseObject;
            if (rootAry && rootAry.count > 0) {
                success(rootAry);
            }else {
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

- (void)getBranchWithParameter:(NSDictionary *)dict
                         success:(SuccessBlock)success
                            fail:(FailBlock)fail {
    NSString *url = [NSString stringWithFormat:@"%@",kGetBranchCityUrl];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSArray *rootAry = (NSArray *)responseObject;
            if (rootAry && rootAry.count > 0) {
                success(rootAry);
            }else {
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

@end
