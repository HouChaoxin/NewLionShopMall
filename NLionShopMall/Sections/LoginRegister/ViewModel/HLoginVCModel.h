//
//  HLoginVCModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ScsBlk)(NSString *state);
typedef void(^FailBlk)(void);

@interface HLoginVCModel : NSObject

- (void)loginRequestWithParameter:(NSDictionary *)dict
                          success:(ScsBlk)success
                             fail:(FailBlk)fail;
- (void)getVertifyCodeWithParameter:(NSDictionary *)dict
                            success:(SuccessBlock)success
                               fail:(FailBlk)fail;
@end
