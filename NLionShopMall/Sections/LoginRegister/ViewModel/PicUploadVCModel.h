//
//  PicUploadVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PicUploadVCModel : NSObject

- (void)uploadImageWithAvatar:(NSString *)avatar
                    directory:(NSString *)directory
                      success:(SuccessBlock)successBlock
                      failure:(FailureBlock)failureBlock;

- (void)addBusinessStoreWithPara:(NSDictionary *)dict
                         success:(SuccessBlock)successBlock
                         failure:(FailBlock)failureBlock;

@end
