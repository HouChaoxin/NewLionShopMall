//
//  JiSuanAccountVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JiSuanAccountVCModel : NSObject

- (void)getBankCityWithParameter:(NSDictionary *)dict
                         success:(SuccessBlock)success
                            fail:(FailBlock)fail;
- (void)getBranchWithParameter:(NSDictionary *)dict
                       success:(SuccessBlock)success
                          fail:(FailBlock)fail;
@end
