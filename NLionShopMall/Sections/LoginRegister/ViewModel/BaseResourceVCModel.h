//
//  BaseResourceVCModel.h
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/21.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseResourceVCModel : NSObject

- (void)getCareerTypeWithParameter:(NSDictionary *)dict
                           success:(SuccessBlock)success
                              fail:(FailBlock)fail;
- (void)getCountryWithParameter:(NSDictionary *)dict
                        success:(SuccessBlock)success
                           fail:(FailBlock)fail;
- (void)getCityWithParameter:(NSDictionary *)dict
                     success:(SuccessBlock)success
                        fail:(FailBlock)fail;
@end
