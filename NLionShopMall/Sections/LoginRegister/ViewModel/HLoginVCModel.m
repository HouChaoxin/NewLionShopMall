//
//  HLoginVCModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "HLoginVCModel.h"
#import "LoginModel.h"

@implementation HLoginVCModel

- (void)loginRequestWithParameter:(NSDictionary *)dict
                          success:(ScsBlk)success
                             fail:(FailBlk)fail {
    NSString *loginUrlStr = [NSString stringWithFormat:@"%@%@",baseUrl,kLoginUrl];
    
    [NetHelper GET:loginUrlStr parameters:dict progressBlock:^(NSInteger downloadProgress) {

    } success:^(id responseObject) {

        NSDictionary *rootDict = (NSDictionary *)responseObject;
        
        if (rootDict && rootDict.count > 0) {
            BOOL status = [rootDict[@"status"] boolValue];
            if (status) {
                
                NSDictionary *data = rootDict[@"data"];
                LoginModel *model = [[LoginModel alloc] initWithdict:data];
                if ([model.store_state isEqualToString:@"1"]) {
                    [[NSUserDefaults standardUserDefaults] setObject:model.mobile forKey:kMemberPhone];
                    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@%@",imgBaseUrl,model.avatar] forKey:kMemberHeadImg];
                    [[NSUserDefaults standardUserDefaults] setObject:model.token forKey:kMemberToken];
                    [[NSUserDefaults standardUserDefaults] setObject:model.sid forKey:kMainStoreId];
                    [[NSUserDefaults standardUserDefaults] setObject:model.sname forKey:kStoreName];
                    [[NSUserDefaults standardUserDefaults] setObject:model.cash_amount forKey:kGuLiJinNum];
                    [[NSUserDefaults standardUserDefaults] setObject:model.uid forKey:kMemberId];
                    [[NSUserDefaults standardUserDefaults] setObject:model.username forKey:kUserName];
                }else if ([model.store_state isEqualToString:@"5"]){
                    [[NSUserDefaults standardUserDefaults] setObject:model.uid forKey:kMemberId];
                }
                success(model.store_state);
            }else{
                fail();
            }
        }else{
            fail();
        }
    } failure:^(NSError *error) {
        NSLog(@"***loginfail***");
        fail();
    }];
}

- (void)getVertifyCodeWithParameter:(NSDictionary *)dict
                            success:(SuccessBlock)success
                               fail:(FailBlk)fail {
    NSString *url = [NSString stringWithFormat:@"%@%@",baseUrl,kLoginVertifyCode];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                NSString *msg = rootDict[@"msg"];
                success(msg);
            }else{
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

@end
