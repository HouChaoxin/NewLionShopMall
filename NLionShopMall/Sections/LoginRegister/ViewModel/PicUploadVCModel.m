//
//  PicUploadVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/22.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "PicUploadVCModel.h"

@implementation PicUploadVCModel

- (void)uploadImageWithAvatar:(NSString *)avatar
                    directory:(NSString *)directory
                      success:(SuccessBlock)successBlock
                      failure:(FailureBlock)failureBlock {
    
    NSString *urlStr = @"http://img.lion-mall.com/upimgs/img-up-iphone";
    NSDictionary *param = [NSMutableDictionary dictionaryWithCapacity:0];
    [param setValue:avatar forKey:@"goods_imgs"];
    [param setValue:@"abc" forKey:@"dirimg"];
    [NetHelper POST:urlStr parameters:param progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        if (responseObject) {
            
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                successBlock(rootDict);
            }else {
                successBlock(nil);
            }
        }else {
            successBlock(nil);
        }
    } failure:^(NSError *error) {
        failureBlock(error);
    }];
}

- (void)addBusinessStoreWithPara:(NSDictionary *)dict
                     success:(SuccessBlock)successBlock
                     failure:(FailBlock)failureBlock {
    NSString *url = [NSString stringWithFormat:@"%@%@",baseUrl,kAddBusinessUrl];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSDictionary *rootDict = (NSDictionary *)responseObject;
            if (rootDict && rootDict.count > 0) {
                
                NSString *msg = rootDict[@"msg"];
                successBlock(msg);
            }else{
                failureBlock();
            }
        }else {
            failureBlock();
        }
    } failure:^(NSError *error) {
        failureBlock();
    }];
}

@end
