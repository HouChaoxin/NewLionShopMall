//
//  BaseResourceVCModel.m
//  NLionShopMall
//
//  Created by hcx_ios on 2018/5/21.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "BaseResourceVCModel.h"

@implementation BaseResourceVCModel

- (void)getCareerTypeWithParameter:(NSDictionary *)dict
                            success:(SuccessBlock)success
                               fail:(FailBlock)fail {
    NSString *url = [NSString stringWithFormat:@"%@%@",baseUrl,kGetCareerUrl];
    [NetHelper GET:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSArray *rootAry = (NSArray *)responseObject;
            if (rootAry && rootAry.count > 0) {
                success(rootAry);
            }else {
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

- (void)getCityWithParameter:(NSDictionary *)dict
                              success:(SuccessBlock)success
                                 fail:(FailBlock)fail {
    NSString *url = [NSString stringWithFormat:@"%@",kGetCityUrl];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSArray *rootAry = (NSArray *)responseObject;
            if (rootAry && rootAry.count > 0) {
                success(rootAry);
            }else {
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

- (void)getCountryWithParameter:(NSDictionary *)dict
                     success:(SuccessBlock)success
                        fail:(FailBlock)fail {
    NSString *url = [NSString stringWithFormat:@"%@",kGetCityUrl];
    [NetHelper POST:url parameters:dict progressBlock:^(NSInteger downloadProgress) {
        
    } success:^(id responseObject) {
        
        NSLog(@"===========%@",responseObject);
        if (responseObject) {
            
            NSArray *rootAry = (NSArray *)responseObject;
            if (rootAry && rootAry.count > 0) {
                success(rootAry);
            }else{
                fail();
            }
        }else {
            fail();
        }
    } failure:^(NSError *error) {
        fail();
    }];
}

@end
