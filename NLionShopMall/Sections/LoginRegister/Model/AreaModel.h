//
//  AreaModel.h
//  FSPickViewContainerViewDemo
//
//  Created by zhuliuquan on 16/1/13.
//  Copyright © 2016年 FORMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AreaModel : NSObject

@property (nonatomic, strong) NSMutableArray *provinceArray;

- (void) analysisWithDictionary: (NSDictionary *)dic;

@end


@interface CityModel : NSObject

@property (nonatomic, strong) NSString *province;
@property (nonatomic, strong) NSMutableArray *cityArray;

@end


@interface CountyModel : NSObject

@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSMutableArray *countyArray;

@end
