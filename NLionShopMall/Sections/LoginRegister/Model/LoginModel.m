//
//  LoginModel.m
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import "LoginModel.h"

@implementation LoginModel

-(instancetype)initWithdict:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.avatar = [self isNil:dict[@"avatar"]];
        self.cash_amount = [self isNil:dict[@"cash_amount"]];
        self.hxpw = [self isNil:dict[@"hxpw"]];
        self.login_type = [self isNil:dict[@"login_type"]];
        self.mobile = [self isNil:dict[@"mobile"]];
        self.point_ison = [self isNil:dict[@"point_ison"]];
        self.sid = [self isNil:dict[@"sid"]];
        self.sname = [self isNil:dict[@"sname"]];
        self.token = [self isNil:dict[@"token"]];
        self.uid = [self isNil:dict[@"uid"]];
        self.username = [self isNil:dict[@"username"]];
        self.store_state = [self isNil:dict[@"store_state"]];
    }
    return self;
}

@end
