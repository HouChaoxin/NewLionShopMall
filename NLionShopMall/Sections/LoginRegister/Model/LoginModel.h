//
//  LoginModel.h
//  LionShopMall
//
//  Created by hcx_ios on 2018/4/26.
//  Copyright © 2018年 LionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoginModel : BaseObject

@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *cash_amount;
@property (nonatomic,copy) NSString *hxpw;
@property (nonatomic,copy) NSString *login_type;
@property (nonatomic,copy) NSString *mobile;
@property (nonatomic,copy) NSString *point_ison;
@property (nonatomic,copy) NSString *sid;
@property (nonatomic,copy) NSString *sname;
@property (nonatomic,copy) NSString *token;
@property (nonatomic,copy) NSString *uid;
@property (nonatomic,copy) NSString *username;
@property (nonatomic,copy) NSString *store_state;

-(instancetype)initWithdict:(NSDictionary *)dict;
@end
