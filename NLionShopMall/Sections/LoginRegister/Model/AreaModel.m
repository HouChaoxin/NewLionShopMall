//
//  AreaModel.m
//  FSPickViewContainerViewDemo
//
//  Created by zhuliuquan on 16/1/13.
//  Copyright © 2016年 FORMS. All rights reserved.
//

#import "AreaModel.h"

@implementation AreaModel

- (void) analysisWithDictionary:(NSDictionary *)dic{
   
    self.provinceArray = [[NSMutableArray alloc] init];
    
    [dic enumerateKeysAndObjectsUsingBlock:^(NSString *key1, NSDictionary *obj1, BOOL *stop1) {
        
        [(NSDictionary *)obj1 enumerateKeysAndObjectsUsingBlock:^(NSString *key2, NSDictionary *obj2, BOOL *stop2) {
            
            CityModel *cityModel = [[CityModel alloc] init];
            cityModel.province = key2;

            [(NSDictionary *)obj2 enumerateKeysAndObjectsUsingBlock:^(NSString * key3, NSDictionary *obj3, BOOL *stop3) {
                
                [(NSDictionary *)obj3 enumerateKeysAndObjectsUsingBlock:^(NSString *key4, NSArray *obj4, BOOL *stop4) {
                    CountyModel *countyModel = [[CountyModel alloc] init];
                    countyModel.city = key4;
                    countyModel.countyArray = [NSMutableArray arrayWithArray:obj4];
                    
                    [cityModel.cityArray addObject:countyModel];
                    
//                    if ([@"ENOUGH" isEqualToString:key4] ) {
//                        *stop4 = YES;
//                    }
                }];
                
//                if ([@"ENOUGH" isEqualToString:key3] ) {
//                    *stop3 = YES;
//                }
            }];
            
            [self.provinceArray addObject:cityModel];
            
//            if ([@"ENOUGH" isEqualToString:key2] ) {
//                *stop2 = YES;
//            }
        }];
        
//        if ([@"ENOUGH" isEqualToString:key1] ) {
//            *stop1 = YES;
//        }
    }];
}

@end



@implementation CityModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.cityArray = [[NSMutableArray alloc] init];
    }
    return self;
}

@end


@implementation CountyModel

@end