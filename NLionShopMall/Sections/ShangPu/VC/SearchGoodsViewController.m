//
//  SearchGoodsViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/21.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "SearchGoodsViewController.h"
#import "AddGoodsViewController.h"
#import "ShangPuCell.h"
#import "LoginOtherDeviceView.h"

@interface SearchGoodsViewController ()<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,LoginOtherDeviceDelegate>

@property (nonatomic ,strong) NSMutableArray *dataSource;
@property (nonatomic ,assign) NSInteger pageIndex;
@property (nonatomic ,assign) BOOL isBottom;

@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *searchBtn;
@property (nonatomic ,strong) UITableView *tableView;
@property (nonatomic ,strong) LoginOtherDeviceView *actionView;

@end

@implementation SearchGoodsViewController

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

-(LoginOtherDeviceView *)actionView{
    if (!_actionView) {
        _actionView = [[LoginOtherDeviceView alloc] init];
        _actionView.delegate = self;
    }
    return _actionView;
}

-(UITableView *)tableView{
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[ShangPuCell class] forCellReuseIdentifier:NSStringFromClass([ShangPuCell class])];
        _tableView.tableFooterView = [UIView new];
        _tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    }
    return _tableView;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self creatTableView];
    [self creatSearchView];
    [self MJrefreshDate];
    [self MJrefreshFooterDate];
}

- (void)creatTableView{

    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.top.equalTo(@64);
    }];
}

- (void)creatSearchView{
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenW, 64)];
    bgView.backgroundColor = [UIColor getColor:@"#f3f3f3"];
    [self.view addSubview:bgView];
    
    UIView *searchView = [[UIView alloc] init];
    searchView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:searchView];
    
    UIImageView *searchImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchicon"]];
    [bgView addSubview:searchImg];
    
    self.searchTF = [[UITextField alloc] init];
    self.searchTF.placeholder  = @"请输入关键字";
    self.searchTF.delegate = self;
    self.searchTF.returnKeyType = UIReturnKeySearch;
    [self.searchTF becomeFirstResponder];
    [searchView addSubview:self.searchTF];
    
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchBtn setTitle:@"取消" forState:0];
    [self.searchBtn setTitleColor:mainColor forState:0];
    [self.searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.searchBtn];
    
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(20);
        make.right.equalTo(bgView);
        make.bottom.equalTo(bgView);
        make.width.equalTo(@(60));
    }];
    
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(20);
        make.left.equalTo(bgView).offset(10);
        make.right.equalTo(self.searchBtn.mas_left);
        make.bottom.equalTo(bgView).offset(-8);
    }];
    searchView.layer.cornerRadius = 5.0;
    searchView.layer.masksToBounds = YES;
    
    [searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchView);
        make.left.equalTo(searchView).offset(5);
        make.width.equalTo(searchImg.mas_height);
    }];
    
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(searchView);
        make.bottom.equalTo(searchView);
        make.left.equalTo(searchImg.mas_right);
        make.right.equalTo(searchView);
    }];
}

- (void)searchBtnClick:(UIButton *)button{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self searchData:textField.text];
    return YES;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShangPuCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShangPuCell class]) forIndexPath:indexPath];
    
    GoodsModel *model = self.dataSource[indexPath.row];
    NSInteger goods_verify = [model.goods_verify integerValue];
    NSInteger is_on_sale = [model.is_on_sale integerValue];
    NSInteger is_delete = [model.is_delete integerValue];
    if (goods_verify == 1 && is_on_sale == 1 && is_delete == 0) {
        
        cell.leftBtn.hidden = NO;
        [cell.leftBtn setTitle:@"下架" forState:0];
        cell.centerBtn.hidden = YES;
        cell.rightBtn.hidden = NO;
        [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
        
    }else if (goods_verify == 0 && is_delete == 0){
       
        cell.leftBtn.hidden = YES;
        cell.centerBtn.hidden = YES;
        cell.rightBtn.hidden = YES;
        [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
        
    }else if (goods_verify == 2 && is_delete == 0){
       
        cell.leftBtn.hidden = NO;
        [cell.leftBtn setTitle:@"编辑" forState:0];
        cell.centerBtn.hidden = YES;
        cell.rightBtn.hidden = NO;
        [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@20);
        }];
        
    }else if (goods_verify == 1 && is_on_sale == 0 && is_delete == 0){
       
        cell.leftBtn.hidden = NO;
        [cell.leftBtn setTitle:@"上架" forState:0];
        cell.centerBtn.hidden = NO;
        cell.rightBtn.hidden = NO;
        [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
    
    
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell.leftBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.centerBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.rightBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    GoodsModel *model = self.dataSource[indexPath.row];
    NSInteger goods_verify = [model.goods_verify integerValue];
    NSInteger is_on_sale = [model.is_on_sale integerValue];
    NSInteger is_delete = [model.is_delete integerValue];
    if (goods_verify == 1 && is_on_sale == 1 && is_delete == 0) {
        return 230;
    }else if (goods_verify == 0 && is_delete == 0){
        return 200;
    }else if (goods_verify == 2 && is_delete == 0){
        return 250;
    }else if (goods_verify == 1 && is_on_sale == 0 && is_delete == 0){
        return 230;
    }else{
        return 0; //额外的分类
    }
}


- (void)actionClick:(UIButton *)button{
    if ([button.titleLabel.text isEqualToString:@"下架"]) {
        [self.actionView showTitle:@"提示" andSubTitle:@"下架商品，可在已下架列表中重新上架" andLeft:@"放弃" andRight:@"继续下架"];
    }else if ([button.titleLabel.text isEqualToString:@"删除"]){
        [self.actionView showTitle:@"提示" andSubTitle:@"删除商品将不可找回" andLeft:@"取消" andRight:@"继续删除"];
    }else if ([button.titleLabel.text isEqualToString:@"编辑"]){
        AddGoodsViewController *vc = [[AddGoodsViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([button.titleLabel.text isEqualToString:@"上架"]){
        [self.actionView showTitle:@"提示" andSubTitle:@"上架商品审核通过后可在线查看" andLeft:@"取消" andRight:@"确认上架"];
    }
}


- (void)didRightClick:(UIButton *)button{
    
    
}

- (void)didLeftClick{
    
    [self.actionView dismiss];
}


- (void)searchData:(NSString *)searchStr{
    
    [self.view endEditing:YES];
    NSString *store_id = @"15249077146730960000";
    NSString *url = [baseUrl stringByAppendingString:@"business/goods-search"];
    NSDictionary *body = @{@"store_id":store_id,
                           @"goods_title":searchStr,
                           @"page":@"0",
                           @"pagesize":@"20",};
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        if (rootDict && rootDict.count > 0) {
            BOOL success = [rootDict[@"status"] boolValue];
            if (success) {
                NSArray *tempArr = rootDict[@"data"][@"goodsList"];
                if (self.pageIndex == 0) {
                    [self.dataSource removeAllObjects];
                }
                if (tempArr.count < 20) {
                    self.isBottom = YES;
                }
                self.dataSource = [GoodsModel mj_objectArrayWithKeyValuesArray:tempArr];
                [self.tableView reloadData];
            }
            else {
                [self.view makeToast:rootDict[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark ---下拉刷新---
-(void)MJrefreshDate{
    
    __unsafe_unretained UITableView *tableView = self.tableView;
    __weak typeof(self)MySelf = self;
    MJRefreshNormalHeader *mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:MySelf refreshingAction:@selector(loadNewData)];
    [mj_header setTitle:@"下拉可以刷新" forState:MJRefreshStateIdle];
    [mj_header setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
    [mj_header setTitle:@"品兑正在加载" forState:MJRefreshStateRefreshing];
    tableView.mj_header = mj_header;
//    [tableView.mj_header beginRefreshing];
}
-(void)loadNewData{
    self.pageIndex = 0;
    self.isBottom = NO;
    [self searchData:self.searchTF.text];
}

#pragma mark ----上拉加载----
-(void)MJrefreshFooterDate{
    __unsafe_unretained UITableView *tableView = self.tableView;
    __weak typeof(self)MySelf = self;
    MJRefreshBackNormalFooter *mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:MySelf refreshingAction:@selector(footRefesh)];
    [mj_footer setTitle:@"上拉加载更多" forState:MJRefreshStateIdle];
    [mj_footer setTitle:@"品兑正在加载" forState:MJRefreshStatePulling];
    [mj_footer setTitle:@"我也是有底线的" forState:MJRefreshStateNoMoreData];
    tableView.mj_footer = mj_footer;
}
-(void)footRefesh{
    
    if (!self.isBottom) {
        self.pageIndex ++ ;
        [self searchData:self.searchTF.text];
    }else{
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

@end
