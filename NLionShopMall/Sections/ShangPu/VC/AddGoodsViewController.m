//
//  AddGoodsViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "AddGoodsViewController.h"
#import "TZImagePickerController.h"
#import <CommonCrypto/CommonDigest.h>
#import <Photos/Photos.h>
#import "CollectionViewCell.h"
#import "AddGoodsView.h"
#import "LoginOtherDeviceView.h"

@interface AddGoodsViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,TZImagePickerControllerDelegate,LoginOtherDeviceDelegate>
{
    CGFloat  _collectView_H;
    UIImageView  *_goodsImg;
    UIButton    *_addImgBtn;
    UIButton *_deleteImgBtn;
    UIButton      *_leftBtn;
    UIButton     *_rightBtn;
    
}
@property (nonatomic ,strong) NSArray *titleArr;
@property (nonatomic ,strong) UILabel *imageTitleLab;
@property (nonatomic ,strong) UICollectionView *collectView;
@property (nonatomic ,strong) LoginOtherDeviceView *actionView;

@property (nonatomic ,strong) NSMutableArray *photosArray;
@property (nonatomic ,strong) NSMutableArray *assestArray;
@property BOOL isSelectOriginalPhoto;

@end

@implementation AddGoodsViewController

-(LoginOtherDeviceView *)actionView{
    if (!_actionView) {
        _actionView = [[LoginOtherDeviceView alloc] init];
        _actionView.delegate = self;
    }
    return _actionView;
}

- (NSMutableArray *)photosArray{
    if (!_photosArray) {
        _photosArray = [NSMutableArray array];
    }
    return _photosArray;
}

- (NSMutableArray *)assestArray{
    if (!_assestArray) {
        self.assestArray = [NSMutableArray array];
    }
    return _assestArray;
}

-(UICollectionView *)collectView{
    if (!_collectView) {
        UICollectionViewFlowLayout *flowLayOut = [[UICollectionViewFlowLayout alloc] init];
        flowLayOut.itemSize = CGSizeMake((screenW - 55)/ 4, (screenW - 55)/ 4);
        flowLayOut.sectionInset = UIEdgeInsetsMake(10, 20, 10, 20);
        flowLayOut.minimumLineSpacing = 5;
        flowLayOut.minimumInteritemSpacing = 5;
        _collectView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayOut];
        _collectView.backgroundColor = [UIColor whiteColor];
        _collectView.delegate = self;
        _collectView.dataSource = self;
        [_collectView registerClass:[CollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        _collectView.scrollEnabled = NO;
    }
    return _collectView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"添加商品";
    
    _collectView_H = (screenW - 55)/4 + 20;
    [self setSubView_layout];
}

- (void)setSubView_layout{
    
    self.titleArr = @[@"商品名称:",@"商品价格:",@"商品单位:",@"商品库存:"];
    NSArray *placeholderArr = @[@"请输入商品名称(最多15字)",@"请输入单品价格",@"最多两个字(例如/斤/件/份/套)",@"非必填项"];
    
    for (int i=0; i<self.titleArr.count; i++) {
        AddGoodsView *goodsView = [[AddGoodsView alloc] initWithFrame:CGRectZero setTitle:self.titleArr[i]];
        goodsView.backgroundColor = [UIColor whiteColor];
        goodsView.textTF.tag = 1000+i;
        goodsView.textTF.placeholder = placeholderArr[i];
        [self.view addSubview:goodsView];

        [goodsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.view).offset(50*i);
            make.left.equalTo(self.view);
            make.right.equalTo(self.view);
            make.height.equalTo(@50);
        }];

        if (i == 1) {
            UILabel *rateLabel = [[UILabel alloc] initWithFrame:CGRectMake(screenW - 65, 50, 50, 50)];
            rateLabel.text = @"元";
            [self.view addSubview:rateLabel];
        }else if (i == 3){
            self.imageTitleLab = [[UILabel alloc] init];
            self.imageTitleLab.text = @"商品图片:";
            [self.view addSubview:self.imageTitleLab];

            UILabel *pointLabel = [[UILabel alloc] init];
            pointLabel.textColor = [UIColor redColor];
            pointLabel.text = @"*";
            [self.view addSubview:pointLabel];
            
            [pointLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(goodsView.mas_bottom).offset(15);
                make.left.equalTo(self.view).offset(15);
                make.width.equalTo(@8);
            }];
            
            [self.imageTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(goodsView.mas_bottom).offset(15);
                make.left.equalTo(pointLabel.mas_right);
            }];
        }
    }

    _addImgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_addImgBtn setImage:[UIImage imageNamed:@"new_Image"] forState:0];
    [_addImgBtn addTarget:self action:@selector(addPhotos:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_addImgBtn];
    
    _goodsImg = [[UIImageView alloc] init];
    _goodsImg.image = [UIImage imageNamed:@"goods_demo"];
    _goodsImg.userInteractionEnabled = YES;
    [self.view addSubview:_goodsImg];

    _deleteImgBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_deleteImgBtn setImage:[UIImage imageNamed:@"photo_delete"] forState:0];
    [_deleteImgBtn addTarget:self action:@selector(deletePhotos:) forControlEvents:UIControlEventTouchUpInside];
    [_goodsImg addSubview:_deleteImgBtn];
    
    [_goodsImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.top.equalTo(self.imageTitleLab.mas_bottom).offset(10);
        make.height.equalTo(@160);
    }];
    _goodsImg.layer.shadowColor = [UIColor blackColor].CGColor;//阴影颜色
    _goodsImg.layer.shadowOffset = CGSizeMake(0, 2);//偏移距离
    _goodsImg.layer.shadowOpacity = 0.5;//不透明度
    _goodsImg.layer.shadowRadius = 5.0;//半径
    
    
    [_addImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(_goodsImg);
        make.width.equalTo(@100);
        make.height.equalTo(@100);
    }];
    
    [_deleteImgBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_goodsImg);
        make.right.equalTo(_goodsImg).offset(-5);
        make.width.equalTo(@20);
        make.height.equalTo(@20);
    }];
    
    _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_leftBtn setTitle:@"取消" forState:0];
    [_leftBtn setTitleColor:[UIColor getColor:@"333333"] forState:0];
    [_leftBtn addTarget:self action:@selector(cancelClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_leftBtn];
    
    _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_rightBtn setTitle:@"提交审核" forState:0];
    _rightBtn.backgroundColor = mainColor;
    [_rightBtn addTarget:self action:@selector(checkClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_rightBtn];
    
    [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_goodsImg.mas_bottom).offset(30);
        make.left.equalTo(self.view).offset((screenW-200)/4);
        make.width.equalTo(@100);
        make.height.equalTo(@40);
    }];
    _leftBtn.layer.cornerRadius = 5.0f;
    _leftBtn.layer.borderWidth = 1.0f;
    _leftBtn.layer.borderColor = mainColor.CGColor;
    _leftBtn.layer.masksToBounds = YES;
    
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_goodsImg.mas_bottom).offset(30);
        make.right.equalTo(self.view).offset(-(screenW-200)/4);
        make.width.equalTo(@100);
        make.height.equalTo(@40);
    }];
    _rightBtn.layer.cornerRadius = 5.0f;
    _rightBtn.layer.masksToBounds = YES;
}


#pragma mark -- 多选图片代理方法
- (void)checkLocalPhoto{
    
    TZImagePickerController *imagePicker = [[TZImagePickerController alloc] initWithMaxImagesCount:1 delegate:self];
    [imagePicker setSortAscendingByModificationDate:NO];
//    imagePicker.isSelectOriginalPhoto = _isSelectOriginalPhoto;
//    imagePicker.selectedAssets = _assestArray;
    imagePicker.allowPickingVideo = NO;
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(TZImagePickerController *)picker didFinishPickingPhotos:(NSArray<UIImage *> *)photos sourceAssets:(NSArray *)assets isSelectOriginalPhoto:(BOOL)isSelectOriginalPhoto{
    self.photosArray = [NSMutableArray arrayWithArray:photos];
    self.assestArray = [NSMutableArray arrayWithArray:assets];
    _isSelectOriginalPhoto = isSelectOriginalPhoto;
    _goodsImg.image = self.photosArray.firstObject;
    _deleteImgBtn.hidden = NO;
    _goodsImg.hidden = NO;
}

- (void)addPhotos:(UIButton *)sender{
    
    [self checkLocalPhoto];
}

- (void)deletePhotos:(UIButton *)sender{
    
    _deleteImgBtn.hidden = YES;
    _goodsImg.hidden = YES;
}

-(void)cancelClick:(UIButton *)button{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)checkClick:(UIButton *)button{
    
    if (!self.photosArray.count) {
        [self.view makeToast:@"请上传商品图片" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    [self upDataImage:self.photosArray[0]];
}

- (void)didRightClick:(UIButton *)button{
    
    
}
- (void)didLeftClick{
    
    [self.actionView dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -- 上传商品
- (void)upDataGoods:(NSString *)imagePath{
    
    NSString *userId = @"3136";
    NSString *username = @"asdd";
    NSString *storeName = @"门店名称";
    NSString *store_id = @"15249077146730960000";
    UITextField *nameTF = (UITextField *)[self.view viewWithTag:1000];
    UITextField *priceTF = (UITextField *)[self.view viewWithTag:1001];
    UITextField *unitTF = (UITextField *)[self.view viewWithTag:1002];
    UITextField *numberTF = (UITextField *)[self.view viewWithTag:1003];
    NSString *goodsName = nameTF.text;
    NSString *price = priceTF.text;
    NSString *unit = unitTF.text;
    NSString *countStr = numberTF.text;
    if (!goodsName.length || !price.length || !unit.length || !countStr.length || !self.photosArray.count) {
        [self.view makeToast:@"请完整商品信息" duration:1.5 position:CSToastPositionCenter];
        return;
    }
    
    NSString *url = [baseUrl stringByAppendingString:@"business/add-goods"];
    NSDictionary *body = @{@"user_id":userId,
                           @"user_name":username,
                           @"store_id":store_id,
                           @"store_name":storeName,
                           @"goods_title":goodsName,
                           @"goods_price":price,
                           @"unit":unit,
                           @"goods_num":countStr,
                           @"goods_image":imagePath,};
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
       
        [self.actionView showTitle:@"提示" andSubTitle:@"上架商品已提交审核，是否继续添加" andLeft:@"返回商铺" andRight:@"继续添加"];
        
//        NSDictionary *rootDict = (NSDictionary *)responseObject;
//        if (rootDict && rootDict.count > 0) {
//            BOOL success = [rootDict[@"status"] boolValue];
//            if (success) {
//
//            }
//            else {
//                [self.view makeToast:rootDict[@"msg"] duration:1.5 position:CSToastPositionCenter];
//            }
//        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

#pragma mark -- 上传图片
- (void)upDataImage:(UIImage *)image{
    
    NSString *url = [imgBaseUrl stringByAppendingString:@"upimgs/goods-up-iphone"];
    NSData *data = UIImageJPEGRepresentation(image, 1.0f);
    NSString *imageStr = [data base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSDictionary *body = @{@"goods_imgs":imageStr};
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        if (rootDict && rootDict.count > 0) {
            BOOL success = [rootDict[@"status"] boolValue];
            if (success) {
                [self upDataGoods:rootDict[@"data"][@"file"]];
            }
            else {
                [self.view makeToast:rootDict[@"msg"] duration:1.5 position:CSToastPositionCenter];
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

@end
