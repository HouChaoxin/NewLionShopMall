//
//  ShangPuCell.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ShangPuCell.h"

@implementation ShangPuCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.icon = [[UIImageView alloc] init];
        self.icon.image = [UIImage imageNamed:@"Image_demo"];
        [self.contentView addSubview:self.icon];
        
        self.bottomView = [[UIView alloc] init];
        self.bottomView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        [self.contentView addSubview:self.bottomView];
        
        self.nameLab = [[UILabel alloc] init];
        self.nameLab.text = @"萝卜白菜豆腐";
        self.nameLab.font = [UIFont systemFontOfSize:15.0f];
        [self.bottomView addSubview:self.nameLab];
        
        self.priceLab = [[UILabel alloc] init];
        self.priceLab.text = @"￥15元/份";
        [self.contentView addSubview:self.priceLab];
        
        self.countLab = [[UILabel alloc] init];
        self.countLab.text = @"100份";
        [self.contentView addSubview:self.countLab];
        
        self.reasonLab = [[UILabel alloc] init];
        self.reasonLab.text = @"图片过于暴露";
        self.reasonLab.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.reasonLab];
        
        self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.leftBtn setTitle:@"下架" forState:0];
        [self.leftBtn setTitleColor:[UIColor getColor:@"333333"] forState:0];
        [self.contentView addSubview:self.leftBtn];
        
        self.centerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.centerBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.centerBtn setTitle:@"编辑" forState:0];
        self.centerBtn.backgroundColor = [UIColor blueColor];
        [self.contentView addSubview:self.centerBtn];
        
        self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rightBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.rightBtn setTitle:@"删除" forState:0];
        [self.rightBtn setTitleColor:[UIColor whiteColor] forState:0];
        self.rightBtn.backgroundColor = mainColor;
        [self.contentView addSubview:self.rightBtn];
        
        self.statusLab = [[UILabel alloc] init];
        self.statusLab.text = @"";
        [self.icon addSubview:self.statusLab];
        
        [self setSubView_layout];
        
    }
    return self;
}

- (void)setSubView_layout{
    
    [self.icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.height.equalTo(@150);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.icon);
        make.right.equalTo(self.icon);
        make.bottom.equalTo(self.icon);
        make.height.equalTo(@40);
    }];
    
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bottomView).offset(10);
        make.centerY.equalTo(self.bottomView);
    }];
    
    [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.icon.mas_bottom).offset(5);
    }];
    
    [self.reasonLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.priceLab.mas_bottom).offset(5);
        make.height.equalTo(@20);
    }];
    
    [self.countLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.top.equalTo(self.icon.mas_bottom).offset(5);
    }];
    
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.reasonLab.mas_bottom).offset(5);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
    self.rightBtn.layer.cornerRadius = 5.0f;
    self.rightBtn.layer.borderWidth = 1.0f;
    self.rightBtn.layer.borderColor = mainColor.CGColor;
    self.rightBtn.layer.masksToBounds = YES;
    
    
    [self.centerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.reasonLab.mas_bottom).offset(5);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-10);
        make.top.equalTo(self.reasonLab.mas_bottom).offset(5);
        make.width.equalTo(@100);
        make.height.equalTo(@30);
    }];
    self.rightBtn.layer.cornerRadius = 5.0f;
    self.rightBtn.layer.masksToBounds = YES;
}

-(void)setModel:(GoodsModel *)model{
    if (model) {
        [self.icon sd_setImageWithURL:[NSURL URLWithString:model.goods_image] placeholderImage:[UIImage imageNamed:@"Image_demo"]];
        self.nameLab.text = model.goods_title;
        self.priceLab.text = [NSString stringWithFormat:@"%@/%@",model.goods_price,model.unit];
        self.countLab.text = [NSString stringWithFormat:@"%@%@",model.goods_num,model.unit];
    }
}




- (void)setFrame:(CGRect)frame {
    CGSize size = frame.size;
    size.height -= 10;
    frame.size = size;
    [super setFrame:frame];
}

@end
