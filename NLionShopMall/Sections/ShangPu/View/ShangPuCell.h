//
//  ShangPuCell.h
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GoodsModel.h"

@interface ShangPuCell : UITableViewCell

@property (nonatomic ,strong) UIImageView *icon;
@property (nonatomic ,strong) UIView *bottomView;
@property (nonatomic ,strong) UILabel *nameLab;
@property (nonatomic ,strong) UILabel *priceLab;
@property (nonatomic ,strong) UILabel *countLab;
@property (nonatomic ,strong) UILabel *reasonLab;
@property (nonatomic ,strong) UIButton *leftBtn;
@property (nonatomic ,strong) UIButton *centerBtn;
@property (nonatomic ,strong) UIButton *rightBtn;

@property (nonatomic ,strong) UILabel *statusLab;

@property (nonatomic ,strong) GoodsModel *model;
@end
