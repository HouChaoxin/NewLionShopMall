//
//  LoginOtherDeviceView.m
//  LionMall
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import "LoginOtherDeviceView.h"

@interface LoginOtherDeviceView ()
{
    UIView *_bgView;
    UILabel *_titleLab;
    UILabel *_subTitleLab;
    UIButton *_leftBtn;
    UIButton *_rightBtn;
    UILabel *_herLink;
    UILabel *_verLink;
}
@end

@implementation LoginOtherDeviceView

- (instancetype)initWithFrame:(CGRect)frame
{
    frame = [UIScreen mainScreen].bounds;
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        
        _bgView= [[UIView alloc] init];
        _bgView.backgroundColor = [UIColor whiteColor];
        [self addSubview:_bgView];
        
        _titleLab = [[UILabel alloc] init];
        _titleLab.text = @"下线通知";
        _titleLab.font = [UIFont boldSystemFontOfSize:16];
        [_bgView addSubview:_titleLab];
        
        _subTitleLab = [[UILabel alloc] init];
        _subTitleLab.text = @"你的账号已在另外一个设备上登录";
        _subTitleLab.font = [UIFont systemFontOfSize:14];
        [_bgView addSubview:_subTitleLab];
        
        _herLink = [[UILabel alloc] init];
        _herLink.backgroundColor = [UIColor getColor:@"e6e6e6"];
        [_bgView addSubview:_herLink];
        
        _verLink = [[UILabel alloc] init];
        _verLink.backgroundColor = [UIColor getColor:@"e6e6e6"];
        [_bgView addSubview:_verLink];
        
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftBtn setTitle:@"退出" forState:0];
        [_leftBtn setTitleColor:[UIColor blackColor] forState:0];
        _leftBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_leftBtn addTarget:self action:@selector(exitClick:) forControlEvents:UIControlEventTouchUpInside];
        [_bgView addSubview:_leftBtn];
        
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightBtn setTitle:@"重新登录" forState:0];
        [_rightBtn setTitleColor:mainColor forState:0];
        _rightBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [_bgView addSubview:_rightBtn];
        [_rightBtn addTarget:self action:@selector(loginClick:) forControlEvents:UIControlEventTouchUpInside];

//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
//        [self addGestureRecognizer:tap];
        
    }
    return self;
}

-(void)exitClick:(UIButton *)button{
    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(didLeftClick)]) {
        [weakSelf.delegate didLeftClick];
    }
}

-(void)loginClick:(UIButton *)button{
    __weak typeof(self) weakSelf = self;
    if ([weakSelf.delegate respondsToSelector:@selector(didRightClick:)]) {
        [weakSelf.delegate didRightClick:button];
    }
}

- (void)showTitle:(NSString *)title andSubTitle:(NSString *)subTitle andLeft:(NSString *)leftStr andRight:(NSString *)rightStr
{
    _titleLab.text = title;
    _subTitleLab.text = subTitle;
    [_leftBtn setTitle:leftStr forState:0];
    [_rightBtn setTitle:rightStr forState:0];
    [self setSubView_layout];
    [[UIApplication sharedApplication].keyWindow addSubview:self];
}

- (void)dismiss
{
    if (self.superview) {
        [self removeFromSuperview];
    }
}

- (void)setSubView_layout{
    
    [_bgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(30);
        make.right.equalTo(self).offset(-30);
        make.height.equalTo(@(170));
    }];
    _bgView.layer.cornerRadius = 6.0f;
    _bgView.layer.masksToBounds = YES;
    
    [_titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_bgView).offset(30);
        make.centerX.equalTo(_bgView);
    }];
    
    [_subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_titleLab.mas_bottom).offset(20);
        make.centerX.equalTo(_bgView);
    }];
    
    [_verLink mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_bgView);
        make.height.equalTo(@(45));
        make.width.equalTo(@(1));
        make.bottom.equalTo(_bgView);
    }];
    
    [_herLink mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bgView).offset(10);
        make.right.equalTo(_bgView).offset(-10);
        make.height.equalTo(@(1));
        make.bottom.equalTo(_bgView).offset(-45);
    }];
    
    [_leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_bgView);
        make.bottom.equalTo(_bgView);
        make.width.equalTo(@(screenW/2 - 30));
        make.height.equalTo(@(45));
    }];
    
    [_rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(_bgView);
        make.bottom.equalTo(_bgView);
        make.width.equalTo(@(screenW/2 - 30));
        make.height.equalTo(@(45));
    }];
}



@end
