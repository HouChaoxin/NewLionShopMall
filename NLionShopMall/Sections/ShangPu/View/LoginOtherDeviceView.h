//
//  LoginOtherDeviceView.h
//  LionMall
//
//  Created by apple on 2018/4/24.
//  Copyright © 2018年 lionmall. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginOtherDeviceDelegate <NSObject>

- (void)didRightClick:(UIButton *)button;
- (void)didLeftClick;
@end

@interface LoginOtherDeviceView : UIView
@property (nonatomic, weak) id<LoginOtherDeviceDelegate> delegate;

- (void)showTitle:(NSString *)title andSubTitle:(NSString *)subTitle andLeft:(NSString *)leftStr andRight:(NSString *)rightStr;
- (void)dismiss;
@end
