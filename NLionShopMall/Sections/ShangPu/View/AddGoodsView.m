//
//  AddGoodsView.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "AddGoodsView.h"

@implementation AddGoodsView

- (instancetype)initWithFrame:(CGRect)frame setTitle:(NSString *)string
{
    self = [super initWithFrame:frame];
    if (self) {
    
        UILabel *pointLab = [[UILabel alloc] init];
        if ([string  isEqualToString:@"商品库存:"]) {
            pointLab.text = @"";
        }else{
            pointLab.text = @"*";
        }
        pointLab.textColor = [UIColor redColor];
        [self addSubview:pointLab];
        
        UIView *link = [[UIView alloc] init];
        link.backgroundColor = [UIColor getColor:@"f3f3f3"];
        [self addSubview:link];
        
        self.titleLab = [[UILabel alloc] init];
        self.titleLab.text = string;
        [self addSubview:self.titleLab];
        
        self.textTF = [[UITextField alloc] init];
        self.textTF.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.textTF];
        
        [pointLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.centerY.equalTo(self);
            make.width.equalTo(@8);
        }];
        
        [link mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(15);
            make.right.equalTo(self).offset(-15);
            make.bottom.equalTo(self);
            make.height.equalTo(@1);
        }];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(pointLab.mas_right);
            make.width.equalTo(@100);
        }];
        
        [self.textTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self);
            make.left.equalTo(self.titleLab.mas_right).offset(10);
            if ([string isEqualToString:@"商品价格:"]) {
                make.right.equalTo(self).offset(-70);
            }else{
                make.right.equalTo(self).offset(-20);
            }
            make.height.equalTo(@30);
        }];
    }
    return self;
}

@end
