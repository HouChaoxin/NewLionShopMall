//
//  ShangPuViewController.h
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ShangPuViewControllerType) {
    OnlineGoodsControllerType  = 0,    //在线
    ExamineGoodsControllerType = 1,   //待审核
    RefuseGoodsControllerType  = 2,    //已拒绝
    DeleteGoodsControllerType  = 3,    //已下架
};

@interface ShangPuViewController : UITableViewController

@property (nonatomic, assign) ShangPuViewControllerType VCType;

@end
