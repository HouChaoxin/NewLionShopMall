//
//  ShangPuViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "ShangPuViewController.h"
#import "AddGoodsViewController.h"
#import "ShangPuCell.h"
#import "GoodsModel.h"
#import "LoginOtherDeviceView.h"



@interface ShangPuViewController ()<LoginOtherDeviceDelegate>

@property (nonatomic ,strong) NSMutableArray *dataSource;
@property (nonatomic ,assign) NSInteger pageIndex;
@property (nonatomic ,assign) BOOL isBottom;
@property (nonatomic ,strong) LoginOtherDeviceView *actionView;
@end

@implementation ShangPuViewController

-(LoginOtherDeviceView *)actionView{
    if (!_actionView) {
        _actionView = [[LoginOtherDeviceView alloc] init];
        _actionView.delegate = self;
    }
    return _actionView;
}

-(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    return _dataSource;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self creatTableView];
    [self MJrefreshDate];
    [self MJrefreshFooterDate];
}

- (void)creatTableView{
    
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerClass:[ShangPuCell class] forCellReuseIdentifier:NSStringFromClass([ShangPuCell class])];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    [self.view endEditing:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ShangPuCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([ShangPuCell class]) forIndexPath:indexPath];
    cell.model = self.dataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (self.VCType) {
        case 0:
            cell.leftBtn.hidden = NO;
            [cell.leftBtn setTitle:@"下架" forState:0];
            cell.centerBtn.hidden = YES;
            cell.rightBtn.hidden = NO;
            [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@0);
            }];
            break;
        case 1:
            cell.leftBtn.hidden = YES;
            cell.centerBtn.hidden = YES;
            cell.rightBtn.hidden = YES;
            [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@0);
            }];
            break;
        case 2:
            cell.leftBtn.hidden = NO;
            [cell.leftBtn setTitle:@"编辑" forState:0];
            cell.centerBtn.hidden = YES;
            cell.rightBtn.hidden = NO;
            [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@20);
            }];
            break;
        case 3:
            cell.leftBtn.hidden = NO;
            [cell.leftBtn setTitle:@"上架" forState:0];
            cell.centerBtn.hidden = NO;
            cell.rightBtn.hidden = NO;
            [cell.reasonLab mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(@0);
            }];
            break;
        default:
            break;
    }
    
    [cell.leftBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.centerBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    [cell.rightBtn addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat height;
    switch (self.VCType) {
        case 0:
            height = 230;
            break;
        case 1:
            height = 200;
            break;
        case 2:
            height = 250;
            break;
        case 3:
            height = 230;
            break;
        default:
            break;
    }
    return height;
}

- (void)actionClick:(UIButton *)button{
    if ([button.titleLabel.text isEqualToString:@"下架"]) {
        [self.actionView showTitle:@"提示" andSubTitle:@"下架商品，可在已下架列表中重新上架" andLeft:@"放弃" andRight:@"继续下架"];
    }else if ([button.titleLabel.text isEqualToString:@"删除"]){
        [self.actionView showTitle:@"提示" andSubTitle:@"删除商品将不可找回" andLeft:@"取消" andRight:@"继续删除"];
    }else if ([button.titleLabel.text isEqualToString:@"编辑"]){
        AddGoodsViewController *vc = [[AddGoodsViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([button.titleLabel.text isEqualToString:@"上架"]){
        [self.actionView showTitle:@"提示" andSubTitle:@"上架商品审核通过后可在线查看" andLeft:@"取消" andRight:@"确认上架"];
    }
}

- (void)didRightClick:(UIButton *)button{
    
    NSLog(@"%@",button.titleLabel.text);
    
}

- (void)didLeftClick{
    
    [self.actionView dismiss];
}

#pragma mark -- 编辑商品
- (void)editGoodsModel:(GoodsModel *)model{
    
    NSString *url = [baseUrl stringByAppendingString:@"business/goods-edit"];
    NSDictionary *body = @{@"goods_id":model.goods_id,
                           @"goods_title":model.goods_title,
                           @"goods_num":model.goods_num,
                           @"goods_price":model.goods_price,
                           @"unit":model.unit,
                           @"goods_image":model.goods_image,
                           @"is_on_sale":@"",// 1上架 0 下架
                           @"is_delete":@"",};//1删除
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        NSLog(@"%@",responseObject);
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
}

- (void)loadBaseData{
    
    NSString *store_id = @"15249077146730960000";
    NSString *url = [BaseUrl stringByAppendingString:@"business/goods-lists"];
    NSDictionary *body = @{@"store_id":store_id,
                           @"status":@(self.VCType + 1),
                           @"page":@(1),
                           @"pagesize":@20,};
    
    [NetHelper POST:url parameters:body progressBlock:nil success:^(id responseObject) {
        
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        NSDictionary *rootDict = (NSDictionary *)responseObject;
        if (rootDict && rootDict.count > 0) {
            BOOL success = [rootDict[@"status"] boolValue];
            if (success) {
                NSArray *tempArr = rootDict[@"data"][@"goodsList"];
                if (self.pageIndex == 0) {
                    [self.dataSource removeAllObjects];
                }
                if (tempArr.count < 20) {
                    self.isBottom = YES;
                }
                self.dataSource = [GoodsModel mj_objectArrayWithKeyValuesArray:tempArr];
                [self.tableView reloadData];
            }
            else {
                [self.view makeToast:rootDict[@"msg"]];
            }
        }
    } failure:^(NSError *error) {
        NSLog(@"%@",error);
    }];
    
}

#pragma mark ---下拉刷新---
-(void)MJrefreshDate{
    
    __unsafe_unretained UITableView *tableView = self.tableView;
    __weak typeof(self)MySelf = self;
    MJRefreshNormalHeader *mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:MySelf refreshingAction:@selector(loadNewData)];
    [mj_header setTitle:@"下拉可以刷新" forState:MJRefreshStateIdle];
    [mj_header setTitle:@"松开立即刷新" forState:MJRefreshStatePulling];
    [mj_header setTitle:@"品兑正在加载" forState:MJRefreshStateRefreshing];
    tableView.mj_header = mj_header;
    [tableView.mj_header beginRefreshing];
}
-(void)loadNewData{
    self.pageIndex = 0;
    self.isBottom = NO;
    [self loadBaseData];
}

#pragma mark ----上拉加载----
-(void)MJrefreshFooterDate{
    __unsafe_unretained UITableView *tableView = self.tableView;
    __weak typeof(self)MySelf = self;
    MJRefreshBackNormalFooter *mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:MySelf refreshingAction:@selector(footRefesh)];
    [mj_footer setTitle:@"上拉加载更多" forState:MJRefreshStateIdle];
    [mj_footer setTitle:@"品兑正在加载" forState:MJRefreshStatePulling];
    [mj_footer setTitle:@"我也是有底线的" forState:MJRefreshStateNoMoreData];
    tableView.mj_footer = mj_footer;
}
-(void)footRefesh{
    
    if (!self.isBottom) {
        self.pageIndex ++ ;
        [self loadBaseData];
    }else{
        [self.tableView.mj_footer endRefreshingWithNoMoreData];
    }
}

@end
