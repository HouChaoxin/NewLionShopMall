//
//  MainMerchantViewController.m
//  NLionShopMall
//
//  Created by apple on 2018/5/15.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import "MainMerchantViewController.h"
#import "AddGoodsViewController.h"
#import "ShangPuViewController.h"
#import "SearchGoodsViewController.h"

@interface MainMerchantViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *searchTF;
@property (nonatomic, strong) UIButton *searchBtn;
@property(nonatomic,strong)NSArray *fenleiArr;

@end

@implementation MainMerchantViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)loadView
{
    [super loadView];
    self.menuViewStyle = WMMenuViewStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.fenleiArr = @[@"在线商品",@"待审核",@"已拒绝",@"已下架"];
    self.menuItemWidth = screenW/4;
    self.menuBGColor = mainColor;
    self.titleSizeSelected = 16;
    self.titleSizeNormal = 16;
    self.menuHeight = 40;
    self.titleColorSelected = [UIColor whiteColor];
    self.titleColorNormal = [UIColor colorWithWhite:1.0 alpha:0.4];
    self.viewFrame = CGRectMake(0, 64, screenW, screenH - 100);
    [self creatSearchView];
    [self reloadData];
}

- (void)creatSearchView{
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, screenW, 64)];
    bgView.backgroundColor = mainColor;
    [self.view addSubview:bgView];
    
    UIView *searchView = [[UIView alloc] init];
    searchView.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:searchView];
    
    UIImageView *searchImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"searchicon"]];
    [bgView addSubview:searchImg];
    
    self.searchTF = [[UITextField alloc] init];
    self.searchTF.font = [UIFont systemFontOfSize:15.0f];
    self.searchTF.placeholder  = @"搜索商品名称";
    self.searchTF.delegate = self;
    self.searchTF.returnKeyType = UIReturnKeySearch;
    //    [self.searchTF becomeFirstResponder];
    [searchView addSubview:self.searchTF];
    
    self.searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.searchBtn setTitle:@"添加" forState:0];
    self.searchBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.searchBtn setTitleColor:[UIColor whiteColor] forState:0];
    [self.searchBtn addTarget:self action:@selector(searchBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.searchBtn];
    
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(bgView);
        make.centerY.equalTo(searchView);
        make.width.equalTo(@(60));
    }];
    
    [searchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(bgView).offset(22);
        make.left.equalTo(bgView).offset(10);
        make.right.equalTo(self.searchBtn.mas_left);
        make.bottom.equalTo(bgView).offset(-8);
    }];
    searchView.layer.cornerRadius = 5.0;
    searchView.layer.masksToBounds = YES;
    
    [searchImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(searchView);
        make.left.equalTo(searchView).offset(5);
        make.width.equalTo(searchImg.mas_height);
    }];
    
    [self.searchTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(searchView);
        make.bottom.equalTo(searchView);
        make.left.equalTo(searchImg.mas_right);
        make.right.equalTo(searchView);
    }];
}

- (void)searchBtnClick:(UIButton *)button{
    
    AddGoodsViewController *vc = [[AddGoodsViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    SearchGoodsViewController *vc = [[SearchGoodsViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
    return NO;
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController{
    return _fenleiArr.count;
}
//此方法只有在加载某一界面时才会调用
- (__kindof UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index{
    
    ShangPuViewController *vc = [[ShangPuViewController alloc]init];
    vc.VCType = index;
    return vc;
}


- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index{
    
    return _fenleiArr[index];
}


@end
