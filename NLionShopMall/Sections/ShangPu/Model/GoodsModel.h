//
//  GoodsModel.h
//  NLionShopMall
//
//  Created by apple on 2018/5/19.
//  Copyright © 2018年 NewLionShopMall. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsModel : NSObject
@property (nonatomic ,strong) NSString *goods_num;   //商品数量
@property (nonatomic ,strong) NSString *model_id;    //
@property (nonatomic ,strong) NSString *goods_image; //商品图片
@property (nonatomic ,strong) NSString *unit;        //单位

@property (nonatomic ,strong) NSString *store_name;  //商铺名字
@property (nonatomic ,strong) NSString *goods_price; //商品价格
@property (nonatomic ,strong) NSString *goods_title; //商品名称
@property (nonatomic ,strong) NSString *goods_id;    //商品ID
@property (nonatomic ,strong) NSString *reason;      //审核拒绝原因

@property (nonatomic ,strong) NSString *is_on_sale;  //是否在售  1上架 0 下架
@property (nonatomic ,strong) NSString *is_delete;   //是否已删除  0 未删除 1 删除
@property (nonatomic ,strong) NSString *goods_verify;//是否审核通过  0：未审核，1：通过  2：审核未通过

@end

